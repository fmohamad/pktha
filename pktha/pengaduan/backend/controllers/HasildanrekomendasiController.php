<?php

namespace backend\controllers;

use Yii;
use common\models\Hasildanrekomendasi;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * RekomendasiController implements the CRUD actions for Rekomendasi model.
 */
class HasildanrekomendasiController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Rekomendasi models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Hasildanrekomendasi::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Rekomendasi model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id,$idx=null)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
            'idx'=>$idx
        ]);
    }

    /**
     * Creates a new Rekomendasi model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Rekomendasi();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function actionCreateid($id)
    {
        $model = new Hasildanrekomendasi();



        if ($model->load(Yii::$app->request->post())) {
             // print_r($model->getErrors());
            $model->pengaduan_id=$id;
            if($model->save()){
            return $this->redirect(['pengaduan/timeline', 'id' => $id]);
            // return $this->redirect(['view', 'id' => $model->id]);
        }
        } else {
            
            return $this->render('create', [
                'model' => $model,
                'idx'=>$id
            ]);
        }
    }

    /**
     * Updates an existing Rekomendasi model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id,$idx)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['pengaduan/timeline', 'id' => $idx]);            
            // return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'idx' =>$idx
            ]);
        }
    }

    /**
     * Deletes an existing Rekomendasi model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id,$idx=null)
    {
        $this->findModel($id)->delete();
        return $this->redirect(['pengaduan/timeline', 'id' => $idx]);            
        // return $this->redirect(['index']);
    }

    /**
     * Finds the Rekomendasi model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Rekomendasi the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Hasildanrekomendasi::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
