<?php

namespace backend\controllers;

use Yii;
use common\models\ImportxlsForm;
use common\models\Pengaduan;
use yii\web\UploadedFile;
use yii\web\Controller;
use yii\data\ArrayDataProvider;



class ImportxlsController extends Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionListview()
    {

    	$datx=array();
    	$session = Yii::$app->session;
		if ($session->has('dataimport')){
			$datx = $session->get('dataimport');			
		}

    	if(Yii::$app->request->post()){


    		if(count($datx)>0){

    			// var_dump($datx);
    			for ($i=0; $i <count($datx) ; $i++) { 

    				$pengaduan =new Pengaduan();

                    


    				
    				$pengaduan->tipe_identitas = count($datx[$i]['tipe_identitas'])>0 ? $datx[$i]['tipe_identitas'] : "-";
    				$pengaduan->nama_identitas= count($datx[$i]['nama_identitas'])>0 ? $datx[$i]['nama_identitas'] : "-"; 
    				// $pengaduan->sarana_pengaduan=$datx[$i]['sarana_pengaduan'];
    				$pengaduan->nomor_surat=count($datx[$i]['nomor_surat'])>0 ? $datx[$i]['nomor_surat'] : "-"; 
    				$pengaduan->tgl_penerima_pengaduan=count($datx[$i]['tgl_penerima_pengaduan'])>0 ? $datx[$i]['tgl_penerima_pengaduan'] : ""; 
    				$pengaduan->tgl_penerima_pktha=count($datx[$i]['tgl_penerima_pktha'])>0 ? $datx[$i]['tgl_penerima_pktha'] : "";
    				$pengaduan->penerima_pengaduan=count($datx[$i]['penerima_pengaduan'])>0 ? $datx[$i]['penerima_pengaduan'] : "-";

    				$pengaduan->id_tahapan="1";
					$pengaduan->telepon_identitas="-";
    				$pengaduan->email_identitas="-";
					$pengaduan->alamat_identitas="-";
    				$pengaduan->kabkota_konflik="0";
					
					$pengaduan->provinsi_konflik="0";    				
					$pengaduan->pihak_terlibat="-";
    				$pengaduan->pihak_berkonflik="-";
    				$pengaduan->tutuntan_pengaduan="-";

    				$pengaduan->sarana_pengaduan="3";




    				$pengaduan->pihak_berkonflik=count($datx[$i]['pihak_berkonflik'])>0 ? $datx[$i]['pihak_berkonflik'] : "-";
    				$pengaduan->lokasi_konflik=count($datx[$i]['lokasi_konflik'])>0 ? $datx[$i]['lokasi_konflik'] : "-";
    				$pengaduan->fungsi_kawasan=count($datx[$i]['fungsi_kawasan'])>0 ? (string)$datx[$i]['fungsi_kawasan'] : "-";
    				$pengaduan->luasan_kawasan=count($datx[$i]['luasan_kawasan'])>0 ? (string)$datx[$i]['luasan_kawasan'] : "-";
    				$pengaduan->tipologi_kasus=count($datx[$i]['tipologi_kasus'])>0 ? $datx[$i]['tipologi_kasus'] : "-";
    				$pengaduan->status_kawasan=count($datx[$i]['status_kawasan'])>0 ? $datx[$i]['status_kawasan'] : "-";
    				$pengaduan->rentang_waktu=count($datx[$i]['rentang_waktu'])>0 ? $datx[$i]['rentang_waktu'] : "-";
    				$pengaduan->resume=count($datx[$i]['resume'])>0 ? $datx[$i]['resume'] : "-";
    				$pengaduan->tutuntan_pengaduan=count($datx[$i]['tutuntan_pengaduan'])>0 ? $datx[$i]['tutuntan_pengaduan'] : "-";
    				$pengaduan->upaya=count($datx[$i]['upaya'])>0 ? $datx[$i]['upaya'] : "-";
    				$pengaduan->pihak_terlibat=count($datx[$i]['pihak_terlibat'])>0 ? $datx[$i]['pihak_terlibat'] : "-";

                    // $pengaduan->save();
                        				
                    


    				if($pengaduan->save()){
    				// 	$session->remove('dataimport');
    				// 	$session->close();	
    				// 	$session = null;
    				// 	$datx = array();
    					// return $this->redirect(['pengaduan/index']);    
    				// }else{
                        // return $this->redirect(['pengaduan/index']);    
    				// 	print_r($pengaduan->getErrors());	
    				}else{
                        // var_dump($i);
                        // echo "<br/>";
                        // echo $datx[$i]['fungsi_kawasan'];
                        // echo $datx[$i]['luasan_kawasan'];
                             print_r($pengaduan->getErrors());   
                        // // print_r($pengaduan->hasErrors());
                        // // $pengaduan->createCommand()->getRawSql();    
                        // // var_dump($datx[$i]);
                        // echo "<br/>";
                        // echo "<br/>";
                    }    				
    			}	
    		}
             $session->remove('dataimport');
             $session->close();  
             $session = null;
             $datx = array();
    	}

    	
    	
    	$provider = new ArrayDataProvider([
    		'allModels' => $datx,
    		'sort' => [
    		'attributes' => ['id', 'nama_identitas',	'tipe_identitas',	'sarana_pengaduan'	,'nomor_surat'	,'tgl_penerima_pengaduan','tgl_penerima_pktha'	,'penerima_pengaduan',	'pihak_berkonflik'	,'lokasi_konflik','fungsi_kawasan',	'luasan_kawasan',	'tipologi_kasus',	'status_kawasan'	,'rentang_waktu',	'resume'	,'tutuntan_pengaduan',	'upaya'	,'pihak_terlibat'],
    		],
    		'pagination' => [
    		'pageSize' => 10,
    		],
    		]);




    	

        return $this->render('listview', [
                	'dataProvider' =>$provider
        ]);
    }

    public function actionImportxls()
    {
    	$model = new ImportxlsForm();

    	if ($model->load(Yii::$app->request->post())) {
                 
            $model->file = UploadedFile::getInstance($model, 'file');

            if($model->upload()){
            	$fileName=Yii::getAlias('@updir').'/uploads/xls/' .md5($model->file->baseName) . '.' . $model->file->extension;


			$data = \moonland\phpexcel\Excel::widget([
        		'mode' => 'import', 
        		'fileName' => $fileName, 
        		'setFirstRecordAsKeys' => true, // if you want to set the keys of record column with first record, if it not set, the header with use the alphabet column on excel. 
        		'setIndexSheetByName' => true, // set this if your excel data with multiple worksheet, the index of array will be set with the sheet name. If this not set, the index will use numeric. 
        		'getOnlySheet' => 'Sheet1', // you can set this property if you want to get the specified sheet from the excel data with multiple worksheet.
    		]);

				$j=0;
				$datx=array();
				
				for ($i=2; $i < count($data) ; $i++) { 
					if($data[$i]['nama_identitas']!=null){
						$datx[$j] = $data[$i];
						$j++;
					}					
				}
				$session = Yii::$app->session;
				$session->set('dataimport', $datx);

                return $this->redirect(['listview']);    
            }
            else{                
                return $this->render('index', [
                'model' => $model,
                ]);
            }

            
            
        } else {
            return $this->render('index', [
                'model' => $model,
            ]);
        }
    }
}
