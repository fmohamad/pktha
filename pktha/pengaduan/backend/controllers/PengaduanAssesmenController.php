<?php

namespace backend\controllers;

use Yii;
use common\models\PengaduanAssesmen;
use common\models\PengaduanRegistrasi;
use common\utils\DateHelper;
use common\models\PengaduanAssesmenSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ServerErrorHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * PengaduanAssesmenController implements the CRUD actions for PengaduanAssesmen model.
 */
class PengaduanAssesmenController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all PengaduanAssesmen models.
     *
     * @return mixed
     */
    public function actionIndex($query = null)
    {
        $searchModel = new PengaduanAssesmenSearch();
        $queryParams = Yii::$app->request->queryParams;
        if (isset($query)) {
            $queryParams['id_pengaduan'] = $query;
        }
        $dataProvider = $searchModel->search($query);

        return $this->render(
            'index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]
        );
    }

    /**
     * Displays a single PengaduanAssesmen model.
     *
     * @param  string $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $regModel = PengaduanRegistrasi::find()->getById($model->id_pengaduan);
        if (!Yii::$app->user->identity->isAdminWilayah($regModel->id_wilayah_konflik)) {
            throw new ServerErrorHttpException('You are not authorized to perform this action.');
        }

        return $this->render(
            'view', [
                    'model' => $model,
                        ]
        );
    }

    /**
     * Creates a new PengaduanAssesmen model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     */
    public function actionCreate($id_pengaduan)
    {

        if (\common\models\PengaduanAssesmen::findOne(['id_pengaduan' => $id_pengaduan])) {
            throw new \yii\web\ServerErrorHttpException('Tahap Assesmen untuk Pengaduan ini telah dibuat.');
        }

        $model = new PengaduanAssesmen();
        $model->id_pengaduan = $id_pengaduan;

        // Upload file
        // $this->handlePengaduanAssesmenUpload($model);
        if ($model->load(Yii::$app->request->post())) {
            $model->created_date = \common\utils\DateHelper::GetCurrentDate();
            $model->modified_date = \common\utils\DateHelper::GetCurrentDate();

            if ($model->save()) {
                return $this->redirect(['//pengaduan-registrasi/view', 'id' => $model->id_pengaduan]);
            }
        } else {
            return $this->render(
                'create', [
                        'model' => $model,
                            ]
            );
        }
    }

    /**
     * Updates an existing PengaduanAssesmen model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param  string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $regModel = PengaduanRegistrasi::find()->getById($model->id_pengaduan);
        if (!Yii::$app->user->identity->isAdminWilayah($regModel->id_wilayah_konflik)) {
            throw new ServerErrorHttpException('You are not authorized to perform this action.');
        }

        if ($model->load(Yii::$app->request->post())) {
            $model->modified_date = DateHelper::GetCurrentDate();
            $path = $regModel->getPath();

            $uploaded_file = UploadedFile::getInstance($model, 'surat_kesediaan_mediasi_filename');
            $subpath = $path . '/surat_kesediaan_mediasi';
            if (!is_dir($subpath)) {
                mkdir($subpath, 0775, true);
            }
            if ($uploaded_file) {
                $name = $uploaded_file->baseName . '.' . $uploaded_file->extension;
                if ($uploaded_file->saveAs($subpath . '/' . $name)) {
                    $model->surat_kesediaan_mediasi_filename = $name;
                } else {
                    throw new ServerErrorHttpException($name . ' could not be uploaded.');
                }
            }

            $uploaded_file = UploadedFile::getInstance($model, 'pemetaan_dinamika_aktor_filename');
            $subpath = $path . '/pemetaan_dinamika_aktor';
            if (!is_dir($subpath)) {
                mkdir($subpath, 0775, true);
            }
            if ($uploaded_file) {
                $name = $uploaded_file->baseName . '.' . $uploaded_file->extension;
                if ($uploaded_file->saveAs($subpath . '/' . $name)) {
                    $model->pemetaan_dinamika_aktor_filename = $name;
                } else {
                    throw new ServerErrorHttpException($name . ' could not be uploaded.');
                }
            }
            if ($model->save()) {
                return $this->redirect(['//pengaduan-registrasi/view', 'id' => $regModel->id]);
            }
        } else {
            return $this->render(
                'update', [
                        'model' => $model,
                            ]
            );
        }
    }

    public function actionCancel($id)
    {
        return $this->redirect(['pengaduan-registrasi/view', 'id' => PengaduanRegistrasi::find()->getById($this->findModel($id)->id_pengaduan)->id]);
    }

    /**
     * Deletes an existing PengaduanAssesmen model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param  string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $regModel = PengaduanRegistrasi::findOne(['id' => $model->id_pengaduan]);
        if (!Yii::$app->user->identity->isAdminWilayah($regModel->id_wilayah_konflik)) {
            throw new ServerErrorHttpException('You are not authorized to perform this action.');
        }
        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * Downloads the specified file type from a model.
     *
     * @param  string $id
     * @throws ServerErrorHttpException
     */
    public function actionDownload($id, $file)
    {
        $model = $this->findModel($id);
        $regModel = PengaduanRegistrasi::findOne(['id' => $model->id_pengaduan]);
        if (!Yii::$app->user->identity->isAdminWilayah($regModel->id_wilayah_konflik)) {
            throw new ServerErrorHttpException('You are not authorized to perform this action.');
        }
        $path = $regModel->getPath();
        $filename = $model[$file . '_filename'];
        $full_path = $path . '/' . $file . '/' . $filename;
        if (is_file($full_path)) {
            Yii:: $app->response->sendFile($full_path);
        } else {
            throw new ServerErrorHttpException($full_path . ' could not be found.');
        }
    }

    /**
     * Finds the PengaduanAssesmen model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param  string $id
     * @return PengaduanAssesmen the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PengaduanAssesmen::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
