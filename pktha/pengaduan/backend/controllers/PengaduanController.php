<?php

namespace backend\controllers;

use Yii;
use common\models\Pengaduan;
use common\models\PengaduanSearch;
use common\models\Assesment;
use common\models\Analisa;
use common\models\Rekomendasi;
use common\models\Hasildanrekomendasi;

use common\models\Tahapan;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\base\ErrorException;


/**
 * PengaduanController implements the CRUD actions for Pengaduan model.
 */
class PengaduanController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function actionStep()
    {
        $model = new Pengaduan();
        return $this->render(
            'step', [
            'model' => $model,
            ]
        );
    }

    public function actionTimeline($id)
    {
        $modelPengaduan = Pengaduan::find()->where(['pengaduan.id' => $id])->joinWith(['dokumen'])->one();//Pengaduan::findOne($id);
        $modelAssesment = Assesment::find()->where(['pengaduan_id' => $id])->orderBy('id')->all();
        $modelAnalisa = Analisa::findOne(['pengaduan_id' => $id]);//->where(['pengaduan_id' => $id])->orderBy('id');
        $modelRekomendasi = Rekomendasi::findOne(['pengaduan_id' => $id]);
        $modelHasildanrekomendasi = Hasildanrekomendasi::findOne(['pengaduan_id' => $id]);
        $modelRekomendasi = Rekomendasi::findOne(['pengaduan_id' => $id]);
        // $modelRekomendasi = new Rekomendasi();
        // $modelTahapan = new Tahapan();

        return $this->render(
            'timeline', [

            'model' => $modelPengaduan,//$this->findModel($id),
            'modelAssesment' => $modelAssesment,
            'modelAnalisa' => $modelAnalisa,
            'modelRekomendasi' => $modelRekomendasi,
            'modelHasildanrekomendasi'=>$modelHasildanrekomendasi
            // 'modelRekomendasi' => $modelRekomendasi->findModel($id),
            // 'modelTahapan' => $modelTahapan->findModel($id),

            ]
        );

        // return $this->render('timeline', [
        //     'model' => $this->findModel($id),
        // ]);
    }



    /**
     * Lists all Pengaduan models.
     *
     * @return mixed
     */

    public function actionRecyclebin($id=null)
    {
        $searchModel = new PengaduanSearch();
        $p=Yii::$app->request->queryParams;

        if($id!=null) {
            $p['PengaduanSearch']['id_tahapan']=$id;
        }
        $dataProvider = $searchModel->searchdeleted($p);

        return $this->render(
            'recyclebin', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            ]
        );
    }

    public function actionIndex()
    {
        $searchModel = new PengaduanSearch();
        $p=Yii::$app->request->queryParams;

        if(isset($p['id_tahapan']) && !isset($p['PengaduanSearch']['id_tahapan'])) {

            $p['PengaduanSearch']['id_tahapan']=$p['id_tahapan'];
        }

        $dataProvider = $searchModel->search($p);

        return $this->render(
            'index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            ]
        );
    }

    /**
     * Displays a single Pengaduan model.
     *
     * @param  integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render(
            'view', [
            'model' => $this->findModel($id),
            ]
        );
    }

    /**
     * Creates a new Pengaduan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Pengaduan();
        $model->id_tahapan = 1;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render(
                'create', [
                'model' => $model,
                ]
            );
        }
    }

    public function actionDownloadxls()
    {
            $model = Pengaduan::find()->where(['deleted'=>0])->all();
            $i=0;
        foreach ($model as $key => $value) {
            $a[$i]['kode'] = $value['kode'];
            $a[$i]['tgl_penerima_pengaduan'] = $value['tgl_penerima_pengaduan'];
            $a[$i]['nama_identitas'] = $value['nama_identitas'];
            $a[$i]['pihak_berkonflik'] = $value['pihak_berkonflik'];

            $a[$i]['lokasi_konflik'] = "Lokasi Konflik : ".$value['lokasi_konflik']
            ."<br/>Provinsi : ".$value->getProvinsi($value['provinsi_konflik'])."<br/>Kabupaten/Kota : ".$value->getKabkota($value['kabkota_konflik']);
            $a[$i]['uraian_pengaduan'] = $value['resume'];
            $a[$i]['status_pengaduan'] = $value->getStatusx($value['status']);

            $a[$i]['tutuntan_pengaduan'] = $value['tutuntan_pengaduan'];

            $a[$i]['tindak_lanjut'] = isset($value->tidaklanjut)?$value->tidaklanjut->hasil_penyelesaian:"-";
            $a[$i]['hasil'] = isset($value->hasildanrekomendasi)?$value->hasildanrekomendasi->hasil:"-";

            // $a[$i]['pihak_terlibat'] = $value['pihak_terlibat'];
            // $a[$i]['nama_identitas'] = $value['nama_identitas'];
            // $a[$i]['sarana_pengaduan'] = $value['sarana_pengaduan'];
            $i++;
        }
            $str = "<table>";
            $str .= "<thead>";
            $str .= "<tr>
                    <th>Nomer Register </th>
                    <th>Tanggal Penerima Pengaduan</th>
                    <th>Nama Pengadu </th>
                    <th>Pihak yang Berkonflik</th>
                    <th>Lokasi Konflik</th>
                    <th>Uraian Pengaduan </th>
                    <th>Status penanganan  Konflik </th>
                    <th>Tuntutan Pengaduan</th>
                    <th>Tindak Lanjut </th>
                    <th>Hasil</th>
                    </tr>";
            $str .= "</thead>";
            $str .= "<tbody>";
        foreach ($a as $key1) {

            $str .= vsprintf(
                "<tr>
                    <td>%s</td>
                    <td>%s</td>
                    <td>%s</td>
                    <td>%s</td>
                    <td>%s</td>
                    <td>%s</td>
                    <td>%s</td>
                    <td>%s</td>
                    <td>%s</td>
                    <td>%s</td>

                    </tr>", $key1
            );

            // $str .= vsprintf("some text %s another text %s extra text %s", $array);
        }
            $str .= "</tbody>";
            $str .= "<table>";

            // var_dump($str);

            return \Yii::$app->response->sendContentAsFile($str, md5(date('mdyhis')).".xls");
    }



    /**
     * Updates an existing Pengaduan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param  integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->id_tahapan = 1;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['timeline', 'id' => $id]);
            // return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render(
                'update', [
                'model' => $model,
                ]
            );
        }
    }

    /**
     * Cancels changes to Pengaduan model.
     *
     * @param  integer $id
     * @return mixed
     */
    public function actionCancel($id)
    {
        return $this->redirect(['pengaduan-registrasi/view', 'id' => PengaduanRegistrasi::find()->getById($this->findModel($id)->id_pengaduan)->id]);
    }

    /**
     * Deletes an existing Pengaduan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param  integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        // $this->findModel($id)->delete();
        $model = $this->findModel($id);
        $model->deleted=1;
        $model->save();

        return $this->redirect(['index']);
    }

    public function actionUndelete($id)
    {
        // $this->findModel($id)->delete();
        $model = $this->findModel($id);
        $model->deleted=0;
        $model->save();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Pengaduan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param  integer $id
     * @return Pengaduan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Pengaduan::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    public function actionTest($id)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
         $model = new \common\models\Kabkota();
         $tst= $model->find($id)->where("proid='".$id."'")->all();
         // $j=\yii\helpers\ArrayHelper::map($model, 'kabid', 'kabkota');
         $rsp ="";
        foreach ($tst as $res) {
            $rsp .= '<option value="'.$res->kabid.'">'.$res->kabkota.'</option>';
        }
        return $rsp;
    }

    public function actionKabid($id)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        $model = new \common\models\Kabkota();

        $rsp=null;
        try {
            $tst= $model->find()->where("kabid='".$id."'")->one();
            $x = $tst->piaps->kabcenter; //$tst;
            if($x!=null) {
                $rsp = json_encode(explode(" ", $x));
            }
        } catch (ErrorException $e) {

        }
        return $rsp;
    }


    public function actionGetkode($id)
    {
        $model = new Pengaduan();
        $rsp=$model->getKode($id);
        // $rsp = "test";
        return $rsp;
    }


    public function actionSite($pid)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        $model = new Pengaduan();
        $model = $this->findAll($id);
        return $model->getBatch();
    }

    public function actionReplay($id)
    {
        // Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        // $model = new Pengaduan();
        // $model = $this->findAll($id);
        // return $model->getBatch();
        return $this->render(
            'replay', [
            'model' => $this->findModel($id),
            ]
        );
    }


}
