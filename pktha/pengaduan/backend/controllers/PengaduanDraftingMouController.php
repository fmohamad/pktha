<?php

namespace backend\controllers;

use Yii;
use common\models\PengaduanDraftingMou;
use common\models\PengaduanDraftingMouSearch;
use common\models\PengaduanRegistrasi;
use common\utils\DateHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;
use yii\filters\VerbFilter;

/**
 * PengaduanDraftingMouController implements the CRUD actions for PengaduanDraftingMou model.
 */
class PengaduanDraftingMouController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors() 
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all PengaduanDraftingMou models.
     *
     * @return mixed
     */
    public function actionIndex() 
    {
        $searchModel = new PengaduanDraftingMouSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render(
            'index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
            ]
        );
    }

    /**
     * Displays a single PengaduanDraftingMou model.
     *
     * @param  string $id
     * @return mixed
     */
    public function actionView($id) 
    {
        $model = $this->findModel($id);
        $regModel = PengaduanRegistrasi::find()->getById($model->id_pengaduan);
        if (!Yii::$app->user->identity->isAdminWilayah($regModel->id_wilayah_konflik)) {
            throw new ServerErrorHttpException('You are not authorized to perform this action.');
        }

        return $this->render(
            'view', [
                    'model' => $model,
            ]
        );
    }

    /**
     * Creates a new PengaduanDraftingMou model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     */
    public function actionCreate() 
    {
        $model = new PengaduanDraftingMou();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render(
                'create', [
                        'model' => $model,
                ]
            );
        }
    }

    /**
     * Updates an existing PengaduanDraftingMou model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param  string $id
     * @return mixed
     */
    public function actionUpdate($id) 
    {
        $model = $this->findModel($id);
        $regModel = PengaduanRegistrasi::find()->getById($model->id_pengaduan);
        if (!Yii::$app->user->identity->isAdminWilayah($regModel->id_wilayah_konflik)) {
            throw new ServerErrorHttpException('You are not authorized to perform this action.');
        }

        if ($model->load(Yii::$app->request->post())) {
            $model->modified_date = DateHelper::GetCurrentDate();
            if ($this->uploadModelFiles($model) && $model->save()) {
                return $this->redirect(['//pengaduan-registrasi/view', 'id' => $model->id_pengaduan]);
            }
        } else {
            return $this->render(
                'update', [
                        'model' => $model,
                ]
            );
        }
    }

    /**
     * Cancels changes to Pengaduan model.
     *
     * @param  integer $id
     * @return mixed
     */
    public function actionCancel($id)
    {
        return $this->redirect(['pengaduan-registrasi/view', 'id' => PengaduanRegistrasi::find()->getById($this->findModel($id)->id_pengaduan)->id]);
    }

    /**
     * Uploads files found in the ActiveForm of this controller's view.
     *
     * @param  mixed reference to model to modify.
     * @return boolean indicating success/failure.
     * @throws ServerErrorHttpException if file could not be uploaded.
     */
    protected function uploadModelFiles(PengaduanDraftingMou $_model) 
    {
        $path = PengaduanRegistrasi::findOne(['id' => $_model->id_pengaduan])->getPath();
        /* It's probably safer to loop through a specified list of attributes
         * instead of through the full list of attributes. */
        $attrs = array('berita_acara', 'foto');

        foreach ($attrs as $key) {
            /* these variables rely on a naming convention of the form
             * [attr]_data and [attr]_filename for model files */
            $data_attr = $key . '_data';
            $file_attr = $key . '_filename';
            $uploaded_file = UploadedFile::getInstance($_model, $file_attr);
            $subpath = $path . '/' . $key;
            if (!is_dir($subpath)) {
                mkdir($subpath, 0775, true);
            }
            if ($uploaded_file) {
                $name = $uploaded_file->baseName . '.' . $uploaded_file->extension;
                if ($uploaded_file->saveAs($subpath . '/' . $name)) {
                    $_model->$file_attr = $name;
                    $_model->save();
                } else {
                    throw new ServerErrorHttpException($name . ' could not be uploaded.');
                }
            } // else attribute is not a file or no file uploaded
        }
        return true;
    }

    /**
     * Deletes an existing PengaduanDraftingMou model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param  string $id
     * @return mixed
     */
    public function actionDelete($id) 
    {
        $model = $this->findModel($id);
        $regModel = PengaduanRegistrasi::find()->getById($model->id_pengaduan);
        if (!Yii::$app->user->identity->isAdminWilayah($regModel->id_wilayah_konflik)) {
            throw new ServerErrorHttpException('You are not authorized to perform this action.');
        }
        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * Downloads the specified file type from a model.
     *
     * @param  string $id
     * @param  string $file type
     * @throws ServerErrorHttpException
     */
    public function actionDownload($id, $file)
    {
        $model = $this->findModel($id);
        $regModel = PengaduanRegistrasi::find()->getById($model->id_pengaduan);
        if (!Yii::$app->user->identity->isAdminWilayah($regModel->id_wilayah_konflik)) {
            throw new ServerErrorHttpException('You are not authorized to perform this action.');
        }
        $path = $regModel->getPath();

        switch ($file) {
        case 'berita_acara':
            $filename = $model->berita_acara_filename;
            break;
        case 'foto':
            $filename = $model->foto_filename;
            break;
        }

        if (isset($filename)) {
            $full_path = $path . '/' . $file . '/' . $filename;
            if (is_file($full_path)) {
                Yii:: $app->response->sendFile($full_path);
            } else {
                throw new ServerErrorHttpException($full_path . ' could not be found.');
            }
        }
    }

    /**
     * Finds the PengaduanDraftingMou model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param  string $id
     * @return PengaduanDraftingMou the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) 
    {
        if (($model = PengaduanDraftingMou::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
