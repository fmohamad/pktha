<?php

namespace backend\controllers;

use Yii;
use common\models\PengaduanPraMediasi;
use common\models\PengaduanPraMediasiSearch;
use common\models\PengaduanRegistrasi;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PengaduanPraMediasiController implements the CRUD actions for PengaduanPraMediasi model.
 */
class PengaduanPraMediasiController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors() 
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all PengaduanPraMediasi models.
     *
     * @return mixed
     */
    public function actionIndex() 
    {
        $searchModel = new PengaduanPraMediasiSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render(
            'index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
            ]
        );
    }

    /**
     * Displays a single PengaduanPraMediasi model.
     *
     * @param  string $id
     * @return mixed
     */
    public function actionView($id) 
    {
        $model = $this->findModel($id);
        $regModel = PengaduanRegistrasi::find()->getById($model->id_pengaduan);
        if (!Yii::$app->user->identity->isAdminWilayah($regModel->id_wilayah_konflik)) {
            throw new ServerErrorHttpException('You are not authorized to perform this action.');
        }

        return $this->render(
            'view', [
                    'model' => $model
            ]
        );
    }

    /**
     * Creates a new PengaduanPraMediasi model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     */
    public function actionCreate() 
    {
        $model = new PengaduanPraMediasi();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render(
                'create', [
                        'model' => $model,
                ]
            );
        }
    }

    /**
     * Updates an existing PengaduanPraMediasi model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param  string $id
     * @return mixed
     */
    public function actionUpdate($id) 
    {
        $model = $this->findModel($id);
        $regModel = PengaduanRegistrasi::find()->getById($model->id_pengaduan);
        if (!Yii::$app->user->identity->isAdminWilayah($regModel->id_wilayah_konflik)) {
            throw new ServerErrorHttpException('You are not authorized to perform this action.');
        }

        if ($model->load(Yii::$app->request->post())) {
            $model->modified_date = \common\utils\DateHelper::GetCurrentDate();
            if ($model->save()) {
                return $this->redirect(['//pengaduan-registrasi/view', 'id' => $regModel->id]);
            }
        } else {
            return $this->render(
                'update', [
                        'model' => $model,
                        'registrationModel' => $regModel,
                ]
            );
        }
    }

    /**
     * Cancels changes to Pengaduan model.
     *
     * @param  integer $id
     * @return mixed
     */
    public function actionCancel($id)
    {
        return $this->redirect(['pengaduan-registrasi/view', 'id' => PengaduanRegistrasi::find()->getById($this->findModel($id)->id_pengaduan)->id]);
    }

    /**
     * Deletes an existing PengaduanPraMediasi model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param  string $id
     * @return mixed
     */
    public function actionDelete($id) 
    {
        $model = $this->findModel($id);
        $regModel = PengaduanRegistrasi::find()->getById($model->id_pengaduan);
        if (!Yii::$app->user->identity->isAdminWilayah($regModel->id_wilayah_konflik)) {
            throw new ServerErrorHttpException('You are not authorized to perform this action.');
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the PengaduanPraMediasi model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param  string $id
     * @return PengaduanPraMediasi the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) 
    {
        if (($model = PengaduanPraMediasi::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
