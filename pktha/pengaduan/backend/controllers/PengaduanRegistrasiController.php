<?php

namespace backend\controllers;

use Yii;
use common\models\PengaduanRegistrasi;
use common\models\PengaduanRegistrasiSearch;
use common\models\ImportExcelForm;
use common\models\Provinsi;
use common\models\Tahapan;
use common\utils\DateHelper;
use common\utils\CaseCodeGenerator;
use backend\viewmodels\UpdatePasswordViewmodel;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ServerErrorHttpException;
use yii\web\UploadedFile;
use yii\filters\VerbFilter;
use moonland\phpexcel\Excel;

/**
 * PengaduanRegistrasiController implements the CRUD actions for PengaduanRegistrasi model.
 */
class PengaduanRegistrasiController extends Controller {

  /**
   * @inheritdoc
   */
  public function behaviors() {
  	return [
  		'verbs' => [
  			'class' => VerbFilter::className(),
  			'actions' => [
  				'delete' => ['POST'],
  			],
  		],
  	];
  }

  /**
   * Lists all PengaduanRegistrasi models.
   *
   * @return mixed
   */
  public function actionIndex($tahap = null) {
  	$searchModel = new PengaduanRegistrasiSearch();
  	$queryParams = Yii::$app->request->queryParams;

// return \yii\helpers\Json::encode($queryParams);

  	if (isset($queryParams['PengaduanRegistrasiSearch'])) {
  		$searchModel->keyword = $queryParams['PengaduanRegistrasiSearch']['keyword'];
  	}

// Filter for user role
  	$user = Yii::$app->user->identity;
  	if (!$user->isAdminWilayah()) {
  		throw new ServerErrorHttpException('You are not authorized to perform this action.');
  	}
  	$wilayah = $user->getWilayahAdmin();
  	if ($wilayah && $wilayah != 10) {
  		$queryParams['id_wilayah_konflik'] = $wilayah;
  	}

  	if (isset($tahap)) {
  		$res = Tahapan::countAllTahapanFirstUnfinished($user, 'pengaduan');

  		$ids = $res[$tahap]['rows'];
  		$dataProvider = $searchModel->search($queryParams, array('id' => $ids));
  	} else {
  		$dataProvider = $searchModel->search($queryParams);
  	}

  	return $this->render(
  		'index', [
  			'searchModel' => $searchModel,
  			'dataProvider' => $dataProvider,
  		]
  	);
  }

  /**
   * Displays a single PengaduanRegistrasi model.
   *
   * @param  string $id
   * @return mixed
   */
  public function actionView($id) {
  	$model = $this->findModel($id);
  	if (!Yii::$app->user->identity->isAdminWilayah($model->id_wilayah_konflik)) {
  		throw new ServerErrorHttpException('You are not authorized to perform this action.');
  	}

  	$models = Tahapan::getAllTahapanById($id, 'pengaduan');
  	return $this->render(
  		'view', [
  			'registrasiModel' => $models['pengaduan_registrasi'],
  			'deskStudyModel' => $models['pengaduan_desk_study'],
  			'assesmenModel' => $models['pengaduan_assesmen'],
  			'praMediasiModel' => $models['pengaduan_pra_mediasi'],
  			'mediasiModel' => $models['pengaduan_mediasi'],
  			'draftingMouModel' => $models['pengaduan_drafting_mou'],
  			'tandaTanganMouModel' => $models['pengaduan_tanda_tangan_mou'],
  		]
  	);
  }

  /**
   * Creates a new PengaduanRegistrasi model.
   * If creation is successful, the browser will be redirected to the 'view' page.
   *
   * @return mixed
   */
  public function actionCreate() {
  	$model = new PengaduanRegistrasi();

  	$model->created_date = DateHelper::GetCurrentDate();
  	$model->modified_date = DateHelper::GetCurrentDate();

  	if ($model->load(Yii::$app->request->post())) {
  		$role = Yii::$app->user->identity->role;
  		$kode_huruf = Yii::$app->request->post('kode_huruf_pengaduan');
  		$rest = 0;
  		if ($role >= 30) {
  			$rest = $role - 30;
  			if (!isset($kode_huruf) || strlen($kode_huruf) == 0) {
  				$kode_huruf = 'P';
  			}
  		} else if ($role >= 20 && $role < 30) {
  			$rest = $role - 20;
  			if (!isset($kode_huruf) || strlen($kode_huruf) == 0) {
  				$kode_huruf = 'D';
  			}
  		}

  		$model->kode_pengaduan = 'KT-' . $kode_huruf . '-' . $rest . '-' . \date('Y') . \date('m') . \date('d') . '-';

  		$model->id_wilayah_konflik = Provinsi::findOne(['id' => $model->id_provinsi_konflik])->id_wilayah;
  		$model->teks_provinsi_konflik = Provinsi::findOne(['id' => $model->id_provinsi_konflik])->nama_provinsi;
  		$model->kunci_pengaduan = sha1(sha1($model->kode_pengaduan));

      // Update kode pengaduan dengan ID.
  		$model->save();
  		$model->kode_pengaduan .= $model->id;

      // Create password for pengaduan.
  		$rawKey = \common\utils\CaseCodeGenerator::random_str(6);
  		$model->kunci_pengaduan = sha1($rawKey);

      // Just to make sure after kode pengaduan is complete, the record is saved.
  		$model->save();
  		$model_id = $model->id;

  		$tahap = Tahapan::getTahapanByProses('pengaduan');
  		foreach ($tahap as $t) {
  			if ($t['nama'] != 'Registrasi' && isset($t['model'])) {
  				$m = new $t['model']();
  				$m->id_pengaduan = $model_id;
  				$m->created_date = DateHelper::GetCurrentDate();
  				$m->modified_date = DateHelper::GetCurrentDate();
  				if (!$m->finished) {
  					$m->finished = false;
  				}
  				$m->save();
  			}
  		}

      /* Save files in subdir named according to the model, ID and
       * kode_pengusulan. Create subdir if it doesn't exist. */
      $path = $model->getPath();
      if (!is_dir($path)) {
      	mkdir($path, 0775, true);
      	chmod($path, 0775);
      }

      // Instead of going to view, why not to confirmation page?
      // return $this->redirect(['view', 'id' => $model->id]);

      $viewmodel = new \common\viewmodels\PengaduanCreatedInfoViewModel();
      $viewmodel->id_pengaduan = $model->id;
      $viewmodel->kode_pengaduan = $model->kode_pengaduan;
      $viewmodel->kunci_pengaduan = $rawKey;

      return $this->render('pengaduancreated', ['model' => $viewmodel]);
  } else {
  	return $this->render('create', ['model' => $model,]);
  }
}

  /**
   * Reads rows from Excel file and create new PengusulanHutanAdat for each
   * entry.
   *
   * @return mixed
   */
  public function actionImportExcel() {
  	$model = new ImportExcelForm();

  	if ($model->load(Yii::$app->request->post())) {
  		$model->file = UploadedFile::getInstance($model, 'file');
  		$path = $model->getPath() . md5($model->file->baseName) . '.' . $model->file->extension;

  		if (!$model->file->saveAs($path)) {
  			throw new ServerErrorHttpException('File upload failed ' . $path);
  		}

  		$data = Excel::import($path, ['setFirstRecordAsKeys' => true, 'setIndexSheetByName' => true,]);

      // Check for Sheet 1 through 5
  		if (!isset($data['Registrasi'], $data['Assesmen'], $data['Pra-Mediasi'], $data['Mediasi'])) {
  			throw new ServerErrorHttpException('Invalid spreadsheet format: Some sheets were missing.');
  		}

  		$lineFail = array();

  		for ($i = 0; $i < count($data['Registrasi']); $i++) {
  			$line = $i + 2;

        // Registrasi
  			$model_i = new PengaduanRegistrasi();
  			$model_i->attributes = $data['Registrasi'][$i];
  			if (!$model_i->created_date) {
  				$model_i->created_date = DateHelper::GetCurrentDate();
  			}
  			$model_i->modified_date = DateHelper::GetCurrentDate();
  			$model_i->kode_pengaduan = CaseCodeGenerator::GetBasicCode(Yii::$app->user->identity->role);
  			$model_i->kunci_pengaduan = sha1(sha1($model_i->kode_pengaduan));
  			if (isset($model_i['teks_provinsi_konflik'])) {
  				$provinsi = Provinsi::findOne(['nama_provinsi' => $model_i->teks_provinsi_konflik]);
  				if (isset($provinsi)) {
  					$model_i->id_provinsi_konflik = $provinsi->id;
  					$model_i->id_wilayah_konflik = $provinsi->id_wilayah;
  				}
  			}
  			if (!$model_i->save()) {
  				Yii::$app->getSession()->setFlash(
  					'warning', 'Registrasi on row ' . $line . ' was unsuccessful.'
  				);
//Yii::getLogger()->log($model_i->attributes, Logger::LEVEL_INFO, __METHOD__);
  				array_push($lineFail, $line);
  				continue;
  			} else {
  				$model_i->kode_pengaduan .= $model_i->id;
  				$model_i->save();
  			}

  			$tahap = Tahapan::getTahapanByProses('pengaduan');
  			foreach ($tahap as $t) {
  				if ($t['nama'] != 'Registrasi' && isset($t['model'])) {
//Yii::getLogger()->log($t['model'], Logger::LEVEL_INFO, __METHOD__);
  					$m = new $t['model']();
// Check that tahapan is in excel file and that the row exists
  					if (isset($data[$t['nama']], $data[$t['nama']][$i])) {
// Filter out empty keys
  						$row = array_filter(
  							$data[$t['nama']][$i], function ($key) {
  								return !in_array($key, array(''));
  							}, ARRAY_FILTER_USE_KEY
  						);
  						$m->attributes = $row;
  					}
  					$m->created_date = DateHelper::GetCurrentDate();
  					$m->modified_date = DateHelper::GetCurrentDate();
  					$m->id_pengaduan = $model_i->id;
  					$m->finished = $m->finished ? $m->finished : false;
//Yii::getLogger()->log($m->attributes, Logger::LEVEL_INFO, __METHOD__);
  					if (!$m->save()) {
  						$model_i->delete();
  						array_push($lineFail, $line);
//Yii::getLogger()->log($m->errors, Logger::LEVEL_INFO, __METHOD__);
  						break;
  					}
  				}
  			}

        /* Save files in a subdirectory named according to the model, ID and
         * kode_pengusulan. Create subdir if it doesn't exist. */
        $path = $model_i->getPath();
        if (!is_dir($path)) {
        	mkdir($path);
        	chmod($path, 0775);
        }
    }
    if (count($lineFail) > 0) {
    	Yii::$app->getSession()->setFlash(
    		'warning', 'Rows ' . implode(', ', $lineFail)
    		. ' were not successfully imported.'
    	);
    }
    throw new ServerErrorHttpException('File was not found on the server.');
//return $this->redirect(['index']);
} else {
	return $this->render('import_excel', ['model' => $model]);
}
}

  /**
   * Reads models from current contents of dataProvider an writes to
   * Excel file
   *
   * @return none
   */
  public function actionExportExcel($tahap = null) {
  	if (!isset($tahap)) {
  		$tahap = Tahapan::getTahapanByProses('pengaduan');
  		$models = array();
  		$names = array();

  		$models[$tahap[0]['nama']] = $tahap[0]['model']::find()->all();
  		$t0 = $tahap[0]['tabel'];
  		for ($i = 1; $i < count($tahap) - 1; $i++) {
  			$model = $tahap[$i]['model'];
  			$nama = $tahap[$i]['nama'];
  			if (isset($model)) {
  				$models[$nama] = $model::find()->all();
  				$names[] = $nama;
  			}
  		}

      /*Excel::export(
              [
                  //'savePath' => Yii::getAlias('@updir').'/xls',
                  'fileName' => 'Pengaduan_Export.xlsx',
                  'isMultipleSheet' => true,
                  'models' => $models
              ]
          );*/

          $data = \yii\helpers\ArrayHelper::toArray($models['Registrasi']);
          $this->exportData($data);

      /*$spreadsheet = new PhpSpreadsheet\Spreadsheet();
      $worksheet = $spreadsheet->getActiveSheet();

      $worksheet->fromArray($data, null, 'A4');
      $writer = new Xlsx($spreadsheet);
       
      header('Content-Type: application/vnd.ms-excel');
      header('Content-Disposition: attachment;filename="download.xlsx"');
      header('Cache-Control: max-age=0');
      $writer->save('php://output');
      */
  } else {
  	$models = array();
  }
}

public function getTableHeaderColumns($table, $alias) {
	$not_in = "'id', 'id_pengaduan'";
	$additional_key = "";
	
	if($table == 'pengaduan_registrasi') {
		$not_in = "'id_tipe_konflik', 'id_wilayah_konflik', 'id_provinsi_konflik', 'id_kota_kabupaten_konflik', 'id_kecamatan_konflik', 'id_desa_kelurahan_konflik', 'id_jenis_pelapor', 'tanggal_pengaduan'";

		$additional_key = array(
			array("pos" => 4, "key" => "tk.nama_konflik")
			, array("pos" => 8, "key" => "w.nama_wilayah")
			, array("pos" => 9, "key" => "p.nama_provinsi")
			, array("pos" => 10, "key" => "kk.nama_kota_kabupaten")
			, array("pos" => 11, "key" => "k.nama_kecamatan")
			, array("pos" => 12, "key" => "dk.nama_desa_kelurahan")
			, array("pos" => 20, "key" => "jp.jenis_pelapor")
		);
	}

	$sql = "select column_name from information_schema.columns where table_schema = 'public' and table_name = '".$table."' and column_name not in (".$not_in.")";

	$data = \Yii::$app->db->createCommand($sql)->queryAll();

	$column_array = array();
	foreach($data as $item) {
		array_push($column_array, $item['column_name']);
	}

	if(is_array($additional_key)) {
		foreach($additional_key as $add) {
			array_splice($column_array, $add['pos'], 0, $add['key']);
		}
	}

	$column_str = "";
	foreach($column_array as $key => $item) {
		$pos = strpos($item, ".");
		if($pos === false)
			$column_str .= $alias.".".$item.", ";
		else {
			$column_str .= $item.", ";

			$column_array[$key] = str_replace(substr($item, 0, $pos + 1), '', $item);
		}
	}

	$column_str = substr($column_str, 0, strlen($column_str) - 2);

	$result = array(
		"column_str" => $column_str
		, "column_array" => $column_array
	);

	return $result;
}

public function getExportHeaderColumns() {
	$data['registrasi'] = $this->getTableHeaderColumns('pengaduan_registrasi', 'pr');
	$data['desk study'] = $this->getTableHeaderColumns('pengaduan_desk_study', 'pds');
	$data['assesment'] = $this->getTableHeaderColumns('pengaduan_assesmen', 'pa');
	$data['pra mediasi'] = $this->getTableHeaderColumns('pengaduan_pra_mediasi', 'ppm');
	$data['mediasi'] = $this->getTableHeaderColumns('pengaduan_mediasi', 'pm');
	$data['drafting mou'] = $this->getTableHeaderColumns('pengaduan_drafting_mou', 'pdm');
	$data['tanda tangan mou'] = $this->getTableHeaderColumns('pengaduan_tanda_tangan_mou', 'ptm');

	$column_array = array();
	$column_str = "";

	foreach($data as $key => $value) {
		$column_str .= $value['column_str'].", ";

		$column_array[$key] = $value['column_array'];
	}

	$column_str = substr($column_str, 0, strlen($column_str) - 2);

	$result = array(
		"column_str" => $column_str
		, "column_array" => $column_array
	);

	return $result;
}

public function getExportData($columns) {
	$sql = "select 
		CASE
			WHEN COALESCE(pds.finished, FALSE) = TRUE AND COALESCE(pa.dialihkan, FALSE) = FALSE AND COALESCE(pa.finished, FALSE) = FALSE AND COALESCE(ppm.finished, FALSE) = FALSE AND COALESCE(pm.finished, FALSE) = FALSE AND COALESCE(pdm.finished, FALSE) = FALSE AND COALESCE(ptm.finished, FALSE) = FALSE THEN 'Assesmen'
			WHEN COALESCE(pds.finished, FALSE) = TRUE AND COALESCE(pa.dialihkan, FALSE) = FALSE AND COALESCE(pa.finished, FALSE) = TRUE AND COALESCE(ppm.finished, FALSE) = FALSE AND COALESCE(pm.finished, FALSE) = FALSE AND COALESCE(pdm.finished, FALSE) = FALSE AND COALESCE(ptm.finished, FALSE) = FALSE THEN 'Pra-Mediasi'
			WHEN COALESCE(pds.finished, FALSE) = TRUE AND COALESCE(pa.dialihkan, FALSE) = FALSE AND COALESCE(pa.finished, FALSE) = TRUE AND COALESCE(ppm.finished, FALSE) = TRUE AND COALESCE(pm.finished, FALSE) = FALSE AND COALESCE(pdm.finished, FALSE) = FALSE AND COALESCE(ptm.finished, FALSE) = FALSE THEN 'Mediasi'
			WHEN COALESCE(pds.finished, FALSE) = TRUE AND COALESCE(pa.dialihkan, FALSE) = FALSE AND COALESCE(pa.finished, FALSE) = TRUE AND COALESCE(ppm.finished, FALSE) = TRUE AND COALESCE(pm.finished, FALSE) = TRUE AND COALESCE(pdm.finished, FALSE) = FALSE AND COALESCE(ptm.finished, FALSE) = FALSE THEN 'Drafting MoU'
			WHEN COALESCE(pds.finished, FALSE) = TRUE AND COALESCE(pa.dialihkan, FALSE) = FALSE AND COALESCE(pa.finished, FALSE) = TRUE AND COALESCE(ppm.finished, FALSE) = TRUE AND COALESCE(pm.finished, FALSE) = TRUE AND COALESCE(pdm.finished, FALSE) = TRUE THEN 'Selesai'
			WHEN COALESCE(pa.dialihkan, FALSE) = TRUE THEN 'Dialihkan'
			ELSE 'Desk Study'
	   END AS status_tahapan, 
		".$columns." 
		FROM pengaduan_registrasi pr
			LEFT JOIN pengaduan_desk_study pds ON pds.id_pengaduan = pr.id
			LEFT JOIN pengaduan_assesmen pa ON pa.id_pengaduan = pr.id
			LEFT JOIN pengaduan_pra_mediasi ppm ON ppm.id_pengaduan = pr.id
			LEFT JOIN pengaduan_mediasi pm ON pm.id_pengaduan = pr.id
			LEFT JOIN pengaduan_drafting_mou pdm ON pdm.id_pengaduan = pr.id
			LEFT JOIN pengaduan_tanda_tangan_mou ptm ON ptm.id_pengaduan = pr.id
			LEFT JOIN provinsi p ON p.id = pr.id_provinsi_konflik
			LEFT JOIN kota_kabupaten kk ON kk.id = pr.id_kota_kabupaten_konflik
			LEFT JOIN kecamatan k ON k.id = pr.id_kecamatan_konflik
			LEFT JOIN desa_kelurahan dk ON dk.id = pr.id_desa_kelurahan_konflik
			LEFT JOIN tipe_konflik tk on tk.id = pr.id_tipe_konflik
			LEFT JOIN wilayah w on w.id = pr.id_wilayah_konflik
			LEFT JOIN jenis_pelapor jp on jp.id = pr.id_jenis_pelapor
	";

	$data = \Yii::$app->db->createCommand($sql)->queryAll();

	$data = \yii\helpers\ArrayHelper::toArray($data);

	return $data;
}

private function processText($str) {
	return ucwords(str_replace("_", " ", $str));
}

public function exportData($data) {
	$columnColor = array('bf8f00', '757171', '0073b7', '605ca8', 'd81b60', 'dd4b39', 'ff851b', '00a65a');
	$columnList = $this->getExportHeaderColumns();
	$columnData = $this->getExportData($columnList['column_str']);

	$headerStyle = array(
        'alignment' => array (
            'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER
        ),
        'font' => array (
			'bold' => true
			, 'color' => array('rgb' => 'ffffff')
		)
    );

	$objPHPExcel = new \PHPExcel();

	//create the header
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', 'Status');
	$objPHPExcel->setActiveSheetIndex(0)->getStyle("A1")->applyFromArray($headerStyle);
	$objPHPExcel->setActiveSheetIndex(0)->getStyle("A1")->applyFromArray(
	    array(
	        'fill' => array(
	            'type' => \PHPExcel_Style_Fill::FILL_SOLID,
	            'color' => array('rgb' => $columnColor[0])
	        )
	    )
	);

	$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:A2');

	$col = 1;
	$row = 1;
	$colIndex = 1;
	$test = '';
	foreach($columnList['column_array'] as $key => $item) {
		$colNum = count($item);
		$beginCol = \PHPExcel_Cell::stringFromColumnIndex($col);

		$objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow($col, $row, strtoupper($this->processText($key)));

		$col += $colNum;

		$lastCol = \PHPExcel_Cell::stringFromColumnIndex($col - 1);

		$objPHPExcel->setActiveSheetIndex(0)->getStyle($beginCol.$row)->applyFromArray($headerStyle);
		$objPHPExcel->setActiveSheetIndex(0)->getStyle($beginCol.$row)->applyFromArray(
		    array(
		        'fill' => array(
		            'type' => \PHPExcel_Style_Fill::FILL_SOLID,
		            'color' => array('rgb' => $columnColor[$colIndex])
		        )
		    )
		);

		$objPHPExcel->setActiveSheetIndex(0)->mergeCells($beginCol.$row.':'.$lastCol.$row);

		$colIndex++;
	}

	$col = 1;
	$row = 2;
	$colIndex = 1;
	foreach($columnList['column_array'] as $key => $item) {
		foreach($item as $column_name) {
			$curCol = \PHPExcel_Cell::stringFromColumnIndex($col);

			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $this->processText($column_name));

			$objPHPExcel->setActiveSheetIndex(0)->getStyle($curCol.$row)->applyFromArray($headerStyle);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle($curCol.$row)->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => \PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => $columnColor[$colIndex])
			        )
			    )
			);

			$col++;
		}

		$colIndex++;
	}

	$col = 0;
	$row = 3;
	foreach($columnData as $key => $item) {
		$col = 0;
		foreach($item as $column_data) {
			$curCol = \PHPExcel_Cell::stringFromColumnIndex($col);
			
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $column_data);
			$objPHPExcel->setActiveSheetIndex(0)->getColumnDimension($curCol)->setAutoSize(true);

			$col++;
		}

		$row++;
	}

	// Set active sheet index to the first sheet, so Excel opens this as the first sheet
	$objPHPExcel->setActiveSheetIndex(0);

	// Redirect output to a client’s web browser (Excel2007)
	header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Content-Disposition: attachment;filename="Pengaduan_Export.xlsx"');
	header('Cache-Control: max-age=0');
	// If you're serving to IE 9, then the following may be needed
	header('Cache-Control: max-age=1');

	// If you're serving to IE over SSL, then the following may be needed
	header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
	header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
	header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
	header ('Pragma: public'); // HTTP/1.0

	$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	$objWriter->save('php://output');
	exit;
}

  /**
   * Updates an existing PengaduanRegistrasi model.
   * If update is successful, the browser will be redirected to the 'view' page.
   *
   * @param  string $id
   * @return mixed
   */
  public function actionUpdate($id) {
  	$model = $this->findModel($id);
  	if (!Yii::$app->user->identity->isAdminWilayah($model->id_wilayah_konflik)) {
  		throw new ServerErrorHttpException('You are not authorized to perform this action.');
  	}

// In case the model doesn't exist
  	$deskStudyModel = \common\models\PengaduanDeskStudy::findOne(['id_pengaduan' => $model->id]);

  	if (!isset($deskStudyModel)) {
  		$deskStudyModel = new \common\models\PengaduanDeskStudy();
  		$deskStudyModel->id_pengaduan = $model->id;
  		$deskStudyModel->created_date = DateHelper::GetCurrentDate();
  		$deskStudyModel->modified_date = DateHelper::GetCurrentDate();
  		if ($deskStudyModel->save()) {
  			return $deskStudyModel->id;
  		}
  	}

  	if ($model->load(Yii::$app->request->post())) {
  		$model->modified_date = DateHelper::GetCurrentDate();

// Get desk study model from database and finished value from form.
  		$deskStudyModel = \common\models\PengaduanDeskStudy::findOne(['id_pengaduan' => $model->id]);
  		$deskStudyFinished = Yii::$app->request->post('pengaduandeskstudy_finished');
  		if (isset($deskStudyModel)) {
// Update desk study model and save.
  			$deskStudyModel->finished = isset($deskStudyFinished);
  			$deskStudyModel->modified_date = DateHelper::GetCurrentDate();
  			$deskStudyModel->save();
  		}

  		if ($model->save()) {
  			return $this->redirect(['view', 'id' => $model->id]);
  		}
  	} else {
  		return $this->render(
  			'update', [
  				'model' => $model,
  			]
  		);
  	}
  }

  /**
   * Cancels changes to Pengaduan model.
   *
   * @param  integer $id
   * @return mixed
   */
  public function actionCancel($id) {
  	return $this->redirect(['view', 'id' => $id]);
  }

  /**
   * Deletes existing Pengaduan models.
   * If deletion is successful, the browser will be redirected to the 'index' page.
   *
   * @param  string $id
   * @return mixed
   */
  public function actionDelete($id) {
// On deletion of a pengaduan also delete its files, if any.
  	$model = $this->findModel($id);
  	if (!Yii::$app->user->identity->isAdminWilayah($model->id_wilayah_konflik)) {
  		throw new ServerErrorHttpException('You are not authorized to perform this action.');
  	}

    // Cascade delete.
  	\common\models\PengaduanDeskStudy::deleteAll('id_pengaduan = '. $id);
  	\common\models\PengaduanAssesmen::deleteAll('id_pengaduan = '. $id);
  	\common\models\PengaduanPraMediasi::deleteAll('id_pengaduan = '. $id);
  	\common\models\PengaduanMediasi::deleteAll('id_pengaduan = '. $id);
  	\common\models\PengaduanDraftingMou::deleteAll('id_pengaduan = '. $id);
  	\common\models\PengaduanTandaTanganMou::deleteAll('id_pengaduan = '. $id);

  	$path = $model->getPath();
  	if (is_dir($path)) {
  		array_map('unlink', glob("$path/*/*"));
  	}
  	$model->delete();

  	return $this->redirect(['index']);
  }

  /**
   * Finds the PengaduanRegistrasi model based on its primary key value.
   * If the model is not found, a 404 HTTP exception will be thrown.
   *
   * @param  string $id
   * @return PengaduanRegistrasi the loaded model
   * @throws NotFoundHttpException if the model cannot be found
   */
  protected function findModel($id) {
  	if (($model = PengaduanRegistrasi::findOne($id)) !== null) {
  		return $model;
  	} else {
  		throw new NotFoundHttpException('The requested page does not exist.');
  	}
  }

  public function actionDownloadExcel() {
  	$path = Yii::getAlias('@updir') . '/xls/template/template_pengaduan.xlsm';
  	if (is_file($path)) {
  		Yii::$app->response->sendFile($path);
  	} else {
  		throw new ServerErrorHttpException($path . ' was not found on the server.');
  	}
  }

  public function actionUpdatepassword($id) {
  	$_model = PengaduanRegistrasi::findOne(['id' => $id]);
  	$model = new UpdatePasswordViewmodel();
  	$model->id = $_model->id;
  	$model->kode = $_model->kode_pengaduan;
  	$model->kunci = '';

  	if ($model->load(Yii::$app->request->post())) {
  		$rawkey = $model->kunci;
  		$_model->kunci_pengaduan = sha1($rawkey);
  		if ($_model->save()) {
  			return $this->render('updatepasswordconfirmed', ['model' => $model]);
  		}
  	}

  	return $this->render('updatepassword', ['model' => $model]);
  }

  public function actionRandomStr() {
  	return \common\utils\CaseCodeGenerator::random_str(6);
  }

}
