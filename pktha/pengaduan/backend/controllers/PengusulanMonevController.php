<?php

namespace backend\controllers;

use Yii;
use common\models\PengusulanRegistrasi;
use common\models\PengusulanMonev;
use common\models\PengusulanMonevSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PengusulanMonevController implements the CRUD actions for PengusulanMonev model.
 */
class PengusulanMonevController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all PengusulanMonev models.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PengusulanMonevSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render(
            'index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
            ]
        );
    }

    /**
     * Displays a single PengusulanMonev model.
     *
     * @param  string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render(
            'view', [
                    'model' => $this->findModel($id),
            ]
        );
    }

    /**
     * Creates a new PengusulanMonev model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @param  integer $id_pengusulan
     * @return mixed
     */
    public function actionCreate($id_pengusulan)
    {
        if (\common\models\PengusulanMonev::findOne(['id_pengusulan' => $id_pengusulan])) {
            throw new \yii\web\ServerErrorHttpException('Tahap IV untuk Pengusulan ini telah dibuat');
        }

        $model = new PengusulanMonev();
        $model->id_pengusulan = $id_pengusulan;

        if ($model->load(Yii::$app->request->post())) {
            $model->created_date = \common\utils\DateHelper::GetCurrentDate();
            $model->modified_date = \common\utils\DateHelper::GetCurrentDate();
            if ($model->save()) {
                return $this->redirect(['pengusulan-registrasi/view', 'id' => $model->id_pengusulan]);
            }
        } else {
            return $this->render(
                'create', [
                        'model' => $model,
                ]
            );
        }
    }

    /**
     * Updates an existing PengusulanMonev model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param  string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $regModel = \common\models\PengusulanRegistrasi::findOne(['id' => $model->id_pengusulan]);
        if (!Yii::$app->user->identity->isAdminWilayah($regModel->id_wilayah)) {
            throw new ServerErrorHttpException('You are not authorized to perform this action.');
        }

        if ($model->load(Yii::$app->request->post())) {
            $model->modified_date = \common\utils\DateHelper::GetCurrentDate();
            if ($model->save()) {
                return $this->redirect(['pengusulan-registrasi/view', 'id' => $model->id_pengusulan]);
            }
        } else {
            return $this->render(
                'update', [
                        'model' => $model,
                ]
            );
        }
    }

    /**
     * Cancels changes to Pengusulan model and redirects to matching Pengusulan view.
     *
     * @param  integer $id
     * @return mixed
     */
    public function actionCancel($id)
    {
        return $this->redirect(['pengusulan-registrasi/view', 'id' => PengusulanRegistrasi::find()->getById($this->findModel($id)->id_pengusulan)->id]);
    }

    /**
     * Deletes an existing PengusulanMonev model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param  string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the PengusulanMonev model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param  string $id
     * @return PengusulanMonev the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PengusulanMonev::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
