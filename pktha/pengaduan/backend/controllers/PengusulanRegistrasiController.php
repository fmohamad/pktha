<?php

namespace backend\controllers;

use Yii;
use common\models\PengusulanPermohonan;
use common\models\PengusulanRegistrasi;
use common\models\PengusulanRegistrasiSearch;
use common\models\PengusulanValidasi;
use common\models\PengusulanVerifikasi;
use common\models\PengusulanPencantuman;
use common\models\PengusulanMonev;
use common\models\Provinsi;
use common\models\ImportExcelForm;
use common\models\Tahapan;
use yii\filters\AccessControl;
use common\utils\DateHelper;
use common\utils\CaseCodeGenerator;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ServerErrorHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use backend\viewmodels\UpdatePasswordViewmodel;
use moonland\phpexcel\Excel;

/**
 * PengusulanRegistrasiController implements the CRUD actions for PengusulanRegistrasi model.
 */
class PengusulanRegistrasiController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ]
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [
                            'index',
                            'view',
                            'create',
                            'delete',
                            'import-excel',
                            'export-excel',
                            'date',
                            'update',
                            'updatepassword',
                            'random-str',
                            'download',
                            'download-excel',
                        ],
                        'allow' => true,
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all PengusulanRegistrasi models.
     *
     * @return mixed
     */
    public function actionIndex($tahap = null) {
        $searchModel = new PengusulanRegistrasiSearch();
        $queryParams = Yii::$app->request->queryParams;

        // return \yii\helpers\Json::encode($queryParams);

        if (isset($queryParams['PengusulanRegistrasiSearch'])) {
            $searchModel->keyword = $queryParams['PengusulanRegistrasiSearch']['keyword'];
        }

        $user = Yii::$app->user->identity;
        if (!$user->isAdminWilayah()) {
            throw new ServerErrorHttpException('You are not authorized to perform this action.');
        }
        $wilayah = $user->getWilayahAdmin();
        if ($wilayah !== null && $wilayah != 10) {
            $queryParams['id_wilayah'] = $wilayah;
        }
        if (isset($tahap)) {
            $res = Tahapan::countAllTahapanFirstUnfinished($user, 'pengusulan');
            $ids = $res[$tahap]['rows'];
            $queryParams['id'] = $ids;
            $dataProvider = $searchModel->search($queryParams, array('id' => $ids));
        } else {
            $dataProvider = $searchModel->search($queryParams);
        }

        return $this->render(
                        'index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                        ]
        );
    }

    /**
     * Displays a single PengusulanRegistrasi model.
     *
     * @param  string $id
     * @return mixed
     */
    public function actionView($id) {
        $model = $this->findModel($id);
        if (!Yii::$app->user->identity->isAdminWilayah($model->id_wilayah)) {
            throw new ServerErrorHttpException('You are not authorized to perform this action.');
        }

        $models = Tahapan::getAllTahapanById($id, 'pengusulan');
        if (!isset($models['pengusulan_validasi']['model'])) {
          $valMod = new \common\models\PengusulanValidasi();
          $valMod->id_pengusulan = $models['pengusulan_registrasi']['model']->id;
          $valMod->created_date = DateHelper::GetCurrentDate();
          $valMod->modified_date = DateHelper::GetCurrentDate();
          $valMod->save();
        }
        if (!isset($models['pengusulan_verifikasi']['model'])) {
          $verMod = new \common\models\PengusulanVerifikasi();
          $verMod->id_pengusulan = $models['pengusulan_registrasi']['model']->id;
          $verMod->created_date = DateHelper::GetCurrentDate();
          $verMod->modified_date = DateHelper::GetCurrentDate();
          $verMod->save();
        }
        if (!isset($models['pengusulan_pencantuman']['model'])) {
          $penMod = new \common\models\PengusulanPencantuman();
          $penMod->id_pengusulan = $models['pengusulan_registrasi']['model']->id;
          $penMod->created_date = DateHelper::GetCurrentDate();
          $penMod->modified_date = DateHelper::GetCurrentDate();
          $penMod->save();
        }
        if (!isset($models['pengusulan_monev']['model'])) {
          $monMod = new \common\models\PengusulanMonev();
          $monMod->id_pengusulan = $models['pengusulan_registrasi']['model']->id;
          $monMod->created_date = DateHelper::GetCurrentDate();
          $monMod->modified_date = DateHelper::GetCurrentDate();
          $monMod->save();
        }
        $models = Tahapan::getAllTahapanById($id, 'pengusulan');
        return $this->render(
                        'view', [
                    'model' => $models['pengusulan_registrasi'],
                    'permohonan' => $models['pengusulan_permohonan'],
                    'model_ii' => $models['pengusulan_validasi'],
                    'model_iii' => $models['pengusulan_verifikasi'],
                    'model_iv' => $models['pengusulan_pencantuman'],
                    'model_v' => $models['pengusulan_monev'],
                        ]
        );
    }

    /**
     * Creates a new PengusulanRegistrasi model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     * @throws NotFoundHttpException if the resource cannot be found
     */
    public function actionCreate() {
        $model = new PengusulanRegistrasi();

        $model->created_date = DateHelper::GetCurrentDate();
        $model->modified_date = DateHelper::GetCurrentDate();

        if ($model->load(Yii::$app->request->post())) {
            $role = Yii::$app->user->identity->role;
            $kode_huruf = Yii::$app->request->post('kode_huruf_pengusulan');
            $rest = 0;
            if ($role >= 30) {
                $rest = $role - 30;
                if (!isset($kode_huruf) || strlen($kode_huruf) == 0) {
                    $kode_huruf = 'P';
                }
            } else if ($role >= 20 && $role < 30) {
                $rest = $role - 20;
                if (!isset($kode_huruf) || strlen($kode_huruf) == 0) {
                    $kode_huruf = 'D';
                }
            }

            $code = 'HA-' . $kode_huruf . '-' . $rest . '-' . \date('Y') . \date('m') . \date('d') . '-';

            $model->kode_pengusulan = $code;
            $model->id_wilayah = Provinsi::findOne(['id' => $model->id_provinsi])->id_wilayah;
            $model->teks_provinsi = Provinsi::findOne(['id' => $model->id_provinsi])->nama_provinsi;

            // We can only upload files after saving cos we need the model ID
            $model->save();
            $this->uploadModelFiles($model);

            // Update kode pengusulan with ID
            $model->kode_pengusulan .= $model->id;
            
            // Create password for pengusulan.
            $rawKey = \common\utils\CaseCodeGenerator::random_str(6);
            $model->kunci_pengusulan = sha1($rawKey);

            // Save record after completing the code.
            $model->save();

            $model_id = $model->id;

            $tahap = Tahapan::getTahapanByProses('pengusulan');
            foreach ($tahap as $t) {
                if ($t['nama'] != 'Registrasi' && isset($t['model'])) {
                    $m = new $t['model']();
                    $m->id_pengusulan = $model_id;
                    $m->created_date = DateHelper::GetCurrentDate();
                    $m->modified_date = DateHelper::GetCurrentDate();
                    $m->finished = $m->finished ? $m->finished : false;
                    $m->save();
                }
            }

            /* Save files in subdir named according to the model, ID and
             * kode_pengusulan. Create subdir if it doesn't exist. */
            $path = $model->getPath();
            if (!is_dir($path)) {
                mkdir($path, 0775, true);
                // chmod($path, 0775);
            }

            // Instead of going to view, why not to confirmation page?
            // return $this->redirect(['view', 'id' => $model->id]);
            
            $viewmodel = new \common\viewmodels\PengusulanCreatedInfoViewModel();
            $viewmodel->id_pengusulan = $model->id;
            $viewmodel->kode_pengusulan = $model->kode_pengusulan;
            $viewmodel->kunci_pengusulan = $rawKey;
            return $this->render('pengusulancreated', ['model' => $viewmodel]);
        } else {
            return $this->render('create', ['model' => $model]);
        }
    }

    /**
     * Reads rows from Excel file and create new PengusulanRegistrasi for each
     * entry.
     *
     * @return mixed
     */
    public function actionImportExcel() {
        $model = new ImportExcelForm();

        if ($model->load(Yii::$app->request->post())) {
            $model->file = UploadedFile::getInstance($model, 'file');
            $path = $model->getPath() . md5($model->file->baseName) . '.' . $model->file->extension;

            if (!$model->file->saveAs($path)) {
                throw new ServerErrorHttpException('File upload failed ' . $path);
            }

            $data = Excel::import(
                            $path, [
                        'setFirstRecordAsKeys' => true,
                        'setIndexSheetByName' => true,
                            ]
            );

            // Check for all worksheets.
            if (!isset($data['Registrasi'], $data['Validasi'], $data['Verifikasi'], $data['Pencantuman dan Penetapan'], $data['Monev dan Pemberdayaan'])) {
                throw new ServerErrorHttpException('Invalid spreadsheet format: Some sheets were missing.');
            }
            $lineFail = array();

            for ($i = 0; $i < count($data['Registrasi']); $i++) {
                $line = $i + 2;

                // Tahap 1 Registrasi
                $model_i = new PengusulanRegistrasi();
                if (isset($data['Registrasi'][$i])) {
                    $model_i->attributes = $data['Registrasi'][$i];
                }
                $model_i->no_ktp_pengusul = (int) $model_i->no_ktp_pengusul;
                if (!$model_i->created_date) {
                    $model_i->created_date = DateHelper::GetCurrentDate();
                }
                $model_i->modified_date = DateHelper::GetCurrentDate();
                $model_i->kode_pengusulan = CaseCodeGenerator::GetHABasicCode(Yii::$app->user->identity->role);
                if (isset($model_i->teks_provinsi)) {
                    $provinsi = Provinsi::findOne(['nama_provinsi' => $model_i->teks_provinsi]);
                    if (isset($provinsi)) {
                        $model_i->id_provinsi = $provinsi->id;
                        $model_i->id_wilayah = $provinsi->id_wilayah;
                    }
                }
                if (!$model_i->save()) {
                    Yii::$app->getSession()->setFlash(
                            'warning', 'Row ' . $line . ' was not successfully imported.'
                    );
                    array_push($lineFail, $line);
                    continue;
                } else {
                    $model_i->kode_pengusulan .= $model_i->id;
                    $model_i->save();
                }

                $tahap = Tahapan::getTahapanByProses('pengusulan');
                foreach ($tahap as $t) {
                    if ($t['nama'] != 'Registrasi' && isset($t['model'])) {
                        $m = new $t['model']();
                        if (isset($data[$t['nama']], $data[$t['nama']][$i])) {
                            $row = array_filter(
                                    $data[$t['nama']][$i], function ($key) {
                                return !in_array($key, array(''));
                            }, ARRAY_FILTER_USE_KEY
                            );
                            $m->attributes = $row;
                        }
                        $m->created_date = $model_i->created_date;
                        $m->modified_date = DateHelper::GetCurrentDate();
                        $m->id_pengusulan = $model_i->id;
                        $m->finished = $m->finished ? $m->finished : false;
                        if (!$m->save()) {
                            $this->deletePengaduan($model_i->id);
                            array_push($lineFail, $line);
                            break;
                        }
                    }
                }

                /* Save files in subdir named according to the model, ID and
                 * kode_pengusulan. Create subdir if it doesn't exist. */
                $path = $model_i->getPath();
                if (!is_dir($path)) {
                    mkdir($path);
                    chmod($path, 0775);
                }
            }
            if (count($lineFail) > 0) {
                Yii::$app->getSession()->setFlash(
                        'warning', 'Rows ' . implode(', ', $lineFail)
                        . ' were not successfully imported.'
                );
            }
            throw new ServerErrorHttpException('File was not found on the server.');
            return $this->redirect(['index']);
        } else {
            return $this->render('import_excel', ['model' => $model]);
        }
    }

    /**
     * Reads models from current contents of dataProvider an writes to
     * Excel file
     *
     * @return none
     */
    public function actionExportExcel() {
        $tahap = Tahapan::getTahapanByProses('pengusulan');
        $models = array();
        $names = array();

        $models[$tahap[0]['nama']] = $tahap[0]['model']::find()->all();
        $t0 = $tahap[0]['tabel'];
        for ($i = 1; $i < count($tahap) - 1; $i++) {
            $model = $tahap[$i]['model'];
            $nama = $tahap[$i]['nama'];
            if (isset($model)) {
                $models[$nama] = $model::find()->all();
                $names[] = $nama;
            }
        }
        //Yii::getLogger()->log($models, \yii\log\Logger::LEVEL_INFO, __METHOD__);
        //throw new ServerErrorHttpException('File was not found on the server.');
        Excel::export(
                [
                    //'savePath' => Yii::getAlias('@updir').'/xls',
                    'fileName' => 'Pengusulan_Export.xls',
                    'isMultipleSheet' => true,
                    'models' => $models
                ]
        );
    }

    /**
     * Updates an existing PengusulanRegistrasi model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param  string $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);
        if (!Yii::$app->user->identity->isAdminWilayah($model->id_wilayah)) {
            throw new ServerErrorHttpException('You are not authorized to perform this action.');
        }

        // In case the model doesn't exist.
        $permohonanModel = \common\models\PengusulanPermohonan::findOne(['id_pengusulan' => $model->id]);
        if (!isset($permohonanModel)) {
            $permohonanModel = new \common\models\PengusulanPermohonan();
            $permohonanModel->id_pengusulan = $model->id;
            $permohonanModel->created_date = DateHelper::GetCurrentDate();
            $permohonanModel->save();
        }

        if ($model->load(Yii::$app->request->post())) {

            // Get finished value from finished checkbox from form.
            $permohonanFinished = Yii::$app->request->post('permohonan_finished');
            $permohonanDikembalikan = Yii::$app->request->post('permohonan_dikembalikan');

            // Get permohonan model from database.
            $permohonanModel = \common\models\PengusulanPermohonan::findOne(['id_pengusulan' => $model->id]);
            if (isset($permohonanModel)) {
                $permohonanModel->dikembalikan = isset($permohonanDikembalikan);
                $permohonanModel->finished = isset($permohonanFinished);
                $permohonanModel->dikembalikan = isset($permohonanDikembalikan);
                $permohonanModel->modified_date = DateHelper::GetCurrentDate();

                // Save permohonan model with no validation.
                $permohonanModel->save(false);
            } else {
                $permohonanModel = new \common\models\PengusulanPermohonan();
                $permohonanModel->id_pengusulan = $model->id;
                $permohonanModel->finished = isset($permohonanFinished);
                $permohonanModel->dikembalikan = isset($permohonanDikembalikan);
                $permohonanModel->modified_date = DateHelper::GetCurrentDate();
                $permohonanModel->created_date = DateHelper::GetCurrentDate();

                // Save permohonan model with no validation.
                $permohonanModel->save(false);
            }

            $model->modified_date = DateHelper::GetCurrentDate();
            $model->id_wilayah = Provinsi::findOne(['id' => $model->id_provinsi])->id_wilayah;
            $model->teks_provinsi = Provinsi::findOne(['id' => $model->id_provinsi])->nama_provinsi;
            if ($this->uploadModelFiles($model) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }
        return $this->render('update', ['model' => $model]);
    }

    /**
     * Cancels changes to Pengusulan model and redirects to matching Pengusulan view.
     *
     * @param  integer $id
     * @return mixed
     */
    public function actionCancel($id) {
        return $this->redirect(['view', 'id' => $id]);
    }

    /**
     * Uploads files in the ActiveForm of PengusulanRegistrasi instances.
     *
     * @param  PengusulanRegistrasi $model
     * @return boolean
     * @throws ServerErrorHttpException if the upload process was not completed.
     */
    protected function uploadModelFiles($_model) {
        /* Save files in a subdirectory named according to the model, ID and
         * kode_pengusulan. Create subdir if it doesn't exist. */
        $path = $_model->getPath();
        if (!is_dir($path)) {
            mkdir($path, 0775, true);
            // chmod($path, 0775);
        }
        /* It's probably safer to loop through a specified list of attributes
         * instead of through the full list of attributes. */
        $attrs = array('form', 'profil_mha', 'surat_kuasa', 'surat_pernyataan',
            'peta', 'produk_hukum_daerah', 'identitas_ketua_adat');

        foreach ($attrs as $key) {
            /* these variables rely on a naming convention of the form
             * [attr]_data and [attr]_filename for model files */
            $data_attr = $key . '_data';
            $_model->uploaded_file = UploadedFile::getInstance($_model, $data_attr);
            $subpath = $path . '/' . $key;
            if (!is_dir($subpath)) {
                mkdir($subpath, 0775, true);
                // chmod($subpath, 0775);
            }
            if ($_model->uploaded_file) {
                if (!$_model->upload($key)) {
                    throw new ServerErrorHttpException($key . ' was not processed.');
                    return false;
                } // else upload successful
            } // else attribute is not a file or no file uploaded
        }
        return true;
    }

    /**
     * Deletes existing PengusulanRegistrasi model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param string $id ID of model
     */
    protected function deletePengusulan($id) {
        // On deletion of a pengusulan also delete its files, if any.
        $model = $this->findModel($id);
        $path = $model->getPath();
        if (is_dir($path)) {
            array_map('unlink', glob("$path/*/*"));
        }

        $model->delete();
    }

    /**
     * Deletes existing PengusulanRegistrasiI-V models.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param  string $id
     * @return mixed
     */
    public function actionDelete($id) {
        // On deletion of a pengusulan also delete its files, if any.
        $model = $this->findModel($id);
        if (!Yii::$app->user->identity->isAdminWilayah($model->id_wilayah)) {
            throw new ServerErrorHttpException('You are not authorized to perform this action.');
        }
        $path = $model->getPath();
        if (is_dir($path)) {
            array_map('unlink', glob("$path/*/*"));
        }

        // Cascade delete.
        PengusulanValidasi::deleteAll('id_pengusulan = '.$id);
        PengusulanVerifikasi::deleteAll('id_pengusulan = '.$id);
        PengusulanPencantuman::deleteAll('id_pengusulan = '.$id);
        PengusulanMonev::deleteAll('id_pengusulan = '.$id);
        PengusulanPermohonan::deleteAll('id_pengusulan = '.$id);
        
        $model->delete();
        return $this->redirect(['index']);
    }

    /**
     * Finds the PengusulanRegistrasi model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param  string $id
     * @return PengusulanRegistrasi the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = PengusulanRegistrasi::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Downloads the requested file.
     *
     * @param  string $id   PengusulanRegistrasi model ID
     * @param  string $file Filename stored in model
     * @throws ServerErrorHttpException if file does not exist.
     */
    public function actionDownload($id, $file) {
        $model = $this->findModel($id);
        if (!Yii::$app->user->identity->isAdminWilayah($model->id_wilayah)) {
            throw new ServerErrorHttpException('You are not authorized to perform this action.');
        }
        $path = $model->getPath();

        $filename = '';
        switch ($file) {
            case 'form':
                $filename = $model->form_filename;
                break;
            case 'identitas_ketua_adat':
                $filename = $model->identitas_ketua_adat_filename;
                break;
            case 'peta':
                $filename = $model->peta_filename;
                break;
            case 'produk_hukum_daerah':
                $filename = $model->produk_hukum_daerah_filename;
                break;
            case 'profil_mha':
                $filename = $model->profil_mha_filename;
                break;
            case 'surat_kuasa':
                $filename = $model->surat_kuasa_filename;
                break;
            case 'surat_pernyataan':
                $filename = $model->surat_pernyataan_filename;
                break;
        }

        if ($filename != '') {
            $path = $model->getPath();
            $full_path = $path . '/' . $file . '/' . $filename;
            str_replace('\\', '/', $full_path);
            if (is_file($full_path)) {
                Yii::$app->response->sendFile($full_path);
            } else {
                throw new ServerErrorHttpException($filename . ' was not found on the server.');
            }
        } else {
            throw new ServerErrorHttpException('File was not found on the server.');
        }
    }

    public function actionDownloadExcel() {
        $path = Yii::getAlias('@updir') . '/xls/template/template_pengusulan.xlsm';
        if (is_file($path)) {
            Yii::$app->response->sendFile($path);
        } else {
            throw new ServerErrorHttpException($path . ' was not found on the server.');
        }
    }

    public function actionUpdatepassword($id) {
        
        // See if desired pengusulan is there.
        $_model = PengusulanRegistrasi::findOne(['id' => $id]);
        
        // Create a viewmodel and update the viewmodel based on selected pengusulan and load necessary attributes.
        $model = new UpdatePasswordViewmodel();
        $model->id = $_model->id;
        $model->kode = $_model->kode_pengusulan;
        
        // Load viewmodel for update corresponding entry.
        if ($model->load(Yii::$app->request->post())) {            
            $rawKey = $model->kunci;
            $_model = PengusulanRegistrasi::findOne(['id' => $id]);
            $_model->kunci_pengusulan = sha1($rawKey);
            if ($_model->save()) {
                return $this->render('updatepasswordconfirmed', ['model' => $model]);
            }
        }
        
        return $this->render('updatepassword', ['model' => $model]);
    }

    public function actionRandomStr() {
        return \common\utils\CaseCodeGenerator::random_str(6);
    }

}
