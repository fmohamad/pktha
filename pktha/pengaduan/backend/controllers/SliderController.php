<?php

namespace backend\controllers;

use Yii;
use common\models\Slider;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * SliderController implements the CRUD actions for slider model.
 */
class SliderController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all slider models.
     * @return mixed
     */
    public function actionIndex()
    {

        // var_dump(Yii::$app->user->identity);
        $dataProvider = new ActiveDataProvider([
            'query' => slider::find()->where('deleted = :did', ['did'=>0]),
            
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionPublish($id)
    {
        $model = $this->findModel($id);
        $model->publish = ($model->publish==1) ? 0 : 1;
        $model->save();
        return $this->redirect(['index']);
        // var_dump($model->publish);
    }

    public function actionRecyclebin()
    {   
        // $modelsearch = new Artikel;
        // $p=Yii::$app->request->queryParams;  

        // var_dump($p);



        $dataProvider = new ActiveDataProvider([
            'query' => slider::find()->where('deleted = :did', ['did'=>1]),
        ]);

        // $dataProvider = $modelsearch->search($p);


        return $this->render('recyclebin', [
            'dataProvider' => $dataProvider,
            
        ]);
    }

    /**
     * Displays a single slider model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new slider model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Slider();

        if ($model->load(Yii::$app->request->post())) {            
            $model->img = UploadedFile::getInstance($model, 'img');
            if($model->upload()){
                return $this->redirect(['index', 'id' => $model->id]);
            }else{
                return $this->render('create', [
                'model' => $model,
            ]);

            }
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }


    }

    /**
     * Updates an existing slider model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {            
            $model->img = UploadedFile::getInstance($model, 'img');
            if($model->upload()){
                return $this->redirect(['index', 'id' => $model->id]);
            }else{

                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }

        // if ($model->load(Yii::$app->request->post()) && $model->save()) {
        //     return $this->redirect(['view', 'id' => $model->id]);
        // } else {
        //     return $this->render('update', [
        //         'model' => $model,
        //     ]);
        // }
    }

    /**
     * Deletes an existing slider model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        // $this->findModel($id)->delete();
        $model = $this->findModel($id);
        $model->deleted=1;
        $model->save();

        return $this->redirect(['index']);
    }

    public function actionUndelete($id)
    {
        // $this->findModel($id)->delete();
        $model = $this->findModel($id);
        $model->deleted=0;
        $model->save();

        return $this->redirect(['index']);
    }

    /**
     * Finds the slider model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return slider the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = slider::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
