<?php

namespace backend\controllers;

use Yii;
use common\models\User;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ServerErrorHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\components\AccessRule;
use backend\models\ChangePasswordForm;
use backend\models\ChangePasswordFormUser;
use backend\models\SignupForm;


/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    public function behaviors()
    {
        return [
        'verbs' => [
            'class' => VerbFilter::className(),
            'actions' => [
                'delete' => ['post'],
            ],
        ],
        'access' => [
            'class' => AccessControl::className(),
            'ruleConfig' => [
                'class' => AccessRule::className(),
            ],
            'only' => [
                'create',
                'delete'
            ],
            'rules' => [
                [
                    'actions' => ['create'],
                    'allow' => true,
                     'roles' => [
                        User::ROLE_ADMIN,
                        User::ROLE_ADMIN_1,
                        User::ROLE_ADMIN_2,
                        User::ROLE_ADMIN_3,
                     ],
                ],
                [
                    'actions' => ['change-password'],
                    'allow' => true,
                    'roles' => [
                        User::ROLE_ADMIN,
                        User::ROLE_ADMIN_1,
                        User::ROLE_ADMIN_2,
                        User::ROLE_ADMIN_3,
                    ],
                ],
                [
                    'actions' => ['delete'],
                    'allow' => true,
                    'roles' => [
                        User::ROLE_ADMIN,
                        User::ROLE_ADMIN_1,
                        User::ROLE_ADMIN_2,
                        User::ROLE_ADMIN_3,
                    ],
                ],
            ],
        ],
        ];
    }

    /**
     * Lists all Users.
     *
     * @return mixed
     */
    public function actionIndex()
    {

        // var_dump(Yii::$app->user->identity->role);
        // var_dump($user->identity->role);

        $dataProvider = new ActiveDataProvider(
            [
            'query' => User::find(),
            ]
        );

        return $this->render(
            'index', [
            'dataProvider' => $dataProvider,
            ]
        );
    }

    /**
     * Displays a single User.
     *
     * @param  integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render(
            'view', [
            'model' => $this->findModel($id),
            ]
        );
    }

    /**
     * Creates a new User.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SignupForm();

        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                // if (Yii::$app->getUser()->login($user)) {
                return $this->redirect(['index']);
                // }
            }
        }

        return $this->render(
            'create', [
            'model' => $model,
            ]
        );
    }

    /**
     * Updates user password.
     * Redirect to index and display flash regarding success state of update.
     *
     * @param  $id string User ID
     * @return mixed
     */
    public function actionChangePassword($id = null)
    {
        $model = new ChangePasswordFormUser;
        $user = Yii::$app->user->identity;
        if ($id) {
            Yii::info(var_dump($id));
            if ($user->getId() < User::ROLE_ADMIN || $id != $user->getId()) {
                throw new ServerErrorHttpException('You are not authorized to perform this action.');
            }
        } else {
            $id = $user->getId();
        }
            $modelUser = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                $modelUser->setPassword($model->password1);
                // $modelUser->updated_at = DateHelper::GetCurrentDate();
                if ($modelUser->save()) {
                    Yii::$app->getSession()->setFlash(
                        'success', 'User password has been reset successfully!'
                    );
                    return $this->redirect(['index']);
                } else {
                    Yii::$app->getSession()->setFlash(
                        'error', 'Password not changed'
                    );
                    return $this->redirect(['index']);
                }
            }
        } else {
            return $this->render(
                'changepassworduser', [
                'model'=>$model
                ]
            );
        }
    }

    // public function actionCreate()
    // {
    //     $model = new SignupForm();

    //     if ($model->load(Yii::$app->request->post()) && $model->save()) {
    //         return $this->redirect(['view', 'id' => $model->id]);
    //     } else {
    //         return $this->render('create', [
    //             'model' => $model,
    //         ]);
    //     }
    // }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param  integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        // $model = $this->findModel($id);
        $modelUpdate = new SignupForm();
        $model = $modelUpdate->findModel($id);

        // var_dump($model);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render(
                'update', [
                'model' => $model,
                ]
            );
        }
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param  integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param  integer $id
     * @return Tahapan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
