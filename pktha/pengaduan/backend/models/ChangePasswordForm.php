<?php
namespace backend\models;

use common\models\User;
use yii\base\Model;
use Yii;

/**
 * Signup form
 */
class ChangePasswordForm extends Model
{
    public $id;
    // public $username;
    public $password;
    public $password1;
    public $password2;
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['password', 'required'],
            ['password', 'string', 'min' => 6],
            ['password','findPasswords'],
            ['password1', 'required'],
            ['password1', 'string', 'min' => 6],
            ['password2', 'required'],
            ['password2', 'string', 'min' => 6],
            ['password2','compare','compareAttribute'=>'password1'],
        ];
    }


    public function findPasswords($attribute, $params)
    {
        $password = User::findByUsername(Yii::$app->user->identity->username);
        if(Yii::$app->security->validatePassword($this->password, $password->password_hash)==false) {
            $this->addError($attribute, 'Old password is incorrect ');
        }
    }

    // /**
    //  * Signs user up.
    //  *
    //  * @return User|null the saved model or null if saving fails
    //  */
    // public function signup()
    // {
    //     if ($this->validate()) {
    //         $user = new User();
    //         $user->username = $this->username;
    //         $user->email = $this->email;
    //         $user->role= $this->role;
    //         $user->setPassword($this->password);
    //         $user->generateAuthKey();
    //         if ($user->save()) {
    //             return $user;
    //         }
    //     }

    //     return null;
    // }

    // public function changepass(){

    // }


    // public function findModel($id)
    // {
    //     if (($model = User::findOne($id)) !== null) {
    //         $this->username  = $model->username;
    //         $this->password  = $model->password_hash;
    //         // $this->email  = $model->email;
    //         // $this->role  = $model->role;
    //         return $this;
    //     } else {
    //         throw new NotFoundHttpException('The requested page does not exist.');
    //     }
    // }
}
