<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mediasi".
 *
 * @property integer $id
 * @property string $data_para_pihak
 * @property string $pokok_permasalahan
 * @property string $objek_mediasi
 * @property string $surat_usulan_mediator
 * @property string $sk_mediator
 * @property string $proses_pertemuan
 * @property string $berita_acara
 * @property string $foto
 */
class Mediasi extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mediasi';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'data_para_pihak', 'pokok_permasalahan', 'objek_mediasi', 'surat_usulan_mediator', 'sk_mediator', 'proses_pertemuan', 'berita_acara', 'foto'], 'required'],
            [['id'], 'integer'],
            [['data_para_pihak', 'pokok_permasalahan', 'objek_mediasi', 'surat_usulan_mediator', 'sk_mediator', 'proses_pertemuan', 'berita_acara', 'foto'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'data_para_pihak' => 'Data Para Pihak',
            'pokok_permasalahan' => 'Pokok Permasalahan',
            'objek_mediasi' => 'Objek Mediasi',
            'surat_usulan_mediator' => 'Surat Usulan Mediator',
            'sk_mediator' => 'Sk Mediator',
            'proses_pertemuan' => 'Proses Pertemuan',
            'berita_acara' => 'Berita Acara',
            'foto' => 'Foto',
        ];
    }
}
