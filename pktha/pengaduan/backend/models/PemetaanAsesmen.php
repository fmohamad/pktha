<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pemetaan_asesmen".
 *
 * @property string $potensi_konflik_dan_potensi_damai
 * @property string $sejarah_konflik
 * @property string $akar_konflik
 * @property string $pemicu_konflik
 * @property string $akselerator_konflik
 * @property string $pemetaan_dinamika_aktor
 * @property string $sistem_representasi
 * @property string $tawaran_tertinggi
 * @property string $tawaran_terendah
 * @property string $rekomendasi
 * @property string $surat_kesediaan_mediasi
 * @property integer $id_form
 */
class PemetaanAsesmen extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pemetaan_asesmen';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['potensi_konflik_dan_potensi_damai', 'sejarah_konflik', 'akar_konflik', 'pemicu_konflik', 'akselerator_konflik', 'sistem_representasi', 'tawaran_tertinggi', 'tawaran_terendah', 'rekomendasi', 'surat_kesediaan_mediasi', 'id_form'], 'required'],
            [['id_form'], 'integer'],
            [['potensi_konflik_dan_potensi_damai', 'akar_konflik', 'pemicu_konflik', 'akselerator_konflik', 'pemetaan_dinamika_aktor', 'sistem_representasi', 'tawaran_tertinggi', 'tawaran_terendah', 'rekomendasi', 'surat_kesediaan_mediasi'], 'string', 'max' => 20],
            [['sejarah_konflik'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'potensi_konflik_dan_potensi_damai' => 'Potensi Konflik Dan Potensi Damai',
            'sejarah_konflik' => 'Sejarah Konflik',
            'akar_konflik' => 'Akar Konflik',
            'pemicu_konflik' => 'Pemicu Konflik',
            'akselerator_konflik' => 'Akselerator Konflik',
            'pemetaan_dinamika_aktor' => 'Pemetaan Dinamika Aktor',
            'sistem_representasi' => 'Sistem Representasi',
            'tawaran_tertinggi' => 'Tawaran Tertinggi',
            'tawaran_terendah' => 'Tawaran Terendah',
            'rekomendasi' => 'Rekomendasi',
            'surat_kesediaan_mediasi' => 'Surat Kesediaan Mediasi',
            'id_form' => 'Id Form',
        ];
    }
}
