<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pra_mediasi".
 *
 * @property integer $id
 * @property string $data_para_pihak
 * @property string $pokok_permasalahan
 * @property string $objek_mediasi
 */
class PraMediasi extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pra_mediasi';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'data_para_pihak', 'pokok_permasalahan', 'objek_mediasi'], 'required'],
            [['id'], 'integer'],
            [['data_para_pihak', 'pokok_permasalahan', 'objek_mediasi'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'data_para_pihak' => 'Data Para Pihak',
            'pokok_permasalahan' => 'Pokok Permasalahan',
            'objek_mediasi' => 'Objek Mediasi',
        ];
    }
}
