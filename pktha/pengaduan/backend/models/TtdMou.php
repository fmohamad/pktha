<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ttd_mou".
 *
 * @property integer $id
 * @property string $dokumen_mou
 */
class TtdMou extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ttd_mou';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'dokumen_mou'], 'required'],
            [['id'], 'integer'],
            [['dokumen_mou'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'dokumen_mou' => 'Dokumen Mou',
        ];
    }
}
