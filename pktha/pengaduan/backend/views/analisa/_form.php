<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Analisa */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="analisa-form">
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'hasil_analisa')->textarea(['rows' => 6]) ?>
    <?= $form->field($model, 'pembahasan')->textarea(['rows' => 6]) ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
