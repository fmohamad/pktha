<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Analisa */

$this->title = 'Create Pemetaan Konflik (Hasil Lapangan)';
// $this->params['breadcrumbs'][] = ['label' => 'Analisa', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => 'Pengaduan', 'url' => ['pengaduan/timeline', 'id' => $idx]];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box box-info">
    <div class="analisa-create box-body">
        <?=
        $this->render('_form', [
            'model' => $model,
        ])
        ?>
    </div>
</div>

