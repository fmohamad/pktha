<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Analisa */

$this->title = 'Update Analisa'; // . ' ' . $model->id;
// $this->params['breadcrumbs'][] = ['label' => 'Analisa', 'url' => ['index']];
// $this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = ['label' => 'Pengaduan', 'url' => ['pengaduan/timeline', 'id' => $idx]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="box box-info">
    <div class="analisa-update box-body">
        <?=
        $this->render('_form', [
            'model' => $model,
        ])
        ?>
    </div>
</div>
