<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Analisa */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Analisa', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-info">
    <div class="analisa-view box-body">
        <p>
            <?= Html::a('Back', ['pengaduan/timeline', 'id' => $idx], ['class' => 'btn btn-primary']) ?>
            <?=
            Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </p>
        <?=
        DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'hasil_analisa:ntext',
                'pembahasan:ntext',
            // 'created_at:datetime',
            // 'updated_at:datetime',
            // 'user_id',
            ],
        ])
        ?>
    </div>
</div>