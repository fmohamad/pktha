<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Artikel';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title">List <?= $this->title; ?></h3>          
        <div class="box-tools pull-right">
            <p>
                <?= Html::a('List Artikel', ['index'], ['class' => 'btn btn-success']) ?>

            </p>
        </div>
    </div>
    <div class="artikel-index box-body">
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $modelsearch,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'judul',
                [
                    'attribute' => 'headline',
                    'format' => 'text',
                    'label' => 'headline',
                    'value' => function ($dataProvider) {
                        return yii\helpers\BaseStringHelper::truncateWords($dataProvider->headline, 10, null, true);
                    },
                ],
                [
                    'attribute' => 'konten',
                    'format' => 'text',
                    'label' => 'Isi',
                    'value' => function ($dataProvider) {
                        return yii\helpers\BaseStringHelper::truncateWords($dataProvider->konten, 10, null, true);
                    },
                ],
                // 'image',
                [
                    'attribute' => 'publish',
                    'value' => function($model) {
                        return $model->publish == 1 ? 'Published' : 'Unpublished';
                    }
                ],
                [
                    'attribute' => 'user_id',
                    'value' => 'user.username',
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{undelete} {remove}',
                    'buttons' => [
                        'undelete' => function ($url) {
                            return Html::a('<span class="glyphicon glyphicon-repeat"></span> &nbsp;&nbsp;&nbsp;', 
                                            $url, 
                                            [
                                                'title' => 'Restore',
                                                'data-pjax' => '0',
                                            ]
                            );
                        },
                        'remove' => function($url) {
                            return Html::a('<span class="glyphicon glyphicon-trash"></span> &nbsp;&nbsp;&nbsp;', 
                                            $url, 
                                            [
                                                'title' => 'Remove',
                                                'data-pjax' => '0',
                                            ]
                            );
                        },
                    ]
                ],
            // 'created_at',
            // 'updated_at',
            // ['class' => 'yii\grid\ActionColumn'],
            ],
        ]);
        ?>
    </div>
</div>