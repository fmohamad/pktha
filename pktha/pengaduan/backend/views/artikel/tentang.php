<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use himiklab\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model common\models\Artikel */

$this->title = 'Update Artikel: ' . ' ' . $model->judul;
$this->params['breadcrumbs'][] = ['label' => 'Artikels', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->judul, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="artikel-update">
    <div class="box box-info">
        <div class="artikel-form box-body">
            <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>
            <?= $form->errorSummary($model); ?>
            <?php //$form->field($model, 'kategori_id')->dropDownList(\yii\helpers\ArrayHelper::map(\common\models\Kategori::find()->all(),'id','kategori'));  ?>
            <?= $form->field($model, 'judul')->textInput(['maxlength' => true]) ?>
            <?php // $form->field($model, 'headline')->textInput(['maxlength' => true]);  ?>
            <?php //$form->field($model, 'konten')->textarea(['rows' => 6]) ?>
            <?=
            $form->field($model, 'konten')->widget(CKEditor::className(), [
                'editorOptions' => ['height' => '300px']
            ])
            ?>
            <?= $form->field($model, 'imageFile')->fileInput() ?>
            <?php if ($model->image) : ?>
            <div class="row">
                <div class="col-md-12">
                    <img src="<?= Yii::getAlias('@web') . '/../../uploads/' . $model->image; ?>" style="max-width: 100%" />
                </div>
            </div>
            <?php endif; ?>
            <?= $form->field($model, 'publish')->dropDownList(array('1' => 'Publish', '0' => 'Unpublished')) ?>
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
