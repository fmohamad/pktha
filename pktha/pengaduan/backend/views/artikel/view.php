<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Artikel */

$this->title = $model->judul;
$this->params['breadcrumbs'][] = ['label' => 'Artikels', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-info">
    <div class="artikel-view box-body">
        <p>
            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?=
            Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        <?= Html::a('Cancel', yii\web\Request::getReferrer(), ['class' => 'btn btn-warning']); ?>
        </p>
        <?php
        $katlist = ['10' => 'Kegiatan', '15' => 'Cerita Sukses', '23' => 'Peraturan dan Perundangan', '35' => 'Buku', '43' => 'Peta']
        ?>
        <?=
        DetailView::widget([
            'model' => $model,
            'attributes' => [
                // 'id',
                    [
                    'attribute' => 'kategori_id',
                    'value' => $katlist[$model->kategori_id]
                ],
                'judul',
                'headline',
                'konten:ntext',
                'image',
                    [
                    'attribute' => 'publish',
                    'value' => $model->publish == 1 ? 'Published' : 'Unpublished'
                ],
                'user.username',
                'created_at:datetime',
                'updated_at:datetime',
            ],
        ])
        ?>
    </div>
</div>
