<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\web\Request;

/* @var $this yii\web\View */
/* @var $model common\models\Assesment */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="box box-color2">
    <!-- /.box-header -->
    <div class="assesment-form box-body">
        <?php
        $form = ActiveForm::begin();
        ?>
        <?= $form->field($model, 'nama_tim')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'anggota_tim')->textarea(['rows' => 6]) ?>
        <?= $form->field($model, 'analisa_awal')->textarea(['rows' => 6]) ?>
        <?= $form->field($model, 'tgl_berangkat')->widget(yii\jui\DatePicker::className('form-control'), ['options' => ['class' => 'form-control'], 'language' => 'id', 'clientOptions' => ['dateFormat' => 'yy-mm-dd']]) ?>
        <?= $form->field($model, 'tgl_pulang')->widget(yii\jui\DatePicker::className('form-control'), ['options' => ['class' => 'form-control'], 'language' => 'id', 'clientOptions' => ['dateFormat' => 'yy-mm-dd']]) ?>
        <?// $form->field($model, 'data_informasi')->textarea(['rows' => 6]) ?>
        <?= $form->field($model, 'penilaian')->textInput(['maxlength' => true]) ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            <?= Html::a('Cancel', Request::getReferrer(), ['class' => 'btn btn-danger']); ?>
            <?php
            ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
