<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Assesment';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title">List Pengaduan</h3>          
        <div class="box-tools pull-right">
            <p>
                <?= Html::a('Create Assesment', ['create'], ['class' => 'btn btn-success']) ?>
            </p>
        </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                // 'id',
                'pengaduan_id',
                'nama_tim',
                'anggota_tim:ntext',
                'tgl_berangkat',
                // 'tgl_pulang',
                // 'data_informasi:ntext',
                // 'analisa_awal:ntext',
                // 'penilaian',
                // 'created_at',
                // 'updated_at',
                // 'user_id',
                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]);
        ?>
    </div>
</div>
