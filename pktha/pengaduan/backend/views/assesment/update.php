<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Assesment */

$this->title = 'Update Verifikasi Kelengkapan Data dan Penapisan Awal: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Pengaduan', 'url' => ['pengaduan/timeline', 'id' => $idx]];
// $this->params['breadcrumbs'][] = ['label' => 'Assesment', 'url' => ['index']];
// $this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="assesment-update">
    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>
</div>
