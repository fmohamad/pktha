<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Assesment */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Pengaduan', 'url' => ['pengaduan/timeline', 'id' => $idx]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-color2">
    <div class="box-header with-border">
      <h3 class="box-title"><?= Html::encode($this->title) ?></h3>          
      <div class="box-tools pull-right">    
    </div>
</div>
<div class="rekomendasi-create box-body">
    <p>
        <?= Html::a('Back', ['pengaduan/timeline', 'id' => $idx], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a('Cancel', yii\web\Request::getReferrer(),['class'=>'btn btn-warning']);?>
    </p>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'pengaduan_id',
            'nama_tim',
            'anggota_tim:ntext',
            'tgl_berangkat',
            'tgl_pulang',
            'data_informasi:ntext',
            'analisa_awal:ntext',
            'penilaian',
            // 'created_at',
            // 'updated_at',
            // 'user_id',
        ],
    ]) ?>
</div>
