<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\DesaKelurahan */

$this->title = Yii::t('app', 'Create Desa Kelurahan');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Desa Kelurahan'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="desa-kelurahan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form_create', [
        'model' => $model,
    ]) ?>

</div>
