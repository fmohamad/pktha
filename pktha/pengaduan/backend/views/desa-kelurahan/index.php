<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\DesaKelurahanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Desa Kelurahan');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="desa-kelurahan-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]);  ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Desa Kelurahan'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php Pjax::begin(); ?>    
    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
            [
                'label' => 'Kecamatan',
                'value' => function($model, $index, $widget) {
                    return \common\models\Kecamatan::findOne(['id' => $model->id_kecamatan])->nama_kecamatan;
                },
            ],
            // 'id_kecamatan',
            'nama_desa_kelurahan:ntext',
            'id',
            'latitude',
            'longitude',
                ['class' => 'yii\grid\ActionColumn'],
        ],
    ]);
    ?>
    <?php Pjax::end(); ?></div>
