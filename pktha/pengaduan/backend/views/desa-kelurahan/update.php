<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\DesaKelurahan */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Desa Kelurahan',
]) . $model->nama_desa_kelurahan;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Desa Kelurahan'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="desa-kelurahan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
