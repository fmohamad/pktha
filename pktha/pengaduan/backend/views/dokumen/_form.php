<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\dokumen */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="dokumen-form">
    <?php //$form = ActiveForm::begin(); ?>
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>
    <?//$form->field($model, 'tipe_dokumen')->textInput() ?>
    <?//$form->field($model, 'id_tahap')->textInput() ?>
    <?= $form->field($model, 'nama')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'keterangan')->textArea(['row' => 2]) ?>
    <? //$form->field($model, 'imageFile')->fileInput() ?>
    <?= $form->field($model, 'url')->fileInput() ?>
    <?//$form->field($model, 'created_at')->textInput() ?>
    <?//$form->field($model, 'updated_at')->textInput() ?>
    <?//$form->field($model, 'user_id')->textInput() ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
