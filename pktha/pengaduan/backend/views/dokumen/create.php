<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\dokumen */

$this->title = 'Tambah dokumen';
$this->params['breadcrumbs'][] = ['label' => 'Dokumens', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-info">
    <div class="dokumen-create box-body">
        <?=
        $this->render('_form', [
            'model' => $model,
        ])
        ?>
    </div>
</div>
