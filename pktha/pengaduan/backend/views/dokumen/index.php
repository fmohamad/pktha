<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Dokumens';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dokumen-index">
    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?= Html::a('Create Dokumen', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
            'id',
            'tipe_dokumen',
            'id_tahap',
            'nama',
            'keterangan',
            // 'url:url',
            // 'created_at',
            // 'updated_at',
            // 'user_id',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]);
    ?>
</div>
