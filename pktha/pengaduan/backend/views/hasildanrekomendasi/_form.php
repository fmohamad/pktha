<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Rekomendasi */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rekomendasi-form">
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'hasil')->textarea(['rows' => 3]) ?>
    <?= $form->field($model, 'hasil_rekomedasi')->textarea(['rows' => 3]) ?>
    <?= $form->field($model, 'tanggal')->textinput() ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
