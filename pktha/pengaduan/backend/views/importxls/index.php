<?php

use Yii;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\Pengaduan */

$this->title = 'Import XlS';
$this->params['breadcrumbs'][] = ['label' => 'Pengaduan', 'url' => ['pengaduan/index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box box-info">
    <!-- /.box-header -->
    <div class="pengaduan-create box-body">
        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>
        <?= $form->field($model, 'file')->fileInput() ?>
        <div class="form-group">
            <?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>
        </div>
        <?php ActiveForm::end(); ?>
        <p>Download template xls : </p>
        <?php
        $url = Yii::getAliash('@updir') . '/tpl/tpl.xlsx';
        ?>
        <?= Html::a('<i class="fa fa-file-excel-o"></i> template xls', $url, ['class' => 'btn']) ?>
    </div>
</div>
