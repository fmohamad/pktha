<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Jenispenyelesaian */

$this->title = 'Update Jenis penyelesaian: ' . ' ' . $model->nama;
$this->params['breadcrumbs'][] = ['label' => 'Jenis penyelesaian', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nama, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="jenispenyelesaian-update">
    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>
</div>
