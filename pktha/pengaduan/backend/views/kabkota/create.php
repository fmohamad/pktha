<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Kabkota */

$this->title = 'Create Kota / Kabupaten';
$this->params['breadcrumbs'][] = ['label' => 'Kabkota', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kabkota-create">
    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>
</div>
