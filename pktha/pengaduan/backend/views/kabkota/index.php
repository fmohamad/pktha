<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Kota / Kabupaten';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title">List <?= $this->title; ?></h3>          
        <div class="box-tools pull-right">
            <p>
                <?= Html::a('Create Kota / Kabupaten', ['create'], ['class' => 'btn btn-success']) ?>
            </p>
        </div>
    </div>
    <div class="kabkota-index box-body">
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                'nama_kota_kabupaten',
                    [
                    'label' => 'Provinsi',
                    'value' => function($model, $index, $widget) {
                        return \common\models\Provinsi::findOne(['id' => $model->id_provinsi])->nama_provinsi;
                    }],
                    ['class' => 'yii\grid\ActionColumn'],
            ],
        ]);
        ?>
    </div>
</div>
