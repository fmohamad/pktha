<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\KotaKabupaten */

$this->title = 'Update Kabkota: ' . ' ' . $model->nama_kota_kabupaten;
$this->params['breadcrumbs'][] = ['label' => 'kota / kabupaten', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="kabkota-update">
    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>
</div>
