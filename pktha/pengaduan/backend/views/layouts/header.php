<?php

use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */
?>

<header class="main-header">
    <div class="navbar-container">
      <a class="logo" href="#"><span class="logo-mini">PKTHA</span><span class="logo-lg"><h5>Sistem Database <br/>Konflik Tenurial v.1.0</h5></span></a>
      <nav class="navbar navbar-static-top" role="navigation">
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">

                <!-- Messages: style can be found in dropdown.less-->

                <!-- User Account: style can be found in dropdown.less -->

                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <span class="hidden-xs"><?= Yii::$app->user->identity->username; ?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li >
                            <?= Html::a('<i class="fa fa-unlock-alt" aria-hidden="true"></i>Change Password', \yii\helpers\Url::to(['/user/change-password'])) ?>
                        </li>
                    </ul>
                </li>
                <li class="dropdown user user-menu">
                    <a href="<?= \yii\helpers\Url::to(['/site/logout']) ?>"class="dropdown-toggle" data-toggle="dropdown" data-method="post">Sign out</a>
                </li>
            </ul>
        </div>
    </nav>
  </div>
</header>
