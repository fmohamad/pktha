<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Mha */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mha-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nama_komunitas')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'id_provinsi')->textInput() ?>

    <?= $form->field($model, 'id_kota_kabupaten')->textInput() ?>

    <?= $form->field($model, 'id_kecamatan')->textInput() ?>

    <?= $form->field($model, 'luas_wilayah_adat')->textInput() ?>

    <?= $form->field($model, 'batas_utara_wilayah_adat')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'batas_selatan_wilayah_adat')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'batas_timur_wilayah_adat')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'batas_barat_wilayah_adat')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'satuan_wilayah_adat')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'kondisi_fisik_wilayah')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'jumlah_kepala_keluarga')->textInput() ?>

    <?= $form->field($model, 'jumlah_pria')->textInput() ?>

    <?= $form->field($model, 'jumlah_wanita')->textInput() ?>

    <?= $form->field($model, 'mata_pencaharian_utama')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'sejarah_singkat_masyarakat_adat')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'pembagian_ruang_menurut_aturan_adat')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'sistem_penguasaan_pengelolaan_wilayah')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'nama_lembaga_adat')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'struktur_lembaga_adat')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'struktur_lembaga_adat_filename')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'tugas_fungsi_pemangku_adat')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'tugas_fungsi_pemangku_adat_filename')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'mekanisme_pengambilan_keputusan')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'aturan_adat_terkait_wilayah_sda')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'aturan_adat_terkait_wilayah_sda_filename')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'aturan_adat_terkait_pranata_sosial')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'aturan_adat_terkait_pranata_sosial_filename')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'contoh_putusan_hukum_adat')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
