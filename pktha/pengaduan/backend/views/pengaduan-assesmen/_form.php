<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\PengaduanAssesmen */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pengaduan-assesmen-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?php echo $form->field($model, 'potensi_konflik_dan_potensi_damai')->textarea(['rows' => 6]) ?>

    <?php echo $form->field($model, 'sejarah_konflik')->textarea(['rows' => 6]) ?>

    <?php echo $form->field($model, 'akar_konflik')->textarea(['rows' => 6]) ?>

    <?php echo $form->field($model, 'pemicu_konflik')->textarea(['rows' => 6]) ?>

    <?php echo $form->field($model, 'akselerator_konflik')->textarea(['rows' => 6]) ?>

    <?php echo $form->field($model, 'pemetaan_dinamika_aktor')->textarea(['rows' => 6]) ?>

    <?php echo $form->field($model, 'pemetaan_dinamika_aktor_filename')->fileInput() ?>

    <?php if ($model->pemetaan_dinamika_aktor_filename) : ?>
        <?php echo Html::a($model->pemetaan_dinamika_aktor_filename, ['pengaduan-assesmen/download', 'id' => $model->id, 'file' => 'pemetaan_dinamika_aktor'], ['class' => 'btn-bg-yellow']) ?>
    <?php endif ?>

    <?php echo $form->field($model, 'sistem_representasi')->textarea(['rows' => 6]) ?>

    <?php echo $form->field($model, 'tawaran_tertinggi')->textInput() ?>

    <?php echo $form->field($model, 'tawaran_terendah')->textInput() ?>

    <?php echo $form->field($model, 'rekomendasi')->textarea(['rows' => 6]) ?>

    <?php echo $form->field($model, 'surat_kesediaan_mediasi_filename')->fileInput() ?>

    <?php if ($model->surat_kesediaan_mediasi_filename) : ?>
        <?php echo Html::a($model->surat_kesediaan_mediasi_filename, ['pengaduan-assesmen/download', 'id' => $model->id, 'file' => 'surat_kesediaan_mediasi'], ['class' => 'btn-bg-yellow']) ?>
    <?php endif ?>

    <?php echo $form->field($model, 'dialihkan')->checkbox() ?>

    <?php echo $form->field($model, 'pesan_pengalihan_kasus')->textarea(['rows' => 6]) ?>

    <?php echo $form->field($model, 'created_date')->textInput(['disabled' => true]) ?>

    <?php echo $form->field($model, 'modified_date')->textInput(['disabled' => true]) ?>

    <?php echo $form->field($model, 'finished')->checkbox() ?>

    <div class="form-group">
        <?php echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?php echo Html::a(Yii::t('app', 'Cancel'), ['cancel', 'id' => $model->id], ['class' => 'btn btn-warning']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
