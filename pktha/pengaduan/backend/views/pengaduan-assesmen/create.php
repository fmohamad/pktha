<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PengaduanAssesmen */

$this->title = Yii::t('app', 'Create Pengaduan Assesmen');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pengaduan Assesmens'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pengaduan-assesmen-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
