<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PengaduanDeskStudy */

$this->title = 'Create Pengaduan Desk Study';
$this->params['breadcrumbs'][] = ['label' => 'Pengaduan Desk Studies', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pengaduan-desk-study-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
