<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\PengaduanDraftingMou */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pengaduan-drafting-mou-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?php echo $form->field($model, 'berita_acara_filename')->fileInput() ?>

    <?php if ($model->berita_acara_filename) : ?>
        <?php echo Html::a($model->berita_acara_filename, ['pengaduan-drafting-mou/download', 'id' => $model->id, 'file' => 'berita_acara'], ['class' => 'btn bg-yellow']) ?>
    <?php endif ?>

    <?php echo $form->field($model, 'foto_filename')->fileInput() ?>

    <?php if ($model->foto_filename) : ?>
        <?php echo Html::a($model->foto_filename, ['pengaduan-drafting-mou/download', 'id' => $model->id, 'file' => 'foto'], ['class' => 'btn bg-yellow']) ?>
    <?php endif ?>

    <?php echo $form->field($model, 'created_date')->textInput(['disabled' => true]) ?>

    <?php echo $form->field($model, 'modified_date')->textInput(['disabled' => true]) ?>

    <?php echo $form->field($model, 'finished')->checkbox() ?>

    <div class="form-group">
        <?php echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?php echo Html::a(Yii::t('app', 'Cancel'), ['cancel', 'id' => $model->id], ['class' => 'btn btn-warning']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
