<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PengaduanPraMediasi */

$this->title = Yii::t('app', 'Create Pengaduan Pra Mediasi');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pengaduan Pra Mediasis'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pengaduan-pra-mediasi-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
