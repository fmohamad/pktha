<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PengaduanPraMediasi */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Pengaduan Pra Mediasi',
]) . $registrationModel->kode_pengaduan;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pengaduan Pra Mediasi'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="pengaduan-pra-mediasi-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model
    ]) ?>

</div>
