<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\PengaduanRegistrasi */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pengaduan-registrasi-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php echo $form->field($model, 'kode_pengaduan')->textInput(['disabled' => true]) ?>

    <?php echo $form->field($model, 'nama_pengadu')->textInput() ?>

    <?php echo $form->field($model, 'no_telepon_pengadu')->textInput() ?>

    <?php echo $form->field($model, 'alamat_lengkap_pengadu')->textarea(['rows' => 6]) ?>

    <?php echo $form->field($model, 'email_pengadu')->textInput() ?>

    <?php echo $form->field($model, 'id_jenis_pelapor')->dropDownList(\yii\helpers\ArrayHelper::map(\common\models\JenisPelapor::find()->all(), 'id', 'jenis_pelapor'))->label('Jenis Pelapor') ?>

    <?php echo $form->field($model, 'id_tipe_konflik')->dropDownList(\yii\helpers\ArrayHelper::map(\common\models\TipeKonflik::find()->all(), 'id', 'nama_konflik'))->label('Jenis Konflik') ?>

    <?php echo $form->field($model, 'pihak_berkonflik_1')->textarea(['rows' => 6]) ?>

    <?php echo $form->field($model, 'pihak_berkonflik_2')->textarea(['rows' => 6]) ?>

    <?php echo $form->field($model, 'pihak_berkonflik_3')->textarea(['rows' => 6]) ?>

    <?php
    echo
    $form->field($model, 'id_wilayah_konflik', ['options' => ['id' => 'select-wilayah-konflik']])->dropDownList(
            \yii\helpers\ArrayHelper::map(\common\models\Wilayah::find()->all(), 'id', 'nama_wilayah'), [
        'prompt' => 'Pilih Wilayah Konflik',
        'onchange' => '$.post("' . \yii\helpers\Url::to(["location/provinsi"]) . '", {id_wilayah: $(this).val()}, function(data){ $("#pengaduanregistrasi-id_provinsi_konflik").html(data); })',
            ]
    )->label('Wilayah Konflik')
    ?>

    <?php
    echo
    $form->field($model, 'id_provinsi_konflik')->dropDownList(
            \yii\helpers\ArrayHelper::map(\common\models\Provinsi::find()->all(), 'id', 'nama_provinsi'), [
        'prompt' => 'Pilih Provinsi Konflik',
        'onchange' => '$.post("' . \yii\helpers\Url::to(["location/kota-kabupaten"]) . '", {id_provinsi: $(this).val()}, function(data){ $("#pengaduanregistrasi-id_kota_kabupaten_konflik").html(data) })'
            ]
    )->label('Provinsi Konflik')
    ?>

    <div class="row">
        <div class="col-md-5">
            <?php
            echo $form->field($model, 'id_kota_kabupaten_konflik')->dropDownList(
                    \yii\helpers\ArrayHelper::map(\common\models\KotaKabupaten::find()->where(['id_provinsi' => $model->id_provinsi_konflik])->all(), 'id', 'nama_kota_kabupaten'), [
                'prompt' => 'Pilih Kota Kabupaten Konflik',
                'onchange' => '$.post("' . \yii\helpers\Url::to(["location/kecamatan"]) . '", {id_kota_kabupaten: $(this).val()}, function(data){ $("#pengaduanregistrasi-id_kecamatan_konflik").html(data) })'
                    ]
            )->label('Kota / Kabupaten')
            ?>
        </div>
        <div class="col-md-5">
            <?php echo $form->field($model, 'teks_kota_kabupaten_konflik')->textInput()->label('Lainnya') ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-5">
            <?php
            echo $form->field($model, 'id_kecamatan_konflik')->dropdownList(
                    \yii\helpers\ArrayHelper::map(\common\models\Kecamatan::find()->where(['id_kota_kabupaten' => $model->id_kota_kabupaten_konflik])->orWhere(['id_kota_kabupaten' => 1])->all(), 'id', 'nama_kecamatan'), [
                'prompt' => 'Pilih Kecamatan Konflik',
                'onchange' => '$.post("' . \yii\helpers\Url::to(["location/desa-kelurahan"]) . '", {id_kecamatan: $(this).val()}, function(data){ $("#pengaduanregistrasi-id_desa_kelurahan_konflik").html(data) })'
                    ]
            )->label('Kecamatan')
            ?>
        </div>
        <div class="col-md-5">
            <?php echo $form->field($model, 'teks_kecamatan_konflik')->textInput()->label('Lainnya') ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-5">
            <?php
            echo $form->field($model, 'id_desa_kelurahan_konflik')->dropDownList(
                    \yii\helpers\ArrayHelper::map(\common\models\DesaKelurahan::find()->where(['id_kecamatan' => $model->id_kecamatan_konflik])->orWhere(['id_kecamatan' => 1])->all(), 'id', 'nama_desa_kelurahan'), [
                'prompt' => 'Pilih Desa / Kelurahan Konflik',
                    ]
            )->label('Desa / Kelurahan')
            ?>
        </div>
        <div class="col-md-5">
            <?php echo $form->field($model, 'teks_desa_kelurahan_konflik')->textInput()->label('Lainnya') ?>
        </div>
    </div>

    <?php echo $form->field($model, 'latitude_daerah_konflik')->textInput() ?>

    <?php echo $form->field($model, 'longitude_daerah_konflik')->textInput() ?>

    <?php echo $form->field($model, 'fungsi_kawasan_konflik')->textarea(['rows' => 6]) ?>

    <?php echo $form->field($model, 'luas_kawasan_konflik')->textInput() ?>
    
    <?php echo $form->field($model, 'tanggal_pengaduan')->widget(yii\jui\DatePicker::className(), [
      'language' => 'en'
    ]) ?>
    
    <!-- Checkbox for Finished Desk Study -->
    <div class="form-group">
        <label>
            <?php 
                $deskStudyModel = common\models\PengaduanDeskStudy::findOne(['id_pengaduan' => $model->id]);
                if ($deskStudyModel->finished) {
                    echo '<input type="checkbox" id="pengaduandeskstudy-finished" name="pengaduandeskstudy_finished" value="1" checked>';
                } else {
                    echo '<input type="checkbox" id="pengaduandeskstudy-finished" name="pengaduandeskstudy_finished" value="1">';
                }
            ?>
            Finished
        </label>

        <div class="help-block"></div>
    </div>
    <!-- END -->

    <?php // echo $form->field($model, 'created_date')->textInput(['disabled' => true]) ?>

    <?php // echo $form->field($model, 'modified_date')->textInput(['disabled' => true]) ?>

    <div class="form-group">
        <?php echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?php echo Html::a(Yii::t('app', 'Cancel'), ['cancel', 'id' => $model->id], ['class' => 'btn btn-warning']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
