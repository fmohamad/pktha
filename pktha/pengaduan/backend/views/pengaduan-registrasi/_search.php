<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\PengaduanRegistrasiSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pengaduan-registrasi-search">

    <?php $form = ActiveForm::begin(
        [
        'action' => ['index'],
        'method' => 'get',
        ]
    ); ?>

    <?php echo $form->field($model, 'keyword') ?>

    <?php //echo $form->field($model, 'no_telepon_pengadu') ?>

    <?php //echo $form->field($model, 'alamat_lengkap_pengadu') ?>

    <?php //echo $form->field($model, 'email_pengadu') ?>

    <?php //echo $form->field($model, 'id_tipe_konflik') ?>

    <?php //echo $form->field($model, 'pihak_berkonflik_1') ?>

    <?php //echo $form->field($model, 'pihak_berkonflik_2') ?>

    <?php // echo $form->field($model, 'pihak_berkonflik_3') ?>

    <?php // echo $form->field($model, 'id_wilayah_konflik') ?>

    <?php // echo $form->field($model, 'id_provinsi_konflik') ?>

    <?php // echo $form->field($model, 'id_kota_kabupaten_konflik') ?>

    <?php // echo $form->field($model, 'id_kecamatan_konflik') ?>

    <?php // echo $form->field($model, 'id_desa_kelurahan_konflik') ?>

    <?php // echo $form->field($model, 'latitude_daerah_konflik') ?>

    <?php // echo $form->field($model, 'longitude_daerah_konflik') ?>

    <?php // echo $form->field($model, 'fungsi_kawasan_konflik') ?>

    <?php // echo $form->field($model, 'luas_kawasan_konflik') ?>

    <?php // echo $form->field($model, 'id') ?>

    <?php // echo $form->field($model, 'created_date') ?>

    <?php // echo $form->field($model, 'modified_date') ?>

    <?php // echo $form->field($model, 'id_jenis_pelapor') ?>

    <?php // echo $form->field($model, 'kode_pengaduan') ?>

    <div class="form-group">
        <?php echo Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?php echo Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
