<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url; // use absolute url?
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\PengaduanRegistrasiSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

// create top-most title
$this->title = 'Pengaduan';
$this->params['breadcrumbs'][] = $this->title;
?>

<!-- create boxed title -->
<div class="pengaduan-index">
    <div class="box box-info">
        <div class="box-header with-border">
            <!-- below is the title is the box -->
            <div class="col-md-3">
                <h3>List Pengaduan</h3>
            </div>
            <div class="col-md-9">
                <div class="box-tools pull-right">
                    <p>
                        <!-- export import buttons on the same line (and box) as boxed title -->
                    <div class="row">
                        <div class="col-md-4 col-sm-4 col-xs-4">
                            <?php echo Html::a('Create Pengaduan', ['create'], ['class' => 'btn btn-success']) ?>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-4">
                            <?php echo Html::a('<i class="fa fa-file-excel-o"></i> Import from Excel', ['import-excel'], ['class' => 'btn btn-success']) ?>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-4">
                            <?php echo Html::a('<i class="fa fa-file-excel-o"></i> Export to Excel', ['export-excel'], ['class' => 'btn btn-success']) ?>
                        </div>
                    </div>
                    <!-- <?php echo Html::a('<i class="fa fa-trash-o"></i> Pengaduan Recycle Bin', ['recyclebin'], ['class' => 'btn btn-danger']) ?> -->
                    </p>
                </div>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">

            <!-- create gridview for the table in the database -->
<!-- <h1><?php echo Html::encode($this->title) ?></h1> -->
            <?php echo $this->render('_search', ['model' => $searchModel]); ?>
            <?php Pjax::begin(); ?>
            <?php
            echo
            GridView::widget(
                    [
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],
                            // 'kode_pengaduan:ntext',
                            [
                                'attribute' => 'kode_pengaduan',
                                'label' => 'Kode Pengaduan',
                                'value' => function ($model, $index, $widget) {
                                    return $model->kode_pengaduan;
                                },
                            ],
                            [
                                'attribute' => 'nama_pengadu',
                                'label' => 'Nama Pengadu',
                                'value' => function ($model, $index, $widget) {
                                    return $model->nama_pengadu;
                                },
                            ],
                            // 'nama_pengadu:ntext',
                            [
                                'attribute' => 'pihak_berkonflik_1',
                                'label' => 'Pihak Berkonflik 1',
                                'value' => function ($model, $index, $widget) {
                                    return $model->pihak_berkonflik_1;
                                }
                            ],
                            [
                                'attribute' => 'pihak_berkonflik_2',
                                'label' => 'Pihak Berkonflik 2',
                                'value' => function ($model, $index, $widget) {
                                    return $model->pihak_berkonflik_2;
                                }
                            ],
                            [
                                'attribute' => 'pihak_berkonflik_3',
                                'label' => 'Pihak Berkonflik 3',
                                'value' => function ($model, $index, $widget) {
                                    return $model->pihak_berkonflik_3;
                                }
                            ],
                            [
                                // 'attribute' => 'id_tipe_konflik', // foreign key dari tabel tipe konflik
                                'label' => 'Tipe Konflik',
                                'value' => function ($model, $index, $widget) {
                                    if ($model->id_tipe_konflik) {
                                        return \common\models\TipeKonflik::findOne(['id' => $model->id_tipe_konflik])->nama_konflik;
                                    }
                                }
                            ],
                            [
                                'attribute' => '', // foreign key dari tabel tipe konflik
                                'label' => 'Status',
                                'format' => 'html',
                                'value' => function ($model, $index, $widget) {
                                    $tahap = $model->getTahapanFirstUnfinishedId();
                                    $nama_tahap = ($tahap['nama'] == 'Selesai')? 'Tanda Tangan MoU' : $tahap['nama'];

                                    return Html::a($nama_tahap, null, ['class' => 'label bg-' . $tahap['warna']]);
                                }
                            ],
                            [
                                'class' => 'yii\grid\ActionColumn',
                                'template' => '{view}{delete}{updatepassword}',
                                'contentOptions' => ['style' => 'min-width: 70px;'],
                                // buttons for each function
                                'buttons' => [
                                    // buttons for timeline function
                                    'view' => function ($url) {
                                        return Html::a(
                                                        '<span class="glyphicon glyphicon-eye-open"></span> &nbsp;&nbsp;&nbsp;', $url, ['title' => 'Pengaduan',
                                                    'data-pjax' => '0',]
                                        );
                                    },
                                    'updatepassword' => function($url) {
                                        return Html::a(
                                                        '<span class="glyphicon glyphicon-qrcode"></span> &nbsp;&nbsp;&nbsp;', $url, ['title' => 'Pengaduan',
                                                    'data-pjax' => '0',]
                                        );
                                    }
                                /* 'replay' => function ($model, $key, $index) {
                                  $url = Url::to(['replay/index', 'id' => $index]);
                                  return Html::a(
                                  '<span class="glyphicon glyphicon-share-alt"></span> &nbsp;&nbsp;&nbsp;', $url, ['title' => 'Reply',
                                  'data-pjax' => '0',]
                                  );
                                  } */
                                ] // buttons
                            ], // ActionColumn
                        ], // 'columns'
                    ]
            ); // GridView widget
            ?>
            <?php Pjax::end(); ?>
        </div>
    </div>
</div>

<style>
    tr#w2-filters {
        display: none;
    }
</style>
