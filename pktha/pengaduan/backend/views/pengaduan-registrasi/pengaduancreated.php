<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$this->title = Yii::t('app', 'Pengaduan Created');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pengaduan Konflik'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>

<div>
    <form>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label">ID</label>
            <div class="col-sm-10"><?php echo $model->id_pengaduan ?></div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Kode Pengaduan</label>
            <div class="col-sm-10"><?php echo $model->kode_pengaduan ?></div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Kunci</label>
            <div class="col-sm-10"><?php echo $model->kunci_pengaduan ?></div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label"></label>
            <div class="col-sm-10">
                <?php echo \yii\bootstrap\Html::a('Lihat Pengaduan', \yii\helpers\Url::to(['/pengaduan-registrasi/view', 'id' => $model->id_pengaduan]), ['class' => 'btn btn-primary'])?>
            </div>
        </div>
    </form>
</div>
