<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PengaduanRegistrasi */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Pengaduan Registrasi',
]) . $model->kode_pengaduan;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pengaduan Registrasi'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="pengaduan-registrasi-update">
    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
