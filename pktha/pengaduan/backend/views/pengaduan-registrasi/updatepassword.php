<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PengaduanRegistrasi */

$this->title = Yii::t('app', 'Update {modelClass}: ', ['modelClass' => 'Password Pengaduan',]) . $model->kode;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pengaduan'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update Password');
?>
<div class="pengaduan-registrasi-update">
    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form_update_password', [
        'model' => $model,
    ]) ?>
</div>
