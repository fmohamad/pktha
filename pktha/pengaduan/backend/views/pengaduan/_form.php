<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Pengaduan */
/* @var $form yii\widgets\ActiveForm */
?>
<?php
Yii::$app->view->registerJsFile('https://maps.googleapis.com/maps/api/js?v=3.exp', ['position' => yii\web\View::POS_END, 'depends' => [\yii\web\JqueryAsset::className()]]);
Yii::$app->view->registerJsFile(Yii::getAlias('@web') . '/js/pengaduan.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
?>



<style type="text/css">
    #map-canvas {
        text-align: center;
        width:570px;
        height:480px;
    }
</style>







<div class="pengaduan-form">

    <?php
    $form = ActiveForm::begin();
    // $model->kode =$model->getKode(1);
    ?>



    <?=
    $form->field($model, 'sarana_pengaduan')->dropDownList(array('1' => 'Website', '2' => 'Surat', '3' => 'Loket', '4' => 'Email', '5' => 'SMS'), [
        'prompt' => '-- sarana pengaduan--',
        'onchange' =>
        '$.get("' . yii::$app->urlManager->createUrl('pengaduan/getkode') . '",
                { id: $(this).val() }
                ).done(function( data ) {
                    console.log(data);
                    $("#pengaduan-kode").val(data);
                })',
    ])
    ?>

    <?= $form->field($model, 'kode')->textInput(['disabled' => true, 'maxlength' => true]) ?>
    <?= $form->field($model, 'gakum')->textInput(['maxlength' => true]) ?>

    <?php
    // $model->id_tahapan = 1;
    // $model->status  = 1;
    ?>



    <?php //if(!$model->isNewRecord): ?>

    <?php //$form->field($model, 'id_tahapan')->dropDownList(\yii\helpers\ArrayHelper::map(\common\models\Tahapan::find()->all(),'id','tahapan'),['disabled'=>true])->label('Tahapan');  ?>

    <?= $form->field($model, 'status')->dropDownList(array('0' => 'Dokumen tidak lengkap', '1' => 'Dokumen Lengkap'), ['disabled' => false]) ?>



    <?php //else :  ?>




    <?php //endif; ?>

    <?= $form->field($model, 'tipe_identitas')->dropDownList(array('1' => 'Kementerian/Lembaga', '2' => 'Individu', '3' => 'Perusahaan', '4' => 'Kelompok/Organisasi Masyarakat')) ?>

    <?= $form->field($model, 'nama_identitas')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'telepon_identitas')->textInput(['maxlength' => true, 'type' => 'tel']) ?>

    <?= $form->field($model, 'email_identitas')->textInput(['maxlength' => true, 'type' => 'email']) ?>

    <?= $form->field($model, 'alamat_identitas')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'nomor_surat')->textInput(['maxlength' => true]) ?>


    <?= $form->field($model, 'tgl_surat')->widget(yii\jui\DatePicker::className('form-control'), ['options' => ['class' => 'form-control'], 'language' => 'id', 'clientOptions' => ['dateFormat' => 'yy-mm-dd']]) ?>

    <?= $form->field($model, 'tgl_penerima_pengaduan')->widget(yii\jui\DatePicker::className('form-control'), ['options' => ['class' => 'form-control'], 'language' => 'id', 'clientOptions' => ['dateFormat' => 'yy-mm-dd']]) ?>                                  

    <?= $form->field($model, 'tgl_penerima_pktha')->widget(yii\jui\DatePicker::className('form-control'), ['options' => ['class' => 'form-control'], 'language' => 'id', 'clientOptions' => ['dateFormat' => 'yy-mm-dd']]) ?>                                  





    <?= $form->field($model, 'penerima_pengaduan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'pihak_berkonflik')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'lokasi_konflik')->textarea(['rows' => 3]) ?>

    <?= $form->field($model, 'wilayah')->dropDownList($model->getListWilayah(), ['prompt' => 'pilih wilayah...']); ?>




    <?=
    $form->field($model, 'provinsi_konflik')->dropDownList(\yii\helpers\ArrayHelper::map(\common\models\Provinsi::find()->all(), 'proid', 'provinsi'), [
        'prompt' => '--Provinsi--',
        'onchange' => '
        $.get(
            "' . yii::$app->urlManager->createUrl('pengaduan/test') . '",
            { id: $(this).val() }
            )
    .done(function( data ) {
        // console.log(data);
        $("#settengah").val("");
        $("select#pengaduan-kabkota_konflik").html(data);
    })',
    ])
    ?>




    <?=
    $form->field($model, 'kabkota_konflik')->dropDownList($model->getKabkotList($model->provinsi_konflik), [
        'prompt' => '--Pilih Kabupaten / Kota--',
        'onchange' => '
        $.get(
            "' . yii::$app->urlManager->createUrl('pengaduan/kabid') . '",
            { id: $(this).val() }
            )
        .done(function( data ) {
            console.log("---------");
            console.log(data);
            $("#settengah").val(data);
            // $("select#pengaduan-kabkota_konflik").html(data);
        })',
    ])
    ?>    


    <input type="hidden" id="settengah" name="settengah" />


    <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myMapModal">
        Buka Map
    </button>

    <?= $form->field($model, 'lot_perkiraaan_lokasi')->textInput() ?>

    <?= $form->field($model, 'lang_perkiraan_lokasi')->textInput() ?>


    <?= $form->field($model, 'status_kawasan')->dropDownList($model->getListStatusKawasan(), ['prompt' => 'pilih status kawasan...']); ?>
    <?= $form->field($model, 'fungsi_kawasan')->dropDownList($model->getListKawasan(), ['prompt' => 'pilih kawasan hutan...']); ?>






    <? //$form->field($model, 'fungsi_kawasan')->dropDownList(array('0' => 'hutan produksi','1' => 'hutan konservasi','2' => 'hutan lindung'),['disabled'=>false])  ?>



    <?= $form->field($model, 'luasan_kawasan')->textInput() ?>

    <?= $form->field($model, 'tipologi_kasus')->textInput(['maxlength' => true]) ?>







    <?= $form->field($model, 'rentang_waktu')->textInput(['maxlength' => true]) ?>





    <?php
    $js = <<<JS
  $("#pengaduan-rentang_waktu").datepicker( {
            format: "mm-yyyy",
            viewMode: "months", 
            minViewMode: "months",
            autoclose: true

        });
    $("#pengaduan-luasan_kawasan").inputmask("decimal", {
            digits: 2,
            autoGroup: true,
            groupSeparator: ",",
            allowPlus: false,
            allowMinus: false,
            
            suffix: " /Ha"
        });

JS;
    $this->registerJs($js, \yii\web\View::POS_END);
    ?>




    <?= $form->field($model, 'resume')->textarea(['row' => 3]) ?>

    <?= $form->field($model, 'tutuntan_pengaduan')->textarea(['rows' => 3]) ?>

    <?= $form->field($model, 'upaya')->textarea(['rows' => 3]) ?>

    <?= $form->field($model, 'pihak_terlibat')->textInput(['maxlength' => true]) ?>



    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a('Cancel', yii\web\Request::getReferrer(), ['class' => 'btn btn-warning']); ?>
    </div>

    <?php ActiveForm::end(); ?>



    <!-- Modal -->
    <div class="modal fade" id="myMapModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">MAP</h4>

                </div>
                <div class="modal-body">

                    <div id="map-canvas" ></div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->




    <!-- Modal -->



</div>

