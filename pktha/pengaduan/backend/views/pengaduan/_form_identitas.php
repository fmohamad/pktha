<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Pengaduan */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="pengaduan-form">
	<div class="row">
		<?php 
		$form = ActiveForm::begin(); 
    // $model->kode =$model->getKode(1);
		?>

		<div class="col-md-12">


			<?= $form->field($model, 'sarana_pengaduan')->dropDownList(array('1' => 'Website','2' => 'Surat','3' => 'Loket','4' => 'Sms/Email'),[
				'prompt'=>'-- sarana pengaduan--',
				'onchange'=>
				'$.get("' . yii::$app->urlManager->createUrl('pengaduan/getkode') . '",
					{ id: $(this).val() }
					).done(function( data ) {
						console.log(data);
						$("#pengaduan-kode").val(data);
					})',

					])  ?>

					<?= $form->field($model, 'kode')->textInput(['disabled'=>true,'maxlength' => true]) ?>



					<?= $form->field($model, 'id_tahapan')->dropDownList(\yii\helpers\ArrayHelper::map(\common\models\Tahapan::find()->all(),'id','tahapan'))->label('Tahapan'); ?>

					<?= $form->field($model, 'status')->dropDownList(array('0' => 'Tidak Layak','1' => 'Layak'))  ?>

					<?= $form->field($model, 'tipe_identitas')->dropDownList(array('1' => 'Kementerian/Lembaga','2' => 'Individu','3' => 'Perusahaan','4' => 'Kelompok/Organisasi Masyaraka'))  ?>

					<?= $form->field($model, 'nama_identitas')->textInput(['maxlength' => true]) ?>

					<?= $form->field($model, 'telepon_identitas')->textInput(['maxlength' => true,'type'=>'tel']) ?>

					<?= $form->field($model, 'email_identitas')->textInput(['maxlength' => true,'type'=>'email']) ?>

					<?= $form->field($model, 'alamat_identitas')->textarea(['rows' => 6]) ?>

				</div>
			</div>
		</div>