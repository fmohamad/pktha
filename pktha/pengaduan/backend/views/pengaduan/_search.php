<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\PengaduanSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pengaduan-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'kode') ?>

    <?= $form->field($model, 'id_tahapan') ?>

    <?= $form->field($model, 'status') ?>

    <?= $form->field($model, 'tipe_identitas') ?>

    <?php // echo $form->field($model, 'nama_identitas') ?>

    <?php // echo $form->field($model, 'telepon_identitas') ?>

    <?php // echo $form->field($model, 'email_identitas') ?>

    <?php // echo $form->field($model, 'alamat_identitas') ?>

    <?php // echo $form->field($model, 'sarana_pengaduan') ?>

    <?php // echo $form->field($model, 'nomor_surat') ?>

    <?php // echo $form->field($model, 'tgl_surat') ?>

    <?php // echo $form->field($model, 'tgl_penerima_pengaduan') ?>

    <?php // echo $form->field($model, 'tgl_penerima_pktha') ?>

    <?php // echo $form->field($model, 'penerima_pengaduan') ?>

    <?php // echo $form->field($model, 'pihak_berkonflik') ?>

    <?php // echo $form->field($model, 'lokasi_konflik') ?>

    <?php // echo $form->field($model, 'kabkota_konflik') ?>

    <?php // echo $form->field($model, 'provinsi_konflik') ?>

    <?php // echo $form->field($model, 'lot_perkiraaan_lokasi') ?>

    <?php // echo $form->field($model, 'lang_perkiraan_lokasi') ?>

    <?php // echo $form->field($model, 'fungsi_kawasan') ?>

    <?php // echo $form->field($model, 'luasan_kawasan') ?>

    <?php // echo $form->field($model, 'tipologi_kasus') ?>

    <?php // echo $form->field($model, 'status_kawasan') ?>

    <?php // echo $form->field($model, 'resume') ?>

    <?php // echo $form->field($model, 'tutuntan_pengaduan') ?>

    <?php // echo $form->field($model, 'upaya') ?>

    <?php // echo $form->field($model, 'pihak_terlibat') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'user_id') ?>

    <?php // echo $form->field($model, 'rentang_waktu') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
