<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pengaduan Recycle bin';
$this->params['breadcrumbs'][] = ['label' => 'Pengaduan', 'url' => ['pengaduan/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pengaduan-index">
    <div class="box box-info">
        <div class="box-header with-border">
          <h3 class="box-title">List Deleted Pengaduan</h3>          
          <div class="box-tools pull-right">
              <p>
                
                <?= Html::a('List Pengaduan', ['index'], ['class' => 'btn btn-success']) ?>
            </p>
        </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body">

        <?php  //echo $this->render('_search', ['model' => $searchModel]); ?>


      <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,

        'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

            // 'id',
        'kode', 
        'pihak_berkonflik',
        [
            'attribute'=>'tutuntan_pengaduan',
            'label' =>'Tuntutan Pengaduan'
        ],
        [    
            'attribute'=>'id_tahapan',   
            'value'=>'tahapan.tahapan',
            'label'=>'Tahapan',
            'filter'=>array(1=>"Registrasi",2=>"Assesment",3=>"Analisa Permasalahan",4=>"Rekomendasi"),
        
        ],        
        [
            'attribute'=>'created_at',
            'label'=>'Created',   
            'value'=>'created_at',            
            'filter'=>false,
            'format' => ['date', 'php:d-m-Y h:i:s']

        ],
            // 'update_at',
            // 'user_id',

        [
            'class' => 'yii\grid\ActionColumn', 
            'template' => '{undelete}',
            'buttons' => [
                'undelete' => function ($url) {
                    return Html::a(
                        '<span class="glyphicon glyphicon-trash"></span> &nbsp;&nbsp;&nbsp;',
                        $url, 
                    [
                        'title' => 'Pengaduan',
                        'data-pjax' => '0',
                    ]
                    );
                }]
            
        ],

        ],
        ]); ?>
    </div>
</div>
