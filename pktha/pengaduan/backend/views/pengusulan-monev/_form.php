<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\PengusulanRegistrasiV */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pengusulan-hutan-adat-v-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <div class="form-group field-pengusulanhutanadativ-tanggal_sk_penetapan">
        <label class="control-label">Kode Pengusulan</label>
        <input type="text" class="form-control" value="<?php echo \common\models\PengusulanRegistrasi::findOne(['id' => $model->id_pengusulan])->kode_pengusulan ?>" disabled="true">
        <div class="help-block"></div>
    </div>

    <!-- <?php echo $form->field($model, 'id_pengusulan')->textInput(['disabled' => true]) ?> -->

    <?php echo $form->field($model, 'perkembangan_hutan_adat')->textarea(['rows' => 6]) ?>

    <?php echo $form->field($model, 'tanggal_monitoring_dan_evaluasi')->widget(yii\jui\DatePicker::className(), [
      'language' => 'en'
    ]) ?>

    <?php echo $form->field($model, 'komoditi_hutan_adat')->textarea(['rows' => 6]) ?>

    <?php echo $form->field($model, 'komoditi_unggulan_hutan_adat')->textarea(['rows' => 6]) ?>

    <?php echo $form->field($model, 'update_kelembagaan_mha')->textarea(['rows' => 6]) ?>

    <?php echo $form->field($model, 'finished')->checkbox() ?>

    <div class="form-group">
        <?php echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?php echo Html::a(Yii::t('app', 'Cancel'), ['cancel', 'id' => $model->id], ['class' => 'btn btn-warning']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
