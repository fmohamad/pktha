<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\PengusulanHutanAdatIVSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pengusulan-hutan-adat-iv-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'no_sk_penetapan') ?>

    <?= $form->field($model, 'tanggal_sk_penetapan') ?>

    <?= $form->field($model, 'perihal_sk_penetapan') ?>

    <?= $form->field($model, 'luas_sk_penetapan') ?>

    <?php // echo $form->field($model, 'no_sk_pencantuman') ?>

    <?php // echo $form->field($model, 'tanggal_sk_pencantuman') ?>

    <?php // echo $form->field($model, 'perihal_sk_pencantuman') ?>

    <?php // echo $form->field($model, 'luas_sk_pencantuman') ?>

    <?php // echo $form->field($model, 'form_verifikasi_filename') ?>

    <?php // echo $form->field($model, 'id_pengusulan') ?>

    <?php // echo $form->field($model, 'finished')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
