<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\PengusulanRegistrasiIVSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Pengusulan Hutan Adat: Pencantuman dan Penetapan');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pengusulan-hutan-adat-iv-index">
    <p>
        <!-- <?php echo Html::a(Yii::t('app', 'Create Pengusulan Hutan Adat IV'), ['create'], ['class' => 'btn btn-success']) ?> -->
    </p>
    <?php Pjax::begin(); ?>
    <?php echo
    GridView::widget(
        [
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'id_pengusulan',
                'label' => 'Kode Pengusulan',
                'value' => function ($model, $index, $widget) {
                    return isset($model->id_pengusulan) ? \common\models\PengusulanRegistrasi::findOne(['id' => $model->id_pengusulan])->kode_pengusulan : '';
                }
            ],
            [
                'label' => 'Nama Pengusul',
                'value' => function ($model, $index, $widget) {
                    return \common\models\PengusulanRegistrasi::findOne(['id' => $model->id_pengusulan])->nama_pengusul;
                }
            ],
            // 'id',
            'no_sk_penetapan:ntext',
            // 'tanggal_sk_penetapan',
            // 'perihal_sk_penetapan:ntext',
            // 'luas_sk_penetapan',
            'no_sk_pencantuman:ntext',
            // 'tanggal_sk_pencantuman',
            // 'perihal_sk_pencantuman:ntext',
            // 'luas_sk_pencantuman',
            // 'form_verifikasi_filename:ntext',
            // 'id_pengusulan',
            // 'finished:boolean',
            [
                'label' => 'Status',
                'format' => 'html',
                'value' => function ($model, $index, $widget) {
                    return $model->finished ? Html::a('Finished', '', ['class' => 'label bg-green']) : Html::a('Not Finished', '', ['class' => 'label bg-red']);
                }
            ],
            ['class' => 'yii\grid\ActionColumn',
                'template' => '{update}'],
        ],
        ]
    );
    ?>
    <?php Pjax::end(); ?></div>
