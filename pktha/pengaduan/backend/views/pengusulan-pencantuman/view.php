<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\PengusulanRegistrasiIV */

$this->title = \common\models\PengusulanRegistrasi::findOne(['id' => $model->id_pengusulan])->kode_pengusulan;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pengusulan Hutan Adat Tahap IV'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pengusulan-hutan-adat-iv-view">

    <h1>Pengusulan Hutan Adat Tahap IV</h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'id_pengusulan',
                'label' => 'Kode Pengusulan',
                'value' => isset($model->id_pengusulan) ? \common\models\PengusulanRegistrasi::findOne(['id' => $model->id_pengusulan])->kode_pengusulan : ''
            ],
            'no_sk_penetapan:ntext',
            'tanggal_sk_penetapan',
            'perihal_sk_penetapan:ntext',
            'luas_sk_penetapan',
            'no_sk_pencantuman:ntext',
            'tanggal_sk_pencantuman',
            'perihal_sk_pencantuman:ntext',
            'luas_sk_pencantuman',
            'form_verifikasi_filename:ntext',
            'finished:boolean',
        ],
    ]) ?>

</div>
