<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<div class="pengsulan-registrasi-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php echo $form->field($model, 'id')->textInput(['readOnly' => true]) ?>
    
    <?php echo $form->field($model, 'kode')->textInput(['readOnly' => true]) ?>
    
    <?php echo $form->field($model, 'kunci')->textInput(['readOnly' => true]) ?>
    
    <div class="form-group">
        <?php echo Html::button('Generate New Password', ['class' => 'btn btn-success', 'onclick' => '$.post("' . \yii\helpers\Url::to(["pengusulan-registrasi/random-str"]) . '", {}, function(data){ $("#updatepasswordviewmodel-kunci").val(data) })' ]); ?>
        <?php echo Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-primary']) ?>
        <?php echo Html::a(Yii::t('app', 'Cancel'), ['cancel', 'id' => $model->id], ['class' => 'btn btn-warning']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
