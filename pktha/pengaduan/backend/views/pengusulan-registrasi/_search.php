<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\PengusulanHutanAdatSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pengusulan-hutan-adat-search">

    <?php $form = ActiveForm::begin(
        [
        'action' => ['index'],
        'method' => 'get',
        ]
    ); ?>

    <?php echo $form->field($model, 'keyword') ?>

    <?php // echo $form->field($model, 'nama_pengusul') ?>

    <?php // echo $form->field($model, 'nama_pengusul') ?>

    <?php // echo $form->field($model, 'alamat_lengkap_pengusul') ?>

    <?php // echo $form->field($model, 'email_pengusul') ?>

    <?php // echo $form->field($model, 'no_telepon_pengusul') ?>

    <?php // echo $form->field($model, 'created_date') ?>

    <?php // echo $form->field($model, 'modified_date') ?>

    <?php // echo $form->field($model, 'id_wilayah') ?>

    <?php // echo $form->field($model, 'id_provinsi') ?>

    <?php // echo $form->field($model, 'id_kota_kabupaten') ?>

    <?php // echo $form->field($model, 'teks_kota_kabupaten') ?>

    <?php // echo $form->field($model, 'id_kecamatan') ?>

    <?php // echo $form->field($model, 'teks_kecamatan') ?>

    <?php // echo $form->field($model, 'id_desa_kelurahan') ?>

    <?php // echo $form->field($model, 'teks_desa_kelurahan') ?>

    <?php // echo $form->field($model, 'dusun') ?>

    <?php // echo $form->field($model, 'keterangan') ?>

    <?php // echo $form->field($model, 'form_filename') ?>

    <?php // echo $form->field($model, 'profil_mha_filename') ?>

    <?php // echo $form->field($model, 'surat_kuasa_filename') ?>

    <?php // echo $form->field($model, 'surat_pernyataan_filename') ?>

    <?php // echo $form->field($model, 'produk_hukum_daerah_filename') ?>

    <?php // echo $form->field($model, 'peta_filename') ?>

    <?php // echo $form->field($model, 'identitas_ketua_adat_filename') ?>

    <div class="form-group">
        <?php echo Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?php echo Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
