<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\Pengaduan */

$this->title = 'Import Pengusulan from Excel';
$this->params['breadcrumbs'][] = ['label' => 'Pengusulan', 'url' => ['pengusulan/index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box box-info">
    <!-- /.box-header -->
    <div class="pengaduan-create box-body">
        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>
        <?= $form->field($model, 'file')->fileInput() ?>
        <div class="form-group">
            <?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
