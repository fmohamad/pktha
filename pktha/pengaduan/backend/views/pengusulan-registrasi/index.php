<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\PengusulanHutanAdatSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Pengusulan Hutan Adat');
$this->params['breadcrumbs'][] = $this->title;
?>

<!-- create boxed title -->
<div class="pengusulan-hutan-adat-index">
    <div class="box box-info">
        <div class="box-header with-border">
            <!-- below is the title is the box -->
            <h3 class="box-title"><?php echo Yii::t('app', 'List Pengusulan') ?></h3>
            <div class="box-tools pull-left">
                <p>
                    <!-- export import buttons on the same line (and box) as boxed title -->
                    <?php echo Html::a(Yii::t('app', '<i class="fa fa-file-excel-o"></i> Download Excel Template'), ['download-excel'], ['class' => 'btn btn-warning']) ?>
                    <?php echo Html::a(Yii::t('app', 'Create Pengusulan'), ['create'], ['class' => 'btn btn-success']) ?>
                    <?php echo Html::a(Yii::t('app', '<i class="fa fa-file-excel-o"></i> Import from Excel'), ['import-excel'], ['class' => 'btn btn-success']) ?>
                    <?php echo Html::a(Yii::t('app', '<i class="fa fa-file-excel-o"></i> Export to Excel'), ['export-excel'], ['class' => 'btn btn-success']) ?>
                </p>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <?php echo $this->render('_search', ['model' => $searchModel]); ?>
            <?php Pjax::begin(); ?>
            <?php
            echo
            GridView::widget(
                    [
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],
                            'kode_pengusulan',
                            'nama_pengusul:ntext',
                            [
                                'attribute' => 'no_ktp_pengusul',
                                'label' => 'No KTP'
                            ],
                            [
                                'attribute' => 'no_telepon_pengusul',
                                'label' => 'No Telepon',
                                'value' => function ($model, $index, $widget) {
                                    if (isset($model->no_telepon_pengusul) && $model->no_telepon_pengusul > 0) {
                                        return '0' . $model->no_telepon_pengusul;
                                    }
                                }
                            ],
                            [
                                'attribute' => '',
                                'label' => 'Wilayah',
                                'value' => function ($model, $index, $widget) {
                                    if ($model->id_wilayah) {
                                        return common\models\Wilayah::findOne(['id' => $model->id_wilayah])->nama_wilayah;
                                    }
                                }
                            ],
                            [
                                'attribute' => '',
                                'label' => 'Status',
                                'format' => 'html',
                                'value' => function ($model, $index, $widget) {
                                    $tahap = $model->getTahapanFirstUnfinishedId();
                                    return Html::a($tahap['nama'], null, ['class' => 'label bg-' . $tahap['warna']]);
                                }
                            ],
                            [
                                'class' => 'yii\grid\ActionColumn',
                                'template' => '{view}{delete}{updatepassword}',
                                // 'contentOptions' => ['style' => 'min-width: 70px;'],
                                'buttons' => [
                                    // buttons for timeline function
                                    'view' => function ($url) {
                                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span> &nbsp;&nbsp;&nbsp;', $url, ['title' => 'Detail Pengaduan', 'data-pjax' => '0']);
                                    },
                                    'updatepassword' => function($url) {
                                        return Html::a('<span class="glyphicon glyphicon-qrcode"></span> &nbsp;&nbsp;&nbsp;', $url, ['title' => 'Update Password', 'data-pjax' => '0']);
                                    }
                                ],
                            ],
                        ]
                    ]
            );
            ?>
            <?php Pjax::end(); ?>
        </div>
    </div>
</div>

<style>
    tr#w2-filters {
        display: none;
    }
</style>
