<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$this->title = Yii::t('app', 'Pengusulan Created');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pengusulan Konflik'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>

<div>
    <form>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label">ID</label>
            <div class="col-sm-10">
                <?php echo $model->id_pengusulan ?>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Kode Pengusulan</label>
            <div class="col-sm-10">
                <?php echo $model->kode_pengusulan ?>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Kunci</label>
            <div class="col-sm-10">
                <?php echo $model->kunci_pengusulan ?>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label">
            </label>
            <div class="col-sm-10">
                <?php echo \yii\bootstrap\Html::a('Lihat Pengusulan', \yii\helpers\Url::to(['/pengusulan-registrasi/view', 'id' => $model->id_pengusulan]), ['class' => 'btn btn-primary'])?>
            </div>
        </div>
    </form>
</div>
