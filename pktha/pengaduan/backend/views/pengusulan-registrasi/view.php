<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\PengusulanHutanAdat */
/* @var $model_ii common\models\PengusulanHutanAdatII */
/* @var $model_iii common\models\PengusulanHutanAdatIII */
/* @var $model_iv common\models\PengusulanHutanAdatIV */
/* @var $model_v common\models\PengusulanHutanAdatV */

$this->title = Yii::t('app', 'Tahapan Pengusulan');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pengusulan Hutan Adat'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<style>
    .timeline-header .accordion-toggle:after {
        /* symbol for "opening" panels */
        font-family: 'FontAwesome';  /* essential for enabling glyphicon */
        content: "\f0d8";    /* adjust as needed, taken from bootstrap.css */
        float: center;        /* adjust as needed */
    }
    .timeline-header .accordion-toggle.collapsed:after {
        /* symbol for "collapsed" panels */
        content: "\f0d7";    /* adjust as needed, taken from bootstrap.css */
    }
    .box-header .accordion-toggle:after {
        /* symbol for "opening" panels */
        font-family: 'FontAwesome';  /* essential for enabling glyphicon */
        content: "\f0d8";    /* adjust as needed, taken from bootstrap.css */
        float: center;        /* adjust as needed */
    }
    .box-header .accordion-toggle.collapsed:after {
        /* symbol for "collapsed" panels */
        content: "\f0d7";    /* adjust as needed, taken from bootstrap.css */
    }</style>

<div class="pengusulan-moneview">
    <div class="box box-info">
        <div class="pengaduan-registrasi-view box-body">

            <!-- TITLE: ID row  in red box-->
            <ul class="timeline">
                <li class="time-label">
                    <span class="bg-blue-active">
                        Kode Pengusulan : <?php echo $model['model']->kode_pengusulan; ?>
                    </span>
                </li>

                <!-- collapsible panel -->
                <!-- I REGISTRASI -->
                <li>
                    <?php
                        $permohonanModel = \common\models\PengusulanPermohonan::findOne(['id_pengusulan' => $model['model']->id]);
                    ?>
                    <?php if (isset($permohonanModel) && $permohonanModel->finished) : ?>
                        <?php echo '<i class="fa fa-check bg-' . $model['warna'] . '-active"></i>'; ?>
                    <?php else : ?>
                        <i class="fa fa-ellipsis-h"></i>
                    <?php endif; ?>
                    <div class="timeline-item">
                        <span class="time pull-right">
                            <?php
                            $url = yii\helpers\Url::to(["pengusulan-registrasi/update", 'id' => $model['model']->id]);
                            echo Html::a('<i class="fa fa-pencil-square-o" style="font-size:20px"></i>', $url, ['title' => 'Update data']);
                            ?>
                        </span>
                        <h3 class="timeline-header"><b>Registrasi & Permohonan</b><a class="accordion-toggle" data-toggle="collapse" href="#collapse1"></a></h3>
                        <div id="collapse1" class="panel-collapse collapse">
                            <div class="box-body">
                                <h5 class="box-header no-padding">
                                    <b><i class="fa fa-user"></i> Identitas Pengusul</b>
                                    <a class="accordion-toggle" data-toggle="collapse" href="#collapseUser"></a>
                                </h5>
                                <div id="collapseUser" class="panel-collapse collapse">
                                    <div class="col-sm-12">
                                        <div style='border-bottom:1px solid #ccc;'></div>
                                    </div><br>
                                    <dl class="dl-horizontal">
                                        <dt>Nama Pengusul</dt>
                                        <dd><?php echo $model['model']->nama_pengusul ?></dd>
                                        <dt>Kode Pengusulan</dt>
                                        <dd><?php echo $model['model']->kode_pengusulan ?></dd>
                                        <dt>No. Telepon</dt>
                                        <dd><?php echo $model['model']->no_telepon_pengusul ?></dd>
                                        <dt>Alamat</dt>
                                        <dd><?php echo $model['model']->alamat_lengkap_pengusul ?></dd>
                                        <dt>Email</dt>
                                        <dd><?php echo $model['model']->email_pengusul ?></dd>
                                    </dl>
                                </div>
                                <h5 class="box-header no-padding">
                                    <b><i class="fa fa-user"></i>Lokasi Usulan</b>
                                    <a class="accordion-toggle" data-toggle="collapse" href="#collapseUsulan"></a>
                                </h5>
                                <div id="collapseUsulan" class="panel-collapse collapse">
                                    <div class="col-sm-12">
                                        <div style='border-bottom:1px solid #ccc;'></div>
                                    </div><br>
                                    <dl class="dl-horizontal">
                                        <dt>Wilayah</dt>
                                        <dd>
                                            <?php if (isset($model['model']->id_wilayah) && $model['model']->id_wilayah > 0) : ?>
                                                <?php echo \common\models\Wilayah::findOne(['id' => $model['model']->id_wilayah])->nama_wilayah ?>
                                            <?php endif ?>
                                        </dd>
                                        <dt>Provinsi</dt>
                                        <dd>
                                            <?php if (isset($model['model']->id_provinsi) && $model['model']->id_provinsi > 0) : ?>
                                                <?php echo \common\models\Provinsi::findOne(['id' => $model['model']->id_provinsi])->nama_provinsi ?>
                                            <?php endif ?>
                                        </dd>
                                        <dt>Kota / Kabupaten</dt>
                                        <dd>
                                            <?php if (isset($model['model']->id_kota_kabupaten) && $model['model']->id_kota_kabupaten > 0) : ?>
                                                <?php echo \common\models\KotaKabupaten::findOne(['id' => $model['model']->id_kota_kabupaten])->nama_kota_kabupaten ?>
                                            <?php else : ?>
                                                <?php echo $model['model']->teks_kota_kabupaten ?>
                                            <?php endif ?>
                                        </dd>
                                        <dt>Kecamatan</dt>
                                        <dd>
                                            <?php if (isset($model['model']->id_kecamatan) && $model['model']->id_kecamatan > 0) : ?>
                                                <?php echo \common\models\Kecamatan::findOne(['id' => $model['model']->id_kecamatan])->nama_kecamatan ?>
                                            <?php else : ?>
                                                <?php echo $model['model']->teks_kecamatan ?>
                                            <?php endif ?>
                                        </dd>
                                        <dt>Desa / Kelurahan</dt>
                                        <dd>
                                            <?php if (isset($model['model']->id_desa_kelurahan) && $model['model']->id_desa_kelurahan > 0) : ?>
                                                <?php echo \common\models\DesaKelurahan::findOne(['id' => $model['model']->id_desa_kelurahan])->nama_desa_kelurahan ?>
                                            <?php else : ?>
                                                <?php echo $model['model']->teks_desa_kelurahan ?>
                                            <?php endif ?>
                                        </dd>
                                        <dt>Dusun</dt>
                                        <dd><?php echo $model['model']->dusun ?></dd>
                                        <dt>Keterangan Lain</dt>
                                        <dd><?php echo $model['model']->keterangan ?></dd>
                                    </dl>
                                </div>
                                <h5 class="box-header no-padding">
                                    <b><i class="fa fa-user"></i>Berkas / Lampiran</b>
                                    <a class="accordion-toggle" data-toggle="collapse" href="#collapseBerkas"></a>
                                </h5>
                                <div id="collapseBerkas" class="panel-collapse collapse">
                                    <div class="col-sm-12">
                                        <div style='border-bottom:1px solid #ccc;'></div>
                                    </div><br>
                                    <dl class="dl-horizontal">
                                        <dt>Formulir Pengusulan</dt>
                                        <dd>
                                            <?php if ($model['model']->form_filename) : ?>
                                                <?php echo Html::a('Download', ['download', 'id' => $model['model']->id, 'file' => 'form']) ?>
                                            <?php else : ?>
                                                <i>Tidak Tersedia</i>
                                            <?php endif ?>
                                        </dd>
                                        <dt>Profil Masyarakat Hutan Adat</dt>
                                        <dd>
                                            <?php if ($model['model']->profil_mha_filename) : ?>
                                                <?php echo Html::a('Download', ['download', 'id' => $model['model']->id, 'file' => 'profil_mha']) ?>
                                            <?php else : ?>
                                                <i>Tidak Tersedia</i>
                                            <?php endif ?>
                                        </dd>
                                        <dt>Produk Hukum Daerah</dt>
                                        <dd>
                                            <?php if ($model['model']->produk_hukum_daerah_filename) : ?>
                                                <?php echo Html::a('Download', ['download', 'id' => $model['model']->id, 'file' => 'produk_hukum_daerah']) ?>
                                            <?php else : ?>
                                                <i>Tidak Tersedia</i>
                                            <?php endif ?>
                                        </dd>
                                        <dt>Surat Kuasa</dt>
                                        <dd>
                                            <?php if ($model['model']->surat_kuasa_filename) : ?>
                                                <?php echo Html::a('Download', ['download', 'id' => $model['model']->id, 'file' => 'surat_kuasa']) ?>
                                            <?php else : ?>
                                                <i>Tidak Tersedia</i>
                                            <?php endif ?>
                                        </dd>
                                        <dt>Surat Pernyataan</dt>
                                        <dd>
                                            <?php if ($model['model']->surat_pernyataan_filename) : ?>
                                                <?php echo Html::a('Download', ['download', 'id' => $model['model']->id, 'file' => 'surat_pernyataan']) ?>
                                            <?php else : ?>
                                                <i>Tidak Tersedia</i>
                                            <?php endif ?>
                                        </dd>
                                        <dt>Peta</dt>
                                        <dd>
                                            <?php if ($model['model']->peta_filename) : ?>
                                                <?php echo Html::a('Download', ['download', 'id' => $model['model']->id, 'file' => 'peta']) ?>
                                            <?php else : ?>
                                                <i>Tidak Tersedia</i>
                                            <?php endif ?>
                                        </dd>
                                        <dt>Identitas ketua Adat</dt>
                                        <dd>
                                            <?php if ($model['model']->identitas_ketua_adat_filename) : ?>
                                                <?php echo Html::a('Download', ['download', 'id' => $model['model']->id, 'file' => 'identitas_ketua_adat']) ?>
                                            <?php else : ?>
                                                <i>Tidak Tersedia</i>
                                            <?php endif ?>
                                        </dd>
                                    </dl>
                                </div>
                            </div>
                        </div>
                        <span class="timeline-footer pull-right" style="color:#999">
                            <i class="fa fa-clock-o"></i> <?php echo $model['model']->modified_date; ?>
                        </span>
                    </div>
                </li>

                <!-- II VALIDASI -->
                <li>
                    <?php if (!isset($model_ii)) : ?>
                        <i class="fa fa-ellipsis-h"></i>
                        <div class="timeline-item">
                            <span class="time pull-right">
                                <?php
                                $url = yii\helpers\Url::to(["pengusulan-validasi/create", 'id_pengusulan' => $model['model']->id]);
                                // echo Html::a('<i class="fa fa-plus-square-o" style="font-size:20px"></i>', $url, ['title' => 'Create Model']);
                                ?>
                            </span>
                            <h3 class="timeline-header"><b>Validasi</b><a class="accordion-toggle" data-toggle="collapse" href="#collapseValidasi"></a></h3>
                        </div>
                    <?php else : ?>
                        <?php if ($model_ii['model']->finished) : ?>
                            <?php echo '<i class="fa fa-check bg-' . $model_ii['warna'] . '-active"></i>'; ?>
                        <?php else : ?>
                            <i class="fa fa-ellipsis-h"></i>
                        <?php endif; ?>
                        <div class="timeline-item">
                            <span class="time pull-right">
                                <?php
                                $url = yii\helpers\Url::to(["pengusulan-validasi/update", 'id' => $model_ii['model']->id]);
                                echo Html::a('<i class="fa fa-pencil-square-o" style="font-size:20px"></i>', $url, ['title' => 'Update data']);
                                ?>
                            </span>
                            <h3 class="timeline-header"><b>Validasi</b><a class="accordion-toggle" data-toggle="collapse" href="#collapseValidasi"></a></h3>
                            <div id="collapseValidasi" class="panel-collapse collapse">
                                <div class="box-body">
                                    <h5 class="box-header no-padding">
                                        <b><i class="fa fa-user"></i> Data Pemohon</b>
                                        <a class="accordion-toggle" data-toggle="collapse" href="#collapseValidasiDataPemohon"></a>
                                    </h5>
                                    <div id="collapseValidasiDataPemohon" class="panel-collapse collapse">
                                        <div class="col-sm-12">
                                            <div style='border-bottom:1px solid #ccc;'></div>
                                        </div><br>
                                        <dl class="dl-horizontal">
                                            <dt>Nama Pemohon</dt>
                                            <dd><?php echo $model_ii['model']->nama_pemohon ?></dd>
                                            <dt>KTP Pemohon</dt>
                                            <dd><?php echo $model_ii['model']->no_ktp_pemohon ?></dd>
                                            <dt>Alamat Pemohon</dt>
                                            <dd><?php echo $model_ii['model']->alamat_pemohon ?></dd>
                                            <dt>Jabatan Pemohon (Khusus Badan Hukum dan MHA)</dt>
                                            <dd><?php echo $model_ii['model']->jabatan_pemohon ?></dd>
                                            <dt>Nama MHA (Khusus MHA)</dt>
                                            <dd><?php echo $model_ii['model']->nama_mha ?></dd>
                                        </dl>
                                    </div>
                                    <h5 class="box-header no-padding">
                                        <b><i class="fa fa-user"></i> Letak dan Luas Hutan HAK</b>
                                        <a class="accordion-toggle" data-toggle="collapse" href="#collapseValidasiLetakLuasHutan"></a>
                                    </h5>
                                    <div id="collapseValidasiLetakLuasHutan" class="panel-collapse collapse">
                                        <div class="col-sm-12">
                                            <div style='border-bottom:1px solid #ccc;'></div>
                                        </div><br>
                                        <dl class="dl-horizontal">
                                            <dt>Desa / Kampung</dt>
                                            <dd><?php echo $model_ii['model']->desa_kelurahan ?></dd>
                                            <dt>Kecamatan</dt>
                                            <dd><?php echo $model_ii['model']->kecamatan ?></dd>
                                            <dt>Kabupaten</dt>
                                            <dd><?php echo $model_ii['model']->kota_kabupaten ?></dd>
                                            <dt>Provinsi</dt>
                                            <dd><?php echo $model_ii['model']->provinsi ?></dd>
                                            <dt>DAS</dt>
                                            <dd><?php echo $model_ii['model']->das ?></dd>
                                            <dt>Luas</dt>
                                            <dd><?php echo $model_ii['model']->luas ?></dd>
                                        </dl>
                                    </div>
                                    <h5 class="box-header no-padding">
                                        <b><i class="fa fa-user"></i> Penilaian Kelengkapan Dokumen Subjek Hutan HAK (Pemohon)</b>
                                        <a class="accordion-toggle" data-toggle="collapse" href="#collapseValidasiKelengkapanDokumenSubjekHutanHak"></a>
                                    </h5>
                                    <div id="collapseValidasiKelengkapanDokumenSubjekHutanHak" class="panel-collapse collapse">
                                        <div class="col-sm-12">
                                            <div style='border-bottom:1px solid #ccc;'></div>
                                        </div><br>
                                        <dl class="dl-horizontal">
                                            <dt>Data Subjek Hutan Hak</dt>
                                            <dd>
                                                <div class="row">
                                                    <div class="col-md-2"><?php echo $model_ii['model']->data_subjek_hutan_hak ? 'Ada' : 'Tidak' ?></div>
                                                    <div class="col-md-2"><?php echo $model_ii['model']->kelengkapan_data_subjek_hutan_hak ? 'Valid' : 'Tidak Valid' ?></div>
                                                </div>
                                            </dd>
                                            <dt>KTP Pemohon</dt>
                                            <dd><?php echo $model_ii['model']->ktp_pemohon > 0 ? 'Ada' : 'Tidak' ?></dd>
                                            <dt>Akte Perusahaan</dt>
                                            <dd>
                                                <div class="row">
                                                    <div class="col-md-2"><?php echo $model_ii['model']->akte_perusahaan ? 'Ada' : 'Tidak' ?></div>
                                                    <div class="col-md-2"><?php echo $model_ii['model']->validitas_akte_perusahaan ? 'Valid' : 'Tidak Valid' ?></div>
                                                </div>
                                            </dd>
                                            <dt>Status Pengakuan (khusus MHA)</dt>
                                            <dd><?php echo $model_ii['model']->status_pengakuan ? 'Diakui' : 'Tidak Diakui' ?></dd>
                                            <dt>Copy Perda/SK Bupati Pengakuan MHA</dt>
                                            <dd><?php echo $model_ii['model']->perda_pengakuan_mha ? 'Ada' : 'Tidak Ada' ?></dd>
                                        </dl>
                                    </div>
                                    <h5 class="box-header no-padding">
                                        <b><i class="fa fa-user"></i> Penilaian Kelengkapan Dokumen Objek Hutan HAK (Pemohon)</b>
                                        <a class="accordion-toggle" data-toggle="collapse" href="#collapseValidasiKelengkapanDokumenObjekHutanHak"></a>
                                    </h5>
                                    <div id="collapseValidasiKelengkapanDokumenObjekHutanHak" class="panel-collapse collapse">
                                        <div class="col-sm-12">
                                            <div style='border-bottom:1px solid #ccc;'></div>
                                        </div><br>
                                        <dl class="dl-horizontal">
                                            <dt>Data Objek Hutan Hak</dt>
                                            <dd>
                                                <div class="row">
                                                    <div class="col-md-2"><?php echo $model_ii['model']->data_objek_hutan_hak ? 'Ada' : 'Tidak' ?></div>
                                                    <div class="col-md-2"><?php echo $model_ii['model']->kelengkapan_data_objek_hutan_hak ? 'Valid' : 'Tidak Valid' ?></div>
                                                </div>
                                            </dd>
                                            <dt>Bukti Kepemilikan</dt>
                                            <dd><?php echo $model_ii['model']->bukti_kepemilikan ? 'Ada' : 'Tidak' ?></dd>
                                            <dt>Perda/SK Bupati wilayah adat/hutan adat (khusus MHA)</dt>
                                            <dd><?php echo $model_ii['model']->perda_wilayah_adat ? 'Ada' : 'Tidak' ?></dd>
                                            <dt>Peta Hutan HAK</dt>
                                            <dd><?php echo $model_ii['model']->peta_hutan_hak ? 'Ada' : 'Tidak Ada' ?></dd>
                                            <dt>Peta Hutan Adat ditandatangani oleh Kepala Daerah</dt>
                                            <dd><?php echo $model_ii['model']->peta_hutan_adat ? 'Ada' : 'Tidak Ada' ?></dd>
                                        </dl>
                                    </div>
                                    <h5 class="box-header no-padding">
                                        <b><i class="fa fa-user"></i> Catatan</b>
                                        <a class="accordion-toggle" data-toggle="collapse" href="#collapseValidasiCatatan"></a>
                                    </h5>
                                    <div id="collapseValidasiCatatan" class="panel-collapse collapse">
                                        <div class="col-sm-12">
                                            <div style='border-bottom:1px solid #ccc;'></div>
                                        </div><br>
                                        <dl class="dl-horizontal">
                                            <dt>Catatan</dt>
                                            <dd><?php echo $model_ii['model']->catatan ?></dd>
                                        </dl>
                                    </div>
                                    <h5 class="box-header no-padding">
                                        <b><i class="fa fa-user"></i> Kesimpulan hasil Penilaian Dokumen Permohonan Hutan Hak </b>
                                        <a class="accordion-toggle" data-toggle="collapse" href="#collapseValidasiSimpulan"></a>
                                    </h5>
                                    <div id="collapseValidasiSimpulan" class="panel-collapse collapse">
                                        <div class="col-sm-12">
                                            <div style='border-bottom:1px solid #ccc;'></div>
                                        </div><br>
                                        <dl class="dl-horizontal">
                                            <dt>Simpulan</dt>
                                            <dd>
                                                <?php if ($model_ii['model']->dikembalikan) : ?>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <input disabled="true" type="checkbox" checked="true" title="Dokumen permohonan hutan hak dapat dilanjutkan untuk proses verifikasi dan validasi." />Dokumen permohonan hutan hak dapat dilanjutkan untuk proses verifikasi dan validasi
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <input disabled="true" type="checkbox" value="" title="Dokumen permohonan hutan hak dikembalikan untuk dilengkapi." />Dokumen permohonan hutan hak dikembalikan untuk dilengkapi.
                                                        </div>
                                                    </div>
                                                <?php else : ?>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <input disabled="true" type="checkbox" title="Dokumen permohonan hutan hak dapat dilanjutkan untuk proses verifikasi dan validasi." />Dokumen permohonan hutan hak dapat dilanjutkan untuk proses verifikasi dan validasi
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <input disabled="true" type="checkbox" checked="true"  value="" title="Dokumen permohonan hutan hak dikembalikan untuk dilengkapi." />Dokumen permohonan hutan hak dikembalikan untuk dilengkapi.
                                                        </div>
                                                    </div>
                                                <?php endif ?>
                                            </dd>
                                        </dl>
                                    </div>
                                </div>
                            </div>
                            <span class="timeline-footer pull-right" style="color:#999">
                                <i class="fa fa-clock-o"></i> <?php echo $model_ii['model']->modified_date; ?>
                            </span>
                        </div>
                    <?php endif ?>
                </li>
                <!-- END -->

                <!-- III VERIFIKASI -->
                <li>
                    <?php if (!isset($model_iii)) : ?>
                        <i class="fa fa-ellipsis-h"></i>
                        <div class="timeline-item">
                            <span class="time pull-right">
                                <?php
                                $url = yii\helpers\Url::to(["pengusulan-verifikasi/create", 'id_pengusulan' => $model['model']->id]);
                                // echo Html::a('<i class="fa fa-plus-square-o" style="font-size:20px"></i>', $url, ['title' => 'Create Model']);
                                ?>
                            </span>
                            <h3 class="timeline-header"><b>Verifikasi</b><a class="accordion-toggle" data-toggle="collapse" href="#collapse5"></a></h3>
                        </div>
                    <?php else : ?>
                        <?php if ($model_iii['model']->finished) : ?>
                            <?php echo '<i class="fa fa-check bg-' . $model_iii['warna'] . '-active"></i>'; ?>
                        <?php else : ?>
                            <i class="fa fa-ellipsis-h"></i>
                        <?php endif; ?>
                        <div class="timeline-item">
                            <span class="time pull-right">
                                <?php
                                $url = yii\helpers\Url::to(["pengusulan-verifikasi/update", 'id' => $model_iii['model']->id]);
                                echo Html::a('<i class="fa fa-pencil-square-o" style="font-size:20px"></i>', $url, ['title' => 'Update data']);
                                ?>
                            </span>
                            <h3 class="timeline-header"><b>Verifikasi</b><a class="accordion-toggle" data-toggle="collapse" href="#collapseVerifikasi"></a></h3>
                            <div id="collapseVerifikasi" class="panel-collapse collapse">
                                <div class="box-body">
                                    <h5 class="box-header no-padding">
                                        <b><i class="fa fa-user"></i> Data Pemohon</b>
                                        <a class="accordion-toggle" data-toggle="collapse" href="#collapseVerifikasiDataPemohon"></a>
                                    </h5>
                                    <div id="collapseVerifikasiDataPemohon" class="panel-collapse collapse">
                                        <div class="col-sm-12">
                                            <div style='border-bottom:1px solid #ccc;'></div>
                                        </div><br>
                                        <dl class="dl-horizontal">
                                            <dt>Nama Pemohon</dt>
                                            <dd><?php echo $model_iii['model']->nama_pemohon ?></dd>
                                            <dt>KTP Pemohon</dt>
                                            <dd><?php echo $model_iii['model']->no_ktp_pemohon ?></dd>
                                            <dt>Alamat Pemohon</dt>
                                            <dd><?php echo $model_iii['model']->alamat_pemohon ?></dd>
                                            <dt>Jabatan (Khusus Badan Hukum dan MHA)</dt>
                                            <dd><?php echo $model_iii['model']->jabatan_pemohon ?></dd>
                                            <dt>Nama MHA (khusus MHA)</dt>
                                            <dd><?php echo $model_iii['model']->nama_mha ?></dd>
                                        </dl>
                                    </div>
                                    <h5 class="box-header no-padding">
                                        <b><i class="fa fa-user"></i> Letak dan Luas Hutan HAK</b>
                                        <a class="accordion-toggle" data-toggle="collapse" href="#collapseVerifikasiLetakLuasHutanHak"></a>
                                    </h5>
                                    <div id="collapseVerifikasiLetakLuasHutanHak" class="panel-collapse collapse">
                                        <div class="col-sm-12">
                                            <div style='border-bottom:1px solid #ccc;'></div>
                                        </div><br>
                                        <dl class="dl-horizontal">
                                            <dt>Desa / Kelurahan</dt>
                                            <dd><?php echo $model_iii['model']->desa_kelurahan ?></dd>
                                            <dt>Kecamatan</dt>
                                            <dd><?php echo $model_iii['model']->kecamatan ?></dd>
                                            <dt>Kota / Kabupaten</dt>
                                            <dd><?php echo $model_iii['model']->kota_kabupaten ?></dd>
                                            <dt>Provinsi</dt>
                                            <dd><?php echo $model_iii['model']->provinsi ?></dd>
                                            <dt>DAS</dt>
                                            <dd><?php echo $model_iii['model']->das ?></dd>
                                            <dt>Luas</dt>
                                            <dd><?php echo $model_iii['model']->luas ?></dd>
                                        </dl>
                                    </div>
                                    <h5 class="box-header no-padding">
                                        <b><i class="fa fa-user"></i> Hasil Verifikasi untuk Permohonan Penetapan Hutan Hak Perseorangan/ Badan Hukum</b>
                                        <a class="accordion-toggle" data-toggle="collapse" href="#collapseVerifikasiHasilPermohonanHutanHak"></a>
                                    </h5>
                                    <div id="collapseVerifikasiHasilPermohonanHutanHak" class="panel-collapse collapse">
                                        <div class="col-sm-12">
                                            <div style='border-bottom:1px solid #ccc;'></div>
                                        </div><br>
                                        <dl class="dl-horizontal">
                                            <dt>Terdapat hak atas tanah yang dimiliki oleh perseorangan/badan hukum/masyarakat hukum adat </dt>
                                            <dd>
                                                <div class="row">
                                                    <div class="col-md-3"><?php echo $model_iii['model']->hak_atas_tanah ? 'Ada' : 'Tidak Ada' ?></div>
                                                    <div class="col-md-3"><?php echo $model_iii['model']->validitas_hak_atas_tanah ? 'Valid' : 'Tidak Valid' ?></div>
                                                </div>
                                            </dd>
                                            <dt>Terdapat tanah yang sebagian atau seluruhnya berupa hutan</dt>
                                            <dd>
                                                <div class="row">
                                                    <div class="col-md-3"><?php echo $model_iii['model']->tanah_berupa_hutan ? 'Ada' : 'Tidak Ada' ?></div>
                                                    <div class="col-md-3"><?php echo $model_iii['model']->validitas_tanah_berupa_hutan ? 'Valid' : 'Tidak Valid' ?></div>
                                                </div>
                                            </dd>
                                            <dt>Surat pernyataan dari perseorangan/badan hukum untuk menetapkan tanahnya sebagai hutan hak </dt>
                                            <dd>
                                                <div class="row">
                                                    <div class="col-md-3"><?php echo $model_iii['model']->surat_pernyataan_penetapan_hutan_hak ? 'Ada' : 'Tidak Ada' ?></div>
                                                    <div class="col-md-3"><?php echo $model_iii['model']->validitas_surat_pernyataan_penetapan_hutan_hak ? 'Valid' : 'Tidak Valid' ?></div>
                                                </div>
                                            </dd>
                                        </dl>
                                    </div>
                                    <h5 class="box-header no-padding">
                                        <b><i class="fa fa-user"></i> Hasil Verifikasi untuk Permohonan Penetapan Hutan Adat</b>
                                        <a class="accordion-toggle" data-toggle="collapse" href="#collapseVerifikasiHasilPermohonanHutanAdat"></a>
                                    </h5>
                                    <div id="collapseVerifikasiHasilPermohonanHutanAdat" class="panel-collapse collapse">
                                        <div class="col-sm-12">
                                            <div style='border-bottom:1px solid #ccc;'></div>
                                        </div><br>
                                        <dl class="dl-horizontal">
                                            <dt>Terdapat masyarakat hukum adat atau hak ulayat yang telah diakui oleh Pemerintah Daerah melalui produk hukum daerah</dt>
                                            <dd>
                                                <div class="row">
                                                    <div class="col-md-3"><?php echo $model_iii['model']->masyarakat_adat_diakui_pemda ? 'Ada' : 'Tidak Ada' ?></div>
                                                    <div class="col-md-3"><?php echo $model_iii['model']->validitas_masyarakat_adat_diakui_pemda ? 'Valid' : 'Tidak Valid' ?></div>
                                                </div>
                                            </dd>
                                            <dt>Terdapat wilayah adat yang sebagian atau seluruhnya berupa hutan</dt>
                                            <dd>
                                                <div class="row">
                                                    <div class="col-md-3"><?php echo $model_iii['model']->wilayah_adat_berupa_hutan ? 'Ada' : 'Tidak Ada' ?></div>
                                                    <div class="col-md-3"><?php echo $model_iii['model']->validitas_wilayah_adat_berupa_hutan ? 'Valid' : 'Tidak Valid' ?></div>
                                                </div>
                                            </dd>
                                            <dt>Surat pernyataan dari ketua adat untuk menetapkan tanahnya sebagai hutan hak </dt>
                                            <dd>
                                                <div class="row">
                                                    <div class="col-md-3"><?php echo $model_iii['model']->surat_pernyataan_ketua_adat ? 'Ada' : 'Tidak Ada' ?></div>
                                                    <div class="col-md-3"><?php echo $model_iii['model']->validitas_surat_pernyataan_ketua_adat ? 'Valid' : 'Tidak Valid' ?></div>
                                                </div>
                                            </dd>
                                        </dl>
                                    </div>
                                    <h5 class="box-header no-padding">
                                        <b><i class="fa fa-user"></i> Catatan</b>
                                        <a class="accordion-toggle" data-toggle="collapse" href="#collapseVerifikasiCatatan"></a>
                                    </h5>
                                    <div id="collapseVerifikasiCatatan" class="panel-collapse collapse">
                                        <div class="col-sm-12">
                                            <div style='border-bottom:1px solid #ccc;'></div>
                                        </div><br>
                                        <dl class="dl-horizontal">
                                            <dt>Terdapat masyarakat hukum adat atau hak ulayat yang telah diakui oleh Pemerintah Daerah melalui produk hukum daerah</dt>
                                            <dd><?php echo $model_iii['model']->catatan ?></dd>
                                        </dl>
                                    </div>
                                    <h5 class="box-header no-padding">
                                        <b><i class="fa fa-user"></i> Simpulan hasil verifikasi</b>
                                        <a class="accordion-toggle" data-toggle="collapse" href="#collapseVerifikasiSimpulan"></a>
                                    </h5>
                                    <div id="collapseVerifikasiSimpulan" class="panel-collapse collapse">
                                        <div class="col-sm-12">
                                            <div style='border-bottom:1px solid #ccc;'></div>
                                        </div><br>
                                        <dl class="dl-horizontal">
                                            <dt>Simpulan</dt>
                                            <dd>
                                                <?php if ($model_iii['model']->validasi_simpulan_hasil_verifikasi) : ?>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <input disabled="true" type="checkbox" checked="true" title="Valid" />Valid
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <input disabled="true" type="checkbox" value="" title="" />Tidak Valid
                                                        </div>
                                                    </div>
                                                <?php else : ?>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <input disabled="true" type="checkbox" />Valid
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <input disabled="true" type="checkbox" checked="true" title="Tidak Valid" />Tidak Valid
                                                        </div>
                                                    </div>
                                                <?php endif ?>
                                            </dd>
                                            <dt>Berita Acara</dt>
                                            <dd>
                                                <?php if ($model_iii['model']->berita_acara_filename) : ?>
                                                    <?php echo Html::a($model_iii['model']->berita_acara_filename, ['pengusulan-verifikasi/download', 'id' => $model_iii['model']->id]) ?>
                                                <?php else : ?>
                                                    <i>Tidak Tersedia</i>
                                                <?php endif ?>
                                            </dd>
                                        </dl>
                                    </div>
                                </div>
                            </div>
                            <span class="timeline-footer pull-right" style="color:#999">
                                <i class="fa fa-clock-o"></i> <?php echo $model_iii['model']->modified_date; ?>
                            </span>
                        </div>
                    <?php endif ?>
                </li>
                <!-- END -->

                <!-- IV SK -->
                <li>
                    <?php if (!isset($model_iv)) : ?>
                        <i class="fa fa-ellipsis-h"></i>
                        <div class="timeline-item">
                            <span class="time pull-right">
                                <?php
                                $url = yii\helpers\Url::to(["pengusulan-pencantuman/create", 'id_pengusulan' => $model['model']->id]);
                                // echo Html::a('<i class="fa fa-plus-square-o" style="font-size:20px"></i>', $url, ['title' => 'Create Model']);
                                ?>
                            </span>
                            <h3 class="timeline-header"><b>Pencantuman dan Penetapan</b><a class="accordion-toggle" data-toggle="collapse" href="#collapse5"></a></h3>
                        </div>
                    <?php else : ?>
                        <?php if ($model_iv['model']->finished) : ?>
                            <?php echo '<i class="fa fa-check bg-' . $model_iv['warna'] . '-active"></i>'; ?>
                        <?php else : ?>
                            <i class="fa fa-ellipsis-h"></i>
                        <?php endif; ?>
                        <div class="timeline-item">
                            <span class="time pull-right">
                                <?php
                                $url = yii\helpers\Url::to(["pengusulan-pencantuman/update", 'id' => $model_iv['model']->id]);
                                echo Html::a('<i class="fa fa-pencil-square-o" style="font-size:20px"></i>', $url, ['title' => 'Update data']);
                                ?>
                            </span>
                            <h3 class="timeline-header"><b>Pencantuman dan Penetapan</b><a class="accordion-toggle" data-toggle="collapse" href="#collapsePenetapanPencantuman"></a></h3>
                            <div id="collapsePenetapanPencantuman" class="panel-collapse collapse">
                                <div class="box-body">
                                    <h5 class="box-header no-padding">
                                        <b><i class="fa fa-user"></i>Penetapan</b>
                                        <a class="accordion-toggle" data-toggle="collapse" href="#collapsePenetapan"></a>
                                    </h5>
                                    <div id="collapsePenetapan" class="panel-collapse collapse">
                                        <div class="col-sm-12">
                                            <div style='border-bottom:1px solid #ccc;'></div>
                                        </div><br>
                                        <dl class="dl-horizontal">
                                            <dt>No SK Penetapan</dt>
                                            <dd><?php echo $model_iv['model']->no_sk_penetapan ?></dd>
                                            <dt>Tanggal SK Penetapan</dt>
                                            <dd><?php echo $model_iv['model']->tanggal_sk_penetapan ?></dd>
                                            <dt>Perihal SK Penetapan</dt>
                                            <dd><?php echo $model_iv['model']->perihal_sk_penetapan ?></dd>
                                            <dt>Luas SK Penetapan</dt>
                                            <dd><?php echo $model_iv['model']->luas_sk_penetapan ?></dd>
                                        </dl>
                                    </div>
                                    <h5 class="box-header no-padding">
                                        <b><i class="fa fa-user"></i>Pencantuman</b>
                                        <a class="accordion-toggle" data-toggle="collapse" href="#collapsePencantuman"></a>
                                    </h5>
                                    <div id="collapsePencantuman" class="panel-collapse collapse">
                                        <div class="col-sm-12">
                                            <div style='border-bottom:1px solid #ccc;'></div>
                                        </div><br>
                                        <dl class="dl-horizontal">
                                            <dt>No SK Percantuman</dt>
                                            <dd><?php echo $model_iv['model']->no_sk_pencantuman ?></dd>
                                            <dt>Tanggal SK Percantuman</dt>
                                            <dd><?php echo $model_iv['model']->tanggal_sk_pencantuman ?></dd>
                                            <dt>Perihal SK Percantuman</dt>
                                            <dd><?php echo $model_iv['model']->perihal_sk_pencantuman ?></dd>
                                            <dt>Luas SK Percantuman</dt>
                                            <dd><?php echo $model_iv['model']->luas_sk_pencantuman ?></dd>
                                            <dt>Form Verifikasi</dt>
                                            <dd>
                                                <?php if ($model_iv['model']->form_verifikasi_filename) : ?>
                                                    <?php echo Html::a($model_iv['model']->form_verifikasi_filename, ['pengusulan-pencantuman/download', 'id' => $model_iii['model']->id]) ?>
                                                <?php else : ?>
                                                    <i>Tidak Tersedia</i>
                                                <?php endif ?>
                                            </dd>
                                        </dl>
                                    </div>
                                </div>
                            </div>

                            <span class="timeline-footer pull-right" style="color:#999">
                                <i class="fa fa-clock-o"></i> <?php echo $model_iv['model']->modified_date ?>
                            </span>
                        </div>
                    <?php endif ?>
                </li>

                <!-- V MONEV -->
                <li>
                    <?php if (!isset($model_v)) : ?>
                        <i class="fa fa-ellipsis-h"></i>
                        <div class="timeline-item">
                            <span class="time pull-right">
                                <?php
                                $url = yii\helpers\Url::to(["pengusulan-monev/create", 'id_pengusulan' => $model['model']->id]);
                                // echo Html::a('<i class="fa fa-plus-square-o" style="font-size:20px"></i>', $url, ['title' => 'Create Model']);
                                ?>
                            </span>
                            <h3 class="timeline-header"><b>Monev dan Pemberdayaan</b><a class="accordion-toggle" data-toggle="collapse" href="#collapse5"></a></h3>
                        </div>
                    <?php else : ?>
                        <?php if ($model_v['model']->finished) : ?>
                            <?php echo '<i class="fa fa-check bg-' . $model_v['warna'] . '-active"></i>'; ?>
                        <?php else : ?>
                            <i class="fa fa-ellipsis-h"></i>
                        <?php endif; ?>
                        <div class="timeline-item">
                            <span class="time pull-right">
                                <?php
                                $url = yii\helpers\Url::to(["pengusulan-monev/update", 'id' => $model_v['model']->id]);
                                echo Html::a('<i class="fa fa-pencil-square-o" style="font-size:20px"></i>', $url, ['title' => 'Update data']);
                                ?>
                            </span>
                            <h3 class="timeline-header"><b>Monev dan Pemberdayaan</b><a class="accordion-toggle" data-toggle="collapse" href="#collapseMonevPerberdayaan"></a></h3>
                            <div id="collapseMonevPerberdayaan" class="panel-collapse collapse">
                                <div class="box-body">
                                    <div class="col-sm-12">
                                        <div style='border-bottom:1px solid #ccc;'></div>
                                    </div><br>
                                    <dl class="dl-horizontal">
                                        <dt>Perkembangan Hutan Adat</dt>
                                        <dd><?php echo $model_v['model']->perkembangan_hutan_adat ?></dd>
                                        <dt>Tanggal Monitoring dan Evaluasi</dt>
                                        <dd><?php echo $model_v['model']->tanggal_monitoring_dan_evaluasi ?></dd>
                                        <dt>Komoditi Hutan Adat</dt>
                                        <dd><?php echo $model_v['model']->komoditi_hutan_adat ?></dd>
                                        <dt>Komoditi Unggulan Hutan Adat</dt>
                                        <dd><?php echo $model_v['model']->komoditi_unggulan_hutan_adat ?></dd>
                                        <dt>Update Kelembagaan MHA</dt>
                                        <dd><?php echo $model_v['model']->update_kelembagaan_mha ?></dd>
                                    </dl>
                                </div>
                            </div>
                            <span class="timeline-footer pull-right" style="color:#999">
                                <i class="fa fa-clock-o"></i> <?php echo $model_v['model']->modified_date; ?>
                            </span>
                        </div>
                    <?php endif ?>
                </li>
            </ul>
        </div>
    </div>
</div>
