<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model \common\models\PengusulanValidasi */
/* @var $form ActiveForm */
?>

<style>
    .timeline-header .accordion-toggle:after {
        /* symbol for "opening" panels */
        font-family: 'FontAwesome';  /* essential for enabling glyphicon */
        content: "\f0d8";    /* adjust as needed, taken from bootstrap.css */
        float: center;        /* adjust as needed */
    }
    .timeline-header .accordion-toggle.collapsed:after {
        /* symbol for "collapsed" panels */
        content: "\f0d7";    /* adjust as needed, taken from bootstrap.css */
    }
    .box-header .accordion-toggle:after {
        /* symbol for "opening" panels */
        font-family: 'FontAwesome';  /* essential for enabling glyphicon */
        content: "\f0d8";    /* adjust as needed, taken from bootstrap.css */
        float: center;        /* adjust as needed */
    }
    .box-header .accordion-toggle.collapsed:after {
        /* symbol for "collapsed" panels */
        content: "\f0d7";    /* adjust as needed, taken from bootstrap.css */
    }
</style>

<div class="pengusulan-hutan-adat-i-i-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
    <div class="form-group field-pengusulanhutanadativ-tanggal_sk_penetapan">
        <label class="control-label">Kode Pengusulan</label>
        <input type="text" class="form-control" value="<?php echo \common\models\PengusulanRegistrasi::findOne(['id' => $model->id_pengusulan])->kode_pengusulan ?>" disabled="true">
        <div class="help-block"></div>
    </div>

    <!-- Data Pemohon -->
    <h5 class="timeline-header"><b>I. Data Pemohon</b><a class="accordion-toggle" data-toggle="collapse" href="#collapseDataPemohon"></a></h5>
    <div id="collapseDataPemohon" class="panel-collapse collapse">
        <div class="box-body">
            <div class="col-sm-12">
                <div style='border-bottom:1px solid #ccc;'></div>
            </div><br>
            <?php echo $form->field($model, 'nama_pemohon')->textInput() ?>
            <?php echo $form->field($model, 'no_ktp_pemohon')->textInput() ?>
            <?php echo $form->field($model, 'alamat_pemohon')->textarea(['rows' => 6]) ?>
            <?php echo $form->field($model, 'jabatan_pemohon')->textInput() ?>
            <?php echo $form->field($model, 'nama_mha')->textInput() ?>
        </div>
    </div>
    <!-- END -->

    <!-- Letak dan Luas Hutan HAK -->
    <h5 class="timeline-header"><b>II. Letak dan Luas Hutan HAK</b><a class="accordion-toggle" data-toggle="collapse" href="#collapseLetakHutan"></a></h5>
    <div id="collapseLetakHutan" class="panel-collapse collapse">
        <div class="box-body">
            <div class="col-sm-12">
                <div style='border-bottom:1px solid #ccc;'></div>
            </div><br>
            <?php echo $form->field($model, 'desa_kelurahan')->textInput() ?>
            <?php echo $form->field($model, 'kecamatan')->textInput() ?>
            <?php echo $form->field($model, 'kota_kabupaten')->textInput() ?>
            <?php echo $form->field($model, 'provinsi')->textInput() ?>
            <?php echo $form->field($model, 'das')->textInput() ?>
            <?php echo $form->field($model, 'luas')->textInput() ?>
            <?php echo $form->field($model, 'batas_utara')->textarea(['rows' => 6]) ?>
            <?php echo $form->field($model, 'batas_selatan')->textarea(['rows' => 6]) ?>
            <?php echo $form->field($model, 'batas_timur')->textarea(['rows' => 6]) ?>
            <?php echo $form->field($model, 'batas_barat')->textarea(['rows' => 6]) ?>
            <?php echo $form->field($model, 'bukti_penguasaan_tanah')->textarea(['rows' => 6]) ?>
            <?php echo $form->field($model, 'status_kawasan')->textarea(['rows' => 6]) ?>
        </div>
    </div>

    <!-- END -->

    <!-- Kelengkapan Dokumen Subjek Hutan HAK (Pemohon) -->

    <h5 class="timeline-header"><b>III. Kelengkapan Dokumen Subjek Hutan HAK (Pemohon)</b><a class="accordion-toggle" data-toggle="collapse" href="#collapseDokumenSubjekHutan"></a></h5>
    <div id="collapseDokumenSubjekHutan" class="panel-collapse collapse">
        <div class="box-body">
            <div class="col-sm-12">
                <div style='border-bottom:1px solid #ccc;'></div>
            </div><br>
            <div class="row">
                <div class="col-md-6">
                    <?php echo $form->field($model, 'data_subjek_hutan_hak')->dropDownList([true => 'Ada', false => 'Tidak']) ?>
                </div>
                <div class="col-md-6">
                    <?php echo $form->field($model, 'kelengkapan_data_subjek_hutan_hak')->dropDownList([true => 'Lengkap', false => 'Tidak']) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <?php echo $form->field($model, 'ktp_pemohon')->dropDownList([true => 'Lengkap', false => 'Tidak']) ?>
                </div>
                <div class="col-md-6">

                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <?php echo $form->field($model, 'akte_perusahaan')->dropDownList([true => 'Lengkap', false => 'Tidak']) ?>
                </div>
                <div class="col-md-6">
                    <?php echo $form->field($model, 'validitas_akte_perusahaan')->dropDownList([true => 'Lengkap', false => 'Tidak']) ?>
                </div>
            </div>
            <?php echo $form->field($model, 'status_pengakuan')->dropDownList([true => 'Diakui', false => 'Tidak']) ?>
            <?php echo $form->field($model, 'perda_pengakuan_mha')->dropDownList([true => 'Ada', false => 'Tidak']) ?>
        </div>
    </div>

    <!-- END -->

    <!-- Penilaian Kelengkapan Dokumen Objek Hutan Hak -->

    <h5 class="timeline-header"><b>IV. Penilaian Kelengkapan Dokumen Objek Hutan Hak</b><a class="accordion-toggle" data-toggle="collapse" href="#collapseDokumenObjekHutan"></a></h5>
    <div id="collapseDokumenObjekHutan" class="panel-collapse collapse">
        <div class="box-body">
            <div class="col-sm-12">
                <div style='border-bottom:1px solid #ccc;'></div>
            </div><br>
            <?php echo $form->field($model, 'data_objek_hutan_hak')->dropDownList([true => 'Ada', false => 'Tidak']) ?>
            <?php echo $form->field($model, 'kelengkapan_data_objek_hutan_hak')->dropDownList([true => 'Lengkap', false => 'Tidak']) ?>
            <?php echo $form->field($model, 'bukti_kepemilikan')->dropDownList([true => 'Ada', false => 'Tidak']) ?>
            <?php echo $form->field($model, 'perda_wilayah_adat')->dropDownList([true => 'Ada', false => 'Tidak']) ?>
            <?php echo $form->field($model, 'peta_hutan_hak')->dropDownList([true => 'Ada', false => 'Tidak']) ?>
            <?php echo $form->field($model, 'peta_hutan_adat')->dropDownList([true => 'Ya', false => 'Tidak']) ?>
        </div>
    </div>

    <!-- END -->

    <!-- Lain-lain -->

    <?php echo $form->field($model, 'catatan')->textarea(['rows' => 6]) ?>

    <!-- END -->

    <!-- Kesimpulan hasil Penilaian Dokumen Permohonan Hutan Hak  -->

    <div class="row">
        <div class="col-md-4">
            <?php echo
            $form->field($model, 'dikembalikan')->dropDownList(
                [
                true => 'Dokumen permohonan hutan hak dapat dilanjutkan untuk proses verifikasi dan validasi',
                false => 'Dokumen permohonan hutan hak dikembalikan untuk dilengkapi'
                ]
            )
            ?>
        </div>
    </div>

    <!-- END -->

    <?php echo $form->field($model, 'finished')->checkbox() ?>

    <!-- <?php echo $form->field($model, 'created_date')->textInput(['disabled' => true]) ?> -->
    <!-- <?php echo $form->field($model, 'modified_date')->textInput(['disabled' => true]) ?> -->


    <div class="form-group">
        <?php echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?php echo Html::a(Yii::t('app', 'Cancel'), ['cancel', 'id' => $model->id], ['class' => 'btn btn-warning']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div><!-- pengusulan-hutan-adat-i-i-_form -->
