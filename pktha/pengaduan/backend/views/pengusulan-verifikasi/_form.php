<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model \common\models\PengusulanRegistrasiIII */
/* @var $form ActiveForm */
?>


<style>
    .timeline-header .accordion-toggle:after {
        /* symbol for "opening" panels */
        font-family: 'FontAwesome';  /* essential for enabling glyphicon */
        content: "\f0d8";    /* adjust as needed, taken from bootstrap.css */
        float: center;        /* adjust as needed */
    }
    .timeline-header .accordion-toggle.collapsed:after {
        /* symbol for "collapsed" panels */
        content: "\f0d7";    /* adjust as needed, taken from bootstrap.css */
    }
    .box-header .accordion-toggle:after {
        /* symbol for "opening" panels */
        font-family: 'FontAwesome';  /* essential for enabling glyphicon */
        content: "\f0d8";    /* adjust as needed, taken from bootstrap.css */
        float: center;        /* adjust as needed */
    }
    .box-header .accordion-toggle.collapsed:after {
        /* symbol for "collapsed" panels */
        content: "\f0d7";    /* adjust as needed, taken from bootstrap.css */
    }
</style>

<div class="pengusulan-hutan-adat-i-i-i-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <div class="form-group field-pengusulanhutanadativ-tanggal_sk_penetapan">
        <label class="control-label">Kode Pengusulan</label>
        <input type="text" class="form-control" value="<?php echo \common\models\PengusulanRegistrasi::findOne(['id' => $model->id_pengusulan])->kode_pengusulan ?>" disabled="true">
        <div class="help-block"></div>
    </div>

    <!-- Data Pemohon -->
    <h5 class="timeline-header"><b>I. Data Pemohon</b><a class="accordion-toggle" data-toggle="collapse" href="#collapseDataPemohon"></a></h5>
    <div id="collapseDataPemohon" class="panel-collapse collapse">
        <div class="box-body">
            <div class="col-sm-12">
                <div style='border-bottom:1px solid #ccc;'></div>
            </div><br>
            <?php echo $form->field($model, 'nama_pemohon') ?>
            <?php echo $form->field($model, 'no_ktp_pemohon') ?>
            <?php echo $form->field($model, 'alamat_pemohon') ?>
            <?php echo $form->field($model, 'jabatan_pemohon') ?>
            <?php echo $form->field($model, 'nama_mha') ?>
        </div>
    </div>
    <!-- END -->

    <!-- Letak dan Luas Hutan HAK -->
    <h5 class="timeline-header"><b>II. Letak dan Luas Hutan HAK</b><a class="accordion-toggle" data-toggle="collapse" href="#collapseLetakHutan"></a></h5>
    <div id="collapseLetakHutan" class="panel-collapse collapse">
        <div class="box-body">
            <div class="col-sm-12">
                <div style='border-bottom:1px solid #ccc;'></div>
            </div><br>
            <?php echo $form->field($model, 'desa_kelurahan') ?>
            <?php echo $form->field($model, 'kecamatan') ?>
            <?php echo $form->field($model, 'kota_kabupaten') ?>
            <?php echo $form->field($model, 'provinsi') ?>
            <?php echo $form->field($model, 'das') ?>
            <?php echo $form->field($model, 'luas') ?>
        </div>
    </div>

    <!-- END -->

    <!-- Hasil Verifikasi untuk Permohonan Penetapan Hutan Hak Perseorangan/ Badan Hukum, sebagai berikut -->

    <h5 class="timeline-header"><b>III.    Hasil Verifikasi untuk Permohonan Penetapan Hutan Hak Perseorangan/ Badan Hukum</b><a class="accordion-toggle" data-toggle="collapse" href="#collapsePenetapanHutan"></a></h5>
    <div id="collapsePenetapanHutan" class="panel-collapse collapse">
        <div class="box-body">
            <div class="col-sm-12">
                <div style='border-bottom:1px solid #ccc;'></div>
            </div><br>
            <div class="row">
                <div class="col-md-6">
                    <?php echo $form->field($model, 'hak_atas_tanah')->dropDownList([true => 'Ada', false => 'Tidak']) ?>
                </div>
                <div class="col-md-6">
                    <?php echo $form->field($model, 'validitas_hak_atas_tanah')->dropDownList([true => 'Valid', false => 'Tidak Valid']) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <?php echo $form->field($model, 'tanah_berupa_hutan')->dropDownList([true => 'Ada', false => 'Tidak']) ?>
                </div>
                <div class="col-md-6">
                    <?php echo $form->field($model, 'validitas_tanah_berupa_hutan')->dropDownList([true => 'Valid', false => 'Tidak Valid']) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <?php echo $form->field($model, 'surat_pernyataan_penetapan_hutan_hak')->dropDownList([true => 'Ada', false => 'Tidak']) ?>
                </div>
                <div class="col-md-6">
                    <?php echo $form->field($model, 'validitas_surat_pernyataan_penetapan_hutan_hak')->dropDownList([true => 'Valid', false => 'Tidak Valid']) ?>
                </div>
            </div>
        </div>
    </div>

    <!-- END -->

    <!-- Hasil Verifikasi untuk Permohonan Penetapan Hutan Adat -->

    <h5 class="timeline-header"><b>IV. Hasil Verifikasi untuk Permohonan Penetapan Hutan Adat</b><a class="accordion-toggle" data-toggle="collapse" href="#collapseDokumenSubjekHutan"></a></h5>
    <div id="collapseDokumenSubjekHutan" class="panel-collapse collapse">
        <div class="box-body">
            <div class="col-sm-12">
                <div style='border-bottom:1px solid #ccc;'></div>
            </div><br>
            <div class="row">
                <div class="col-md-6">
                    <?php echo $form->field($model, 'masyarakat_adat_diakui_pemda')->dropDownList([true => 'Ada', false => 'Tidak']) ?>
                </div>
                <div class="col-md-6">
                    <?php echo $form->field($model, 'validitas_masyarakat_adat_diakui_pemda')->dropDownList([true => 'Lengkap', false => 'Tidak']) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <?php echo $form->field($model, 'wilayah_adat_berupa_hutan')->dropDownList([true => 'Ada', false => 'Tidak']) ?>
                </div>
                <div class="col-md-6">
                    <?php echo $form->field($model, 'validitas_wilayah_adat_berupa_hutan')->dropDownList([true => 'Valid', false => 'Tidak Valid']) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <?php echo $form->field($model, 'surat_pernyataan_ketua_adat')->dropDownList([true => 'Ada', false => 'Tidak']) ?>
                </div>
                <div class="col-md-6">
                    <?php echo $form->field($model, 'validitas_surat_pernyataan_ketua_adat')->dropDownList([true => 'Valid', false => 'Tidak Valid']) ?>
                </div>
            </div>
        </div>
    </div>

    <!-- END -->

    <!-- Catatan -->
    <h5 class="timeline-header"><b>V. Catatan</b><a class="accordion-toggle" data-toggle="collapse" href="#collapseCatatan"></a></h5>
    <div id="collapseCatatan" class="panel-collapse collapse">
        <div class="box-body">
            <div class="col-sm-12">
                <div style='border-bottom:1px solid #ccc;'></div>
            </div><br>
            <div class="row">
                <div class="col-md-12">
                    <?php echo $form->field($model, 'catatan')->textarea(['rows' => 6]) ?>
                </div>
            </div>

        </div>
    </div>
    <!-- END -->

    <!-- Simpulan -->
    <h5 class="timeline-header"><b>VI. Simpulan Hasil Verifikasi</b><a class="accordion-toggle" data-toggle="collapse" href="#collapseSimpulan"></a></h5>
    <div id="collapseSimpulan" class="panel-collapse collapse">
        <div class="box-body">
            <div class="col-sm-12">
                <div style='border-bottom:1px solid #ccc;'></div>
            </div><br>
            <div class="row">
                <div class="col-md-6">
                    <?php echo $form->field($model, 'validasi_simpulan_hasil_verifikasi')->dropDownList([true => 'Valid', false => 'Tidak Valid']) ?>
                    <?php echo $form->field($model, 'berita_acara_data')->fileInput() ?>
                    <?php if ($model->berita_acara_filename) : ?>
                        <?php echo Html::a($model->berita_acara_filename, ['download' . 'id' => $model->id], ['class' => 'btn bg-yellow']) ?>
                    <?php endif ?>
                    <?php echo $form->field($model, 'finished')->checkbox() ?>
                </div>
            </div>
        </div>
    </div>
    <!-- END -->

    <!-- <?php echo $form->field($model, 'id_pengusulan') ?> -->
    <!-- <?php echo $form->field($model, 'created_date') ?> -->
    <!-- <?php echo $form->field($model, 'modified_date') ?> -->

    <div class="form-group">
        <?php echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?php echo Html::a(Yii::t('app', 'Cancel'), ['cancel', 'id' => $model->id], ['class' => 'btn btn-warning']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div><!-- pengusulan-hutan-adat-i-i-i-_form -->
