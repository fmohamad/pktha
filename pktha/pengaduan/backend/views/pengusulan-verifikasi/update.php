<?php

use yii\helpers\Html;
use common\models\PengusulanRegistrasi;

/* @var $this yii\web\View */
/* @var $model common\models\PengusulanRegistrasiIII */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
            'modelClass' => 'Pengusulan Hutan Adat Tahap III',
        ]) . PengusulanRegistrasi::findOne(['id' => $model->id_pengusulan])->kode_pengusulan;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pengusulan Hutan Adat'), 'url' => ['pengusulan-registrasi/index']];
$this->params['breadcrumbs'][] = ['label' => PengusulanRegistrasi::findOne(['id' => $model->id_pengusulan])->kode_pengusulan, 'url' => ['pengusulan-registrasi/view', 'id' => $model->id_pengusulan]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="pengusulan-registrasi-iii-update">
    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>
</div>
