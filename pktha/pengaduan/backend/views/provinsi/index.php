<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Provinsi';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title">List <?= $this->title; ?></h3>          
        <div class="box-tools pull-right">
            <p>
                <?= Html::a('Create Provinsi', ['create'], ['class' => 'btn btn-success']) ?>
            </p>
        </div>
    </div>
    <div class="provinsi-index box-body">
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                'nama_provinsi',
                    [
                    'label' => 'Wilayah',
                    'value' => function($model, $index, $widget) {
                        return \common\models\Wilayah::findOne(['id' => $model->id_wilayah])->nama_wilayah;
                    }
                ],
                    ['class' => 'yii\grid\ActionColumn'],
            ],
        ]);
        ?>
    </div>
</div>
