<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Provinsi */

$this->title = $model->nama_provinsi;
$this->params['breadcrumbs'][] = ['label' => 'Provinsi', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title"><?= $this->title; ?></h3>          
        <div class="box-tools pull-right">
            <p>
                <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                <?=
                Html::a('Delete', ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this item?',
                        'method' => 'post',
                    ],
                ])
                ?>
            </p>
        </div>
    </div>
    <div class="provinsi-view box-body">
        <table class="table table-striped table-bordered detail-view">
            <tbody>
                <tr>
                    <th>Nama Provinsi</th>
                    <td><?= $model->nama_provinsi ?></td>
                </tr>
                <tr>
                    <th>Latitude</th>
                    <td><?= $model->latitude ?></td>
                </tr>
                <tr>
                    <th>Longitude</th>
                    <td><?= $model->longitude ?></td>
                </tr>
                <tr>
                    <th>Wilayah</th>
                    <td>
                        <?= \common\models\Wilayah::findOne(['id' => $model->id_wilayah])->nama_wilayah ?>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
