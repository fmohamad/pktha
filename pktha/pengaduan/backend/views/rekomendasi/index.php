<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Rekomendasi';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-info">
    <div class="box-header with-border">
      <h3 class="box-title">List <?=$this->title;?></h3>          
      <div class="box-tools pull-right">
          <p>
        <?= Html::a('Create Rekomendasi', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    </div>
</div>
<!-- /.box-header -->



<div class="rekomendasi-index box-body">


    

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            // 'jenispenyelesaian_id',
            [
                'attribute'=>'jenispenyelesaian_id',
                'value'=>'jenispenyelesaian.nama'
            ],
            'hasil_penyelesaian:ntext',
            'created_at:datetime',
            'updated_at:datetime',
            // 'user_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>

</div>