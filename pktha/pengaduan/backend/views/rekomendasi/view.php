<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Rekomendasi */


$xx=\yii\helpers\ArrayHelper::map(\common\models\Jenispenyelesaian::find()->all(),'id','nama');
$this->title = $xx[$model['jenispenyelesaian_id']];


$this->params['breadcrumbs'][] = ['label' => 'Rekomendasi', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-info">
    <div class="box-header with-border">
      <h3 class="box-title"><?=$this->title;?></h3>          
      <div class="box-tools pull-right">
    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id , 'idx'=>$idx], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id , 'idx'=>$idx], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
    </div>
</div>

<div class="rekomendasi-view box-body">



    

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            // 'id',
            [
                'attribute'=>'jenispenyelesaian_id',
                'value'=>$xx[$model['jenispenyelesaian_id']]
            ],
            'hasil_penyelesaian:ntext',
            // 'created_at',
            // 'updated_at',
            // 'user_id',
        ],
    ]) ?>

</div>
</div>
