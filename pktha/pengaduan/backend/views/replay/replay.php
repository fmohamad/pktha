<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use himiklab\ckeditor\CKEditor;



/* @var $this yii\web\View */
/* @var $model common\models\Pengaduan */

$this->title = 'Reply';
$this->params['breadcrumbs'][] = ['label' => 'Pengaduan', 'url' => ['pengaduan/index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box box-info">
    
<!-- /.box-header -->

<div class="pengaduan-create box-body">

    <div class="pengaduan-form">

    <?php $form = ActiveForm::begin();?>
    <?= $form->field($model, 'to')->input('email');?>
    <?= $form->field($model, 'cc')?>
    <?= $form->field($model, 'subject')?>
    

    <?= $form->field($model, 'content')->widget(CKEditor::className(), [
        'editorOptions' => ['height' => '300px']
    ])?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Kirim' : 'Kirim', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>


	<?php ActiveForm::end(); ?>

    

</div>
</div>
