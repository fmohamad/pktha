<?php

use yii\helpers\Html;
use yii\grid\GridView;
use mdm\admin\components\Helper;


/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Sliders';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-info">
    <div class="box-header with-border">
      <h3 class="box-title">List <?php echo $this->title;?></h3>
      <div class="box-tools pull-right">
          <p>
        <?php echo Html::a('Create Slider', ['create'], ['class' => 'btn btn-success']) ?>
        <?php echo Html::a('Slider Recylce bin', ['recyclebin'], ['class' => 'btn btn-danger']) ?>
    </p>
    </div>
</div>

<div class="slider-index box-body">
    <?php echo GridView::widget(
        [
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            'name',
            'desc:ntext',
            // 'created_at',
            // 'updated_at',
            // 'img',
            [
            'attribute'=>'publish',
            'value'=>function ($model) {
                return $model->publish==1?'Published':'Unpublished';
            }
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                // 'template' => '{publish} {view} {update} {delete}',
                                    'template' => Helper::filterActionColumn('{publish} {view} {update} {delete}'),

                'buttons' => [
                'publish' => function ($url) {
                    return Html::a(
                        '<span class="glyphicon glyphicon-upload"></span> &nbsp;',
                        $url,
                        [
                            'title' => 'Publish',
                            'data-pjax' => '0',
                        ]
                    );
                }
                ]
            ],
        ],
        ]
    ); ?>

</div>
</div>
