<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Sliders';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-info">
    <div class="box-header with-border">
      <h3 class="box-title">List <?=$this->title;?></h3>          
      <div class="box-tools pull-right">
          <p>
        <?= Html::a('List Slider', ['index'], ['class' => 'btn btn-success']) ?>
        
    </p>
    </div>
</div>

<div class="slider-index box-body">



    

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            'name',
            'desc:ntext',
            // 'created_at',
            // 'updated_at',
            // 'img',

            // ['class' => 'yii\grid\ActionColumn'],
            [
            'class' => 'yii\grid\ActionColumn', 
            'template' => '{undelete}',
            'buttons' => [
                'undelete' => function ($url) {
                    return Html::a(
                        '<span class="glyphicon glyphicon-trash"></span> &nbsp;&nbsp;&nbsp;',
                        $url, 
                    [
                        'title' => 'Pengaduan',
                        'data-pjax' => '0',
                    ]
                    );
                }]
            
            ],
        ],
    ]); ?>

</div>
</div>