<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\models\User;
?>

            <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>

                <?php echo $form->field($model, 'username') ?>

                <?php echo $form->field($model, 'email') ?>

                <?php echo $form->field($model, 'password')->passwordInput() ?>
                <?php echo $form->field($model, 'role')->dropDownList(
                    [
                    User::ROLE_USER => 'User',
                    User::ROLE_MODERATOR => 'Moderator',
                    User::ROLE_ADMIN_WILAYAH_1 => 'Administrator Wilayah Maluku dan Papua',
                    User::ROLE_ADMIN_WILAYAH_2 => 'Administrator Wilayah Sulawesi',
                    User::ROLE_ADMIN_WILAYAH_3 => 'Administrator Wilayah Kalimantan',
                    User::ROLE_ADMIN_WILAYAH_4 => 'Administrator Wilayah Jawa, Bali, dan Nusa Tenggara',
                    User::ROLE_ADMIN_WILAYAH_5 => 'Administrator Wilayah Sumatera',
                    User::ROLE_ADMIN_1 => 'Administrator Pusat 1',
                    User::ROLE_ADMIN_2 => 'Administrator Pusat 2',
                    User::ROLE_ADMIN_3 => 'Administrator Pusat 3',
                    User::ROLE_ADMIN => 'Administrator Situs'
                    ]
                ) ?>

                <div class="form-group">
                    <?php echo Html::submitButton('Create', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
                </div>

            <?php ActiveForm::end(); ?>

