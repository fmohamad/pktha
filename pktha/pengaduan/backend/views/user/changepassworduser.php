<?php 
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

$this->title = 'Reset Password User';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="site-changepassword">

    
    <p>Please fill out the following fields to change password :</p>
    
    <?php $form = ActiveForm::begin([
        'id'=>'changepassword-form',
        'options'=>['class'=>'form-horizontal'],
        'fieldConfig'=>[
            'template'=>"{label}\n<div class=\"col-lg-3\">
                        {input}</div>\n<div class=\"col-lg-5\">
                        {error}</div>",
            'labelOptions'=>['class'=>'col-lg-2 control-label'],
        ],
    ]); ?>
        
        
        <?= $form->field($model,'password1',['inputOptions'=>[
            'placeholder'=>'New Password'
        ]])->passwordInput()->label('New Password'); ?>
        
        <?= $form->field($model,'password2',['inputOptions'=>[
            'placeholder'=>'Repeat New Password'
        ]])->passwordInput()->label('Repeat New Password'); ?>
        
        <div class="form-group">
            <div class="col-lg-offset-2 col-lg-11">
                <?= Html::submitButton('Change password',[
                    'class'=>'btn btn-primary'
                ]) ?>
                <?= Html::a('Cancel', Url::toRoute("user/index"),['class'=>'btn btn-danger']);?>
            </div>

            

        </div>
    <?php ActiveForm::end(); ?>
</div>