<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\UsulanHutanAdat */

$this->title = 'Create Usulan Hutan Adat';
$this->params['breadcrumbs'][] = ['label' => 'Usulan Hutan Adats', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="usulan-hutan-adat-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
