<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UsulanHutanAdatSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Usulan Hutan Adat';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box box-info">
	<div class="box-header with-border">
		<h3 class="box-title">List <?=$this->title;?></h3>
		<div class="box-tools pull-right">
	  <p>
       <!-- <?= Html::a('Tambah Usulan Hutan Adat', ['create'], ['class' => 'btn btn-success']) ?> -->
    </p>
	</div>
</div>
		
<div class="usulan-hutan-adat-index box-body">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_form',
            'nama:ntext',
            'email:ntext',
            'alamat:ntext',
            'telpon:ntext',
            'form:ntext',

            ['class' => 'yii\grid\ActionColumn',
													'template' => '{download}',
							'contentOptions' => ['style'=>'min-width: 10px; text-align:center;'],
							'buttons' => [
								'download' => function ($url, $model) {
									$urlx = Url::to(['usulan-hutan-adat/download', 'id' => $model->id]);
									return Html::a(
										'<span class="glyphicon glyphicon-download"></span>',
										$urlx,
										[
											'title' => 'Download'.$model-upload,
											'data-pjax' => '0',
										]
									);
								}
							],
						]
        ],
    ]); ?>
</div>
