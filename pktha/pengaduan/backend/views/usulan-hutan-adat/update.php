<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\UsulanHutanAdat */

$this->title = 'Update Usulan Hutan Adat: ' . $model->id_form;
$this->params['breadcrumbs'][] = ['label' => 'Usulan Hutan Adats', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_form, 'url' => ['view', 'id' => $model->id_form]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="usulan-hutan-adat-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
