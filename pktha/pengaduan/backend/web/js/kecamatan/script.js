/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {
    function onProvinsiChanged() {
        if ($(this).val()) {
            $.post("/pktha/pengaduan/backend/web/index.php?r=location/kota-kabupaten", {id_provinsi: $(this).val()}, function (data) {
                console.log(data);
                $('#select-kota-kabupaten').html(data);
            });
        } else {
            $('#select-kota-kabupaten').html('<option value="0">Lainnya</option>');
        }
    }
    
    function onKotaKabupatenChanged() {
        if ($(this).val()) {
            console.log($(this).val());
            $.post("/pktha/pengaduan/backend/web/index.php?r=location/kecamatan", {id_kota_kabupaten: $(this).val()}, function (data) {
                console.log(data);
                $('#select-kecamatan').html(data);
            });
        } else {
            $('#select-kecamatan').html('<option value="0">Lainnya</option>');
        }
    }
    
    function onKecamatanChanged() {
        if ($(this).val()) {
            $.post("/pktha/pengaduan/backend/web/index.php?r=location/desa-kelurahan", {id_kecamatan: $(this).val()}, function (data) {
                console.log(data);
                $('#select-desa-kelurahan').html(data);
            });
        } else {
            $('#select-desa-kelurahan').html('<option value="0">Lainnya</option>');
        }
    }

    $('#select-provinsi').on('change', onProvinsiChanged);
    $('#select-kota-kabupaten').on('change', onKotaKabupatenChanged);
    $('#select-kecamatan').on('change', onKecamatanChanged);
});