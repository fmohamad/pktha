<?php

namespace common\models;
use yii\behaviors\TimestampBehavior;

use Yii;

/**
 * This is the model class for table "assesment".
 *
 * @property integer $id
 * @property integer $pengaduan_id
 * @property string $nama_tim
 * @property string $anggota_tim
 * @property string $tgl_berangkat
 * @property string $tgl_pulang
 * @property string $data_informasi
 * @property string $analisa_awal
 * @property string $penilaian
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $user_id
 */
class Assesment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'assesment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // [['nama_tim', 'anggota_tim', 'tgl_berangkat', 'tgl_pulang', 'data_informasi', 'analisa_awal', 'penilaian'], 'required'],
        [['nama_tim', 'anggota_tim', 'tgl_berangkat', 'tgl_pulang', 'analisa_awal'], 'required'],
            
            [['anggota_tim', 'data_informasi', 'analisa_awal'], 'string'],
            [['tgl_berangkat', 'tgl_pulang'], 'safe'],
            [['nama_tim'], 'string', 'max' => 20],
            // [['penilaian'], 'string', 'max' => 10]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'pengaduan_id' => 'Pengaduan ID',
            'nama_tim' => 'Nama Tim',
            'anggota_tim' => 'Anggota Tim',
            'tgl_berangkat' => 'Tanggal Mulai',
            'tgl_pulang' => 'Tanggal Akhir',
            'data_informasi' => 'Data Informasi',
            'analisa_awal' => 'Analisa Awal',
            'penilaian' => 'Rekomendasi Awal',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'user_id' => 'User ID',
        ];
    }
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    public function beforeSave($insert)
    {        
        if (parent::beforeSave($insert)) {
            if($this->isNewRecord){ 
                //Pengaduan
                    $pengaduan = Pengaduan::findOne($this->pengaduan_id);
                    $pengaduan->id_tahapan = 2;
                    $pengaduan->save();
                //pengaduan


                // $this->kode = $this->getKode($this->sarana_pengaduan);
                $this->user_id=Yii::$app->user->getId(); 
            }
            return true;
        } else {
            return false;
        }
    }
}
