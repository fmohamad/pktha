<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;


/**
 * This is the model class for table "dokumen".
 *
 * @property integer $id
 * @property integer $tipe_dokumen
 * @property integer $id_tahap
 * @property string $nama
 * @property string $keterangan
 * @property string $url
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $user_id
 */
class Dokumen extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dokumen';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[ 'id_tahap', 'nama', 'keterangan', 'url'], 'required'],
            [['tipe_dokumen', 'id_tahap', 'created_at', 'updated_at', 'user_id'], 'integer'],
            [['nama'], 'string'],
            [['keterangan'], 'string'],
            [['url'], 'file', 'skipOnEmpty' => false],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tipe_dokumen' => 'Tipe Dokumen',
            'id_tahap' => 'Id Tahap',
            'nama' => 'Nama',
            'keterangan' => 'Keterangan',
            'url' => 'Url',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'user_id' => 'User ID',
        ];
    }
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    public function beforeSave($insert)
    {        
        if (parent::beforeSave($insert)) {
            if($this->isNewRecord){             
                // $this->kode = $this->getKode($this->sarana_pengaduan);
                $this->user_id=Yii::$app->user->getId(); 
            }
            return true;
        } else {
            return false;
        }
    }

    
    
    public function upload()
    {
        if ($this->validate()) {            
            $this->url->saveAs(Yii::getAlias('@updir').'/uploads/' . $this->url->baseName . '.' . $this->url->extension);
            return true;
        } else {
            return false;
        }
    }

}
