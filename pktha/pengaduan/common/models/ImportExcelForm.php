<?php

namespace common\models;

use Yii;
use yii\base\Model;

class ImportExcelForm extends Model
{
    public $file;

    public function rules()
    {
        return [
            [['file'],'required'],
            [['file'],'file','extensions'=>['xlsx','xlsm','xls'],'maxSize'=>1024 * 1024 * 5],
        ];
    }

    public function attributeLabels()
    {
        return [
            'file'=>'Select File',
        ];
    }

    public function upload()
    {
        if ($this->validate()) {
            $this->file->saveAs($this->getPath() . md5($this->file->baseName) . '.' . $this->file->extension);
            return true;
        } else {
            return false;
        }
    }

    public function getPath()
    {
        // return Yii::getAlias('@updir') . '/xls/';
        return dirname(dirname(__DIR__)) . '/uploads/xls/';
    }
}
