<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "jenis_pelapor".
 *
 * @property string $id
 * @property string $jenis_pelapor
 */
class JenisPelapor extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'jenis_pelapor';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['jenis_pelapor'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'jenis_pelapor' => Yii::t('app', 'Jenis Pelapor'),
        ];
    }

    /**
     * @inheritdoc
     * @return JenisPelaporQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new JenisPelaporQuery(get_called_class());
    }
}
