<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "kabkota".
 *
 * @property string $kabid
 * @property string $proid
 * @property string $kabkota
 * @property integer $id_sumber
 *
 * @property Provinsi $pro
 */
class Kabkota extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kabkota';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kabid'], 'required'],
            [['id_sumber'], 'integer'],
            [['kabid', 'proid'], 'string', 'max' => 10],
            [['kabkota'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'kabid' => 'Kabid',
            'proid' => 'Proid',
            'kabkota' => 'Kabkota',
            'id_sumber' => 'Id Sumber',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPro()
    {
        return $this->hasOne(Provinsi::className(), ['proid' => 'proid']);
    }
    public function getPiaps()
    {
        return $this->hasOne(Piaps::className(), ['kabid' => 'kabid']);
    }
}
