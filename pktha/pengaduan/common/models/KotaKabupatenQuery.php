<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[KotaKabupaten]].
 *
 * @see KotaKabupaten
 */
class KotaKabupatenQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return KotaKabupaten[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return KotaKabupaten|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
    
    /**
     * 
     * @param type $id
     * @return type
     */
    public function getById($id) {
        return $this->select(['*'])->where(['id'=>$id])->one();
    }
    
    /**
     * 
     * @param type $id_provinsi
     * @return type
     */
    public function getByProvinsiId($id_provinsi) {
        return $this->select(['*'])->where(['id_provinsi'=>$id_provinsi])->all();
    }
}
