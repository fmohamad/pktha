<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%mha}}".
 *
 * @property string $id
 * @property string $nama_komunitas
 * @property string $bahasa
 * @property string $id_provinsi
 * @property string $id_kota_kabupaten
 * @property string $id_kecamatan
 * @property double $luas_wilayah_adat
 * @property string $batas_utara_wilayah_adat
 * @property string $batas_selatan_wilayah_adat
 * @property string $batas_timur_wilayah_adat
 * @property string $batas_barat_wilayah_adat
 * @property string $satuan_wilayah_adat
 * @property string $kondisi_fisik_wilayah
 * @property string $jumlah_kepala_keluarga
 * @property string $jumlah_pria
 * @property string $jumlah_wanita
 * @property string $mata_pencaharian_utama
 * @property string $sejarah_singkat_masyarakat_adat
 * @property string $pembagian_ruang_menurut_aturan_adat
 * @property string $sistem_penguasaan_pengelolaan_wilayah
 * @property string $nama_lembaga_adat
 * @property string $struktur_lembaga_adat
 * @property string $struktur_lembaga_adat_filename
 * @property string $tugas_fungsi_pemangku_adat
 * @property string $tugas_fungsi_pemangku_adat_filename
 * @property string $mekanisme_pengambilan_keputusan
 * @property string $aturan_adat_terkait_wilayah_sda
 * @property string $aturan_adat_terkait_wilayah_sda_filename
 * @property string $aturan_adat_terkait_pranata_sosial
 * @property string $aturan_adat_terkait_pranata_sosial_filename
 * @property string $contoh_putusan_hukum_adat
 * @property string $jenis_ekosistem
 * @property string $potensi_manfaat_sda
 * @property string $sumber_pangan
 * @property string $sumber_kesehatan_kecantikan
 * @property string $sumber_papan_infrastruktur
 * @property string $sumber_sandang
 * @property string $sumber_rempah_bumbu
 * @property string $sumber_pendapatan_ekonomi
 * @property string $peta_wilayah_adat_filename
 * @property boolean $status_registrasi_musyawarah
 * @property string $nama
 * @property string $jabatan
 * @property string $alamat
 * @property string $telepon
 * @property string $email
 *
 * @property Kecamatan $idKecamatan
 * @property KotaKabupaten $idKotaKabupaten
 * @property Provinsi $idProvinsi
 */
class Mha extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%mha}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama_komunitas', 'bahasa', 'batas_utara_wilayah_adat', 'batas_selatan_wilayah_adat', 'batas_timur_wilayah_adat', 'batas_barat_wilayah_adat', 'satuan_wilayah_adat', 'kondisi_fisik_wilayah', 'mata_pencaharian_utama', 'sejarah_singkat_masyarakat_adat', 'pembagian_ruang_menurut_aturan_adat', 'sistem_penguasaan_pengelolaan_wilayah', 'nama_lembaga_adat', 'struktur_lembaga_adat', 'struktur_lembaga_adat_filename', 'tugas_fungsi_pemangku_adat', 'tugas_fungsi_pemangku_adat_filename', 'mekanisme_pengambilan_keputusan', 'aturan_adat_terkait_wilayah_sda', 'aturan_adat_terkait_wilayah_sda_filename', 'aturan_adat_terkait_pranata_sosial', 'aturan_adat_terkait_pranata_sosial_filename', 'contoh_putusan_hukum_adat', 'jenis_ekosistem', 'potensi_manfaat_sda', 'sumber_pangan', 'sumber_kesehatan_kecantikan', 'sumber_papan_infrastruktur', 'sumber_sandang', 'sumber_rempah_bumbu', 'sumber_pendapatan_ekonomi', 'peta_wilayah_adat_filename', 'nama', 'jabatan', 'alamat', 'email'], 'string'],
            [['id_provinsi', 'id_kota_kabupaten', 'id_kecamatan', 'jumlah_kepala_keluarga', 'jumlah_pria', 'jumlah_wanita'], 'integer'],
            [['luas_wilayah_adat', 'telepon'], 'number'],
            [['status_registrasi_musyawarah'], 'boolean'],
            [['id_kecamatan'], 'exist', 'skipOnError' => true, 'targetClass' => Kecamatan::className(), 'targetAttribute' => ['id_kecamatan' => 'id']],
            [['id_kota_kabupaten'], 'exist', 'skipOnError' => true, 'targetClass' => KotaKabupaten::className(), 'targetAttribute' => ['id_kota_kabupaten' => 'id']],
            [['id_provinsi'], 'exist', 'skipOnError' => true, 'targetClass' => Provinsi::className(), 'targetAttribute' => ['id_provinsi' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'nama_komunitas' => Yii::t('app', 'Nama Komunitas'),
            'bahasa' => Yii::t('app', 'Bahasa'),
            'id_provinsi' => Yii::t('app', 'Provinsi'),
            'id_kota_kabupaten' => Yii::t('app', 'Kota/Kabupaten'),
            'id_kecamatan' => Yii::t('app', 'Kecamatan'),
            'luas_wilayah_adat' => Yii::t('app', 'Luas Wilayah Adat'),
            'batas_utara_wilayah_adat' => Yii::t('app', 'Batas Utara Wilayah Adat'),
            'batas_selatan_wilayah_adat' => Yii::t('app', 'Batas Selatan Wilayah Adat'),
            'batas_timur_wilayah_adat' => Yii::t('app', 'Batas Timur Wilayah Adat'),
            'batas_barat_wilayah_adat' => Yii::t('app', 'Batas Barat Wilayah Adat'),
            'satuan_wilayah_adat' => Yii::t('app', 'Satuan Wilayah Adat'),
            'kondisi_fisik_wilayah' => Yii::t('app', 'Kondisi Fisik Wilayah'),
            'jumlah_kepala_keluarga' => Yii::t('app', 'Jumlah Kepala Keluarga'),
            'jumlah_pria' => Yii::t('app', 'Jumlah Penduduk Laki-Laki'),
            'jumlah_wanita' => Yii::t('app', 'Jumlah Penduduk Wanita'),
            'mata_pencaharian_utama' => Yii::t('app', 'Mata Pencaharian Utama'),
            'sejarah_singkat_masyarakat_adat' => Yii::t('app', 'Sejarah Singkat Masyarakat Adat'),
            'pembagian_ruang_menurut_aturan_adat' => Yii::t('app', 'Pembagian Ruang Menurut Aturan Adat '),
            'sistem_penguasaan_pengelolaan_wilayah' => Yii::t('app', 'Sistem Penguasaan dan Pengelolaan Wilayah '),
            'nama_lembaga_adat' => Yii::t('app', 'Nama Lembaga Adat'),
            'struktur_lembaga_adat' => Yii::t('app', 'Struktur Lembaga Adat '),
            'struktur_lembaga_adat_filename' => Yii::t('app', 'Berkas Terkait Struktur Lembaga Adat '),
            'tugas_fungsi_pemangku_adat' => Yii::t('app', 'Tugas dan Fungsi Masing-masing Pemangku Adat'),
            'tugas_fungsi_pemangku_adat_filename' => Yii::t('app', 'Berkas Terkait Tugas dan Fungsi Masing-masing Pemangku Adat'),
            'mekanisme_pengambilan_keputusan' => Yii::t('app', 'Mekanisme Pengambilan Keputusan '),
            'aturan_adat_terkait_wilayah_sda' => Yii::t('app', 'Aturan Adat yang berkaitan dengan Pengelolaan Wilayah dan Sumberdaya Alam '),
            'aturan_adat_terkait_wilayah_sda_filename' => Yii::t('app', 'Berkas Terkait Aturan Adat yang berkaitan dengan Pengelolaan Wilayah dan Sumberdaya Alam '),
            'aturan_adat_terkait_pranata_sosial' => Yii::t('app', 'Aturan Adat yang berkaitan dengan pranata sosial '),
            'aturan_adat_terkait_pranata_sosial_filename' => Yii::t('app', 'Berkas Terkait Aturan Adat yang berkaitan dengan pranata sosial '),
            'contoh_putusan_hukum_adat' => Yii::t('app', 'Satu contoh keputusan dari penerapan humum adat '),
            'jenis_ekosistem' => Yii::t('app', 'Jenis ekosistem '),
            'potensi_manfaat_sda' => Yii::t('app', 'Potensi dan Manfaat Keanekaragaman Hayati'),
            'sumber_pangan' => Yii::t('app', 'Sumber Pangan (karbohidrat: padi, umbi-umbian, jagung sagu; protein: jenis kacang-kacangan; vitamin: sayuran/buah)'),
            'sumber_kesehatan_kecantikan' => Yii::t('app', 'Sumber Kesehatan dan Kecantikan (tumbuhan obat, tumbuhan kosmetik) '),
            'sumber_papan_infrastruktur' => Yii::t('app', 'Sumber Papan dan Bahan Infrastruktur '),
            'sumber_sandang' => Yii::t('app', 'Sumber Sandang'),
            'sumber_rempah_bumbu' => Yii::t('app', 'Sumber Rempah-Rempah dan Bumbu'),
            'sumber_pendapatan_ekonomi' => Yii::t('app', 'Sumber Pendapatan Ekonomi'),
            'peta_wilayah_adat_filename' => Yii::t('app', 'Berkas Peta Wilayah Adat'),
            'status_registrasi_musyawarah' => Yii::t('app', 'Apakah wilayah adat yang diregistrasikan telah dimusyawarahkan?'),
            'nama' => Yii::t('app', 'Nama Pemohon'),
            'jabatan' => Yii::t('app', 'Jabatan Pemohon'),
            'alamat' => Yii::t('app', 'Alamat Surat Menyurat'),
            'telepon' => Yii::t('app', 'No. Telepon'),
            'email' => Yii::t('app', 'Alamat Email'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdKecamatan()
    {
        return $this->hasOne(Kecamatan::className(), ['id' => 'id_kecamatan']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdKotaKabupaten()
    {
        return $this->hasOne(KotaKabupaten::className(), ['id' => 'id_kota_kabupaten']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdProvinsi()
    {
        return $this->hasOne(Provinsi::className(), ['id' => 'id_provinsi']);
    }

    /**
     * @inheritdoc
     * @return MhaQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new MhaQuery(get_called_class());
    }
    
    /**
     * @return path to files related to this model.
     */
    public function getPath() {
        $updir = Yii::getAlias('@updir') . '/mha/' . $this->id;
        if (!is_dir($updir)) {
            mkdir($updir, 0775, true);
        }
        return $updir;
    }
}
