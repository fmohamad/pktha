<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Mha;

/**
 * MhaSearch represents the model behind the search form about `common\models\Mha`.
 */
class MhaSearch extends Mha
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_provinsi', 'id_kota_kabupaten', 'id_kecamatan', 'jumlah_kepala_keluarga', 'jumlah_pria', 'jumlah_wanita'], 'integer'],
            [['nama_komunitas', 'batas_utara_wilayah_adat', 'batas_selatan_wilayah_adat', 'batas_timur_wilayah_adat', 'batas_barat_wilayah_adat', 'satuan_wilayah_adat', 'kondisi_fisik_wilayah', 'mata_pencaharian_utama', 'sejarah_singkat_masyarakat_adat', 'pembagian_ruang_menurut_aturan_adat', 'sistem_penguasaan_pengelolaan_wilayah', 'nama_lembaga_adat', 'struktur_lembaga_adat', 'struktur_lembaga_adat_filename', 'tugas_fungsi_pemangku_adat', 'tugas_fungsi_pemangku_adat_filename', 'mekanisme_pengambilan_keputusan', 'aturan_adat_terkait_wilayah_sda', 'aturan_adat_terkait_wilayah_sda_filename', 'aturan_adat_terkait_pranata_sosial', 'aturan_adat_terkait_pranata_sosial_filename', 'contoh_putusan_hukum_adat'], 'safe'],
            [['luas_wilayah_adat'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Mha::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'id_provinsi' => $this->id_provinsi,
            'id_kota_kabupaten' => $this->id_kota_kabupaten,
            'id_kecamatan' => $this->id_kecamatan,
            'luas_wilayah_adat' => $this->luas_wilayah_adat,
            'jumlah_kepala_keluarga' => $this->jumlah_kepala_keluarga,
            'jumlah_pria' => $this->jumlah_pria,
            'jumlah_wanita' => $this->jumlah_wanita,
        ]);

        $query->andFilterWhere(['like', 'nama_komunitas', $this->nama_komunitas])
            ->andFilterWhere(['like', 'batas_utara_wilayah_adat', $this->batas_utara_wilayah_adat])
            ->andFilterWhere(['like', 'batas_selatan_wilayah_adat', $this->batas_selatan_wilayah_adat])
            ->andFilterWhere(['like', 'batas_timur_wilayah_adat', $this->batas_timur_wilayah_adat])
            ->andFilterWhere(['like', 'batas_barat_wilayah_adat', $this->batas_barat_wilayah_adat])
            ->andFilterWhere(['like', 'satuan_wilayah_adat', $this->satuan_wilayah_adat])
            ->andFilterWhere(['like', 'kondisi_fisik_wilayah', $this->kondisi_fisik_wilayah])
            ->andFilterWhere(['like', 'mata_pencaharian_utama', $this->mata_pencaharian_utama])
            ->andFilterWhere(['like', 'sejarah_singkat_masyarakat_adat', $this->sejarah_singkat_masyarakat_adat])
            ->andFilterWhere(['like', 'pembagian_ruang_menurut_aturan_adat', $this->pembagian_ruang_menurut_aturan_adat])
            ->andFilterWhere(['like', 'sistem_penguasaan_pengelolaan_wilayah', $this->sistem_penguasaan_pengelolaan_wilayah])
            ->andFilterWhere(['like', 'nama_lembaga_adat', $this->nama_lembaga_adat])
            ->andFilterWhere(['like', 'struktur_lembaga_adat', $this->struktur_lembaga_adat])
            ->andFilterWhere(['like', 'struktur_lembaga_adat_filename', $this->struktur_lembaga_adat_filename])
            ->andFilterWhere(['like', 'tugas_fungsi_pemangku_adat', $this->tugas_fungsi_pemangku_adat])
            ->andFilterWhere(['like', 'tugas_fungsi_pemangku_adat_filename', $this->tugas_fungsi_pemangku_adat_filename])
            ->andFilterWhere(['like', 'mekanisme_pengambilan_keputusan', $this->mekanisme_pengambilan_keputusan])
            ->andFilterWhere(['like', 'aturan_adat_terkait_wilayah_sda', $this->aturan_adat_terkait_wilayah_sda])
            ->andFilterWhere(['like', 'aturan_adat_terkait_wilayah_sda_filename', $this->aturan_adat_terkait_wilayah_sda_filename])
            ->andFilterWhere(['like', 'aturan_adat_terkait_pranata_sosial', $this->aturan_adat_terkait_pranata_sosial])
            ->andFilterWhere(['like', 'aturan_adat_terkait_pranata_sosial_filename', $this->aturan_adat_terkait_pranata_sosial_filename])
            ->andFilterWhere(['like', 'contoh_putusan_hukum_adat', $this->contoh_putusan_hukum_adat]);

        return $dataProvider;
    }
}
