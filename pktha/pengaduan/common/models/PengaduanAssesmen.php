<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "pengaduan_assesmen".
 *
 * @property string $id_pengaduan
 * @property string $potensi_konflik_dan_potensi_damai
 * @property string $sejarah_konflik
 * @property string $akar_konflik
 * @property string $pemicu_konflik
 * @property string $akselerator_konflik
 * @property string $pemetaan_dinamika_aktor
 * @property string $sistem_representasi
 * @property string $tawaran_tertinggi
 * @property string $tawaran_terendah
 * @property string $rekomendasi
 * @property string $id
 * @property string $created_date
 * @property string $modified_date
 * @property boolean $finished
 * @property string $surat_kesediaan_mediasi_filename
 * @property boolean $dialihkan
 * @property string $pesan_pengalihan_kasus
 */
class PengaduanAssesmen extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pengaduan_assesmen';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_pengaduan'], 'required'],
            [['id_pengaduan'], 'integer'],
            [['potensi_konflik_dan_potensi_damai',
                'tawaran_tertinggi',
                'tawaran_terendah',
                'sejarah_konflik',
                'akar_konflik',
                'pemicu_konflik',
                'akselerator_konflik',
                'pemetaan_dinamika_aktor',
                'sistem_representasi',
                'rekomendasi',
                'pesan_pengalihan_kasus'], 'string'],
            [['surat_kesediaan_mediasi_filename',], 'file', 'extensions' => 'doc, docx, pdf, odt'],
            [['pemetaan_dinamika_aktor_filename',], 'file', 'extensions' => 'pdf, jpg, jpeg, png, gif'],
            [['created_date', 'modified_date'], 'safe'],
            [['finished', 'dialihkan'], 'boolean'],
            [['id_pengaduan'], 'exist', 'skipOnError' => true, 'targetClass' => PengaduanRegistrasi::className(), 'targetAttribute' => ['id_pengaduan' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_pengaduan' => Yii::t('app', 'id pengaduan'),
            'potensi_konflik_dan_potensi_damai' => Yii::t('app', 'Potensi Konflik dan Potensi Damai'),
            'sejarah_konflik' => Yii::t('app', 'Sejarah Konflik'),
            'akar_konflik' => Yii::t('app', 'Akar Konflik'),
            'pemicu_konflik' => Yii::t('app', 'Pemicu Konflik'),
            'akselerator_konflik' => Yii::t('app', 'Akselerator Konflik'),
            'pemetaan_dinamika_aktor' => Yii::t('app', 'Pemetaan Dinamika Aktor'),
            'pemetaan_dinamika_aktor_filename' => Yii::t('app', 'Pemetaan Dinamika Aktor File'),
            'sistem_representasi' => Yii::t('app', 'Sistem Representasi'),
            'tawaran_tertinggi' => Yii::t('app', 'Tawaran Tertinggi'),
            'tawaran_terendah' => Yii::t('app', 'Tawaran Terendah'),
            'rekomendasi' => Yii::t('app', 'Rekomendasi'),
            'id' => Yii::t('app', 'ID'),
            'created_date' => Yii::t('app', 'Created Date'),
            'modified_date' => Yii::t('app', 'Modified Date'),
            'finished' => Yii::t('app', 'Finished'),
            'surat_kesediaan_mediasi_filename' => Yii::t('app', 'Surat Kesediaan Mediasi'),
            'dialihkan' => Yii::t('app', 'Kasus Dialihkan'),
            'pesan_pengalihan_kasus' => Yii::t('app', 'Pesan Pengalihan Kasus'),
        ];
    }

    /**
     * @inheritdoc
     * @return PengaduanAssesmenQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PengaduanAssesmenQuery(get_called_class());
    }
}
