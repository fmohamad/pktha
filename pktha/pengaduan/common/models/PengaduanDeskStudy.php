<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "pengaduan_desk_study".
 *
 * @property integer $id
 * @property integer $id_pengaduan
 * @property boolean $dialihkan
 *
 * @property PengaduanRegistrasi $idPengaduan
 */
class PengaduanDeskStudy extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pengaduan_desk_study';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_pengaduan'], 'required'],
            [['id_pengaduan'], 'integer'],
            [['dialihkan', 'finished'], 'boolean'],
            [['created_date', 'modified_date'], 'safe'],
            [['id_pengaduan'], 'exist', 'skipOnError' => true, 'targetClass' => PengaduanRegistrasi::className(), 'targetAttribute' => ['id_pengaduan' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_pengaduan' => 'Id Pengaduan',
            'dialihkan' => 'Dialihkan',
            'created_date' => 'Tanggal Pembuatan Kasus',
            'modified_date' => 'Tanggal Modifikasi Terakhir',
            'finished' => 'Selesai',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdPengaduan()
    {
        return $this->hasOne(PengaduanRegistrasi::className(), ['id' => 'id_pengaduan']);
    }

    /**
     * @inheritdoc
     * @return PengaduanDeskStudyQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PengaduanDeskStudyQuery(get_called_class());
    }
}
