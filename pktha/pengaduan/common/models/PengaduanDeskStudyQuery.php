<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[PengaduanDeskStudy]].
 *
 * @see PengaduanDeskStudy
 */
class PengaduanDeskStudyQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return PengaduanDeskStudy[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return PengaduanDeskStudy|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
