<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "pengaduan_drafting_mou".
 *
 * @property string $id_pengaduan
 * @property string $berita_acara_filename
 * @property string $id
 * @property string $created_date
 * @property string $modified_date
 * @property boolean $finished
 * @property string $foto_filename
 */
class PengaduanDraftingMou extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pengaduan_drafting_mou';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_pengaduan'], 'required'],
            [['id_pengaduan'], 'integer'],
            [['berita_acara_filename'], 'file', 'extensions' => 'pdf, docx, doc, odt'],
            [['foto_filename'], 'file', 'extensions' => 'jpg, jpeg, bmp'],
            [['created_date', 'modified_date'], 'safe'],
            [['finished'], 'boolean'],
            [['id_pengaduan'], 'exist', 'skipOnError' => true, 'targetClass' => PengaduanRegistrasi::className(), 'targetAttribute' => ['id_pengaduan' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_pengaduan' => Yii::t('app', 'Id Pengaduan'),
            'berita_acara_filename' => Yii::t('app', 'Berita Acara'),
            'id' => Yii::t('app', 'ID'),
            'created_date' => Yii::t('app', 'Created Date'),
            'modified_date' => Yii::t('app', 'Modified Date'),
            'finished' => Yii::t('app', 'Finished'),
            'foto_filename' => Yii::t('app', 'Foto'),
        ];
    }

    /**
     * @inheritdoc
     * @return PengaduanDraftingMouQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PengaduanDraftingMouQuery(get_called_class());
    }
}
