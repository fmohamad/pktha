<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\PengaduanMediasi;

/**
 * PengaduanMediasiSearch represents the model behind the search form about `common\models\PengaduanMediasi`.
 */
class PengaduanMediasiSearch extends PengaduanMediasi
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_pengaduan', 'id'], 'integer'],
            [['data_para_pihak', 'berita_acara_filename', 'foto_filename', 'pokok_permasalahan', 'objek_mediasi', 'surat_usulan_mediator_filename', 'sk_mediator_filename', 'proses_pertemuan', 'created_date', 'modified_date'], 'safe'],
            [['finished'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PengaduanMediasi::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_pengaduan' => $this->id_pengaduan,
            'id' => $this->id,
            'created_date' => $this->created_date,
            'modified_date' => $this->modified_date,
            // 'finished' => $this->finished,
            'finished' => false,
        ]);
        
        $query->join('LEFT JOIN', 'pengaduan_assesmen', 'pengaduan_assesmen.id_pengaduan = pengaduan_mediasi.id_pengaduan');
        $query->join('LEFT JOIN', 'pengaduan_pra_mediasi', 'pengaduan_pra_mediasi.id_pengaduan = pengaduan_mediasi.id_pengaduan');
        $query->where(['pengaduan_assesmen.finished' => true]);
        $query->where(['pengaduan_pra_mediasi.finished' => true]);

        $query->andFilterWhere(['like', 'data_para_pihak', $this->data_para_pihak])
            ->andFilterWhere(['like', 'berita_acara_filename', $this->berita_acara_filename])
            ->andFilterWhere(['like', 'foto_filename', $this->foto_filename])
            ->andFilterWhere(['like', 'pokok_permasalahan', $this->pokok_permasalahan])
            ->andFilterWhere(['like', 'objek_mediasi', $this->objek_mediasi])
            ->andFilterWhere(['like', 'surat_usulan_mediator_filename', $this->surat_usulan_mediator_filename])
            ->andFilterWhere(['like', 'sk_mediator_filename', $this->sk_mediator_filename])
            ->andFilterWhere(['like', 'proses_pertemuan', $this->proses_pertemuan]);

        return $dataProvider;
    }
}
