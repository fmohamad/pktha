<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "pengaduan_pra_mediasi".
 *
 * @property string $id_pengaduan
 * @property string $data_para_pihak
 * @property string $pokok_permasalahan
 * @property string $objek_mediasi
 * @property string $id
 * @property string $created_date
 * @property string $modified_date
 * @property boolean $finished
 */
class PengaduanPraMediasi extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pengaduan_pra_mediasi';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_pengaduan'], 'required'],
            [['id_pengaduan'], 'integer'],
            [['data_para_pihak', 'pokok_permasalahan', 'objek_mediasi'], 'string'],
            [['created_date', 'modified_date'], 'safe'],
            [['finished'], 'boolean'],
            [['id_pengaduan'], 'exist', 'skipOnError' => true, 'targetClass' => PengaduanRegistrasi::className(), 'targetAttribute' => ['id_pengaduan' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_pengaduan' => Yii::t('app', 'Id Pengaduan'),
            'data_para_pihak' => Yii::t('app', 'Data Para Pihak'),
            'pokok_permasalahan' => Yii::t('app', 'Pokok Permasalahan'),
            'objek_mediasi' => Yii::t('app', 'Objek Mediasi'),
            'id' => Yii::t('app', 'ID'),
            'created_date' => Yii::t('app', 'Created Date'),
            'modified_date' => Yii::t('app', 'Modified Date'),
            'finished' => Yii::t('app', 'Finished'),
        ];
    }

    /**
     * @inheritdoc
     * @return PengaduanPraMediasiQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PengaduanPraMediasiQuery(get_called_class());
    }
}
