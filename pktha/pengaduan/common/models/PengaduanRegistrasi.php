<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "pengaduan_registrasi".
 *
 * @property string $nama_pengadu
 * @property string $no_telepon_pengadu
 * @property string $alamat_lengkap_pengadu
 * @property string $email_pengadu
 * @property integer $id_tipe_konflik
 * @property string $pihak_berkonflik_1
 * @property string $pihak_berkonflik_2
 * @property string $pihak_berkonflik_3
 * @property integer $id_wilayah_konflik
 * @property integer $id_provinsi_konflik
 * @property integer $id_kota_kabupaten_konflik
 * @property integer $id_kecamatan_konflik
 * @property integer $id_desa_kelurahan_konflik
 * @property double $latitude_daerah_konflik
 * @property double $longitude_daerah_konflik
 * @property string $fungsi_kawasan_konflik
 * @property double $luas_kawasan_konflik
 * @property string $id
 * @property string $created_date
 * @property string $modified_date
 * @property string $id_jenis_pelapor
 * @property string $kode_pengaduan
 * @property string $kunci_pengaduan
 * @property string $teks_provinsi_konflik
 * @property string $teks_kota_kabupaten_konflik
 * @property string $teks_kecamatan_konflik
 * @property string $teks_desa_kelurahan_konflik
 * @property string $tanggal_pengaduan 
 */
class PengaduanRegistrasi extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pengaduan_registrasi';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
        [[
        'nama_pengadu',
        'alamat_lengkap_pengadu',
        'email_pengadu',
        'pihak_berkonflik_1',
        'pihak_berkonflik_2',
        'pihak_berkonflik_3',
        'teks_provinsi_konflik',
        'teks_kota_kabupaten_konflik',
        'teks_kecamatan_konflik',
        'teks_desa_kelurahan_konflik',
        'fungsi_kawasan_konflik',
        'kode_pengaduan',
        'kunci_pengaduan'], 'string'],
        [[
        'id_tipe_konflik',
        'id_wilayah_konflik',
        'id_provinsi_konflik',
        'id_kota_kabupaten_konflik',
        'id_kecamatan_konflik',
        'id_desa_kelurahan_konflik',
        'id_jenis_pelapor'], 'integer'],
        [[
        'no_telepon_pengadu',
        'latitude_daerah_konflik',
        'longitude_daerah_konflik',
        'luas_kawasan_konflik'], 'number'],
        [[
        'created_date',
        'modified_date',
        'tanggal_pengaduan'], 'safe'],
        [[
        'nama_pengadu',
        'no_telepon_pengadu',
        'id_provinsi_konflik',
        'alamat_lengkap_pengadu'], 'required'],
        [['id_desa_kelurahan_konflik'], 'exist', 'skipOnError' => true, 'targetClass' => DesaKelurahan::className(), 'targetAttribute' => ['id_desa_kelurahan_konflik' => 'id']],
        [['id_jenis_pelapor'], 'exist', 'skipOnError' => true, 'targetClass' => JenisPelapor::className(), 'targetAttribute' => ['id_jenis_pelapor' => 'id']],
        [['id_kecamatan_konflik'], 'exist', 'skipOnError' => true, 'targetClass' => Kecamatan::className(), 'targetAttribute' => ['id_kecamatan_konflik' => 'id']],
        [['id_kota_kabupaten_konflik'], 'exist', 'skipOnError' => true, 'targetClass' => KotaKabupaten::className(), 'targetAttribute' => ['id_kota_kabupaten_konflik' => 'id']],
        [['id_provinsi_konflik'], 'exist', 'skipOnError' => true, 'targetClass' => Provinsi::className(), 'targetAttribute' => ['id_provinsi_konflik' => 'id']],
        [['id_tipe_konflik'], 'exist', 'skipOnError' => true, 'targetClass' => TipeKonflik::className(), 'targetAttribute' => ['id_tipe_konflik' => 'id']],
        [['id_wilayah_konflik'], 'exist', 'skipOnError' => true, 'targetClass' => Wilayah::className(), 'targetAttribute' => ['id_wilayah_konflik' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
        'nama_pengadu' => Yii::t('app', 'Nama Pengadu (wajib)'),
        'no_telepon_pengadu' => Yii::t('app', 'No Telepon Pengadu (wajib)'),
        'alamat_lengkap_pengadu' => Yii::t('app', 'Alamat Lengkap Pengadu (wajib)'),
        'email_pengadu' => Yii::t('app', 'Alamat email Pengadu'),
        'id_tipe_konflik' => Yii::t('app', 'Tipologi Konflik'),
        'pihak_berkonflik_1' => Yii::t('app', 'Pihak Berkonflik Pertama'),
        'pihak_berkonflik_2' => Yii::t('app', 'Pihak Berkonflik Kedua'),
        'pihak_berkonflik_3' => Yii::t('app', 'Pihak Berkonflik Ketiga'),
        'id_wilayah_konflik' => Yii::t('app', 'Wilayah Konflik'),
        'id_provinsi_konflik' => Yii::t('app', 'Provinsi Konflik (wajib)'),
        'id_kota_kabupaten_konflik' => Yii::t('app', 'Kota atau Kabupaten Konflik'),
        'id_kecamatan_konflik' => Yii::t('app', 'Kecamatan Konflik'),
        'id_desa_kelurahan_konflik' => Yii::t('app', 'Desa atau Kelurahan Konflik'),
        'latitude_daerah_konflik' => Yii::t('app', 'Latitude Daerah Konflik'),
        'longitude_daerah_konflik' => Yii::t('app', 'Longitude Daerah Konflik'),
        'fungsi_kawasan_konflik' => Yii::t('app', 'Fungsi Kawasan Konflik'),
        'luas_kawasan_konflik' => Yii::t('app', 'Luas Kawasan Konflik (ha)'),
        'id' => Yii::t('app', 'ID'),
        'created_date' => Yii::t('app', 'Tanggal Pembuatan Kasus'),
        'modified_date' => Yii::t('app', 'Tanggal Modifikasi Terakhir'),
        'id_jenis_pelapor' => Yii::t('app', 'Jenis Pelapor'),
        'kode_pengaduan' => Yii::t('app', 'Kode Pengaduan'),
        ];
    }

    /**
     * @inheritdoc
     * @return PengaduanRegistrasiQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PengaduanRegistrasiQuery(get_called_class());
    }

    public function getTipeKonflik($pid)
    {
        $model = TipeKonflik::findOne($pid);
        return $model['nama_konflik'];
    }

    public function getPath()
    {
        return Yii::getAlias('@updir') . '/pengaduan_konflik/' . $this->id . '-' . $this->kode_pengaduan;
    }

    /**
     * @inheritdoc
     * @return Tahapan the first unfinished Tahapan for this PengaduanRegistrasi
     */
    public function getTahapanFirstUnfinishedId()
    {
        // $wilayah = Yii::$app->user->identity->wilayah;
        $role = Yii::$app->user->identity->role;
        $desk = null;
        
        /* if ($role < 30) {
            // $desk = PengaduanDeskStudy::findOne(['id_pengaduan' => $this->id]);
            $desk = PengaduanDeskStudy::find()
                    ->select('*')
                    ->from(PengaduanDeskStudy::tableName())
                    ->where([PengaduanDeskStudy::tableName() . 'id_pengaduan' => $this->id])
                    ->join('LEFT JOIN', PengaduanRegistrasi::tableName(), PengaduanRegistrasi::tableName() . '.id = ' . PengaduanDeskStudy::tableName() . '.id_pengaduan')
                    ->where([PengaduanRegistrasi::tableName() . '.id_wilayah_konflik' => $wilayah])
                    ->one();
        } else { */
            $desk = PengaduanDeskStudy::findOne(['id_pengaduan' => $this->id]);
        // }
        
        $assesmen = PengaduanAssesmen::findOne(['id_pengaduan' => $this->id]);
        if ((isset($assesmen) && $assesmen->dialihkan === true) || (isset($desk) && $desk->dialihkan === true)) {
            return array(
                'nama' => 'Dialihkan',
                'warna' => 'black');
        }
        $tahap = Tahapan::getTahapanByProses('pengaduan');
        for ($i = 1; $i < count($tahap) - 1; $i++) {
            $row = $tahap[$i]['model']::findOne(['id_pengaduan' => $this->id]);
            if ($row['finished'] === null || $row['finished'] == false) {
                return $tahap[$i];
            }
        }
        return $tahap[count($tahap) - 1];
    }

}
