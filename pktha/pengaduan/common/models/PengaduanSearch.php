<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Pengaduan;
use yii\data\Sort;


/**
 * PengaduanSearch represents the model behind the search form about `common\models\Pengaduan`.
 */
class PengaduanSearch extends Pengaduan
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_tahapan', 'status', 'tipe_identitas', 'kabkota', 'provinsi', 'created_at', 'updated_at', 'user_id'], 'integer'],
            [['kode', 'nama_identitas', 'telepon_identitas', 'email_identitas', 'alamat_identitas', 'sarana_pengaduan', 'nomor_surat', 'tgl_surat', 'tgl_penerima_pengaduan', 'tgl_penerima_pktha', 'penerima_pengaduan', 'pihak_berkonflik', 'lokasi_konflik', 'fungsi_kawasan', 'luasan_kawasan', 'tipologi_kasus', 'status_kawasan', 'resume', 'tutuntan_pengaduan', 'upaya', 'pihak_terlibat', 'rentang_waktu'], 'safe'],
            [['lot_perkiraaan_lokasi', 'lang_perkiraan_lokasi'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Pengaduan::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['id'=>SORT_DESC]]

        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'id_tahapan' => $this->id_tahapan,
            'status' => $this->status,
            'tipe_identitas' => $this->tipe_identitas,
            'tgl_surat' => $this->tgl_surat,
            'tgl_penerima_pengaduan' => $this->tgl_penerima_pengaduan,
            'tgl_penerima_pktha' => $this->tgl_penerima_pktha,
            'kabkota' => $this->kabkota,
            'provinsi' => $this->provinsi,
            'lot_perkiraaan_lokasi' => $this->lot_perkiraaan_lokasi,
            'lang_perkiraan_lokasi' => $this->lang_perkiraan_lokasi,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'user_id' => $this->user_id,
            'deleted' =>0
        ]);

        $query->andFilterWhere(['like', 'kode', $this->kode])
            ->andFilterWhere(['like', 'nama_identitas', $this->nama_identitas])
            ->andFilterWhere(['like', 'telepon_identitas', $this->telepon_identitas])
            ->andFilterWhere(['like', 'email_identitas', $this->email_identitas])
            ->andFilterWhere(['like', 'alamat_identitas', $this->alamat_identitas])
            ->andFilterWhere(['like', 'sarana_pengaduan', $this->sarana_pengaduan])
            ->andFilterWhere(['like', 'nomor_surat', $this->nomor_surat])
            ->andFilterWhere(['like', 'penerima_pengaduan', $this->penerima_pengaduan])
            ->andFilterWhere(['like', 'pihak_berkonflik', $this->pihak_berkonflik])
            ->andFilterWhere(['like', 'lokasi_konflik', $this->lokasi_konflik])
            ->andFilterWhere(['like', 'fungsi_kawasan', $this->fungsi_kawasan])
            ->andFilterWhere(['like', 'luasan_kawasan', $this->luasan_kawasan])
            ->andFilterWhere(['like', 'tipologi_kasus', $this->tipologi_kasus])
            ->andFilterWhere(['like', 'status_kawasan', $this->status_kawasan])
            ->andFilterWhere(['like', 'resume', $this->resume])
            ->andFilterWhere(['like', 'tutuntan_pengaduan', $this->tutuntan_pengaduan])
            ->andFilterWhere(['like', 'upaya', $this->upaya])
            ->andFilterWhere(['like', 'pihak_terlibat', $this->pihak_terlibat])
            ->andFilterWhere(['like', 'rentang_waktu', $this->rentang_waktu]);

        return $dataProvider;
    }
    public function searchdeleted($params)
    {
        $query = Pengaduan::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['id'=>SORT_DESC]]

        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'id_tahapan' => $this->id_tahapan,
            'status' => $this->status,
            'tipe_identitas' => $this->tipe_identitas,
            'tgl_surat' => $this->tgl_surat,
            'tgl_penerima_pengaduan' => $this->tgl_penerima_pengaduan,
            'tgl_penerima_pktha' => $this->tgl_penerima_pktha,
            'kabkota_konflik' => $this->kabkota_konflik,
            'provinsi_konflik' => $this->provinsi_konflik,
            'lot_perkiraaan_lokasi' => $this->lot_perkiraaan_lokasi,
            'lang_perkiraan_lokasi' => $this->lang_perkiraan_lokasi,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'user_id' => $this->user_id,
            'deleted' =>1
        ]);

        $query->andFilterWhere(['like', 'kode', $this->kode])
            ->andFilterWhere(['like', 'nama_identitas', $this->nama_identitas])
            ->andFilterWhere(['like', 'telepon_identitas', $this->telepon_identitas])
            ->andFilterWhere(['like', 'email_identitas', $this->email_identitas])
            ->andFilterWhere(['like', 'alamat_identitas', $this->alamat_identitas])
            ->andFilterWhere(['like', 'sarana_pengaduan', $this->sarana_pengaduan])
            ->andFilterWhere(['like', 'nomor_surat', $this->nomor_surat])
            ->andFilterWhere(['like', 'penerima_pengaduan', $this->penerima_pengaduan])
            ->andFilterWhere(['like', 'pihak_berkonflik', $this->pihak_berkonflik])
            ->andFilterWhere(['like', 'lokasi_konflik', $this->lokasi_konflik])
            ->andFilterWhere(['like', 'fungsi_kawasan', $this->fungsi_kawasan])
            ->andFilterWhere(['like', 'luasan_kawasan', $this->luasan_kawasan])
            ->andFilterWhere(['like', 'tipologi_kasus', $this->tipologi_kasus])
            ->andFilterWhere(['like', 'status_kawasan', $this->status_kawasan])
            ->andFilterWhere(['like', 'resume', $this->resume])
            ->andFilterWhere(['like', 'tutuntan_pengaduan', $this->tutuntan_pengaduan])
            ->andFilterWhere(['like', 'upaya', $this->upaya])
            ->andFilterWhere(['like', 'pihak_terlibat', $this->pihak_terlibat])
            ->andFilterWhere(['like', 'rentang_waktu', $this->rentang_waktu]);

        return $dataProvider;
    }
}
