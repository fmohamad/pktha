<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%pengusulan_monev}}".
 *
 * @property string $id
 * @property string $id_pengusulan
 * @property string $modified_date
 * @property string $created_date
 * @property string $perkembangan_hutan_adat
 * @property string $tanggal_monitoring_dan_evaluasi
 * @property string $komoditi_hutan_adat
 * @property string $komoditi_unggulan_hutan_adat
 * @property string $update_kelembagaan_mha
 * @property boolean $finished
 */
class PengusulanMonev extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%pengusulan_monev}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_pengusulan'], 'integer'],
            [['modified_date', 'created_date', 'tanggal_monitoring_dan_evaluasi'], 'safe'],
            [['perkembangan_hutan_adat', 'komoditi_hutan_adat', 'komoditi_unggulan_hutan_adat', 'update_kelembagaan_mha'], 'string'],
            [['finished'], 'boolean'],
            [['id_pengusulan'], 'exist', 'skipOnError' => true, 'targetClass' => PengusulanRegistrasi::className(), 'targetAttribute' => ['id_pengusulan' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_pengusulan' => Yii::t('app', 'Id Pengusulan'),
            'modified_date' => Yii::t('app', 'Modified Date'),
            'created_date' => Yii::t('app', 'Created Date'),
            'perkembangan_hutan_adat' => Yii::t('app', 'Perkembangan Hutan Adat'),
            'tanggal_monitoring_dan_evaluasi' => Yii::t('app', 'Tanggal Monitoring dan Evaluasi'),
            'komoditi_hutan_adat' => Yii::t('app', 'Komoditi Hutan Adat'),
            'komoditi_unggulan_hutan_adat' => Yii::t('app', 'Komoditi Unggulan Hutan Adat'),
            'update_kelembagaan_mha' => Yii::t('app', 'Update Kelembagaan MHA'),
            'finished' => Yii::t('app', 'Selesai'),
        ];
    }

    /**
     * @inheritdoc
     * @return PengusulanMonevQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PengusulanMonevQuery(get_called_class());
    }
}
