<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\PengusulanMonev;

/**
 * PengusulanMonevSearch represents the model behind the search form about `\common\models\PengusulanMonev`.
 */
class PengusulanMonevSearch extends PengusulanMonev
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_pengusulan'], 'integer'],
            [['modified_date', 'created_date', 'perkembangan_hutan_adat', 'tanggal_monitoring_dan_evaluasi', 'komoditi_hutan_adat', 'komoditi_unggulan_hutan_adat', 'update_kelembagaan_mha'], 'safe'],
            [['finished'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PengusulanMonev::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'id_pengusulan' => $this->id_pengusulan,
            'modified_date' => $this->modified_date,
            'created_date' => $this->created_date,
            'tanggal_monitoring_dan_evaluasi' => $this->tanggal_monitoring_dan_evaluasi,
            'finished' => $this->finished,
        ]);
        
        $query->join('LEFT JOIN', '{{%pengusulan_hutan_adat_II}}', '{{%pengusulan_hutan_adat_II}}.id_pengusulan = {{%pengusulan_hutan_adat_V}}.id_pengusulan');
        $query->join('LEFT JOIN', '{{%pengusulan_hutan_adat_III}}', '{{%pengusulan_hutan_adat_III}}.id_pengusulan = {{%pengusulan_hutan_adat_V}}.id_pengusulan');
        $query->join('LEFT JOIN', '{{%pengusulan_hutan_adat_IV}}', '{{%pengusulan_hutan_adat_IV}}.id_pengusulan = {{%pengusulan_hutan_adat_V}}.id_pengusulan');
        $query->where(['{{%pengusulan_hutan_adat_II}}.finished' => true]);
        $query->where(['{{%pengusulan_hutan_adat_III}}.finished' => true]);
        $query->where(['{{%pengusulan_hutan_adat_IV}}.finished' => true]);

        $query->andFilterWhere(['like', 'perkembangan_hutan_adat', $this->perkembangan_hutan_adat])
            ->andFilterWhere(['like', 'komoditi_hutan_adat', $this->komoditi_hutan_adat])
            ->andFilterWhere(['like', 'komoditi_unggulan_hutan_adat', $this->komoditi_unggulan_hutan_adat])
            ->andFilterWhere(['like', 'update_kelembagaan_mha', $this->update_kelembagaan_mha]);

        return $dataProvider;
    }
}
