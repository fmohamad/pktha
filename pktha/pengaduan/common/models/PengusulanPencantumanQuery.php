<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[PengusulanPencantuman]].
 *
 * @see PengusulanPencantuman
 */
class PengusulanPencantumanQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return PengusulanPencantuman[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return PengusulanPencantuman|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
