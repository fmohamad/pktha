<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "pengusulan_permohonan".
 *
 * @property integer $id
 * @property integer $id_pengusulan
 * @property boolean $dikembalikan
 * @property boolean $finished
 *
 * @property PengusulanRegistrasi $idPengusulan
 */
class PengusulanPermohonan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pengusulan_permohonan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id'], 'integer'],
            [['id_pengusulan'], 'required'],
            [['id_pengusulan'], 'integer'],
            [['created_date', 'modified_date'], 'safe'],
            [['dikembalikan', 'finished'], 'boolean'],
            [['id_pengusulan'], 'exist', 'skipOnError' => true, 'targetClass' => PengusulanRegistrasi::className(), 'targetAttribute' => ['id_pengusulan' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_pengusulan' => 'Id Pengusulan',
            'dikembalikan' => 'Dikembalikan',
            'created_date' => Yii::t('app', 'Tanggal Pembuatan Kasus'),
            'modified_date' => Yii::t('app', 'Tanggal Modifikasi Terakhir'),
            'finished' => 'Finished',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdPengusulan()
    {
        return $this->hasOne(PengusulanRegistrasi::className(), ['id' => 'id_pengusulan']);
    }

    /**
     * @inheritdoc
     * @return PengusulanPermohonanQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PengusulanPermohonanQuery(get_called_class());
    }
}
