<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[PengusulanRegistrasi]].
 *
 * @see PengusulanRegistrasi
 */
class PengusulanRegistrasiQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return PengusulanRegistrasi[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return PengusulanRegistrasi|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     *
     * @param type $id
     * @return type
     */
    public function getById($id)
    {
        return $this->select(['*'])->where(['id'=>$id])->one();
    }
}
