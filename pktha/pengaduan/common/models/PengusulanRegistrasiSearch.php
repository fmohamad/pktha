<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\PengusulanRegistrasi;

/**
 * PengusulanRegistrasiSearch represents the model behind the search form about `common\models\PengusulanRegistrasi`.
 */
class PengusulanRegistrasiSearch extends PengusulanRegistrasi
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[
                'kode_pengusulan',
                'nama_pengusul',
                'alamat_lengkap_pengusul',
                'email_pengusul',
                'teks_provinsi',
                'teks_kota_kabupaten',
                'teks_kecamatan',
                'teks_desa_kelurahan',
                'dusun',
                'keterangan',
                'form_filename',
                'profil_mha_filename',
                'surat_kuasa_filename',
                'surat_pernyataan_filename',
                'produk_hukum_daerah_filename',
                'peta_filename',
                'identitas_ketua_adat_filename', 
                'nama_mha'], 'string'],
            [[
                'no_ktp_pengusul',
                'no_telepon_pengusul'], 'number'],
            [[
                'id_wilayah',
                'id_provinsi',
                'id_kota_kabupaten',
                'id_kecamatan',
                'id_desa_kelurahan'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     *
     * @var type string
     */
    public $keyword;

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $conds = null)
    {
        $query = PengusulanRegistrasi::find();

        // add conditions that should always apply here
        if (isset($conds)) {
            foreach ($conds as $key => $item) {
                $query->andWhere(['in', $key, $item]);
            }
        }

        $dataProvider = new ActiveDataProvider(['query' => $query,]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $this->id_wilayah = isset($params['id_wilayah']) ? $params['id_wilayah'] : $this->id_wilayah;

        $query->andFilterWhere(
            [
            'id' => $this->id,
            'no_telepon_pengusul' => $this->no_telepon_pengusul,
            'created_date' => $this->created_date,
            'modified_date' => $this->modified_date,
            'id_wilayah' => $this->id_wilayah,
            'id_provinsi' => $this->id_provinsi,
            'id_kota_kabupaten' => $this->id_kota_kabupaten,
            'id_kecamatan' => $this->id_kecamatan,
            'id_desa_kelurahan' => $this->id_desa_kelurahan,
            ]
        );

        if (!isset($this->keyword)) {
            $query->andFilterWhere(['ilike', 'nama_pengusul', $this->nama_pengusul])
                ->andFilterWhere(['ilike', 'alamat_lengkap_pengusul', $this->alamat_lengkap_pengusul])
                ->andFilterWhere(['ilike', 'email_pengusul', $this->email_pengusul])
                ->andFilterWhere(['ilike', 'teks_kota_kabupaten', $this->teks_kota_kabupaten])
                ->andFilterWhere(['ilike', 'teks_kecamatan', $this->teks_kecamatan])
                ->andFilterWhere(['ilike', 'teks_desa_kelurahan', $this->teks_desa_kelurahan])
                ->andFilterWhere(['ilike', 'dusun', $this->dusun])
                ->andFilterWhere(['ilike', 'keterangan', $this->keterangan])
                ->andFilterWhere(['ilike', 'form_filename', $this->form_filename])
                ->andFilterWhere(['ilike', 'profil_mha_filename', $this->profil_mha_filename])
                ->andFilterWhere(['ilike', 'surat_kuasa_filename', $this->surat_kuasa_filename])
                ->andFilterWhere(['ilike', 'surat_pernyataan_filename', $this->surat_pernyataan_filename])
                ->andFilterWhere(['ilike', 'produk_hukum_daerah_filename', $this->produk_hukum_daerah_filename])
                ->andFilterWhere(['ilike', 'peta_filename', $this->peta_filename])
                ->andFilterWhere(['ilike', 'identitas_ketua_adat_filename', $this->identitas_ketua_adat_filename])
                ->andFilterWhere(['ilike', 'nama_mha', $this->nama_mha]);
        } else {
            $query->orFilterWhere(['ilike', 'nama_pengusul', $this->keyword])
                ->orFilterWhere(['ilike', 'alamat_lengkap_pengusul', $this->keyword])
                ->orFilterWhere(['ilike', 'email_pengusul', $this->keyword])
                ->orFilterWhere(['ilike', 'teks_kota_kabupaten', $this->keyword])
                ->orFilterWhere(['ilike', 'teks_kecamatan', $this->keyword])
                ->orFilterWhere(['ilike', 'teks_desa_kelurahan', $this->keyword])
                ->orFilterWhere(['ilike', 'dusun', $this->keyword])
                ->orFilterWhere(['ilike', 'keterangan', $this->keyword])
                ->orFilterWhere(['ilike', 'form_filename', $this->keyword])
                ->orFilterWhere(['ilike', 'profil_mha_filename', $this->keyword])
                ->orFilterWhere(['ilike', 'surat_kuasa_filename', $this->keyword])
                ->orFilterWhere(['ilike', 'surat_pernyataan_filename', $this->keyword])
                ->orFilterWhere(['ilike', 'produk_hukum_daerah_filename', $this->keyword])
                ->orFilterWhere(['ilike', 'peta_filename', $this->peta_filename])
                ->orFilterWhere(['ilike', 'identitas_ketua_adat_filename', $this->keyword])
                ->orFilterWhere(['ilike', 'nama_mha', $this->keyword]);
        }

        return $dataProvider;
    }
}
