<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%pengusulan_validasi}}".
 *
 * @property string $id
 * @property string $id_pengusulan
 * @property string $created_date
 * @property string $modified_date
 * @property boolean $finished
 * @property string $nama_pemohon
 * @property string $alamat_pemohon
 * @property string $nama_mha
 * @property string $jabatan_pemohon
 * @property string $desa_kelurahan
 * @property string $kecamatan
 * @property string $kota_kabupaten
 * @property string $provinsi
 * @property string $das
 * @property double $luas
 * @property boolean $data_subjek_hutan_hak
 * @property boolean $kelengkapan_data_subjek_hutan_hak
 * @property boolean $ktp_pemohon
 * @property boolean $akte_perusahaan
 * @property boolean $validitas_akte_perusahaan
 * @property boolean $status_pengakuan
 * @property boolean $perda_pengakuan_mha
 * @property boolean $data_objek_hutan_hak
 * @property boolean $kelengkapan_data_objek_hutan_hak
 * @property boolean $bukti_kepemilikan
 * @property boolean $perda_wilayah_adat
 * @property boolean $peta_hutan_hak
 * @property boolean $peta_hutan_adat
 * @property boolean $dikembalikan
 * @property string $catatan
 * @property string $batas_utara
 * @property string $batas_selatan
 * @property string $batas_timur
 * @property string $batas_barat
 * @property string $bukti_penguasaan_tanah
 * @property string $status_kawasan
 * @property string $no_ktp_pemohon
 * @property boolean $dikembalikan
 */
class PengusulanValidasi extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%pengusulan_validasi}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_pengusulan'], 'integer'],
            [[
                'created_date',
                'modified_date'], 'safe'],
            [[
                'created_date',
                'modified_date'], 'default', 'value' => function ($model, $attribute) {
                    return \common\utils\DateHelper::GetCurrentDate();
                }],
            [[
                'finished',
                'data_subjek_hutan_hak',
                'kelengkapan_data_subjek_hutan_hak',
                'ktp_pemohon', 'akte_perusahaan',
                'validitas_akte_perusahaan',
                'status_pengakuan',
                'perda_pengakuan_mha',
                'data_objek_hutan_hak',
                'kelengkapan_data_objek_hutan_hak',
                'bukti_kepemilikan',
                'perda_wilayah_adat',
                'peta_hutan_hak',
                'peta_hutan_adat',
                'dikembalikan'], 'boolean'],
            [[
                'finished',
                'data_subjek_hutan_hak',
                'kelengkapan_data_subjek_hutan_hak',
                'ktp_pemohon', 'akte_perusahaan',
                'validitas_akte_perusahaan',
                'status_pengakuan',
                'perda_pengakuan_mha',
                'data_objek_hutan_hak',
                'kelengkapan_data_objek_hutan_hak',
                'bukti_kepemilikan',
                'perda_wilayah_adat',
                'peta_hutan_hak',
                'peta_hutan_adat',
                'dikembalikan'], 'default', 'value' => false],
            [[
                'nama_pemohon',
                'alamat_pemohon',
                'nama_mha',
                'jabatan_pemohon',
                'desa_kelurahan',
                'kecamatan',
                'kota_kabupaten',
                'provinsi',
                'das',
                'catatan',
                'batas_utara',
                'batas_selatan',
                'batas_timur',
                'batas_barat',
                'bukti_penguasaan_tanah',
                'status_kawasan'], 'string'],
            [[
                'nama_pemohon',
                'alamat_pemohon',
                'nama_mha',
                'jabatan_pemohon',
                'desa_kelurahan',
                'kecamatan',
                'kota_kabupaten',
                'provinsi',
                'das',
                'catatan',
                'batas_utara',
                'batas_selatan',
                'batas_timur',
                'batas_barat',
                'bukti_penguasaan_tanah',
                'status_kawasan'], 'default'],
            [[
                'luas',
                'no_ktp_pemohon'], 'number'],
            [['id_pengusulan'], 'exist', 'skipOnError' => true, 'targetClass' => PengusulanRegistrasi::className(), 'targetAttribute' => ['id_pengusulan' => 'id']],
            ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_pengusulan' => Yii::t('app', 'Id Pengusulan'),
            'created_date' => Yii::t('app', 'Created Date'),
            'modified_date' => Yii::t('app', 'Modified Date'),
            'finished' => Yii::t('app', 'Selesai'),
            'nama_pemohon' => Yii::t('app', 'Nama Pemohon'),
            'alamat_pemohon' => Yii::t('app', 'Alamat Pemohon'),
            'nama_mha' => Yii::t('app', 'Nama MHA (khusus MHA)'),
            'jabatan_pemohon' => Yii::t('app', 'Jabatan Pemohon (khusus Badan Hukum dan MHA)'),
            'desa_kelurahan' => Yii::t('app', 'Desa / Kelurahan'),
            'kecamatan' => Yii::t('app', 'Kecamatan'),
            'kota_kabupaten' => Yii::t('app', 'Kota / Kabupaten'),
            'provinsi' => Yii::t('app', 'Provinsi'),
            'das' => Yii::t('app', 'DAS (Daerah Aliran Sungai)'),
            'luas' => Yii::t('app', 'Luas'),
            'data_subjek_hutan_hak' => Yii::t('app', 'Data Subjek Hutan HAK (Ada atau Tidak)'),
            'kelengkapan_data_subjek_hutan_hak' => Yii::t('app', 'Kelengkapan Data Subjek Hutan Hak (Lengkap atau Tidak)'),
            'ktp_pemohon' => Yii::t('app', 'KTP Pemohon'),
            'akte_perusahaan' => Yii::t('app', 'Akte Perusahaan (Ada atau Tidak)'),
            'validitas_akte_perusahaan' => Yii::t('app', 'Akte Perusahaan (Valid atau Tidak)'),
            'status_pengakuan' => Yii::t('app', 'Status Pengakuan (khusus MHA, diakui atau tidak)'),
            'perda_pengakuan_mha' => Yii::t('app', 'Perda / SK Bupati Pengakuan MHA (Ada atau Tidak)'),
            'data_objek_hutan_hak' => Yii::t('app', 'Data Objek Hutan Hak (Ada atau Tidak)'),
            'kelengkapan_data_objek_hutan_hak' => Yii::t('app', 'Data Objek Hutan Hak (Lengkap atau Tidak)'),
            'bukti_kepemilikan' => Yii::t('app', 'Bukti Kepemilikan'),
            'perda_wilayah_adat' => Yii::t('app', 'Perda / SK Bupati wilayah adat/hutan adat (khusus MHA)'),
            'peta_hutan_hak' => Yii::t('app', 'Peta Hutan HAK'),
            'peta_hutan_adat' => Yii::t('app', 'Peta Hutan Adat yang ditandatangani Kepala Daerah'),
            'catatan' => Yii::t('app', 'Catatan'),
            'batas_utara' => Yii::t('app', 'Batas Utara'),
            'batas_selatan' => Yii::t('app', 'Batas Selatan'),
            'batas_timur' => Yii::t('app', 'Batas Timur'),
            'batas_barat' => Yii::t('app', 'Batas Barat'),
            'bukti_penguasaan_tanah' => Yii::t('app', 'Bukti Penguasaan Tanah'),
            'status_kawasan' => Yii::t('app', 'Status Kawasan'),
            'no_ktp_pemohon' => Yii::t('app', 'No KTP Pemohon'),
            'dikembalikan' => Yii::t('app', 'Kesimpulan hasil Penilaian Dokumen Permohonan Hutan Hak '),
        ];
    }

    /**
     * @inheritdoc
     * @return PengusulanValidasiQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PengusulanValidasiQuery(get_called_class());
    }
}
