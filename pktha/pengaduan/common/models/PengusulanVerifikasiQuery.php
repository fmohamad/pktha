<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[PengusulanVerifikasi]].
 *
 * @see PengusulanVerifikasi
 */
class PengusulanVerifikasiQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return PengusulanVerifikasi[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return PengusulanVerifikasi|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
