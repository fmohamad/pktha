<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db;

class Potensikonflik extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'tmp_potensi_konflik_2';
    }

    public function getProvinsi() {
        return $this->find()->select('UPPER(provinsi) AS provinsi')->groupBy('provinsi')->orderBy('provinsi')->asArray()->all();
    }

    public function getProvinsiq() {
        return $this->find()->select('provinsi')->groupBy('provinsi')->orderBy('provinsi')->asArray()->all();
    }

    public function getKabkota($provinsi) {
        return $this->find()->select('kabkot')->where('provinsi = :provinsi', [':provinsi' => $provinsi])->groupBy('kabkot')->orderBy('kabkot')->asArray()->all();
    }

    public function getKecamatan($kabkot) {
        return $this->find()->select('kecamatan')->where('kabkot = :kabkot', [':kabkot' => $kabkot])->groupBy('kecamatan')->orderBy('kecamatan')->asArray()->all();
    }

    public function getDraw($kecamatan) {

        $sql = "SELECT 
        row_to_json (fc) AS geojson
        FROM
        (
        SELECT
        'FeatureCollection' AS TYPE,
        array_to_json (ARRAY_AGG(f)) AS features
        FROM
        (
        SELECT
        'Feature' AS TYPE,
        ST_AsGeoJSON (lg.geom) :: json AS geometry,
        row_to_json (
        (
        SELECT
        l
        FROM
        (
        SELECT                                      
        fungsi_kws AS fungsi_kws,
        desa AS desa,
        kecamatan AS kecamatan,
        nama_kws AS nama_kws,
        desa AS nama_des,
        kabkot AS kabkot,
        provinsi AS provinsi
        ) AS l
        )
        ) AS properties
        FROM
        tmp_potensi_konflik_2 AS lg WHERE kabkot='" . $kecamatan . "'
        ) AS f
        ) AS fc";

        return Yii::$app->db->createCommand($sql)->queryOne();
    }

    public function getKecamatandraw($kecamatan) {

        $a = array();
        // ST_AsGeoJSON(ST_Union(geom)) as singlegeom
        $b = $this->find()->select('ST_AsGeoJSON(ST_Union(geom)) as geojson')->where('kabkot = :kecamatan', [':kecamatan' => $kecamatan])->asArray()->one();
        // $a=array("type"=>"FeatureCollection","features"=>type);
        // $bb =json_decode($b);
        // var_dump($b);
        // $jx=array();
        // foreach ($b as $kx) {
        //     $jx[] = json_decode($kx['geojson'],true);
        // }
        // var_dump($jx);
        // var_dump($b);

        $bx['type'] = "Feature";
        $bx['geometry'] = json_decode($b['geojson'], true);


        // $a['type']='FeatureCollection';
        // $a['features']=array($bx);
        $bx['properties'] = array("name" => "MultiPolygon");



// "properties": {
//     "name": "MultiPolygon",
//     "style": {
//         color: "black",
//         opacity: 1,
//         fillColor: "white",
//         fillOpacity: 1
//     }
//   }
        // $a['features']='geometry';
        return $bx;
    }

}
