<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
/**
 * This is the model class for table "rekomendasi".
 *
 * @property integer $id
 * @property integer $jenispenyelesaian_id
 * @property string $hasil_penyelesaian
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $user_id
 */
class Rekomendasi extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rekomendasi';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['jenispenyelesaian_id', 'hasil_penyelesaian'], 'required'],
            [['jenispenyelesaian_id', 'created_at', 'updated_at', 'user_id'], 'integer'],
            [['hasil_penyelesaian'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'jenispenyelesaian_id' => 'Tindak Lanjut Penanganan Konflik',
            'hasil_penyelesaian' => 'Hasil Penyelesaian',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'user_id' => 'User ID',
        ];
    }
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    public function beforeSave($insert)
    {        
        if (parent::beforeSave($insert)) {
            if($this->isNewRecord){  
                $pengaduan = Pengaduan::findOne($this->pengaduan_id);
                $pengaduan->id_tahapan = 4;
                $pengaduan->save();              
                $this->user_id=Yii::$app->user->getId(); 
            }
            return true;
        } else {
            return false;
        }
    }
    public function getJenispenyelesaian()
    {
        return $this->hasOne(Jenispenyelesaian::className(), ['id' => 'jenispenyelesaian_id']);
    }
    // public function getListTindakLanjutPenangananKonflik()
    // {

    //     $x=[1=>'Tindak Lanjut Penanganan Konflik',2=>'Subdit Pemetaan Konflik',3=>'Subdit Penanganan Konflik',4=>'Subdit Penanganan Tenurial',5=>'Subdit Pengakuan Hutan Adat dan Perlindungan Kearifan lokal',6=>'Penegakan Hukum',7=>'Pihak Lain (isian)'];

    //     return $this->hasOne(Jenispenyelesaian::className(), ['id' => 'jenispenyelesaian_id']);
    // }






}
