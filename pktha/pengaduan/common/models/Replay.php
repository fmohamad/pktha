<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "slider".
 *
 * @property integer $id
 * @property string $name
 * @property string $desc
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $img
 */
class Replay extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'replay';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['to','subject','content'], 'required'],
            [['to','cc','subject','content'], 'string'],
            [['created_at', 'updated_at','pengaduan_id'], 'integer'],
            // [['name'], 'string', 'max' => 20],
            // [['img'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'to' => 'To',
            'cc' => 'Cc',
            'subject' => 'Subject',
            'content' => 'Content',            
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',            
        ];
    }
        public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }
}
