<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "statistik_pengusulan".
 *
 * @property string $id
 * @property integer $verifikasi
 * @property integer $validasi
 * @property integer $penetapan_pencantuman
 * @property integer $monev_pemberdayaan
 * @property string $created_date
 */
class StatistikPengusulan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'statistik_pengusulan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id', 'verifikasi', 'validasi', 'penetapan_pencantuman', 'monev_pemberdayaan'], 'integer'],
            [['created_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'verifikasi' => Yii::t('app', 'Verifikasi'),
            'validasi' => Yii::t('app', 'Validasi'),
            'penetapan_pencantuman' => Yii::t('app', 'Penetapan dan Pencantuman'),
            'monev_pemberdayaan' => Yii::t('app', 'Monev dan Pemberdayaan'),
            'created_date' => Yii::t('app', 'Created Date'),
        ];
    }

    /**
     * @inheritdoc
     * @return StatistikPengusulanQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new StatistikPengusulanQuery(get_called_class());
    }
}
