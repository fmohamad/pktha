<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[StatistikPengusulan]].
 *
 * @see StatistikPengusulan
 */
class StatistikPengusulanQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return StatistikPengusulan[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return StatistikPengusulan|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
