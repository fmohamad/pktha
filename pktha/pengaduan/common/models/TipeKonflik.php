<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tipe_konflik".
 *
 * @property string $id
 * @property string $nama_konflik
 */
class TipeKonflik extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tipe_konflik';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama_konflik'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'nama_konflik' => Yii::t('app', 'Nama Konflik'),
        ];
    }

    /**
     * @inheritdoc
     * @return TipeKonflikQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TipeKonflikQuery(get_called_class());
    }
}
