<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[TipeKonflik]].
 *
 * @see TipeKonflik
 */
class TipeKonflikQuery extends \yii\db\ActiveQuery {
    /* public function active()
      {
      return $this->andWhere('[[status]]=1');
      } */

    /**
     * @inheritdoc
     * @return TipeKonflik[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return TipeKonflik|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }

    public function getById($id_konflik) {
        return $this->select(['*'])->where(['id'=>$id])->one();;
    }

}
