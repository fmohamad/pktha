<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%wilayah}}".
 *
 * @property string $nama_wilayah
 * @property string $id
 * @property double $latitude
 * @property double $longitude
 * @property double $zoom
 */
class Wilayah extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%wilayah}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama_wilayah'], 'string'],
            [['latitude', 'longitude', 'zoom'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'nama_wilayah' => Yii::t('app', 'Nama Wilayah'),
            'id' => Yii::t('app', 'Id, AUTO INCREMENT'),
            'latitude' => Yii::t('app', 'Latitude'),
            'longitude' => Yii::t('app', 'Longitude'),
            'zoom' => Yii::t('app', '
'),
        ];
    }

    /**
     * @inheritdoc
     * @return WilayahQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new WilayahQuery(get_called_class());
    }
}
