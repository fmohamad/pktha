<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace common\utils;

/**
 * Description of Statistic
 *
 * @author wizard
 */
class Datapeta
{

    //put your code here

    public static function getJumlahPencantumanHutanAdat()
    {
        $sql = 'SELECT COUNT(id) FROM pencantuman_hutan_adat';
        $result = \Yii::$app->db->createCommand($sql)->queryOne();
        return $result['count'];
    }

    public static function getJumlahPotensiHutanAdat()
    {
        $sql = 'SELECT COUNT(id) FROM potensi_hutan_adat';
        $result = \Yii::$app->db->createCommand($sql)->queryOne();
        return $result['count'];
    }

    public static function getSpasialPencantumanHutanAdat($id)
    {
        $query = "SELECT
            'Feature' As type
            , ST_AsGeoJSON(ST_Simplify(geom, 0.001))::json As geometry
            , row_to_json((SELECT l FROM (SELECT id, kode_prov, masy_adat, fungsi_kws, tgl_adat, ls_adat, keterangan, sk_adat) As l)) As properties
            FROM pencantuman_hutan_adat
            WHERE id = " . $id . "
            ORDER by id asc";

        $geojson = "SELECT row_to_json(fc)
FROM (SELECT 'FeatureCollection' As type, array_to_json(array_agg(f)) As features
FROM ( " . $query . " ) As f ) As fc;";

        $result = \Yii::$app->db->createCommand($geojson)->queryOne();

        return reset($result);
    }

    public static function getSpasialPotensiHutanAdat($id)
    {
        $query = "SELECT
            'Feature' As type
            , ST_AsGeoJSON(ST_Simplify(geom, 0.001))::json As geometry
            , row_to_json((SELECT l FROM (SELECT id, wilayah_ad, prov, kab, luas_ha) As l)) As properties
            FROM potensi_hutan_adat
            WHERE id = " . $id . "
            ORDER by id asc";

        $geojson = "SELECT row_to_json(fc)
FROM (SELECT 'FeatureCollection' As type, array_to_json(array_agg(f)) As features
FROM ( " . $query . " ) As f ) As fc;";

        $result = \Yii::$app->db->createCommand($geojson)->queryOne();

        return reset($result);
    }

    public static function getSpasialProdukHukumProvinsi($id)
    {
        $query = "SELECT
            'Feature' As type
            , ST_AsGeoJSON(ST_Simplify(indo_provinsi.geom, 0.001))::json As geometry
            , row_to_json((SELECT l FROM (SELECT provinsi, instruksi_gubernur, keputusan_bersama,
                peraturan_daerah,
                peraturan_daerah_provinsi, peraturan_gubernur, total_produk, (instruksi_gubernur + keputusan_bersama+
                peraturan_daerah+
                peraturan_daerah_provinsi+ peraturan_gubernur) AS total_provinsi
            ) As l)) As properties
            FROM produk_hukum_provinsi
            JOIN indo_provinsi ON produk_hukum_provinsi.id_provinsi = indo_provinsi.id_1
            WHERE indo_provinsi.id = " . $id . "";

        $geojson = "SELECT row_to_json(fc)
            FROM (SELECT 'FeatureCollection' As type, array_to_json(array_agg(f)) As features
            FROM ( " . $query . " ) As f ) As fc;";

        $result = \Yii::$app->db->createCommand($geojson)->queryOne();

        return reset($result);
    }

    public static function getSpasialProdukHukumKabupaten($id)
    {
        $query = "SELECT
            'Feature' As type
            , ST_AsGeoJSON(ST_Simplify(indo_kota_kab.geom, 0.001))::json As geometry
            , row_to_json((SELECT l FROM (SELECT kabupaten, keputusan_bupati,
                keputusan_walikota, peraturan_bupati, peraturan_daerah, peraturan_daerah_kabupaten,
                peraturan_daerah_kota,
                peraturan_walikota, total_produk,(keputusan_bupati+
                keputusan_walikota+ peraturan_bupati+ peraturan_daerah+ peraturan_daerah_kabupaten+
                peraturan_daerah_kota+
                peraturan_walikota) AS total_kabupaten
            ) As l)) As properties
            FROM produk_hukum_kabupaten
            JOIN indo_kota_kab ON produk_hukum_kabupaten.id_kab_kota = indo_kota_kab.id_kabkota
            WHERE indo_kota_kab.id_prov = " . $id . "";

        $geojson = "SELECT row_to_json(fc)
            FROM (SELECT 'FeatureCollection' As type, array_to_json(array_agg(f)) As features
            FROM ( " . $query . " ) As f ) As fc;";

        $result = \Yii::$app->db->createCommand($geojson)->queryOne();

        return reset($result);
    }

    public static function getSpasialLokasiPengaduan($role = null)
    {
        $sql = "SELECT id_tipe_konflik, pihak_berkonflik_1, pihak_berkonflik_2, pihak_berkonflik_3, id_wilayah_konflik, id_provinsi_konflik, id_kota_kabupaten_konflik, id_kecamatan_konflik, id_desa_kelurahan_konflik,
            latitude_daerah_konflik, longitude_daerah_konflik, pengaduan_registrasi.id, teks_provinsi_konflik, teks_kota_kabupaten_konflik, teks_kecamatan_konflik, teks_desa_kelurahan_konflik,
            nama_provinsi, nama_kota_kabupaten, nama_kecamatan, nama_desa_kelurahan, nama_konflik,
            pengaduan_assesmen.finished AS finished_assesmen, pengaduan_pra_mediasi.finished AS finished_pra_mediasi, pengaduan_mediasi.finished AS finished_mediasi, pengaduan_drafting_mou.finished AS finished_drafting_mou, pengaduan_tanda_tangan_mou.finished AS finished_tanda_tangan_mou
            FROM pengaduan_registrasi
            LEFT OUTER JOIN provinsi ON (provinsi.id = pengaduan_registrasi.id_provinsi_konflik)
            LEFT OUTER JOIN kota_kabupaten ON (kota_kabupaten.id = pengaduan_registrasi.id_kota_kabupaten_konflik)
            LEFT OUTER JOIN kecamatan ON (kecamatan.id = pengaduan_registrasi.id_kecamatan_konflik)
            LEFT OUTER JOIN desa_kelurahan ON (desa_kelurahan.id = pengaduan_registrasi.id_desa_kelurahan_konflik)
            LEFT OUTER JOIN pengaduan_assesmen ON (pengaduan_assesmen.id_pengaduan = pengaduan_registrasi.id)
            LEFT OUTER JOIN pengaduan_pra_mediasi ON (pengaduan_pra_mediasi.id_pengaduan = pengaduan_registrasi.id)
            LEFT OUTER JOIN pengaduan_mediasi ON (pengaduan_mediasi.id_pengaduan = pengaduan_registrasi.id)
            LEFT OUTER JOIN pengaduan_drafting_mou ON (pengaduan_drafting_mou.id_pengaduan = pengaduan_registrasi.id)
            LEFT OUTER JOIN pengaduan_tanda_tangan_mou ON (pengaduan_tanda_tangan_mou.id_pengaduan = pengaduan_registrasi.id)
            LEFT OUTER JOIN tipe_konflik ON (tipe_konflik.id = pengaduan_registrasi.id_tipe_konflik)
";

        if (isset($role)) {
            if ($role >= 30) {
                // Administrator Pusat
            } else if ($role <= 29 && $role >= 20) {
                // Administrator Wilayah
                $id_wilayah = $role - 20;
                $sql .= (' WHERE pengaduan_registrasi.id_wilayah_konflik = ' . $id_wilayah);
            }
        }


        $result = \Yii::$app->db->createCommand($sql);

        $geojson = array(
            'type' => 'FeatureCollection',
            'features' => array()
        );
        foreach ($result->queryAll() as $row) {
            $properties = $row;

            unset($properties['latitude_daerah_konflik']);
            unset($properties['longitude_daerah_konflik']);
            $feature = array(
                'type' => 'Feature',
                'properties' => $properties,
                'geometry' => array(
                    'type' => 'Point',
                    'coordinates' => array(
                        json_decode($row['longitude_daerah_konflik']),
                        json_decode($row['latitude_daerah_konflik'])
                    )
                )
            );
            array_push($geojson['features'], $feature);
        }

        return \yii\helpers\Json::encode($geojson);
    }

    public static function getSpasialKasus2016()
    {
        $sql = "SELECT * FROM kasus_2016";

        $result = \Yii::$app->db->createCommand($sql);

        $geojson = array(
            'type' => 'FeatureCollection',
            'features' => array()
        );
        foreach ($result->queryAll() as $row) {
            $properties = $row;

            unset($properties['latitude']);
            unset($properties['longitude']);
            $feature = array(
                'type' => 'Feature',
                'properties' => $properties,
                'geometry' => array(
                    'type' => 'Point',
                    'coordinates' => array(
                        json_decode($row['longitude']),
                        json_decode($row['latitude'])
                    )
                )
            );
            array_push($geojson['features'], $feature);
        }

        return \yii\helpers\Json::encode($geojson);
    }

    public static function getSpasialKasus2017()
    {

        $sql = "SELECT * FROM kasus_2017";

        $result = \Yii::$app->db->createCommand($sql);

        $geojson = array(
            'type' => 'FeatureCollection',
            'features' => array()
        );
        foreach ($result->queryAll() as $row) {
            $properties = $row;

            unset($properties['latitude']);
            unset($properties['longitude']);
            $feature = array(
                'type' => 'Feature',
                'properties' => $properties,
                'geometry' => array(
                    'type' => 'Point',
                    'coordinates' => array(
                        json_decode($row['longitude']),
                        json_decode($row['latitude'])
                    )
                )
            );
            array_push($geojson['features'], $feature);
        }

        return \yii\helpers\Json::encode($geojson);
    }
}
