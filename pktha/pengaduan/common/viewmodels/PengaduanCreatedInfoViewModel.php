<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace common\viewmodels;

/**
 * Description of PengaduanCreatedInfoViewModel
 *
 * @author wizard
 */
class PengaduanCreatedInfoViewModel extends \yii\base\Model {
    //put your code here
    
    public $id_pengaduan;
    public $kode_pengaduan;
    public $kunci_pengaduan;
    
    public function rules() {
        return [
            [['id_pengaduan', 'kode_pengaduan', 'kunci_pengaduan'], 'string'],
            [['id_pengaduan', 'kode_pengaduan', 'kunci_pengaduan'], 'safe'],
        ];
    }
    
    public function __contruct($_kode_pengaduan = '', $_kunci_pengaduan = '') {
        $this->kode_pengaduan = $_kode_pengaduan;
        $this->kunci_pengaduan = $_kunci_pengaduan;
    }
}
