<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace common\viewmodels;

/**
 * Description of PengaduanNotFoundViewModel
 *
 * @author wizard
 */
class PengaduanNotFoundViewModel extends \yii\base\Model {
    //put your code here
    
    public $kode_pengaduan;
    
    public function rules() {
        return [
            [['kode_pengaduan'], 'string'],
        ];
    }
    
    public function __construct($_kode_pengaduan = '') {
        $this->kode_pengaduan = $_kode_pengaduan;
    }
}
