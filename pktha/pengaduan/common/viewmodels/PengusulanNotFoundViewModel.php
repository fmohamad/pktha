<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace common\viewmodels;

/**
 * Description of PengusulanNotFoundViewModel
 *
 * @author wizard
 */
class PengusulanNotFoundViewModel extends \yii\base\Model {
    //put your code here
    
    public $kode_pengusulan;
    
    public function rules() {
        return [
            [['kode_pengusulan'], 'string'],
        ];
    }
    
    public function __construct($_kode_pengusulan = '') {
        $this->kode_pengusulan = $_kode_pengusulan;
    }
}
