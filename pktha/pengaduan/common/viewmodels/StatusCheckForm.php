<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace common\viewmodels;

/**
 * Description of StatusCheckForm
 *
 * @author wizard
 */
class StatusCheckForm extends \yii\base\Model {
    //put your code here
    
    public $kode_kasus;
    public $kunci_kasus;
    
    public function rules() {
        return [
            [['kode_kasus'], 'string',],
            [['kode_kasus'], 'required',],
            [['kode_kasus'], 'safe',],
            [['kunci_kasus'], 'string',],
            [['kunci_kasus'], 'required',],
            [['kunci_kasus'], 'safe',],
        ];
    }
    
}
