<?php

namespace frontend\controllers;

use yii\web\Controller;

class ApiController extends Controller {

  public function actionStart() {
    return 'API Controller starts now.';
  }
  
  public function actionDocs() {
    return $this->renderPartial('docs');
  }

  public function actionGetPengaduanAll() {
    $sql = "
        SELECT 
            pr.nama_pengadu
            , pr.latitude_daerah_konflik
            , pr.longitude_daerah_konflik
            , p.nama_provinsi
            , kk.nama_kota_kabupaten
            , k.nama_kecamatan
            , dk.nama_desa_kelurahan
            , CASE
                WHEN COALESCE(pds.finished, FALSE) = TRUE AND COALESCE(pa.dialihkan, FALSE) = FALSE AND COALESCE(pa.finished, FALSE) = FALSE AND COALESCE(ppm.finished, FALSE) = FALSE AND COALESCE(pm.finished, FALSE) = FALSE AND COALESCE(pdm.finished, FALSE) = FALSE AND COALESCE(ptm.finished, FALSE) = FALSE THEN 'Assesmen'
                WHEN COALESCE(pds.finished, FALSE) = TRUE AND COALESCE(pa.dialihkan, FALSE) = FALSE AND COALESCE(pa.finished, FALSE) = TRUE AND COALESCE(ppm.finished, FALSE) = FALSE AND COALESCE(pm.finished, FALSE) = FALSE AND COALESCE(pdm.finished, FALSE) = FALSE AND COALESCE(ptm.finished, FALSE) = FALSE THEN 'Pra-Mediasi'
                WHEN COALESCE(pds.finished, FALSE) = TRUE AND COALESCE(pa.dialihkan, FALSE) = FALSE AND COALESCE(pa.finished, FALSE) = TRUE AND COALESCE(ppm.finished, FALSE) = TRUE AND COALESCE(pm.finished, FALSE) = FALSE AND COALESCE(pdm.finished, FALSE) = FALSE AND COALESCE(ptm.finished, FALSE) = FALSE THEN 'Mediasi'
                WHEN COALESCE(pds.finished, FALSE) = TRUE AND COALESCE(pa.dialihkan, FALSE) = FALSE AND COALESCE(pa.finished, FALSE) = TRUE AND COALESCE(ppm.finished, FALSE) = TRUE AND COALESCE(pm.finished, FALSE) = TRUE AND COALESCE(pdm.finished, FALSE) = FALSE AND COALESCE(ptm.finished, FALSE) = FALSE THEN 'Drafting MoU'
                WHEN COALESCE(pds.finished, FALSE) = TRUE AND COALESCE(pa.dialihkan, FALSE) = FALSE AND COALESCE(pa.finished, FALSE) = TRUE AND COALESCE(ppm.finished, FALSE) = TRUE AND COALESCE(pm.finished, FALSE) = TRUE AND COALESCE(pdm.finished, FALSE) = TRUE THEN 'Selesai'
                WHEN COALESCE(pa.dialihkan, FALSE) = TRUE THEN 'Dialihkan'
                ELSE 'Desk Study'
            END AS status_pengaduan
        FROM pengaduan_registrasi pr
            LEFT JOIN pengaduan_desk_study pds ON pds.id_pengaduan = pr.id
            LEFT JOIN pengaduan_assesmen pa ON pa.id_pengaduan = pr.id
            LEFT JOIN pengaduan_pra_mediasi ppm ON ppm.id_pengaduan = pr.id
            LEFT JOIN pengaduan_mediasi pm ON pm.id_pengaduan = pr.id
            LEFT JOIN pengaduan_drafting_mou pdm ON pdm.id_pengaduan = pr.id
            LEFT JOIN pengaduan_tanda_tangan_mou ptm ON ptm.id_pengaduan = pr.id
            LEFT JOIN provinsi p ON p.id = pr.id_provinsi_konflik
            LEFT JOIN kota_kabupaten kk ON kk.id = pr.id_kota_kabupaten_konflik
            LEFT JOIN kecamatan k ON k.id = pr.id_kecamatan_konflik
            LEFT JOIN desa_kelurahan dk ON dk.id = pr.id_desa_kelurahan_konflik
    ";

    $data = \Yii::$app->db->createCommand($sql)->queryAll();
	
	$sql = "
		SELECT status.status_name, count(data.status_pengaduan) as pengaduan_count
		FROM
		(
			SELECT 1 as status_id, 'Desk Study' AS status_name
			UNION ALL
			SELECT 2 as status_id, 'Assesmen' AS status_name
			UNION ALL
			SELECT 3 as status_id, 'Pra-Mediasi' AS status_name
			UNION ALL
			SELECT 4 as status_id, 'Mediasi' AS status_name
			UNION ALL
			SELECT 5 as status_id, 'Drafting MoU' AS status_name
			UNION ALL
			SELECT 6 as status_id, 'Selesai' AS status_name
			UNION ALL
			SELECT 7 as status_id, 'Dialihkan' AS status_name
		) status
		LEFT JOIN
		(
			SELECT
				CASE
					WHEN COALESCE(pds.finished, FALSE) = TRUE AND COALESCE(pa.dialihkan, FALSE) = FALSE AND COALESCE(pa.finished, FALSE) = FALSE AND COALESCE(ppm.finished, FALSE) = FALSE AND COALESCE(pm.finished, FALSE) = FALSE AND COALESCE(pdm.finished, FALSE) = FALSE AND COALESCE(ptm.finished, FALSE) = FALSE THEN 'Assesmen'
					WHEN COALESCE(pds.finished, FALSE) = TRUE AND COALESCE(pa.dialihkan, FALSE) = FALSE AND COALESCE(pa.finished, FALSE) = TRUE AND COALESCE(ppm.finished, FALSE) = FALSE AND COALESCE(pm.finished, FALSE) = FALSE AND COALESCE(pdm.finished, FALSE) = FALSE AND COALESCE(ptm.finished, FALSE) = FALSE THEN 'Pra-Mediasi'
					WHEN COALESCE(pds.finished, FALSE) = TRUE AND COALESCE(pa.dialihkan, FALSE) = FALSE AND COALESCE(pa.finished, FALSE) = TRUE AND COALESCE(ppm.finished, FALSE) = TRUE AND COALESCE(pm.finished, FALSE) = FALSE AND COALESCE(pdm.finished, FALSE) = FALSE AND COALESCE(ptm.finished, FALSE) = FALSE THEN 'Mediasi'
					WHEN COALESCE(pds.finished, FALSE) = TRUE AND COALESCE(pa.dialihkan, FALSE) = FALSE AND COALESCE(pa.finished, FALSE) = TRUE AND COALESCE(ppm.finished, FALSE) = TRUE AND COALESCE(pm.finished, FALSE) = TRUE AND COALESCE(pdm.finished, FALSE) = FALSE AND COALESCE(ptm.finished, FALSE) = FALSE THEN 'Drafting MoU'
					WHEN COALESCE(pds.finished, FALSE) = TRUE AND COALESCE(pa.dialihkan, FALSE) = FALSE AND COALESCE(pa.finished, FALSE) = TRUE AND COALESCE(ppm.finished, FALSE) = TRUE AND COALESCE(pm.finished, FALSE) = TRUE AND COALESCE(pdm.finished, FALSE) = TRUE THEN 'Selesai'
					WHEN COALESCE(pa.dialihkan, FALSE) = TRUE THEN 'Dialihkan'
					ELSE 'Desk Study'
				END AS status_pengaduan
			FROM pengaduan_registrasi pr
				LEFT JOIN pengaduan_desk_study pds ON pds.id_pengaduan = pr.id
				LEFT JOIN pengaduan_assesmen pa ON pa.id_pengaduan = pr.id
				LEFT JOIN pengaduan_pra_mediasi ppm ON ppm.id_pengaduan = pr.id
				LEFT JOIN pengaduan_mediasi pm ON pm.id_pengaduan = pr.id
				LEFT JOIN pengaduan_drafting_mou pdm ON pdm.id_pengaduan = pr.id
				LEFT JOIN pengaduan_tanda_tangan_mou ptm ON ptm.id_pengaduan = pr.id
		) data ON data.status_pengaduan = status.status_name
		GROUP BY status_id, status_name
		ORDER BY status_id
	";
	
	$statRS = \Yii::$app->db->createCommand($sql)->queryAll();
	
	$stat = array();
	foreach($statRS as $item)
		$stat[$item['status_name']] = $item['pengaduan_count'];
	
	$result = array(
		"statistics" => $stat
		, "data" => $data
	);
	
	\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

    return $result;
  }

  public function actionGetPengaduanByStep($step = null) {
    $_steps = [
        'desk-study' => 'AND (pengaduan_desk_study.finished = true) '
        . 'AND (pengaduan_assesmen.dialihkan = false) '
        . 'AND (pengaduan_assesmen.finished = false) '
        . 'AND (pengaduan_pra_mediasi.finished = false)'
        . 'AND (pengaduan_mediasi.finished = false)'
        . 'AND (pengaduan_drafting_mou.finished = false)'
        . 'AND (pengaduan_tanda_tangan_mou.finished = false)',
        'assesmen' => 'AND (pengaduan_desk_study.finished = true) '
        . 'AND (pengaduan_assesmen.dialihkan = false) '
        . 'AND (pengaduan_assesmen.finished = true) '
        . 'AND (pengaduan_pra_mediasi.finished = false) '
        . 'AND (pengaduan_mediasi.finished = false) '
        . 'AND (pengaduan_drafting_mou.finished = false) '
        . 'AND (pengaduan_tanda_tangan_mou.finished = false) ',
        'pra-mediasi' => 'AND (pengaduan_desk_study.finished = true) '
        . 'AND (pengaduan_assesmen.dialihkan = false) '
        . 'AND (pengaduan_assesmen.finished = true) '
        . 'AND (pengaduan_pra_mediasi.finished = true) '
        . 'AND (pengaduan_mediasi.finished = false) '
        . 'AND (pengaduan_drafting_mou.finished = false) '
        . 'AND (pengaduan_tanda_tangan_mou.finished = false) ',
        'mediasi' => 'AND (pengaduan_desk_study.finished = true) '
        . 'AND (pengaduan_assesmen.dialihkan = false) '
        . 'AND (pengaduan_assesmen.finished = true) '
        . 'AND (pengaduan_pra_mediasi.finished = true) '
        . 'AND (pengaduan_mediasi.finished = true) '
        . 'AND (pengaduan_drafting_mou.finished = false) '
        . 'AND (pengaduan_tanda_tangan_mou.finished = false) ',
        'drafting-mou' => 'AND (pengaduan_desk_study.finished = true) '
        . 'AND (pengaduan_assesmen.dialihkan = false) '
        . 'AND (pengaduan_assesmen.finished = true) '
        . 'AND (pengaduan_pra_mediasi.finished = true) '
        . 'AND (pengaduan_mediasi.finished = true) '
        . 'AND (pengaduan_drafting_mou.finished = true) '
        . 'AND (pengaduan_tanda_tangan_mou.finished = false) ',
        'tanda-tangan-mou' => 'AND (pengaduan_desk_study.finished = true) '
        . 'AND (pengaduan_assesmen.dialihkan = false) '
        . 'AND (pengaduan_assesmen.finished = true) '
        . 'AND (pengaduan_pra_mediasi.finished = true) '
        . 'AND (pengaduan_mediasi.finished = true) '
        . 'AND (pengaduan_drafting_mou.finished = true) '
        . 'AND (pengaduan_tanda_tangan_mou.finished = true) ',
        'selesai' => 'AND (pengaduan_desk_study.finished = true) '
        . 'AND (pengaduan_assesmen.finished = true) '
        . 'AND (pengaduan_pra_mediasi.finished = true) '
        . 'AND (pengaduan_mediasi.finished = true) '
        . 'AND (pengaduan_drafting_mou.finished = true) '
        . 'AND (pengaduan_tanda_tangan_mou.finished = true) ',
        'dialihkan' => 'AND (pengaduan_desk_study.finished = false) '
        . 'AND (pengaduan_assesmen.dialihkan = true) '
        . 'AND (pengaduan_assesmen.finished = true) '
        . 'AND (pengaduan_pra_mediasi.finished = false) '
        . 'AND (pengaduan_mediasi.finished = false) '
        . 'AND (pengaduan_drafting_mou.finished = false) '
        . 'AND (pengaduan_tanda_tangan_mou.finished = false) ',
    ];

    $_def_columns = 'pengaduan_registrasi.nama_pengadu,
                pengaduan_registrasi.latitude_daerah_konflik,
                pengaduan_registrasi.longitude_daerah_konflik,
                provinsi.nama_provinsi,
                kota_kabupaten.nama_kota_kabupaten,
                kecamatan.nama_kecamatan,
                desa_kelurahan.nama_desa_kelurahan';

    $_columns = [
        'desk-study' => $_def_columns . '',
        'assesmen' => $_def_columns . '',
        'pra-mediasi' => $_def_columns . '',
        'mediasi' => $_def_columns . '',
        'drafting-mou' => $_def_columns . '',
        'tanda-tangan-mou' => $_def_columns . '',
        'selesai' => $_def_columns . '',
        'dialihkan' => $_def_columns . ''
    ];

    if (isset($_steps[$step])) {
      $sql = '
            SELECT 
                ' . $_columns[$step] . '
                
            FROM 
                pengaduan_registrasi, 
                pengaduan_desk_study,
                pengaduan_assesmen, 
                pengaduan_pra_mediasi, 
                pengaduan_mediasi, 
                pengaduan_drafting_mou, 
                pengaduan_tanda_tangan_mou,
                provinsi,
                kota_kabupaten,
                kecamatan,
                desa_kelurahan

            WHERE 
                (pengaduan_registrasi.id = pengaduan_assesmen.id_pengaduan) 
                AND (pengaduan_registrasi.id_provinsi_konflik = provinsi.id)
                AND (pengaduan_registrasi.id_kota_kabupaten_konflik = kota_kabupaten.id)
                AND (pengaduan_registrasi.id_kecamatan_konflik = kecamatan.id)
                AND (pengaduan_registrasi.id_desa_kelurahan_konflik = desa_kelurahan.id)
                AND (pengaduan_registrasi.id = pengaduan_desk_study.id_pengaduan) 
                AND (pengaduan_registrasi.id = pengaduan_pra_mediasi.id_pengaduan) 
                AND (pengaduan_registrasi.id = pengaduan_mediasi.id_pengaduan) 
                AND (pengaduan_registrasi.id = pengaduan_drafting_mou.id_pengaduan) 
                AND (pengaduan_registrasi.id = pengaduan_tanda_tangan_mou.id_pengaduan)';

      $sql = $sql . $_steps[$step];

      $result = \Yii::$app->db->createCommand($sql)->queryAll();

      \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

      return $result;
    } else {
      return 'Data Pengaduan tidak tersedia';
    }
  }

  public function actionGetPengusulanAll() {
    $sql = "
        SELECT 
            reg.nama_pengusul
            , COALESCE(reg.nama_mha, '') as nama_mha
            , prov.nama_provinsi
            , kokab.nama_kota_kabupaten
            , kec.nama_kecamatan
            , dekel.nama_desa_kelurahan
            , COALESCE(cantum.latitude, 0) as latitude
            , COALESCE(cantum.longitude, 0) as longitude
            , COALESCE(cantum.no_sk_penetapan, '') as no_sk_penetapan
            , COALESCE(cantum.tanggal_sk_penetapan, '1970-01-01') as tanggal_sk_penetapan
            , COALESCE(cantum.luas_sk_penetapan, '0') as luas_sk_penetapan
            , COALESCE(cantum.no_sk_pencantuman, '') as no_sk_pencantuman
            , COALESCE(cantum.tanggal_sk_pencantuman, '1970-01-01') as tanggal_sk_pencantuman
            , COALESCE(cantum.luas_sk_pencantuman, 0) as luas_sk_pencantuman
            --, COALESCE(mohon.finished, FALSE) as mohon_val
            --, COALESCE(mohon.dikembalikan, FALSE) as kembalika_val
            --, COALESCE(val.finished, FALSE) as validasi_val
            --, COALESCE(ver.finished, FALSE) as verifikasi_val
            --, COALESCE(cantum.finished, FALSE) as pencantuman_val
            --, COALESCE(monev.finished, FALSE) as monev_val
            , CASE
                    WHEN COALESCE(mohon.finished, FALSE) = TRUE AND COALESCE(mohon.dikembalikan, FALSE) = FALSE AND COALESCE(val.finished, FALSE) = FALSE AND COALESCE(ver.finished, FALSE) = FALSE AND COALESCE(cantum.finished, FALSE) = FALSE AND COALESCE(monev.finished, FALSE) = FALSE THEN 'Validasi'
                    WHEN COALESCE(mohon.finished, FALSE) = TRUE AND COALESCE(val.finished, FALSE) = TRUE AND COALESCE(ver.finished, FALSE) = FALSE AND COALESCE(cantum.finished, FALSE) = FALSE AND COALESCE(monev.finished, FALSE) = FALSE THEN 'Verifikasi'
                    WHEN COALESCE(mohon.finished, FALSE) = TRUE AND COALESCE(val.finished, FALSE) = TRUE AND COALESCE(ver.finished, FALSE) = TRUE AND COALESCE(cantum.finished, FALSE) = FALSE AND COALESCE(monev.finished, FALSE) = FALSE THEN 'Pencantuman'
                    --WHEN COALESCE(mohon.finished, FALSE) = TRUE AND COALESCE(val.finished, FALSE) = TRUE AND COALESCE(ver.finished, FALSE) = TRUE AND COALESCE(cantum.finished, FALSE) = TRUE AND COALESCE(monev.finished, FALSE) = TRUE THEN 'Monev'
                    WHEN COALESCE(mohon.finished, FALSE) = TRUE AND COALESCE(val.finished, FALSE) = TRUE AND COALESCE(ver.finished, FALSE) = TRUE AND COALESCE(cantum.finished, FALSE) = TRUE AND COALESCE(monev.finished, FALSE) = FALSE THEN 'Monev'
                    WHEN COALESCE(mohon.finished, FALSE) = TRUE AND COALESCE(val.finished, FALSE) = TRUE AND COALESCE(ver.finished, FALSE) = TRUE AND COALESCE(cantum.finished, FALSE) = TRUE AND COALESCE(monev.finished, FALSE) = TRUE THEN 'Selesai'
                    WHEN COALESCE(mohon.finished, FALSE) = TRUE AND COALESCE(mohon.dikembalikan, FALSE) = TRUE AND COALESCE(val.finished, FALSE) = FALSE AND COALESCE(ver.finished, FALSE) = FALSE AND COALESCE(cantum.finished, FALSE) = FALSE AND COALESCE(monev.finished, FALSE) = FALSE THEN 'Dikembalikan'
                    ELSE 'Permohonan'
                END AS status_pengusulan
        FROM pengusulan_registrasi reg
            LEFT JOIN pengusulan_validasi val ON val.id_pengusulan = reg.id
            LEFT JOIN pengusulan_verifikasi ver ON ver.id_pengusulan = reg.id
            LEFT JOIN pengusulan_monev monev ON monev.id_pengusulan = reg.id
            LEFT JOIN pengusulan_pencantuman cantum ON cantum.id_pengusulan = reg.id
            LEFT JOIN pengusulan_permohonan mohon ON mohon.id_pengusulan = reg.id
            LEFT JOIN provinsi prov ON prov.id = reg.id_provinsi
            LEFT JOIN kota_kabupaten kokab ON kokab.id = reg.id_kota_kabupaten
            LEFT JOIN kecamatan kec ON kec.id = reg.id_kecamatan
            LEFT JOIN desa_kelurahan dekel ON dekel.id = reg.id_desa_kelurahan
    ";

    $data = \Yii::$app->db->createCommand($sql)->queryAll();
    
    \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

    $sql = "
        SELECT status.status_name, count(data.status_pengusulan) as pengusulan_count
        FROM
        (
            SELECT 1 as status_id, 'Permohonan' AS status_name
            UNION ALL
            SELECT 2 as status_id, 'Validasi' AS status_name
            UNION ALL
            SELECT 3 as status_id, 'Verifikasi' AS status_name
            UNION ALL
            SELECT 4 as status_id, 'Pencantuman' AS status_name
            UNION ALL
            SELECT 5 as status_id, 'Monev' AS status_name
            UNION ALL
            SELECT 6 as status_id, 'Selesai' AS status_name
            UNION ALL
            SELECT 7 as status_id, 'Dikembalikan' AS status_name
        ) status
        LEFT JOIN
        (
            SELECT 
                reg.id
                , reg.nama_pengusul
                , COALESCE(reg.nama_mha, '') as nama_mha
                , prov.nama_provinsi
                , kokab.nama_kota_kabupaten
                , kec.nama_kecamatan
                , dekel.nama_desa_kelurahan
                , COALESCE(cantum.latitude, 0) as latitude
                , COALESCE(cantum.longitude, 0) as longitude
                , COALESCE(cantum.no_sk_penetapan, '') as no_sk_penetapan
                , COALESCE(cantum.tanggal_sk_penetapan, '1970-01-01') as tanggal_sk_penetapan
                , COALESCE(cantum.luas_sk_penetapan, '0') as luas_sk_penetapan
                , COALESCE(cantum.no_sk_pencantuman, '') as no_sk_pencantuman
                , COALESCE(cantum.tanggal_sk_pencantuman, '1970-01-01') as tanggal_sk_pencantuman
                , COALESCE(cantum.luas_sk_pencantuman, 0) as luas_sk_pencantuman
                , COALESCE(mohon.finished, FALSE) as mohon_val
                , COALESCE(mohon.dikembalikan, FALSE) as kembalika_val
                , COALESCE(val.finished, FALSE) as validasi_val
                , COALESCE(ver.finished, FALSE) as verifikasi_val
                , COALESCE(cantum.finished, FALSE) as pencantuman_val
                , COALESCE(monev.finished, FALSE) as monev_val
                , CASE
                                WHEN COALESCE(mohon.finished, FALSE) = TRUE AND COALESCE(mohon.dikembalikan, FALSE) = FALSE AND COALESCE(val.finished, FALSE) = FALSE AND COALESCE(ver.finished, FALSE) = FALSE AND COALESCE(cantum.finished, FALSE) = FALSE AND COALESCE(monev.finished, FALSE) = FALSE THEN 'Validasi'
                                WHEN COALESCE(mohon.finished, FALSE) = TRUE AND COALESCE(val.finished, FALSE) = TRUE AND COALESCE(ver.finished, FALSE) = FALSE AND COALESCE(cantum.finished, FALSE) = FALSE AND COALESCE(monev.finished, FALSE) = FALSE THEN 'Verifikasi'
                                WHEN COALESCE(mohon.finished, FALSE) = TRUE AND COALESCE(val.finished, FALSE) = TRUE AND COALESCE(ver.finished, FALSE) = TRUE AND COALESCE(cantum.finished, FALSE) = FALSE AND COALESCE(monev.finished, FALSE) = FALSE THEN 'Pencantuman'
                                --WHEN COALESCE(mohon.finished, FALSE) = TRUE AND COALESCE(val.finished, FALSE) = TRUE AND COALESCE(ver.finished, FALSE) = TRUE AND COALESCE(cantum.finished, FALSE) = TRUE AND COALESCE(monev.finished, FALSE) = TRUE THEN 'Monev'
                                WHEN COALESCE(mohon.finished, FALSE) = TRUE AND COALESCE(val.finished, FALSE) = TRUE AND COALESCE(ver.finished, FALSE) = TRUE AND COALESCE(cantum.finished, FALSE) = TRUE AND COALESCE(monev.finished, FALSE) = FALSE THEN 'Monev'
                                WHEN COALESCE(mohon.finished, FALSE) = TRUE AND COALESCE(val.finished, FALSE) = TRUE AND COALESCE(ver.finished, FALSE) = TRUE AND COALESCE(cantum.finished, FALSE) = TRUE AND COALESCE(monev.finished, FALSE) = TRUE THEN 'Selesai'
                                WHEN COALESCE(mohon.finished, FALSE) = TRUE AND COALESCE(mohon.dikembalikan, FALSE) = TRUE AND COALESCE(val.finished, FALSE) = FALSE AND COALESCE(ver.finished, FALSE) = FALSE AND COALESCE(cantum.finished, FALSE) = FALSE AND COALESCE(monev.finished, FALSE) = FALSE THEN 'Dikembalikan'
                                ELSE 'Permohonan'
                        END AS status_pengusulan
            FROM pengusulan_registrasi reg
                LEFT JOIN pengusulan_validasi val ON val.id_pengusulan = reg.id
                LEFT JOIN pengusulan_verifikasi ver ON ver.id_pengusulan = reg.id
                LEFT JOIN pengusulan_monev monev ON monev.id_pengusulan = reg.id
                LEFT JOIN pengusulan_pencantuman cantum ON cantum.id_pengusulan = reg.id
                LEFT JOIN pengusulan_permohonan mohon ON mohon.id_pengusulan = reg.id
                LEFT JOIN provinsi prov ON prov.id = reg.id_provinsi
                LEFT JOIN kota_kabupaten kokab ON kokab.id = reg.id_kota_kabupaten
                LEFT JOIN kecamatan kec ON kec.id = reg.id_kecamatan
                LEFT JOIN desa_kelurahan dekel ON dekel.id = reg.id_desa_kelurahan
        ) data ON data.status_pengusulan = status.status_name
        GROUP BY status_id, status_name
        ORDER BY status_id
    ";
    
    $statRS = \Yii::$app->db->createCommand($sql)->queryAll();
    
    $stat = array();
    foreach($statRS as $item)
        $stat[$item['status_name']] = $item['pengusulan_count'];
    
    $result = array(
        "statistics" => $stat
        , "data" => $data
    );

    return $data;
  }

  public function actionGetPengusulanByStep($step = null) {
    $_steps = [
        'permohonan' => 'AND (pengusulan_permohonan.finished = true)'
        . 'AND (pengusulan_permohonan.dikembalikan = false)'
        . 'AND (pengusulan_validasi.finished = false)'
        . 'AND (pengusulan_verifikasi.finished = false)'
        . 'AND (pengusulan_pencantuman.finished = false)'
        . 'AND (pengusulan_monev.finished = false)',
        'validasi' => 'AND (pengusulan_permohonan.finished = true)'
        . 'AND (pengusulan_validasi.finished = true)'
        . 'AND (pengusulan_verifikasi.finished = false)'
        . 'AND (pengusulan_pencantuman.finished = false)'
        . 'AND (pengusulan_monev.finished = false)',
        'verifikasi' => 'AND (pengusulan_permohonan.finished = true)'
        . 'AND (pengusulan_validasi.finished = true)'
        . 'AND (pengusulan_verifikasi.finished = true)'
        . 'AND (pengusulan_pencantuman.finished = false)'
        . 'AND (pengusulan_monev.finished = false)',
        'monev' => 'AND (pengusulan_permohonan.finished = true)'
        . 'AND (pengusulan_validasi.finished = true)'
        . 'AND (pengusulan_verifikasi.finished = true)'
        . 'AND (pengusulan_pencantuman.finished = true)'
        . 'AND (pengusulan_monev.finished = true)',
        'pencantuman' => 'AND (pengusulan_permohonan.finished = true)'
        . 'AND (pengusulan_validasi.finished = true)'
        . 'AND (pengusulan_verifikasi.finished = true)'
        . 'AND (pengusulan_pencantuman.finished = true)'
        . 'AND (pengusulan_monev.finished = false)',
        'selesai' => 'AND (pengusulan_permohonan.finished = true)'
        . 'AND (pengusulan_validasi.finished = true)'
        . 'AND (pengusulan_verifikasi.finished = true)'
        . 'AND (pengusulan_pencantuman.finished = true)'
        . 'AND (pengusulan_monev.finished = true)',
        'dikembalikan' => 'AND (pengusulan_permohonan.finished = true)'
        . 'AND (pengusulan_permohonan.dikembalikan = true)'
        . 'AND (pengusulan_validasi.finished = false)'
        . 'AND (pengusulan_verifikasi.finished = false)'
        . 'AND (pengusulan_pencantuman.finished = false)'
        . 'AND (pengusulan_monev.finished = false)',
    ];

    $_def_columns = "pengusulan_registrasi.nama_pengusul,
                pengusulan_registrasi.nama_mha,
                provinsi.nama_provinsi, 
                kota_kabupaten.nama_kota_kabupaten,
                kecamatan.nama_kecamatan,
                desa_kelurahan.nama_desa_kelurahan";

    $_columns = [
        'permohonan' => $_def_columns,
        'validasi' => $_def_columns,
        'verifikasi' => $_def_columns,
        'pencantuman' => $_def_columns . ',
                pengusulan_pencantuman.latitude,
                pengusulan_pencantuman.longitude,
                pengusulan_pencantuman.luas_sk_penetapan,
                pengusulan_pencantuman.luas_sk_pencantuman,
                pengusulan_pencantuman.no_sk_pencantuman,
                pengusulan_pencantuman.no_sk_penetapan,
                pengusulan_pencantuman.tanggal_sk_penetapan,
                pengusulan_pencantuman.tanggal_sk_pencantuman',
        'monev' => $_def_columns,
        'selesai' => $_def_columns,
        'dikembalikan' => $_def_columns,
    ];

    if (isset($_steps[$step])) {
      $sql = '
            SELECT 
                ' . $_columns[$step] . '
                
            FROM 
                pengusulan_registrasi, 
                pengusulan_validasi, 
                pengusulan_verifikasi, 
                pengusulan_monev, 
                pengusulan_pencantuman, 
                pengusulan_permohonan,
                provinsi,
                kota_kabupaten,
                kecamatan,
                desa_kelurahan

            WHERE 
                (pengusulan_registrasi.id = pengusulan_validasi.id_pengusulan)
                AND (pengusulan_registrasi.id_provinsi = provinsi.id)
                AND (pengusulan_registrasi.id_kota_kabupaten = kota_kabupaten.id)
                AND (pengusulan_registrasi.id_kecamatan = kecamatan.id)
                AND (pengusulan_registrasi.id_desa_kelurahan = desa_kelurahan.id)
                AND (pengusulan_registrasi.id = pengusulan_verifikasi.id_pengusulan) 
                AND (pengusulan_registrasi.id = pengusulan_permohonan.id_pengusulan) 
                AND (pengusulan_registrasi.id = pengusulan_monev.id_pengusulan) 
                AND (pengusulan_registrasi.id = pengusulan_pencantuman.id_pengusulan)';

      $sql = $sql . $_steps[$step];

      $result = \Yii::$app->db->createCommand($sql)->queryAll();

      \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

      return $result;
    } else {
      return 'Data Pengusulan tidak tersedia';
    }
  }

  public function actionGetPengaduanNotFinished() {
    $all = \common\models\PengaduanRegistrasi::find()->count();
    $finished = count(\yii\helpers\Json::decode($this->actionGetPengaduanByStep('selesai')));

    return ($all - $finished);
  }

  public function actionGetPengusulanNotFinished() {
    $all = \common\models\PengusulanRegistrasi::find()->count();
    $finished = count(\yii\helpers\Json::decode($this->actionGetPengusulanByStep('selesai')));

    return ($all - $finished);
  }

}
