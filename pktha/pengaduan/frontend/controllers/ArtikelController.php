<?php

namespace frontend\controllers;

use common\models\Artikel;

class ArtikelController extends \yii\web\Controller {

    public function actionStart() {
      return 'Artikel Controller starts now.';
    }
  
    public function actionIndex() {
        return $this->render('index');
    }

    public function actionCeritasukses($id) {
        // find customers whose age is 30 and whose status is 1
        // $artikel = Artikel::findAll(['kategori_id' => $id, 'publish' => 1]);
        // var_dump($artikel);
        $models = Artikel::find()
                ->where(['kategori_id' => $id, 'publish' => 1])
                ->all();

        return $this->render('index', [
                    'models' => $models,
        ]);
    }
}
