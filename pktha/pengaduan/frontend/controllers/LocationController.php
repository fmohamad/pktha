<?php

namespace frontend\controllers;

class LocationController extends \yii\web\Controller {

    public function actionDesaKelurahan() {
        $id_kecamatan = \Yii::$app->request->post('id_kecamatan');
        $desaKelurahan = \frontend\models\DesaKelurahan::find()->where(['id_kecamatan' => $id_kecamatan])->orWhere(['id_kecamatan' => 1])->orderBy(['id' => SORT_DESC])->all();

        if (isset($desaKelurahan)) {
            return $this->renderPartial('desa-kelurahan', ['model' => $desaKelurahan]);
        } else {
            return $this->renderPartial('desa-kelurahan', ['model' => null]);
        }
    }

    public function actionKecamatan() {
        $id_kota_kabupaten = \Yii::$app->request->post('id_kota_kabupaten');
        $kecamatan = \frontend\models\Kecamatan::find()->where(['id_kota_kabupaten' => $id_kota_kabupaten])->orWhere(['id_kota_kabupaten' => 1])->orderBy(['id' => SORT_DESC])->all();

        if (isset($kecamatan)) {
            return $this->renderPartial('kecamatan', ['model' => $kecamatan]);
        } else {
            return $this->renderPartial('kecamatan', ['model' => null]);
        }
    }

    public function actionKotaKabupaten() {
        $id_provinsi = \Yii::$app->request->post('id_provinsi');
        $kotaKabupaten = \frontend\models\KotaKabupaten::find()->where(['id_provinsi' => $id_provinsi])->orWhere(['id_provinsi' => 1])->orderBy(['id' => SORT_DESC])->all();

        if (isset($kotaKabupaten)) {
            return $this->renderPartial('kota-kabupaten', ['model' => $kotaKabupaten]);
        } else {
            return $this->renderPartial('kota-kabupaten', ['model' => null]);
        }
    }

    public function actionProvinsi() {
        $id_wilayah = \Yii::$app->request->post('id_wilayah');
        $provinsi = \frontend\models\Provinsi::find()->where(['id_wilayah'=> $id_wilayah])->orWhere(['id_wilayah' => 6])->orderBy(['id' => SORT_DESC])->all();

        if (isset($provinsi)) {
            return $this->renderPartial('provinsi', ['model' => $provinsi]);
        } else {
            return $this->renderPartial('provinsi', ['model' => null]);
        }
    }

}
