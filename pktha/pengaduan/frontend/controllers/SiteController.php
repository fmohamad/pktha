<?php

namespace frontend\controllers;

use Yii;
use common\models\LoginForm;
use common\models\Pengaduan;
use common\models\Slider;
use common\models\Artikel;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use frontend\models\UsulanForm;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\ServerErrorHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\UploadedFile;

/**
 * Site controller
 */
class SiteController extends Controller {

  /**
   * @keputusan_bupati) as keputusan_bupati,
    count (keputusan_walikota) as keputusan_walikota, count (peraturan_bupati) as peraturan_bupati, count (peraturan_daerah) as peraturan_daerah, count (peraturan_daerah_kabupaten) as peraturan_daerah_kabupaten,
    count (peraturan_daerah_kota) as peraturan_daerah_kota,
    count (peraturan_walikota) as peraturan_walikota
    FROM produk_hukum_daerah"
   */
  public function behaviors() {
    return [
        'access' => [
            'class' => AccessControl::className(),
            'only' => ['logout', 'signup', 'kasus2017',],
            'rules' => [
                    [
                    'actions' => ['pencantumanhutanadat', 'cek', 'potensihutanadat', 'petapotensi', 'produkhukumprovinsi', 'lokasipengaduan', 'kasus2016', 'kasus2017',],
                    'allow' => true,
                ],
                    [
                    'actions' => ['signup'],
                    'allow' => true,
                    'roles' => ['?'],
                ],
                    [
                    'actions' => ['logout'],
                    'allow' => true,
                    'roles' => ['@'],
                ],
            ],
        ],
        'verbs' => [
            'class' => VerbFilter::className(),
            'actions' => [
                'logout' => ['post'],
            ],
        ],
    ];
  }

  /**
   * @inheritdoc
   */
  public function actions() {
    return [
        'error' => [
            'class' => 'yii\web\ErrorAction',
        ],
        'captcha' => [
            'class' => 'yii\captcha\CaptchaAction',
            'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
        ],
    ];
  }

  /**
   * Displays homepage.
   *
   * @return mixed
   */
  public function actionIndex() {
    $x1 = \common\models\Pengaduan::find()->where(['id_tahapan' => 1, 'deleted' => 0])->count();
    $x2 = \common\models\Pengaduan::find()->where(['id_tahapan' => 2, 'deleted' => 0])->count();
    $x3 = \common\models\Pengaduan::find()->where(['id_tahapan' => 3, 'deleted' => 0])->count();
    $x4 = \common\models\Pengaduan::find()->where(['id_tahapan' => 4, 'deleted' => 0])->count();

    $session = Yii::$app->session;

    $tp['t1'] = $x1;
    $tp['t2'] = $x2;
    $tp['t3'] = $x3;
    $tp['t4'] = $x4;

    $session['tahap'] = $tp;

    $model = new Pengaduan();
    $mdlSlider = Slider::find()->where(['publish' => 1, 'deleted' => 0])->all();
    $cds = Artikel::find()->where(["kategori_id" => 15, 'publish' => 1, 'deleted' => 0])->orderBy('id desc')->limit(2)->all();
    $bdp = Artikel::find()->where(["kategori_id" => 10, 'publish' => 1, 'deleted' => 0])->orderBy('id desc')->limit(2)->all();

    if ($model->load(Yii::$app->request->post()) && $model->save()) {
      $model = new Pengaduan();

      $session['showmsg'] = true;

      return $this->render(
                      'index', [
                  'model' => $model,
                  'mdlSlider' => $mdlSlider,
                  'cds' => $cds,
                  'bdp' => $bdp,
                      ]
      );
    } else {
      $sisaPengaduan = \common\models\Tahapan::countAllTahapanFirstUnfinished(30, 'pengaduan');
      $sisaPengusulan = \common\models\Tahapan::countAllTahapanFirstUnfinished(30, 'pengusulan');
      return $this->render(
                      'index', [
                  'model' => $model,
                  'mdlSlider' => $mdlSlider,
                  'cds' => $cds,
                  'bdp' => $bdp,
                  'tahapPengaduan' => $sisaPengaduan,
                  'countDialihkan' => \common\models\Tahapan::countDialihkan(30)['pengaduan_assesmen']['count'],
                  'tahapPengusulan' => $sisaPengusulan,
                  'jumlah_potensi' => \common\utils\DataPeta::GetJumlahPotensiHutanAdat(),
                  'jumlah_pencantuman' => \common\utils\DataPeta::GetJumlahPencantumanHutanAdat(),
                      ]
      );
    }
  }

  /*
   * Commented because of potential confidential data breach.
   * 
    // Open internal data into frontend.
    public function actionListPengaduan($tahap) {
    if (Yii::$app->request->post()) {

    } else {
    $folderNames = [
    "Pendaftaran" => "\backend\\views\\pengaduan-registrasi",
    "Desk Study" => "\backend\\views\\pengaduan-desk-study",
    "Assesmen" => "\backend\\views\\pengaduan-assesmen",
    "Pra-Mediasi" => "\backend\\views\\pengaduan-pra-mediasi",
    "Mediasi" => "\backend\views\\pengaduan-mediasi",
    "Drafting MoU" => "\backend\\views\\pengaduan-drafting-mou",
    "Tanda Tangan MoU" => "\backend\\views\\pengaduan-tanda-tangan-mou",
    ];
    $modelTables = [
    "Pendaftaran" => "\common\models\PengaduanRegistrasi",
    "Desk Study" => "\common\models\PengaduanDeskStudy",
    "Assesmen" => "\common\models\PengaduanAssesmen",
    "Pra-Mediasi" => "\common\models\PengaduanPraMediasi",
    "Mediasi" => "\common\models\PengaduanMediasi",
    "Drafting MoU" => "\common\models\PengaduanDraftingMou",
    "Tanda Tangan MoU" => "\common\models\PengaduanTandaTanganMou",
    ];
    $searchTables = [
    "Pendaftaran" => "\common\models\PengaduanRegistrasiSearch",
    "Desk Study" => "\common\models\PengaduanDeskStudySearch",
    "Assesmen" => "\common\models\PengaduanAssesmenSearch",
    "Pra-Mediasi" => "\common\models\PengaduanPraMediasiSearch",
    "Mediasi" => "\common\models\PengaduanMediasiSearch",
    "Drafting MoU" => "\common\models\PengaduanDraftingMouSearch",
    "Tanda Tangan MoU" => "\common\models\PengaduanTandaTanganMouSearch",
    ];
    $selectedTable = $modelTables[$tahap];
    $selectedSearch = $searchTables[$tahap];
    $selectedFolder = $folderNames[$tahap];

    $queryParams = Yii::$app->request->queryParams;

    $searchModel = new $selectedSearch();
    $dataProvider = $searchModel->search($queryParams);

    if ($tahap !== 'Pendaftaran')
    $model = $selectedTable::findAll(['finished' => true]);
    else
    $model = $selectedTable::find()->all();

    return $this->render('ListPengaduan', ['model' => $model, 'view' => $selectedFolder, 'searchModel' => $searchModel, 'dataProvider' => $dataProvider, 'tahap' => $tahap]);
    }
    }

    // Open internal data into frontend.
    public function actionListPengusulan($tahap) {
    if (Yii::$app->request->post()) {

    } else {
    $tables = [
    "Pendaftaran" => '\common\models\PengusulanRegistrasi',
    "Permohonan" => '\common\models\PengusulanPermohonan',
    "Validasi" => '\common\models\PengusulanValidasi',
    "Verifikasi" => '\common\models\PengusulanVerifikasi',
    "Monev" => '\common\models\PengusulanMonev',
    "Pencantuman dan Penetapan" => '\common\models\PengusulanPencantuman',
    ];
    $searchTables = [
    "Pendaftaran" => '\common\models\PengusulanRegistrasiSearch',
    "Permohonan" => '\common\models\PengusulanPermohonanSearch',
    "Validasi" => '\common\models\PengusulanValidasiSearch',
    "Verifikasi" => '\common\models\PengusulanVerifikasiSearch',
    "Monev" => '\common\models\PengusulanMonevSearch',
    "Pencantuman dan Penetapan" => '\common\models\PengusulanPencantumanSearch',
    ];
    $selectedTable = $tables[$tahap];
    $selectedSearch = $searchTables[$tahap];

    $queryParams = Yii::$app->request->queryParams;
    $searchModel = new $selectedSearch();
    $dataProvider = $searchModel->search($queryParams);

    if ($tahap !== 'Pendaftaran')
    $model = $selectedTable::findAll(['finished' => true]);
    else
    $model = $selectedTable::all();
    return $this->render('ListPengusulan', ['model' => $model, 'tahap' => $tahap, 'dataProvider' => $dataProvider]);
    }
    }


   * 
   * End of commected section of potential data breach-resulting function
   */

  public function actionKabkota($id) {
    Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
    $model = new \common\models\Kabkota();
    $tst = $model->find($id)->where("proid='" . $id . "'")->all();

    $rsp = "";
    foreach ($tst as $res) {
      $rsp .= '<option value="' . $res->kabid . '">' . $res->kabkota . '</option>';
    }


    return $rsp;
  }

  /**
   * Logs in a user.
   *
   * @return mixed
   */
  public function actionLogin() {
    if (!\Yii::$app->user->isGuest) {
      return $this->goHome();
    }

    $model = new LoginForm();
    if ($model->load(Yii::$app->request->post()) && $model->login()) {
      return $this->goBack();
    } else {
      return $this->render(
                      'login', [
                  'model' => $model,
                      ]
      );
    }
  }

  /**
   * Logs out the current user.
   *
   * @return mixed
   */
  public function actionLogout() {
    Yii::$app->user->logout();

    return $this->goHome();
  }

  /**
   * Displays contact page.
   *
   * @return mixed
   */
  public function actionContact() {
    $model = new ContactForm();
    if ($model->load(Yii::$app->request->post()) && $model->validate()) {
      if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
        //Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
        Yii::$app->session->setFlash('success', 'Terima kasih telah menghubungi kami, kami akan segera menghubungi anda kembali.');
      } else {
        Yii::$app->session->setFlash('error', 'Maaf ada kesalahan pada sistem kami, silahkan coba beberapa saat lagi.');
      }

      return $this->refresh();
    } else {
      return $this->render(
                      'contact', [
                  'model' => $model,
                      ]
      );
    }
  }

  public function actionUsulan() {
    $model = new UsulanForm();

    if ($model->load(Yii::$app->request->post())) {

      $model->upload = UploadedFile::getInstance($model, 'upload');

      if ($model->upload()) {

        if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
          //Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
          Yii::$app->session->setFlash('success', 'Terima kasih telah menghubungi kami, kami akan segera menghubungi anda kembali.');
        } else {
          Yii::$app->session->setFlash('error', 'Maaf ada kesalahan pada sistem kami, silahkan coba beberapa saat lagi.');
        }
      } else {
        Yii::$app->session->setFlash('error', 'Maaf usulan anda tidak dapat kami proses, silahkan coba beberapa saat lagi.');
      }
      return $this->refresh();
    } else {
      return $this->render(
                      'usulan', [
                  'model' => $model,
                      ]
      );
    }
  }

  /**
   * Displays about page.
   *
   * @return mixed
   */
  public function actionAbout() {

    $model = Artikel::find()->where(['id' => '7'])->one();

    return $this->render(
                    'about', [
                'model' => $model,
                    ]
    );
  }

  public function actionTatacara() {

    $model = Artikel::find()->where(['kategori_id' => '13', 'id' => 1])->one();

    return $this->render(
                    'tatacara', [
                'model' => $model,
                    ]
    );
  }

  public function actionTatacaraPengaduanKonflik() {
    $model = Artikel::find()->where(['id' => 24])->one();

    return $this->render(
                    'TatacaraPengaduanKonflik', [
                'model' => $model,
                    ]
    );
  }

  public function actionTatacaraPengusulanHutanAdat() {
    $model = Artikel::find()->where(['id' => 23])->one();

    return $this->render(
                    'TatacaraPengusulanHutanAdat', [
                'model' => $model,
                    ]
    );
  }

  public function actionCeritasukses() {
    // ->where(['kategori_id' => $id,'publish' => 1])
    return $this->render(
                    'ceritasukses', [
                'dataProvider' => new \yii\data\ActiveDataProvider(
                        [
                    'query' => Artikel::find()->where(["kategori_id" => 15, 'publish' => 1])->orderBy('id desc'),
                    'pagination' => ['pageSize' => 5],
                        ]
                ),
                    ]
    );
  }

  public function actionKegiatandanberita() {
    return $this->render(
                    'kegiatanberita', [
                'dataProvider' => new \yii\data\ActiveDataProvider(
                        [
                    'query' => Artikel::find()->where(["kategori_id" => 10, 'publish' => 1])->orderBy('id desc'),
                    'pagination' => ['pageSize' => 10],
                        ]
                ),
                    ]
    );
  }

  public function actionPeraturandanperundangan() {
    $model = Artikel::find()->where(['id' => '20'])->one();

    return $this->render(
                    'peraturandanperundangan', [
                'dataProvider' => new \yii\data\ActiveDataProvider(
                        [
                    'query' => Artikel::find()->where(["kategori_id" => 23, 'publish' => 1])->orderBy('id desc'),
                    'pagination' => ['pageSize' => 10],
                        ]
                ),
                    ]
    );
  }

  public function actionPeta() {
    return $this->render(
                    "peta", [
                "dataProvider" => new \yii\data\ActiveDataProvider(
                        [
                    "query" => Artikel::find()->where(["kategori_id" => 43, "publish" => 1])->orderBy('id desc'),
                    "pagination" => ["pageSize" => 10]
                        ]
                )
                    ]
    );
  }

  public function actionBuku() {
    return $this->render(
                    "buku", [
                "dataProvider" => new \yii\data\ActiveDataProvider(
                        [
                    "query" => Artikel::find()->where(["kategori_id" => 35, "publish" => 1])->orderBy('id desc'),
                    "pagination" => ["pageSize" => 10]
                        ]
                )
                    ]
    );
  }

  public function actionDetail($id) {
    $model = Artikel::find()->where(['id' => $id])->one();

    return $this->render(
                    $model['kategori_id'] == 23 ? "detailundang" : "detail", [
                'model' => $model,
                    ]
    );
  }

  public function actionView($st, $id) {
    $model = $this->findModel($id);
    $Artikel = new Artikel;
    return $this->render(
                    'view', [
                'model' => $model,
                'st' => $st
                    ]
    );
  }

  protected function findModel($id, $withList = []) {
    $query = Artikel::find()
            ->where(['id' => $id])
            ->with('user');
    $model = $query->one();
    if ($model === null) {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
    return $model;
  }

  /**
   * Signs user up.
   *
   * @return mixed
   */
  public function actionSignup() {
    $model = new SignupForm();
    if ($model->load(Yii::$app->request->post())) {
      if ($user = $model->signup()) {
        if (Yii::$app->getUser()->login($user)) {
          return $this->goHome();
        }
      }
    }

    return $this->render(
                    'signup', [
                'model' => $model,
                    ]
    );
  }

  /**
   * Requests password reset.
   *
   * @return mixed
   */
  public function actionRequestPasswordReset() {
    $model = new PasswordResetRequestForm();
    if ($model->load(Yii::$app->request->post()) && $model->validate()) {
      if ($model->sendEmail()) {
        Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

        return $this->goHome();
      } else {
        Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
      }
    }

    return $this->render(
                    'requestPasswordResetToken', [
                'model' => $model,
                    ]
    );
  }

  /**
   * Resets password.
   *
   * @param  string $token
   * @return mixed
   * @throws BadRequestHttpException
   */
  public function actionResetPassword($token) {
    try {
      $model = new ResetPasswordForm($token);
    } catch (InvalidParamException $e) {
      throw new BadRequestHttpException($e->getMessage());
    }

    if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
      Yii::$app->session->setFlash('success', 'New password was saved.');

      return $this->goHome();
    }

    return $this->render(
                    'resetPassword', [
                'model' => $model,
                    ]
    );
  }

  /**
   * Display form pengaduan.
   */
  public function actionPengaduan() {
    return $this->render(
                    'pengaduan', [
                'model' => null,
                    ]
    );
  }

  /**
   * Display form pengaduan2.
   */
  public function actionPengaduan2() {
    return $this->render(
                    'pengaduan2', [
                'model' => null,
                    ]
    );
  }

  /**
   * Display foto kegiatan.
   *
   * @return type
   */
  public function actionFotoKegiatan() {
    return $this->render(
                    "fotokegiatan", [
                'model' => null,
                    ]
    );
  }

  /**
   * Display link pengaduan.
   *
   * @return type
   */
  public function actionLinkPengaduan() {
    return $this->render(
                    "linkpengaduan", [
                'model' => null,
                    ]
    );
  }

  /**
   * Display jumlah penanganan pengaduan.
   *
   * @return type
   */
  public function actionJumlahPenangananPengaduan() {
    $sisaPengaduan = \common\models\Tahapan::countAllTahapanFirstUnfinished(30, 'pengaduan');
    $sisaPengusulan = \common\models\Tahapan::countAllTahapanFirstUnfinished(30, 'pengusulan');

    return $this->render(
                    "jumlahpenangananpengaduan", [
                'model' => null,
                'tahapPengaduan' => $sisaPengaduan,
                'countDialihkan' => \common\models\Tahapan::countDialihkan(30)['pengaduan_assesmen']['count'],
                'tahapPengusulan' => $sisaPengusulan,
                    ]
    );
  }

  /**
   * Display peta potensi.
   *
   * @return type
   */
  public function actionPetaPotensi() {

    return $this->render(
                    'petapotensi', [
                'jumlah_potensi' => \common\utils\DataPeta::GetJumlahPotensiHutanAdat(),
                'jumlah_pencantuman' => \common\utils\DataPeta::GetJumlahPencantumanHutanAdat(),
                    ]
    );
  }

  /**
   * Display Direktorat PKTHA
   *
   * @return type
   */
  public function actionDirektoratPktha() {
    $model = Artikel::findOne(['id' => 26]);

    return $this->render(
                    "direktoratpktha", [
                'model' => $model,
                    ]
    );
  }

  public function actionStrukturOrganisasi() {
    $model = Artikel::findOne(['id' => 27]);

    return $this->render(
                    "strukturorganisasi", [
                'model' => $model,
                    ]
    );
  }

  public function actionTugasPokokDanFungsi() {
    $model = Artikel::findOne(['id' => 28]);

    return $this->render(
                    "tugaspokokdanfungsi", [
                'model' => $model,
                    ]
    );
  }

  public function actionLainLain() {
    return $this->render(
                    "lainlain", [
                'model' => null,
                    ]
    );
  }

  public function actionPengusulan() {
    $model = new \frontend\models\UsulanForm();

    if ($model->load(Yii::$app->request->post())) {
      if ($model->validate()) {
        // form inputs are valid, do something here
        return;
      }
    }

    return $this->render(
                    'FormPengusulanRegistrasi', [
                'model' => $model,
                    ]
    );
  }

  public function actionPeraturanTerbaru() {
    $model = Artikel::find()->where(['id' => '20'])->one();

    return $this->render(
                    'peraturanterbaru', [
                'dataProvider' => new \yii\data\ActiveDataProvider(
                        [
                    'query' => Artikel::find()->where(["kategori_id" => 23, 'publish' => 1])->orderBy('id desc'),
                    'pagination' => ['pageSize' => 8],
                        ]
                ),
                    ]
    );
  }

  public function actionKontakDirektoratPktha() {
    $model = Artikel::findOne(['id' => 25]);

    return $this->render(
                    "kontakdirektoratpktha", [
                'model' => $model,
                    ]
    );
  }

  public function actionKontakDirjenPskl() {
    $model = Artikel::findOne(['id' => 29]);
    return $this->render(
                    "kontakdirjenpskl", [
                'model' => $model,
                    ]
    );
  }

  public function actionFormPengaduanKonflik() {
    $model = new \common\models\PengaduanRegistrasi();

    if ($model->load(Yii::$app->request->post())) {
      // Pasang tanggal created date, modified date. created date sama dengan modified date, dan kode konflik.
      $model->created_date = \common\utils\DateHelper::GetCurrentDate();
      $model->modified_date = \common\utils\DateHelper::GetCurrentDate();
      $model->tanggal_pengaduan = \common\utils\DateHelper::GetCurrentDate();
      $model->kode_pengaduan = \common\utils\CaseCodeGenerator::GetCode();

      // Generating key
      $rawKey = \common\utils\CaseCodeGenerator::random_str(6);
      $model->kunci_pengaduan = sha1($rawKey);

      if (!$model->save()) {
        throw new ServerErrorHttpException('Gagal Submit Pengaduan Konflik.');
      } else {
        $model->kode_pengaduan .= $model->id;
        $model->save();
      }

      $model_id = $model->id;

      // Buat record terkait konflik ini di masing-masing tabel tahap.
      $model_deskstudy = new \common\models\PengaduanDeskStudy();
      $model_deskstudy->id_pengaduan = $model->id;
      $model_deskstudy->created_date = \common\utils\DateHelper::GetCurrentDate();
      $model_deskstudy->modified_date = \common\utils\DateHelper::GetCurrentDate();
      $model_deskstudy->finished = false;
      $model_deskstudy->save();

      $model_assesmen = new \common\models\PengaduanAssesmen();
      $model_assesmen->id_pengaduan = $model_id;
      $model_assesmen->created_date = \common\utils\DateHelper::GetCurrentDate();
      $model_assesmen->modified_date = \common\utils\DateHelper::GetCurrentDate();
      $model_assesmen->finished = false;

      $model_drafting_mou = new \common\models\PengaduanDraftingMou();
      $model_drafting_mou->id_pengaduan = $model_id;
      $model_drafting_mou->created_date = \common\utils\DateHelper::GetCurrentDate();
      $model_drafting_mou->modified_date = \common\utils\DateHelper::GetCurrentDate();
      $model_drafting_mou->finished = false;

      $model_mediasi = new \common\models\PengaduanMediasi();
      $model_mediasi->id_pengaduan = $model_id;
      $model_mediasi->created_date = \common\utils\DateHelper::GetCurrentDate();
      $model_mediasi->modified_date = \common\utils\DateHelper::GetCurrentDate();
      $model_mediasi->finished = false;

      $model_pra_mediasi = new \common\models\PengaduanPraMediasi();
      $model_pra_mediasi->id_pengaduan = $model_id;
      $model_pra_mediasi->created_date = \common\utils\DateHelper::GetCurrentDate();
      $model_pra_mediasi->modified_date = \common\utils\DateHelper::GetCurrentDate();
      $model_pra_mediasi->finished = false;

      $model_tanda_tangan_mou = new \common\models\PengaduanTandaTanganMou();
      $model_tanda_tangan_mou->id_pengaduan = $model_id;
      $model_tanda_tangan_mou->created_date = \common\utils\DateHelper::GetCurrentDate();
      $model_tanda_tangan_mou->modified_date = \common\utils\DateHelper::GetCurrentDate();
      $model_tanda_tangan_mou->finished = false;


      /*
       * Only save if child tables are okay.
       */
      if (!$model_assesmen->save() || !$model_drafting_mou->save() || !$model_mediasi->save() || !$model_pra_mediasi->save() || !$model_tanda_tangan_mou->save()) {
        $this->deletePengaduan($model_id);
        throw new ServerErrorHttpException('Pengaduan konflik gagal.');
      } else {
        $pengaduanInfo = new \common\viewmodels\PengaduanCreatedInfoViewModel();
        $pengaduanInfo->kode_pengaduan = $model->kode_pengaduan;
        $pengaduanInfo->kunci_pengaduan = $rawKey;

        return $this->render('PengaduanCreated', ['model' => $pengaduanInfo]);
      }
    } else {
      return $this->render(
                      'FormPengaduanKonflik', [
                  'model' => $model,
                  'jumlah_potensi' => \common\utils\DataPeta::GetJumlahPotensiHutanAdat(),
                  'jumlah_pencantuman' => \common\utils\DataPeta::GetJumlahPencantumanHutanAdat()
                      ]
      );
    }
  }

  /**
   * Deletes existing Pengaduan models.
   * If deletion is successful, the browser will be redirected to the 'index' page.
   *
   * @param  string $id
   * @return mixed
   */
  public function deletePengaduan($id) {
    // On deletion of a pengaduan also delete its files, if any.
    if (($model = \common\models\PengaduanRegistrasi::findOne($id)) !== null) {
      $path = $model->getPath();
      if (is_dir($path)) {
        array_map('unlink', glob("$path/*/*"));
      }
      $model->delete();
    }
  }

  public function actionFormPengusulanRegistrasi() {
    $model = new \common\models\PengusulanRegistrasi();

    if ($model->load(Yii::$app->request->post())) {
      if (!$model->validate()) {
        echo 'error validation';
        echo \yii\helpers\Json::encode($model->getErrors());
        return;
      }
      if ($model->validate()) {

        // form inputs are valid, do something here

        $model->created_date = \common\utils\DateHelper::GetCurrentDate();
        $model->modified_date = \common\utils\DateHelper::GetCurrentDate();
        $model->kode_pengusulan = \common\utils\CaseCodeGenerator::GetHACode();
        $model->tanggal_pengusulan = \common\utils\DateHelper::GetCurrentDate();

        $rawKey = \common\utils\CaseCodeGenerator::random_str(6);
        $model->kunci_pengusulan = sha1($rawKey);

        // Upload files
        $resources = array(
            'form' => UploadedFile::getInstance($model, 'form_data'),
            'profil_mha' => UploadedFile::getInstance($model, 'profil_mha_data'),
            'surat_kuasa' => UploadedFile::getInstance($model, 'surat_kuasa_data'),
            'surat_pernyataan' => UploadedFile::getInstance($model, 'surat_pernyataan_data'),
            'produk_hukum_daerah' => UploadedFile::getInstance($model, 'produk_hukum_daerah_data'),
            'peta' => UploadedFile::getInstance($model, 'peta_data'),
            'identitas_ketua_adat' => UploadedFile::getInstance($model, 'identitas_ketua_adat_data'),
        );

        $attrs = array('form', 'profil_mha', 'surat_kuasa', 'surat_pernyataan', 'produk_hukum_daerah', 'peta', 'identitas_ketua_adat');
        if ($model->save()) {

          $model->kode_pengusulan .= $model->id;

          $uploadPath = $model->getPath();
          foreach ($attrs as $value) {
            $dirpath = $uploadPath . '/' . $value;
            mkdir($dirpath, 0775, true);

            if (isset($resources[$value])) {
              $filename = $resources[$value]->baseName . '.' . $resources[$value]->extension;
              $filepath = $dirpath . '/' . $filename;
              $resources[$value]->saveAs($filepath);
              $model[$value . '_filename'] = $filename;
            }
          }
          $model->save();
          // if ($model->save()) {
          // Create Validasi.
          $pengusulanII = new \common\models\PengusulanValidasi();
          $pengusulanII->id_pengusulan = $model->id;
          $pengusulanII->created_date = \common\utils\DateHelper::GetCurrentDate();
          $pengusulanII->modified_date = \common\utils\DateHelper::GetCurrentDate();
          $pengusulanII->finished = false;
          $pengusulanII->save();

          // Create Verifikasi.
          $pengusulanIII = new \common\models\PengusulanVerifikasi();
          $pengusulanIII->id_pengusulan = $model->id;
          $pengusulanIII->created_date = \common\utils\DateHelper::GetCurrentDate();
          $pengusulanIII->modified_date = \common\utils\DateHelper::GetCurrentDate();
          $pengusulanIII->finished = false;
          $pengusulanIII->save();

          // Create pencantuman.
          $pengusulanIV = new \common\models\PengusulanPencantuman();
          $pengusulanIV->id_pengusulan = $model->id;
          $pengusulanIV->created_date = \common\utils\DateHelper::GetCurrentDate();
          $pengusulanIV->modified_date = \common\utils\DateHelper::GetCurrentDate();
          $pengusulanIV->finished = false;
          $pengusulanIV->save();

          // Create Monev.
          $pengusulanV = new \common\models\PengusulanMonev();
          $pengusulanV->id_pengusulan = $model->id;
          $pengusulanV->created_date = \common\utils\DateHelper::GetCurrentDate();
          $pengusulanV->modified_date = \common\utils\DateHelper::GetCurrentDate();
          $pengusulanV->finished = false;
          $pengusulanV->save();

          // Create Permohonan model.
          $pengusulanPermohonan = new \common\models\PengusulanPermohonan();
          $pengusulanPermohonan->id_pengusulan = $model->id;
          $pengusulanPermohonan->dikembalikan = false;
          $pengusulanPermohonan->finished = false;
          $pengusulanPermohonan->created_date = \common\utils\DateHelper::GetCurrentDate();
          $pengusulanPermohonan->modified_date = \common\utils\DateHelper::GetCurrentDate();
          if (!$pengusulanPermohonan->save(false)) {
            return 'Error Entry Permohonan';
          }

          $pengusulanInfo = new \common\viewmodels\PengusulanCreatedInfoViewModel();
          $pengusulanInfo->kode_pengusulan = $model->kode_pengusulan;
          $pengusulanInfo->kunci_pengusulan = $rawKey;

          return $this->render('PengusulanCreated', ['model' => $pengusulanInfo]);
          // } else {
          //    throw new \yii\web\ServerErrorHttpException('Usulan tidak berhasil dikirim. Mohon coba lagi di lain waktu.');
          // }
        } else {
          throw new \yii\web\ServerErrorHttpException('Usulan tidak berhasil dikirim. Mohon coba lagi di lain waktu.');
        }
      }
    } else {
      return $this->render('FormPengusulanRegistrasi', ['model' => $model]);
    }
  }

  public function actionFormRegistrasiMasyarakatHukumAdat() {
    $model = new \common\models\Mha();

    if ($model->load(Yii::$app->request->post())) {
      if ($model->validate()) {
        // form inputs are valid, do something here
        // Save the mode first to get id.
        if ($model->save()) {
          // Save external file
          $array = ['struktur_lembaga_adat_filename', 'tugas_fungsi_pemangku_adat_filename', 'aturan_adat_terkait_wilayah_sda_filename', 'aturan_adat_terkait_pranata_sosial_filename'];
          $uploadPath = $model->getPath();

          foreach ($array as $value) {
            $dirpath = $uploadPath . '/' . $value;
            if (!is_dir($dirpath)) {
              mkdir($dirpath, 0775, true);
            }

            $res = UploadedFile::getInstance($model, $value);
            // if ($res) {
            $filename = $res->baseName . '.' . $res->extension;
            $filepath = $dirpath . '/' . $filename;
            $res->saveAs($filepath);
            // }
          }

          if ($model->save()) {
            return;
          }
        }
        return;
      }
    }

    return $this->render('FormRegistrasiMasyarakatHukumAdat', [
                'model' => $model,
    ]);
  }

  public function actionTatacaraRegistrasiMasyarakatHukumAdat() {
    return $this->actionFormRegistrasiMasyarakatHukumAdat();
  }

  public function actionLoginDashboard() {
    return $this->redirect(Yii::$app->urlManagerBackend->baseUrl);
  }

  public function actionPencantumanhutanadat($id) {
    $pencantuman = \common\utils\DataPeta::GetSpasialPencantumanHutanAdat($id);

    return ($pencantuman);
  }

  public function actionPotensihutanadat($id) {
    $potensi = \common\utils\DataPeta::GetSpasialPotensiHutanAdat($id);

    return ($potensi);
  }

  public function actionProdukhukumprovinsi($id) {
    $produkprovinsi = \common\utils\DataPeta::GetSpasialProdukHukumProvinsi($id);

    return ($produkprovinsi);
  }

  public function actionProdukhukumkabupaten($id) {
    $produkkabupaten = \common\utils\DataPeta::GetSpasialProdukHukumKabupaten($id);

    return ($produkkabupaten);
  }

  public function actionLokasipengaduan($role = null) {
    $lokasipengaduan = \common\utils\DataPeta::GetSpasialLokasiPengaduan($role);

    return ($lokasipengaduan);
  }

  public function actionKasus2016() {
    $kasus2016 = \common\utils\DataPeta::GetSpasialKasus2016();

    return ($kasus2016);
  }

  public function actionKasus2017() {
    $kasus2017 = \common\utils\DataPeta::GetSpasialKasus2017();

    return ($kasus2017);
  }

  public function actionApiStart() {
    return 'API Works. Documentation will later be provided.';
  }

  public function actionGetPengaduanByStep($step = null) {
    $_steps = [
        'desk-study' => 'AND (pengaduan_desk_study.finished = true) '
        . 'AND (pengaduan_assesmen.dialihkan = false) '
        . 'AND (pengaduan_assesmen.finished = false) '
        . 'AND (pengaduan_pra_mediasi.finished = false)'
        . 'AND (pengaduan_mediasi.finished = false)'
        . 'AND (pengaduan_drafting_mou.finished = false)'
        . 'AND (pengaduan_tanda_tangan_mou.finished = false)',
        'assesmen' => 'AND (pengaduan_desk_study.finished = true) '
        . 'AND (pengaduan_assesmen.dialihkan = false) '
        . 'AND (pengaduan_assesmen.finished = true) '
        . 'AND (pengaduan_pra_mediasi.finished = false) '
        . 'AND (pengaduan_mediasi.finished = false) '
        . 'AND (pengaduan_drafting_mou.finished = false) '
        . 'AND (pengaduan_tanda_tangan_mou.finished = false) ',
        'pra-mediasi' => 'AND (pengaduan_desk_study.finished = true) '
        . 'AND (pengaduan_assesmen.dialihkan = false) '
        . 'AND (pengaduan_assesmen.finished = true) '
        . 'AND (pengaduan_pra_mediasi.finished = true) '
        . 'AND (pengaduan_mediasi.finished = false) '
        . 'AND (pengaduan_drafting_mou.finished = false) '
        . 'AND (pengaduan_tanda_tangan_mou.finished = false) ',
        'mediasi' => 'AND (pengaduan_desk_study.finished = true) '
        . 'AND (pengaduan_assesmen.dialihkan = false) '
        . 'AND (pengaduan_assesmen.finished = true) '
        . 'AND (pengaduan_pra_mediasi.finished = true) '
        . 'AND (pengaduan_mediasi.finished = true) '
        . 'AND (pengaduan_drafting_mou.finished = false) '
        . 'AND (pengaduan_tanda_tangan_mou.finished = false) ',
        'drafting-mou' => 'AND (pengaduan_desk_study.finished = true) '
        . 'AND (pengaduan_assesmen.dialihkan = false) '
        . 'AND (pengaduan_assesmen.finished = true) '
        . 'AND (pengaduan_pra_mediasi.finished = true) '
        . 'AND (pengaduan_mediasi.finished = true) '
        . 'AND (pengaduan_drafting_mou.finished = true) '
        . 'AND (pengaduan_tanda_tangan_mou.finished = false) ',
        'tanda-tangan-mou' => 'AND (pengaduan_desk_study.finished = true) '
        . 'AND (pengaduan_assesmen.dialihkan = false) '
        . 'AND (pengaduan_assesmen.finished = true) '
        . 'AND (pengaduan_pra_mediasi.finished = true) '
        . 'AND (pengaduan_mediasi.finished = true) '
        . 'AND (pengaduan_drafting_mou.finished = true) '
        . 'AND (pengaduan_tanda_tangan_mou.finished = true) ',
        'selesai' => 'AND (pengaduan_desk_study.finished = true) '
        . 'AND (pengaduan_assesmen.finished = true) '
        . 'AND (pengaduan_pra_mediasi.finished = true) '
        . 'AND (pengaduan_mediasi.finished = true) '
        . 'AND (pengaduan_drafting_mou.finished = true) '
        . 'AND (pengaduan_tanda_tangan_mou.finished = true) ',
        'dialihkan' => 'AND (pengaduan_desk_study.finished = false) '
        . 'AND (pengaduan_assesmen.dialihkan = true) '
        . 'AND (pengaduan_assesmen.finished = true) '
        . 'AND (pengaduan_pra_mediasi.finished = false) '
        . 'AND (pengaduan_mediasi.finished = false) '
        . 'AND (pengaduan_drafting_mou.finished = false) '
        . 'AND (pengaduan_tanda_tangan_mou.finished = false) ',
    ];
    
    $_def_columns = 'pengaduan_registrasi.nama_pengadu,
                pengaduan_registrasi.latitude_daerah_konflik,
                pengaduan_registrasi.longitude_daerah_konflik,
                provinsi.nama_provinsi,
                kota_kabupaten.nama_kota_kabupaten,
                kecamatan.nama_kecamatan,
                desa_kelurahan.nama_desa_kelurahan';
    
    $_columns = [
        'desk-study' => $_def_columns . '',
        'assesmen' => $_def_columns . '',
        'pra-mediasi' => $_def_columns . '',
        'mediasi' => $_def_columns . '',
        'drafting-mou' => $_def_columns . '',
        'tanda-tangan-mou' => $_def_columns . '',
        'selesai' => $_def_columns . '',
        'dialihkan' => $_def_columns . ''
    ];

    if (isset($_steps[$step])) {
      $sql = '
            SELECT 
                ' . $_columns[$step] . '
                
            FROM 
                pengaduan_registrasi, 
                pengaduan_desk_study,
                pengaduan_assesmen, 
                pengaduan_pra_mediasi, 
                pengaduan_mediasi, 
                pengaduan_drafting_mou, 
                pengaduan_tanda_tangan_mou,
                provinsi,
                kota_kabupaten,
                kecamatan,
                desa_kelurahan

            WHERE 
                (pengaduan_registrasi.id = pengaduan_assesmen.id_pengaduan) 
                AND (pengaduan_registrasi.id_provinsi_konflik = provinsi.id)
                AND (pengaduan_registrasi.id_kota_kabupaten_konflik = kota_kabupaten.id)
                AND (pengaduan_registrasi.id_kecamatan_konflik = kecamatan.id)
                AND (pengaduan_registrasi.id_desa_kelurahan_konflik = desa_kelurahan.id)
                AND (pengaduan_registrasi.id = pengaduan_desk_study.id_pengaduan) 
                AND (pengaduan_registrasi.id = pengaduan_pra_mediasi.id_pengaduan) 
                AND (pengaduan_registrasi.id = pengaduan_mediasi.id_pengaduan) 
                AND (pengaduan_registrasi.id = pengaduan_drafting_mou.id_pengaduan) 
                AND (pengaduan_registrasi.id = pengaduan_tanda_tangan_mou.id_pengaduan)';

      $sql = $sql . $_steps[$step];

      $result = \Yii::$app->db->createCommand($sql)->queryAll();

      return \yii\helpers\Json::encode($result);
    } else {
      return 'Data Pengaduan tidak tersedia';
    }
  }

  public function actionGetPengusulanByStep($step = null) {
    $_steps = [
        'permohonan' => 'AND (pengusulan_permohonan.finished = true)'
        . 'AND (pengusulan_permohonan.dikembalikan = false)'
        . 'AND (pengusulan_validasi.finished = false)'
        . 'AND (pengusulan_verifikasi.finished = false)'
        . 'AND (pengusulan_pencantuman.finished = false)'
        . 'AND (pengusulan_monev.finished = false)',
        'validasi' => 'AND (pengusulan_permohonan.finished = true)'
        . 'AND (pengusulan_validasi.finished = true)'
        . 'AND (pengusulan_verifikasi.finished = false)'
        . 'AND (pengusulan_pencantuman.finished = false)'
        . 'AND (pengusulan_monev.finished = false)',
        'verifikasi' => 'AND (pengusulan_permohonan.finished = true)'
        . 'AND (pengusulan_validasi.finished = true)'
        . 'AND (pengusulan_verifikasi.finished = true)'
        . 'AND (pengusulan_pencantuman.finished = false)'
        . 'AND (pengusulan_monev.finished = false)',
        'monev' => 'AND (pengusulan_permohonan.finished = true)'
        . 'AND (pengusulan_validasi.finished = true)'
        . 'AND (pengusulan_verifikasi.finished = true)'
        . 'AND (pengusulan_pencantuman.finished = true)'
        . 'AND (pengusulan_monev.finished = true)',
        'pencantuman' => 'AND (pengusulan_permohonan.finished = true)'
        . 'AND (pengusulan_validasi.finished = true)'
        . 'AND (pengusulan_verifikasi.finished = true)'
        . 'AND (pengusulan_pencantuman.finished = true)'
        . 'AND (pengusulan_monev.finished = false)',
        'selesai' => 'AND (pengusulan_permohonan.finished = true)'
        . 'AND (pengusulan_validasi.finished = true)'
        . 'AND (pengusulan_verifikasi.finished = true)'
        . 'AND (pengusulan_pencantuman.finished = true)'
        . 'AND (pengusulan_monev.finished = true)',
        'dikembalikan' => 'AND (pengusulan_permohonan.finished = true)'
        . 'AND (pengusulan_permohonan.dikembalikan = true)'
        . 'AND (pengusulan_validasi.finished = false)'
        . 'AND (pengusulan_verifikasi.finished = false)'
        . 'AND (pengusulan_pencantuman.finished = false)'
        . 'AND (pengusulan_monev.finished = false)',
    ];
    
    $_def_columns = "pengusulan_registrasi.nama_pengusul,
                pengusulan_registrasi.nama_mha,
                provinsi.nama_provinsi, 
                kota_kabupaten.nama_kota_kabupaten,
                kecamatan.nama_kecamatan,
                desa_kelurahan.nama_desa_kelurahan";
    
    $_columns = [
        'permohonan' => $_def_columns,
        'validasi' => $_def_columns,
        'verifikasi' => $_def_columns,
        'pencantuman' => $_def_columns . ',
                pengusulan_pencantuman.latitude,
                pengusulan_pencantuman.longitude,
                pengusulan_pencantuman.luas_sk_penetapan,
                pengusulan_pencantuman.luas_sk_pencantuman,
                pengusulan_pencantuman.no_sk_pencantuman,
                pengusulan_pencantuman.no_sk_penetapan,
                pengusulan_pencantuman.tanggal_sk_penetapan,
                pengusulan_pencantuman.tanggal_sk_pencantuman',
        'monev' => $_def_columns,
        'selesai' => $_def_columns,
        'dikembalikan' => $_def_columns,
    ];
    
    if (isset($_steps[$step])) {
      $sql = '
            SELECT 
                ' . $_columns[$step] . '
                
            FROM 
                pengusulan_registrasi, 
                pengusulan_validasi, 
                pengusulan_verifikasi, 
                pengusulan_monev, 
                pengusulan_pencantuman, 
                pengusulan_permohonan,
                provinsi,
                kota_kabupaten,
                kecamatan,
                desa_kelurahan

            WHERE 
                (pengusulan_registrasi.id = pengusulan_validasi.id_pengusulan)
                AND (pengusulan_registrasi.id_provinsi = provinsi.id)
                AND (pengusulan_registrasi.id_kota_kabupaten = kota_kabupaten.id)
                AND (pengusulan_registrasi.id_kecamatan = kecamatan.id)
                AND (pengusulan_registrasi.id_desa_kelurahan = desa_kelurahan.id)
                AND (pengusulan_registrasi.id = pengusulan_verifikasi.id_pengusulan) 
                AND (pengusulan_registrasi.id = pengusulan_permohonan.id_pengusulan) 
                AND (pengusulan_registrasi.id = pengusulan_monev.id_pengusulan) 
                AND (pengusulan_registrasi.id = pengusulan_pencantuman.id_pengusulan)';

      $sql = $sql . $_steps[$step];

      $result = \Yii::$app->db->createCommand($sql)->queryAll();

      return \yii\helpers\Json::encode($result);
    } else {
      return 'Data Pengusulan tidak tersedia';
    }
  }

  public function actionGetPengaduanNotFinished() {
    $all = \common\models\PengaduanRegistrasi::find()->count();
    $finished = count(\yii\helpers\Json::decode($this->actionGetPengaduanByStep('selesai')));

    return ($all - $finished);
  }

  public function actionGetPengusulanNotFinished() {
    $all = \common\models\PengusulanRegistrasi::find()->count();
    $finished = count(\yii\helpers\Json::decode($this->actionGetPengusulanByStep('selesai')));

    return ($all - $finished);
  }
}
