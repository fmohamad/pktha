<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace frontend\controllers;

use yii\web\Controller;

/**
 * Description of StatisticsController
 *
 * @author wizard
 */
class StatisticsController extends \yii\web\Controller {

    //put your code here

    public static function GetPengaduanRegistrasi() {
        $sql = '
            SELECT 
                pengaduan_registrasi.kode_pengaduan
                
            FROM 
                pengaduan_registrasi, 
                pengaduan_assesmen, 
                pengaduan_pra_mediasi, 
                pengaduan_mediasi, 
                pengaduan_drafting_mou, 
                pengaduan_tanda_tangan_mou

            WHERE 
                (pengaduan_registrasi.id = pengaduan_assesmen.id_pengaduan) 
                AND (pengaduan_registrasi.id = pengaduan_pra_mediasi.id_pengaduan) 
                AND (pengaduan_registrasi.id = pengaduan_mediasi.id_pengaduan) 
                AND (pengaduan_registrasi.id = pengaduan_drafting_mou.id_pengaduan) 
                AND (pengaduan_registrasi.id = pengaduan_tanda_tangan_mou.id_pengaduan) 
                AND (pengaduan_assesmen.finished = false)';

        $result = \Yii::$app->db->createCommand($sql)->queryAll();

        $count = count($result);

        return $count;
    }

    public static function GetPengaduanAssessment() {

        $sql = '
            SELECT 
                pengaduan_registrasi.kode_pengaduan
                
            FROM 
                pengaduan_registrasi, 
                pengaduan_assesmen, 
                pengaduan_pra_mediasi, 
                pengaduan_mediasi, 
                pengaduan_drafting_mou, 
                pengaduan_tanda_tangan_mou

            WHERE 
                (pengaduan_registrasi.id = pengaduan_assesmen.id_pengaduan) 
                AND (pengaduan_registrasi.id = pengaduan_pra_mediasi.id_pengaduan) 
                AND (pengaduan_registrasi.id = pengaduan_mediasi.id_pengaduan) 
                AND (pengaduan_registrasi.id = pengaduan_drafting_mou.id_pengaduan) 
                AND (pengaduan_registrasi.id = pengaduan_tanda_tangan_mou.id_pengaduan) 
                AND (pengaduan_assesmen.finished = true AND pengaduan_assesmen.finished IS NOT NULL)
                AND (pengaduan_pra_mediasi.finished = false OR pengaduan_pra_mediasi.finished IS NULL)
                AND (pengaduan_mediasi.finished = false OR pengaduan_mediasi.finished IS NULL)
                AND (pengaduan_drafting_mou.finished = false OR pengaduan_drafting_mou.finished IS NULL)
                AND (pengaduan_tanda_tangan_mou.finished = false OR pengaduan_tanda_tangan_mou.finished IS NULL)';

        $result = \Yii::$app->db->createCommand($sql)->queryAll();

        $count = count($result);

        return $count;
    }

    public static function GetPengaduanPraMediasi() {

        $sql = '
            SELECT 
                pengaduan_registrasi.kode_pengaduan
	
            FROM 
                pengaduan_registrasi, 
                pengaduan_assesmen, 
                pengaduan_pra_mediasi, 
                pengaduan_mediasi, 
                pengaduan_drafting_mou, 
                pengaduan_tanda_tangan_mou 

            WHERE 
                (pengaduan_registrasi.id = pengaduan_assesmen.id_pengaduan) 
                AND (pengaduan_registrasi.id = pengaduan_pra_mediasi.id_pengaduan) 
                AND (pengaduan_registrasi.id = pengaduan_mediasi.id_pengaduan) 
                AND (pengaduan_registrasi.id = pengaduan_drafting_mou.id_pengaduan) 
                AND (pengaduan_registrasi.id = pengaduan_tanda_tangan_mou.id_pengaduan) 
                AND (pengaduan_assesmen.finished = true AND pengaduan_assesmen.kasus_dialihkan = false)
                AND (pengaduan_pra_mediasi.finished = true AND pengaduan_pra_mediasi.finished IS NOT NULL)';

        $result = \Yii::$app->db->createCommand($sql)->queryAll();

        $count = count($result);

        return $count;
    }

    public static function GetPengaduanMediasi() {

        $sql = '
            SELECT
                pengaduan_registrasi.kode_pengaduan
	
            FROM 
                pengaduan_registrasi, 
                pengaduan_assesmen, 
                pengaduan_pra_mediasi, 
                pengaduan_mediasi, 
                pengaduan_drafting_mou, 
                pengaduan_tanda_tangan_mou

            WHERE 
                (pengaduan_registrasi.id = pengaduan_assesmen.id_pengaduan) 
                AND (pengaduan_registrasi.id = pengaduan_pra_mediasi.id_pengaduan) 
                AND (pengaduan_registrasi.id = pengaduan_mediasi.id_pengaduan) 
                AND (pengaduan_registrasi.id = pengaduan_drafting_mou.id_pengaduan) 
                AND (pengaduan_registrasi.id = pengaduan_tanda_tangan_mou.id_pengaduan) 
                AND (pengaduan_assesmen.finished = true)
                AND (pengaduan_assesmen.kasus_dialihkan = false OR pengaduan_assesmen.kasus_dialihkan IS NULL)
                AND (pengaduan_pra_mediasi.finished = true)
                AND (pengaduan_mediasi.finished = true AND pengaduan_mediasi.finished IS NOT NULL)
                AND (pengaduan_drafting_mou.finished = false OR pengaduan_drafting_mou.finished IS NULL)
                AND (pengaduan_tanda_tangan_mou.finished = false OR pengaduan_tanda_tangan_mou.finished IS NULL)';

        $result = \Yii::$app->db->createCommand($sql)->queryAll();

        $count = count($result);

        return $count;
    }

    public static function GetPengaduanDraftingMoU() {

        $sql = '
            SELECT 
                pengaduan_registrasi.kode_pengaduan
	
            FROM 
                pengaduan_registrasi, 
                pengaduan_assesmen, 
                pengaduan_pra_mediasi, 
                pengaduan_mediasi, 
                pengaduan_drafting_mou, 
                pengaduan_tanda_tangan_mou

            WHERE 
                (pengaduan_registrasi.id = pengaduan_assesmen.id_pengaduan) 
                AND (pengaduan_registrasi.id = pengaduan_pra_mediasi.id_pengaduan) 
                AND (pengaduan_registrasi.id = pengaduan_mediasi.id_pengaduan) 
                AND (pengaduan_registrasi.id = pengaduan_drafting_mou.id_pengaduan) 
                AND (pengaduan_registrasi.id = pengaduan_tanda_tangan_mou.id_pengaduan) 
                AND (pengaduan_assesmen.finished = true)
                AND (pengaduan_pra_mediasi.finished = true)
                AND (pengaduan_mediasi.finished = true)
                AND (pengaduan_drafting_mou.finished = true AND pengaduan_drafting_mou.finished IS NOT NULL)
                AND (pengaduan_tanda_tangan_mou.finished = false OR pengaduan_tanda_tangan_mou.finished IS NULL)';

        $result = \Yii::$app->db->createCommand($sql)->queryAll();

        $count = count($result);

        return $count;
    }

    public static function GetPengaduanTandaTanganMoU() {

        $sql = '
            SELECT 
                pengaduan_registrasi.kode_pengaduan
	
            FROM 
                pengaduan_registrasi, 
                pengaduan_assesmen, 
                pengaduan_pra_mediasi, 
                pengaduan_mediasi, 
                pengaduan_drafting_mou, 
                pengaduan_tanda_tangan_mou

            WHERE 
                (pengaduan_registrasi.id = pengaduan_assesmen.id_pengaduan) 
                AND (pengaduan_registrasi.id = pengaduan_pra_mediasi.id_pengaduan) 
                AND (pengaduan_registrasi.id = pengaduan_mediasi.id_pengaduan) 
                AND (pengaduan_registrasi.id = pengaduan_drafting_mou.id_pengaduan) 
                AND (pengaduan_registrasi.id = pengaduan_tanda_tangan_mou.id_pengaduan) 
                AND (pengaduan_assesmen.finished = true)
                AND (pengaduan_pra_mediasi.finished = true)
                AND (pengaduan_mediasi.finished = true)
                AND (pengaduan_drafting_mou.finished = true)
                AND (pengaduan_tanda_tangan_mou.finished = true AND pengaduan_tanda_tangan_mou.finished IS NOT NULL)';

        $result = \Yii::$app->db->createCommand($sql)->queryAll();

        $count = count($result);

        return $count;
    }

    public static function GetPengaduanDialihkan() {
        $sql = '
            SELECT 
                pengaduan_registrasi.kode_pengaduan
	
            FROM 
                pengaduan_registrasi, 
                pengaduan_assesmen, 
                pengaduan_pra_mediasi, 
                pengaduan_mediasi, 
                pengaduan_drafting_mou, 
                pengaduan_tanda_tangan_mou

            WHERE 
                (pengaduan_registrasi.id = pengaduan_assesmen.id_pengaduan) 
                AND (pengaduan_registrasi.id = pengaduan_pra_mediasi.id_pengaduan) 
                AND (pengaduan_registrasi.id = pengaduan_mediasi.id_pengaduan) 
                AND (pengaduan_registrasi.id = pengaduan_drafting_mou.id_pengaduan) 
                AND (pengaduan_registrasi.id = pengaduan_tanda_tangan_mou.id_pengaduan) 
                AND (pengaduan_assesmen.finished = true AND pengaduan_assesmen.kasus_dialihkan = true)';

        $result = \Yii::$app->db->createCommand($sql)->queryAll();

        $count = count($result);

        return $count;
    }
    
    public static function GetPengusulanValidasi() {
        $sql = '
            SELECT 
                pengusulan_hutan_adat."kode_pengusulan"	
            FROM 
                pengusulan_hutan_adat, 
                "pengusulan_hutan_adat_II",
                "pengusulan_hutan_adat_III",
                "pengusulan_hutan_adat_IV",
                "pengusulan_hutan_adat_V"

            WHERE 
                (pengusulan_hutan_adat.id = "pengusulan_hutan_adat_II"."id_pengusulan")
                AND (pengusulan_hutan_adat.id = "pengusulan_hutan_adat_III"."id_pengusulan") 
                AND (pengusulan_hutan_adat.id = "pengusulan_hutan_adat_IV"."id_pengusulan") 
                AND (pengusulan_hutan_adat.id = "pengusulan_hutan_adat_V"."id_pengusulan") 
                AND ("pengusulan_hutan_adat_II".finished = true)
                AND ("pengusulan_hutan_adat_III".finished = false OR "pengusulan_hutan_adat_III".finished IS NULL)
                AND ("pengusulan_hutan_adat_IV".finished = false OR "pengusulan_hutan_adat_IV".finished IS NULL)
                AND ("pengusulan_hutan_adat_V".finished = false OR "pengusulan_hutan_adat_V".finished IS NULL)';
        
        $result = \Yii::$app->db->createCommand($sql)->queryAll();

        $count = count($result);

        return $count;
    }
    
    public static function GetPengusulanVerifikasi() {
        $sql = '
            SELECT 
                pengusulan_hutan_adat.id
                
            FROM 
                pengusulan_hutan_adat, 
                "pengusulan_hutan_adat_II",
                "pengusulan_hutan_adat_III",
                "pengusulan_hutan_adat_IV",
                "pengusulan_hutan_adat_V"

            WHERE 
                (pengusulan_hutan_adat.id = "pengusulan_hutan_adat_II"."id_pengusulan") 
                AND (pengusulan_hutan_adat.id = "pengusulan_hutan_adat_III"."id_pengusulan") 
                AND (pengusulan_hutan_adat.id = "pengusulan_hutan_adat_IV"."id_pengusulan") 
                AND (pengusulan_hutan_adat.id = "pengusulan_hutan_adat_V"."id_pengusulan") 
                AND ("pengusulan_hutan_adat_II".finished = true)
                AND ("pengusulan_hutan_adat_III".finished = true)
                AND ("pengusulan_hutan_adat_IV".finished = false OR "pengusulan_hutan_adat_IV".finished IS NULL)
                AND ("pengusulan_hutan_adat_V".finished = false OR "pengusulan_hutan_adat_V".finished IS NULL)';
        
        $result = \Yii::$app->db->createCommand($sql)->queryAll();

        $count = count($result);

        return $count;
    }
    
    public static function GetPengusulanPencantumanPenetapan() {
        $sql = '
            SELECT 
                pengusulan_hutan_adat.id
	
            FROM 
                pengusulan_hutan_adat, 
                "pengusulan_hutan_adat_II",
                "pengusulan_hutan_adat_III",
                "pengusulan_hutan_adat_IV",
                "pengusulan_hutan_adat_V"

            WHERE 
                (pengusulan_hutan_adat.id = "pengusulan_hutan_adat_II"."id_pengusulan") 
                AND (pengusulan_hutan_adat.id = "pengusulan_hutan_adat_III"."id_pengusulan") 
                AND (pengusulan_hutan_adat.id = "pengusulan_hutan_adat_IV"."id_pengusulan") 
                AND (pengusulan_hutan_adat.id = "pengusulan_hutan_adat_V"."id_pengusulan") 
                AND ("pengusulan_hutan_adat_II".finished = true)
                AND ("pengusulan_hutan_adat_III".finished = true)
                AND ("pengusulan_hutan_adat_IV".finished = true)
                AND ("pengusulan_hutan_adat_V".finished = false OR "pengusulan_hutan_adat_V".finished IS NULL)';
        
        $result = \Yii::$app->db->createCommand($sql)->queryAll();

        $count = count($result);

        return $count;
    }
    
    public static function GetPengusulanMonevPemberdayaan() {
        $sql = '
            SELECT 
                pengusulan_hutan_adat.id
	
            FROM 
                pengusulan_hutan_adat, 
                "pengusulan_hutan_adat_II",
                "pengusulan_hutan_adat_III",
                "pengusulan_hutan_adat_IV",
                "pengusulan_hutan_adat_V"

            WHERE 
                (pengusulan_hutan_adat.id = "pengusulan_hutan_adat_II"."id_pengusulan") 
                AND (pengusulan_hutan_adat.id = "pengusulan_hutan_adat_III"."id_pengusulan") 
                AND (pengusulan_hutan_adat.id = "pengusulan_hutan_adat_IV"."id_pengusulan") 
                AND (pengusulan_hutan_adat.id = "pengusulan_hutan_adat_V"."id_pengusulan") 
                AND ("pengusulan_hutan_adat_II".finished = true)
                AND ("pengusulan_hutan_adat_III".finished = true)
                AND ("pengusulan_hutan_adat_IV".finished = true)
                AND ("pengusulan_hutan_adat_V".finished = true)';
        
        $result = \Yii::$app->db->createCommand($sql)->queryAll();

        $count = count($result);

        return $count;
    }

}
