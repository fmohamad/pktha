<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use common\models\Contact;

/**
 * ContactForm is the model behind the contact form.
 */
class ContactForm extends Model
{
    public $name;
    public $email;
    public $subject;
    public $body;
    public $verifyCode;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['name', 'email', 'subject', 'body'], 'required'],
            // email has to be a valid email address
            ['email', 'email'],
            // verifyCode needs to be entered correctly
            ['verifyCode', 'captcha'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'verifyCode' => 'Verification Code',
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     *
     * @param  string  $email the target email address
     * @return boolean whether the email was sent
     */
    public function sendEmail($email)
    {
        
        $contact =  new Contact();
        $contact->nama = $this->name;
        $contact->email = $this->email;
        $contact->prihal = $this->subject;
        $contact->isi = $this->body;
        $contact->save();//


        // Yii::$app->mailer->compose()
        //     ->setFrom(["pengaduan.pktha@gmail.com"])
        //         ->setTo( ["vei_btx@yahoo.com"])
        //         ->setSubject("ngirim kamfrett " . date ("H:i:s"))
        //         ->setTextBody("Useless body frontend")
        //         ->send();



        return true;
// Yii::$app->mailer->compose()
//         ->setFrom(["akps.menlhk@gmail.com"])
//         ->setTo( ["vei_btx@yahoo.com"])
//         ->setSubject("ngirim kamfrett " . date ("H:i:s"))
//         ->setTextBody("Useless body")
//         ->send();


        // return Yii::$app->mailer->compose()
        //     ->setTo([$email=>'SISTEM PENGADUAN KONFLIK TENURIAL DAN HUTAN ADAT'])
        //     ->setFrom([$this->email => $this->name])

        //     // ->setFrom([$email=>'PENGADUAN KONFLIK TENURIAL DAN HUTAN ADAT'])
        //     // ->setTo([$this->email => $this->name])

        //     ->setSubject($this->subject)
        //     ->setTextBody($this->body)
        //     ->send();
    }
}
