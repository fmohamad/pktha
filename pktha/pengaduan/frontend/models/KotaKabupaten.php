<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "{{%kota_kabupaten}}".
 *
 * @property string $id_provinsi
 * @property string $nama_kota_kabupaten
 * @property string $id
 * @property double $latitude
 * @property double $longitude
 */
class KotaKabupaten extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%kota_kabupaten}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_provinsi'], 'required'],
            [['id_provinsi'], 'integer'],
            [['nama_kota_kabupaten'], 'string'],
            [['latitude', 'longitude'], 'number'],
            [['id_provinsi'], 'exist', 'skipOnError' => true, 'targetClass' => Provinsi::className(), 'targetAttribute' => ['id_provinsi' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_provinsi' => Yii::t('app', 'Id Provinsi'),
            'nama_kota_kabupaten' => Yii::t('app', 'Nama Kota/Kabupaten'),
            'id' => Yii::t('app', 'ID'),
            'latitude' => Yii::t('app', 'Latitude'),
            'longitude' => Yii::t('app', 'Longitude'),
        ];
    }

    /**
     * @inheritdoc
     * @return KotaKabupatenQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new KotaKabupatenQuery(get_called_class());
    }
}
