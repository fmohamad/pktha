<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Mha */
/* @var $form ActiveForm */
?>
<div class="FormRegistrasiMasyarakatHukumAdat">

    <?php
    $form = ActiveForm::begin([
                'options' => ['enctype' => 'multipart/form-data'],
    ]);
    ?>

    <section> <!-- Kontak Pemohon -->
        <?= $form->field($model, 'nama') ?>
        <?= $form->field($model, 'jabatan') ?>
        <?= $form->field($model, 'alamat') ?>
        <?= $form->field($model, 'email') ?>  
        <?= $form->field($model, 'telepon') ?>
    </section>

    <section> <!-- Kewilayahan -->
        <?=
        $form->field($model, 'id_provinsi')->dropDownList(
                \yii\helpers\ArrayHelper::map(\common\models\Provinsi::find()->all(), 'id', 'nama_provinsi'), [
            'prompt' => 'Pilih Provinsi',
            'onchange' => '$.post("' . \yii\helpers\Url::to(["location/kota-kabupaten"]) . '", {id_provinsi: $(this).val()}, function(data){ $("#mha-id_kota_kabupaten").html(data); })',
                ]
        )->label('Provinsi')
        ?>
        <?=
        $form->field($model, 'id_kota_kabupaten')->dropDownList(
                \yii\helpers\ArrayHelper::map(\common\models\KotaKabupaten::find()->all(), 'id', 'nama_kota_kabupaten'), [
            'prompt' => 'Pilih Kota/Kabupaten',
            'onchange' => '$.post("' . \yii\helpers\Url::to(["location/kecamatan"]) . '", {id_kota_kabupaten: $(this).val()}, function(data){ $("#mha-id_kecamatan").html(data); })',
                ]
        )->label('Kota/Kabupaten')
        ?>
        <?=
        $form->field($model, 'id_kecamatan')->dropDownList(
                \yii\helpers\ArrayHelper::map(\common\models\Kecamatan::find()->where(['id' => 1])->all(), 'id', 'nama_kecamatan'), [
            'prompt' => 'Pilih Kecamatan',
                ]
        )->label('Kecamatan')
        ?>
    </section>

    <section> <!-- Kewilayahan -->
        <?= $form->field($model, 'nama_komunitas') ?>
        <?= $form->field($model, 'bahasa') ?>
        <?= $form->field($model, 'batas_utara_wilayah_adat') ?>
        <?= $form->field($model, 'batas_selatan_wilayah_adat') ?>
        <?= $form->field($model, 'batas_timur_wilayah_adat') ?>
        <?= $form->field($model, 'batas_barat_wilayah_adat') ?>
        <?= $form->field($model, 'satuan_wilayah_adat') ?>
        <?= $form->field($model, 'kondisi_fisik_wilayah') ?>
        <?= $form->field($model, 'luas_wilayah_adat') ?>
    </section>

    <section> <!-- Kependudukan -->
        <?= $form->field($model, 'jumlah_kepala_keluarga') ?>
        <?= $form->field($model, 'jumlah_pria') ?>
        <?= $form->field($model, 'jumlah_wanita') ?>
        <?= $form->field($model, 'mata_pencaharian_utama') ?>
    </section>

    <section> <!-- Sejarah -->
        <?= $form->field($model, 'sejarah_singkat_masyarakat_adat')->textarea() ?>
    </section>

    <section> <!-- Hak atas Tanah dan Pengelolaan Wilayah -->
        <?= $form->field($model, 'pembagian_ruang_menurut_aturan_adat') ?>
        <?= $form->field($model, 'sistem_penguasaan_pengelolaan_wilayah') ?>
    </section>

    <section> <!-- Kelembagaan Adat -->
        <div class="row">
            <div class="col-xs-12">
                <?= $form->field($model, 'nama_lembaga_adat') ?>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'struktur_lembaga_adat') ?>
            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'struktur_lembaga_adat_filename')->fileInput() ?>
            </div>
        </div>
        <div class="row">
            <div>
                <div class="col-xs-6">
                    <?= $form->field($model, 'tugas_fungsi_pemangku_adat') ?>
                </div>
                <div class="col-xs-6">
                    <?= $form->field($model, 'tugas_fungsi_pemangku_adat_filename')->fileInput() ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <?= $form->field($model, 'mekanisme_pengambilan_keputusan') ?>
            </div>
        </div>
    </section>

    <section> <!-- Hukum Adat -->
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'aturan_adat_terkait_wilayah_sda') ?>
            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'aturan_adat_terkait_wilayah_sda_filename')->fileInput() ?>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'aturan_adat_terkait_pranata_sosial') ?>
            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'aturan_adat_terkait_pranata_sosial_filename')->fileInput() ?>    
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <?= $form->field($model, 'contoh_putusan_hukum_adat') ?>
            </div>
        </div>
    </section>

    <section> <!-- Keanekaragaman Hayati -->
        <?= $form->field($model, 'jenis_ekosistem') ?>
        <?= $form->field($model, 'potensi_manfaat_sda') ?>
        <?= $form->field($model, 'sumber_pangan') ?>
        <?= $form->field($model, 'sumber_kesehatan_kecantikan') ?>
        <?= $form->field($model, 'sumber_papan_infrastruktur') ?>
        <?= $form->field($model, 'sumber_sandang') ?>
        <?= $form->field($model, 'sumber_rempah_bumbu') ?>
        <?= $form->field($model, 'sumber_pendapatan_ekonomi') ?>
    </section>

    <section> <!-- Peta Wilayah Adat -->
        <?= $form->field($model, 'peta_wilayah_adat_filename')->fileInput() ?>
    </section>

    <section>
        <?= $form->field($model, 'status_registrasi_musyawarah') ?>
    </section>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div><!-- FormRegistrasiMasyarakatHukumAdat -->
