<?php
/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\widgets\ListView;

$this->title = 'Tata Cara Pengaduan Konflik';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-tatacara site-page">
    <h1><?= Html::encode($this->title) ?></h1>
    <div class="row">
        <?php if (isset($model->judul)) :  ?>
        <div class="col-md-12 isikonten">
            <h5><?= $model->judul; ?></h5>
            <?php if (strlen($model->image) > 3): ?>
                <img src="<?= Yii::getAlias('@web') . '/../../uploads/' . $model->image; ?>" alt="image" width="100%" class="img-responsive" /> 
            <?php endif; ?>
            <p><?= $model->konten; ?></p>
        </div>
        <?php else: ?>
        <div class="col-md-12 isikonten">
            <div>tidak ada konten</div>
        </div>
        <?php endif; ?>
    </div>
    <p class="center"><a class="btn btn-primary" href="<?= \yii\helpers\Url::to(['site/form-pengaduan-konflik']) ?>">Ajukan Pengaduan</a></p>
</div>