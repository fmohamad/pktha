<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<h1>Tata Cara Pengusulan Hutan Adat</h1>
<!--
<div class="row">
    <div class="col-md-12">
        <div class="col-md-6">
            <h4>Alur Perizinan Hutan Adat di luar Kawasan Hutan (APL)</h4>
            <image src="../web/images/hutan-adat/alur-perizinan-hutan-adat-di-luar-apl.jpeg" width="500" />
        </div>
        <div class="col-md-6">
            <h4>Alur Perizinan Hutan Adat di dalam Kawasan Hutan (APL)</h4>
            <image src="../web/images/hutan-adat/alur-perizinan-hutan-adat-didalam-apl.jpeg" width="500" />
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-offset-3 col-md-6">
        <h4>Alur Proses Verifikasi dan Validasi Hutan Adat</h4>
        <image src="../web/images/hutan-adat/alur-proses-verifikasi-validasi-hutan-adat.jpeg" width="500" />
    </div>
</div>
-->
<div class="row">
    <h1><?= \yii\helpers\Html::encode($this->title) ?></h1>
    <div class="row">
        <?php if (isset($model->judul)) : ?>
            <div class="col-md-12 isikonten">
                <!-- <h5><?= $model->judul; ?></h5 >-->
                <?php if (strlen($model->image) > 3): ?>
                    <img src="<?= Yii::getAlias('@web') . '/../../uploads/' . $model->image; ?>" alt="image" width="100%" class="img-responsive" /> 
                <?php endif; ?>
                <p><?= $model->konten; ?></p>
            </div>
        <?php else: ?>
            <div class="col-md-12 isikonten">
                <div>tidak ada konten</div>
            </div>
        <?php endif; ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="col-md-3">
            <p class="center"><a class="btn btn-primary" href="<?= \yii\helpers\Url::to(['site/form-pengusulan-registrasi']) ?>">Ajukan Pengusulan Hutan Adat</a></p>
        </div>
        <div class="col-md-3">
            <button class = "btn btn-primary" type="submit" onclick="window.open('doc/Form Identifikasi Masyarakat Hukum Adat.doc')">Download Form Identifikasi</button>
        </div>
        <div class="col-md-3">
            <button class = "btn btn-primary" type="submit" onclick="window.open('doc/Surat Formulir Permohonan Hutan Hak.doc')">Download Surat Formulir</button>
        </div>
        <div class="col-md-3">
            <button class = "btn btn-primary" type="submit" onclick="window.open('doc/Surat Pernyataan.docx')">Download Surat Pernyataan</button>
        </div>
    </div>
</div>




