<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$form = ActiveForm::begin();
?>

<div class="right_sidebar last" style="padding:0px;">
    <div id= "top_header_menu_r">
        <h6><i class="fa fa-file"></i>&nbsp;&nbsp;FORM PENGADUAN</h6>
    </div>
    <div id= "top_header_menu_r2">
        <h6><i class="fa fa-user"></i>&nbsp;&nbsp;IDENTITAS PENGGUNA</h6>
    </div>
    <div class="contbox_st1">
        <?= $form->field($model, 'id_tahapan')->hiddenInput(['value' => 1])->label(''); ?>
        <?= $form->field($model, 'nama_identitas')->textInput(['maxlength' => true])->label('Nama') ?>
        <?= $form->field($model, 'telepon_identitas')->textInput(['maxlength' => true, 'type' => 'tel'])->label('Telepon') ?>
        <?= $form->field($model, 'email_identitas')->textInput(['maxlength' => true, 'type' => 'email'])->label('Email') ?>
        <?= $form->field($model, 'alamat_identitas')->textarea(['rows' => 2])->label('Alamat') ?>
        <?= $form->field($model, 'tipe_identitas')->dropDownList(array('1' => 'Kementerian/Lembaga', '2' => 'Individu', '3' => 'Perusahaan', '4' => 'Kelompok/Organisasi Masyarakat'))->label('Identitas Pelapor') ?>
    </div>
    <div id= "top_header_menu_r2">
        <h6><i class="fa fa-file-text"></i>&nbsp;&nbsp;PENGADUAN</h6>
    </div>
    <div class="contbox_st1">
        <?= $form->field($model, 'tutuntan_pengaduan')->textInput(['maxlength' => true])->label('Tema') ?>
        <?= $form->field($model, 'pihak_berkonflik')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'rentang_waktu')->textInput() ?>

        <?= $form->field($model, 'wilayah')->dropDownList($model->getListWilayah(), ['prompt' => 'pilih wilayah...']); ?>


        <?php
        $js = <<<JS
  $("#pengaduan-rentang_waktu").datepicker( {
            format: "mm-yyyy",
            viewMode: "months", 
            minViewMode: "months",
            autoclose: true

        });
        
    
JS;
        $this->registerJs($js, \yii\web\View::POS_READY);
        ?>


        <?=
        $form->field($model, 'provinsi_konflik')->dropDownList(\yii\helpers\ArrayHelper::map(\common\models\Provinsi::find()->all(), 'proid', 'provinsi'), [
            'prompt' => '--Provinsi--',
            'onchange' => '
        $.get(
            "' . yii::$app->urlManager->createUrl('site/kabkota') . '",
            { id: $(this).val() }
            )
    .done(function( data ) {

        $("select#pengaduan-kabkota_konflik").html(data);
    })',
        ])->label('Provinsi')
        ?>

        <?= $form->field($model, 'kabkota_konflik')->dropDownList($model->getKabkotList($model->provinsi_konflik), ['prompt' => '--Pilih Kabupaten / Kota--',])->label('Kabkota') ?>


        <?= $form->field($model, 'lokasi_konflik')->textInput(['maxlength' => true])->label('Lokasi Lengkap') ?>
        <?= $form->field($model, 'fungsi_kawasan')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'luasan_kawasan')->textInput(['maxlength' => true]) ?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Lapor' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            <br/>
        </div>

        <?php ActiveForm::end(); ?>

    </div>


    <div id= "top_header_menu_r">
        <h6><i class="fa fa-thumbs-o-up"></i>&nbsp;&nbsp;<?= Html::a('USULAN HUTAN ADAT', ['usulan']); ?></h6>
    </div>

    <div id="top_header_menu_r3">
        <h6><i class="fa fa-question-circle"></i>&nbsp;&nbsp;TATA CARA</h6>
    </div>

    <div class="contbox_st1">
        <div class="section_holder64">
            <div class="list_st3">
                <div class="icon"><i class="fa fa-angle-right"></i></div>
                <div class="text">Tahap 1 Registrasi dan Penyaringan Registrasi</div>
            </div>
            <!--end item-->

            <div class="list_st3">
                <div class="icon"><i class="fa fa-angle-right"></i></div>
                <div class="text">Tahap 2 Verifikasi Kelengkapan Data</div>
            </div>
            <!--end item-->

            <div class="list_st3">
                <div class="icon"><i class="fa fa-angle-right"></i></div>
                <div class="text">Tahap 3 Assesment Lapangan</div>
            </div>
            <!--end item-->

            <div class="list_st3">
                <div class="icon"><i class="fa fa-angle-right"></i></div>
                <div class="text">Tahap 4 Analisa Permasalahan</div>
            </div>
            <!--end item-->

            <div class="list_st3">
                <div class="icon"><i class="fa fa-angle-right"></i></div>
                <div class="text"> Tahap 5 Rekomendasi</div>
            </div>
            <!--end item-->
            <br/>
            <br/>
            <?php
            $url = Url::to(['site/tatacara']);
            ?>
            <a href="#"><?= Html::a("Selengkapnya >>", $url); ?></a>
            <!--end item-->
        </div>
    </div>
    <div id="top_header_menu_r3">
        <h6><i class="fa fa-gavel"></i>&nbsp;&nbsp;Peraturan dan Perundangan:</h6>
    </div>
    <div class="contbox_st1">
        <div class="section_holder64">
            <?php
            $aa = \yii\helpers\ArrayHelper::map(\common\models\Artikel::find()->where(['kategori_id' => 23])->limit(10)->all(), 'kategori_id', 'judul');
            ?>
            <?php
            foreach ($aa as $key => $value) :
                ?>
                <div class="list_st1 two">

                    <div class="icon"><i class="fa fa-leaf"></i></div>
                    <div class="text"><?= $value; ?></div>
                </div>
                <?php
            endforeach;
            ?>
            <!--end item-->

            <!--end item--> 
        </div>
    </div>
</div>



