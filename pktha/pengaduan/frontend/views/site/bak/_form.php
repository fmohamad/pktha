<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
$form = ActiveForm::begin();
?>

    <h4>Identitas Pengguna</h4>
    <?= $form->field($model,'id_tahapan')->hiddenInput(['value'=>1])->label('');?>
    <?= $form->field($model, 'nama_identitas')->textInput(['maxlength' => true])->label('Nama') ?>
    <?= $form->field($model, 'telepon_identitas')->textInput(['maxlength' => true,'type'=>'tel'])->label('Telepon') ?>
    <?= $form->field($model, 'email_identitas')->textInput(['maxlength' => true,'type'=>'email'])->label('Email') ?>
    <?= $form->field($model, 'alamat_identitas')->textarea(['rows' => 2])->label('Alamat') ?>
    <?= $form->field($model, 'tipe_identitas')->dropDownList(array('1' => 'Kementerian/Lembaga','2' => 'Individu','3' => 'Perusahaan','4' => 'Kelompok/Organisasi Masyaraka'))->label('Identitas Pelapor')  ?>
    <h4>Pengaduan</h4>
    <?= $form->field($model, 'tutuntan_pengaduan')->textInput(['maxlength' => true])->label('Tema') ?>
    <?= $form->field($model, 'pihak_berkonflik')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'rentang_waktu')->textInput() ?>


    <?= $form->field($model, 'provinsi_konflik')->dropDownList(\yii\helpers\ArrayHelper::map(\common\models\Provinsi::find()->all(),'proid','provinsi'),
        [
        'prompt'=>'--Provinsi--',

        'onchange'=>'
        $.get(
            "' . yii::$app->urlManager->createUrl('site/kabkota') . '",
            { id: $(this).val() }
            )
    .done(function( data ) {

        $("select#pengaduan-kabkota_konflik").html(data);
    })',
    ])->label('Provinsi') ?>

    <?= $form->field($model, 'kabkota_konflik')->dropDownList($model->getKabkotList($model->provinsi_konflik),['prompt'=>'--Pilih Kabupaten / Kota--',])->label('Kabkota') ?>


	<?= $form->field($model, 'lokasi_konflik')->textInput(['maxlength' => true])->label('Lokasi Lengkap') ?>
	<?= $form->field($model, 'fungsi_kawasan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'luasan_kawasan')->textInput(['maxlength' => true]) ?>



        <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Lapor' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>

    </div>
<br/>

    <?php ActiveForm::end(); ?>
