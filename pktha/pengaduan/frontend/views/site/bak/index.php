<?php
use yii\helpers\Html; 
/* @var $this yii\web\View */

$this->title = 'Sistem Database Konflik Tenurial versi 1';
$this->registerJsFile(Yii::getAlias('@web').'/js/jquery.flexslider.js',['depends' => [yii\web\JqueryAsset::className()]]);
$this->registerCssFile(Yii::getAlias('@web').'/css/flexslider.css');

// $this->registerJsFile(Yii::getAlias('@web').'js/jquery.flexslider.js',['position' => yii\web\View::POS_END]);
?>
<?php
$this->registerJs('
    $(window).load(function(){

        $(".flexslider").flexslider({
            animation: "slide",
            slideshow: true,
            start: function(slider){
              $("body").removeClass("loading");
          }
      });
});', yii\web\View::POS_END);
?>

<div class="row headline"><!-- Begin Headline -->
    <div class="col-md-8">
        <div class="flexslider nudge">
            <ul class="slides">

            <?php 

            
            foreach ($mdlSlider as $ms) :?>
                
                <li><a href="#"><img src="<?=Yii::getAlias('@web').'/../../uploads/slider/'.$ms->img;?>" alt="image" class="img-responsive imgx" /></a></li>
                <?php endforeach;?>    





        </ul>
    </div>
    <div class="preview-main">
        <h5 class="title-bg"><i class="fa fa-star"></i>Cerita Sukses</h5>
        <div class="row">
            <?php foreach ($cds as $cdsx) :?>
                
            <div class="col-md-6">
                <a href="#" class="thumbnail"><img src="<?=Yii::getAlias('@web').'/../../uploads/'.$cdsx->image;?>" alt="image" class="img-responsive imgx" /></a>
                <span class="project-details">
                <?= Html::a($cdsx->judul, ['detail', 'id' => $cdsx->id]); ?>
                <?=yii\helpers\BaseStringHelper::truncate($cdsx->headline,50,' ...');?></span>
            </div>
            <?php endforeach;?>    


        </div>
    </div>
    <br/>
     <div class="preview-main">
        <h5 class="title-bg"><i class="fa fa-newspaper-o"></i>Berita &amp; Peristiwa</h5>
        <div class="row">
            <?php foreach ($bdp as $bdpx) :?>
                
            <div class="col-md-6">
                <a href="#" class="thumbnail"><img src="<?=Yii::getAlias('@web').'/../../uploads/'.$bdpx->image;?>"  alt="image" class="img-responsive imgx" /></a>
                <span class="project-details">
                <?= Html::a($bdpx->judul, ['detail', 'id' => $bdpx->id]); ?>
                <?=yii\helpers\BaseStringHelper::truncate($bdpx->headline,50,' ...');?></span>
            </div>
            <?php endforeach;?>    

        </div>
    </div>
</div>
<div class="col-md-4">
    <div class="form-pengaduan">
        <h3>Form Pengaduan</h3>
        <?= $this->render('_form', [
            'model' => $model,
            ]) ?>
        </div>
    </div>
</div><!-- End Headline -->
