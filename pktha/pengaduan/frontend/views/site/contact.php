<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use common\widgets\Alert;

$this->title = 'HUBUNGI KAMI';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-contact site-page">
    <div class="container">
        <h1><?= Html::encode($this->title) ?></h1>

        <p>Hubungi kami untuk menanyakan permasalahan seputar perijinan dan pendaftaran</p>

        <div class="row">
            <div class="col-lg-5">
                <br/>
                <?= Alert::widget() ?>
                <br/>
                <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>
                <br/>
                <?= $form->field($model, 'name')->label('Nama'); ?>

                <?= $form->field($model, 'email')->label('Alamat Email'); ?>

                <?= $form->field($model, 'subject')->label('Perihal'); ?>

                <?= $form->field($model, 'body')->textArea(['rows' => 6])->label('Isi'); ?>

                <?=
                $form->field($model, 'verifyCode')->label('Kode Verifikasi')->widget(Captcha::className(), [
                    'template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-6">{input}</div></div>',
                ])
                ?>

                <div class="form-group">
<?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'contact-button']); ?>
                </div>

<?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
