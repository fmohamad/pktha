<?php
/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = $model->judul;

$url = 'site/' . strtolower(str_replace(" ", "", $model->kategori->kategori));

if ($model->kategori->kategori == "Kegiatan") {
    $url = 'site/kegiatandanberita';
}

$this->params['breadcrumbs'][] = ['label' => $model->kategori->kategori, 'url' => [$url]];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="site-single site-page">
    <div class="container">
        <div class="section">
            <div class="post_img">
                <div class="img"><div class="date"> <span><?= Yii::$app->formatter->asDatetime($model->created_at, "php:d"); ?></span><?= Yii::$app->formatter->asDatetime($model->created_at, "php: M Y"); ?></div>
                    <?php
                    if (isset($model->image)) {
                        ?>
                        <div class="row">
                            <div class="col-md-2"></div>
                            <div class="col-md-8">
                                <img src="<?= Yii::getAlias('@web') . '/../../uploads/' . $model->image; ?>" alt="image" class="img_size1" width="100%">
                            </div>
                            <div class="col-md-2"></div>
                        </div>
                        <?php
                    }
                    ?>
                </div>
                <div class="bottom_strip"></div>
                <div class="bottom_shape"></div>
            </div> <!--end img-->
            <div class="post_text">
                <a href="#"><h3 class="uppercase padd_top1"><?= Html::encode($model->judul) ?></h3></a>
                <span><?= $model->headline ?></span>
                <p><?= $model->konten ?></p><br>
                <div class="divider_line"></div>
            </div>
        </div>
    </div>
</div>