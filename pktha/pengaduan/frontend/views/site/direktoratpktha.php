<div class="site-page">
    <div class="row">
        <div class="title_wrapper2">
            <div class="ikon2 ikon_7" style="width: 90px; margin-right: 10px;"></div>
            <?php if ($model && $model->judul) : ?>
                <h2><?= $model->judul ?></h2>
            <?php else : ?>
                <h2>Direktorat PKTHA</h2>
            <?php endif ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?php if ($model && $model->image) : ?>
                <img src="<?= Yii::getAlias('@web') . '/../../uploads/' . $model->image; ?>" alt="image" width="100%" class="img-responsive" /> 
            <?php endif ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?php if ($model && $model->konten) : ?>
                <?= $model->konten ?>
            <?php else : ?>
                <div class="under_construction cl_4">
                    <div class="ikon_uc"></div>
                    THIS PAGE IS UNDER CONSTRUCTION
                </div>
            <?php endif ?>
        </div>
    </div>
</div>
