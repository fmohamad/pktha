<?php

use yii\helpers\Html;
use yii\jui\Dialog;
use yii\web\View;
?>
<?php
$session = Yii::$app->session;
?>

<?php
Yii::$app->view->registerCSSFile('css/map/leaflet.css', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerCSSFile('//maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerCSSFile('css/map/L.Control.ZoomBar.css', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerCSSFile('css/map/L.Control.Locate.css', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerCSSFile('css/map/leaflet.groupedlayercontrol.min.css', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerCSSFile('css/map/L.Control.BetterScale.css', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerCSSFile('css/map/iconLayers.css', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerCSSFile('css/map/MarkerCluster.css', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerCSSFile('css/map/MarkerCluster.Default.css', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerCSSFile('css/map/leaflet_geosearch.css', ['position' => yii\web\View::POS_HEAD]);

Yii::$app->view->registerJsFile('js/map/leaflet-src.js', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerJsFile('js/map/L.Control.ZoomBar.js', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerJsFile('js/map/L.Control.Locate.js', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerJsFile('js/map/leaflet-color-markers.js', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerJsFile('js/map/leaflet.groupedlayercontrol.min.js', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerJsFile('js/map/L.Control.BetterScale.js', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerJsFile('js/map/iconLayers.js', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerJsFile('js/map/leaflet-providers.js', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerJsFile('js/map/leaflet.markercluster.js', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerJsFile('js/map/esri_leaflet.js', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerJsFile('js/map/jquery.js', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerJsFile('js/map/feature_group.js', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerJsFile('js/map/leaflet_geosearch2.js', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerJsFile('js/map/leaflet-fill-Pattern.js', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerJsFile('js/map/panggil_peta.js', ['position' => yii\web\View::POS_END]);
Yii::$app->view->registerJs('var link_lokasi_pengaduan = ' . json_encode(yii\helpers\Url::to(['/site/lokasipengaduan'])) . ';', yii\web\View::POS_HEAD);
Yii::$app->view->registerJs('var link_pencantuman_HA = ' . json_encode(yii\helpers\Url::to(['/site/pencantumanhutanadat'])) . ';', yii\web\View::POS_HEAD);
Yii::$app->view->registerJs('var link_potensi_HA = ' . json_encode(yii\helpers\Url::to(['/site/potensihutanadat'])) . ';', yii\web\View::POS_HEAD);
Yii::$app->view->registerJs('var link_kasus2016 = ' . json_encode(yii\helpers\Url::to(['/site/kasus2016'])) . ';', yii\web\View::POS_HEAD);
Yii::$app->view->registerJs('var link_kasus2017 = ' . json_encode(yii\helpers\Url::to(['/site/kasus2017'])) . ';', yii\web\View::POS_HEAD);
Yii::$app->view->registerJs('var link_produk_hukum_provinsi = ' . json_encode(yii\helpers\Url::to(['/site/produkhukumprovinsi'])) . ';', yii\web\View::POS_HEAD);
Yii::$app->view->registerJs('var link_produk_hukum_kabupaten = ' . json_encode(yii\helpers\Url::to(['/site/produkhukumkabupaten'])) . ';', yii\web\View::POS_HEAD);
Yii::$app->view->registerJs('var jumlah_pencantuman = ' . $jumlah_pencantuman . ';', yii\web\View::POS_HEAD);
Yii::$app->view->registerJs('var jumlah_potensi = ' . $jumlah_potensi . ';', yii\web\View::POS_HEAD);
?>

<?php
if ($session['showmsg']) {
  $session['showmsg'] = false;
  ?>
  <script type="text/javascript">
    alert("Data anda telah masuk \n kami akan segera memperoses pengaduan anda.\n Terima kasih.");
  </script>
  <?php
}
?>

<div class="section_holder50 ">
    <div class="content" style="padding:0px;">
        <div style="height:30px;">
        </div>
        <div class="tp-banner-container">
            <div class="tp-banner2" >
                <ul>
                    <?php foreach ($mdlSlider as $ms) : ?>
                      <li>
                          <div class="row">
                              <div class="col-md-12">
                                  <!-- <?php echo $ms->name ?> -->
                              </div>
                          </div>
                          <a href="#"><img src="<?php echo Yii::getAlias('@web') . '/../../uploads/slider/' . $ms->img; ?>" width="100%" alt="image" class="img-responsive" /></a>
                          <div class="row">
                              <div class="col-md-12">
                                  <!-- <?php echo $ms->desc ?> -->
                              </div>
                          </div>
                      </li>
                    <?php endforeach; ?>
                </ul>
                <div class="tp-bannertimer"></div>
            </div>
        </div>

        <!-- white space  -->
        <div style="height:30px;">

        </div>

        <!-- LINK PENGADUAN -->
        <div class="box_wrapper" style="padding:12px;">
            <div style="margin-bottom:10px;">
                <div id= "lpengaduan" class=" col-sm-4 col-md-4 text_center" >
                    <h6>LINK PENGADUAN</h6>
                </div>
                <div class="col-sm-4 col-md-4 lkonflik2 ">
                    <button class="btn lkonflik text_center full_w cl_3" onclick="location.href = '<?php echo \yii\helpers\Url::to(['site/form-pengaduan-konflik']) ?>'" type="button">
                        <h6><i class="fa fa-map-marker"></i>&nbsp;&nbsp;PENGADUAN KONFLIK TENURIAL</h6>
                    </button>
                </div>
                <div class=" col-sm-4 col-md-4 lusulan2 ">
                    <button  class="btn lusulan text_center full_w cl_2" onclick="location.href = '<?php echo \yii\helpers\Url::to(['site/form-pengusulan-registrasi']) ?>'" type="button">
                        <h6><i class="fa fa-tree"></i>&nbsp;&nbsp;USULAN HUTAN ADAT</h6>
                    </button>
                </div>
            </div>
            <div class="clear">
            </div>
        </div>
        <div class="title_wrapper">
            <div class="ikon ikon_1" >
            </div>
            <h6><b>STATISTIK PENANGANAN PENGADUAN & PENGUSULAN</b></h6>
        </div>
        <div id="statpengaduan" style="margin-bottom: 0;">
            <div class="statpeng">
                <div class="row statwrap cl_4">
                    <div class="col-sm-2 col-md-2 boxed200">
                        <div class="summary bg-green">
                            <div class="icon">
                                Total Pengaduan Konflik Terdaftar<br />
                                <div class="numTotal"><?= $tahapPengaduan['Registrasi']['count'] ?></div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-1 col-md-1">
                        <div class="arrowed">
                            <div class="arrow-right"></div>
                        </div>
                    </div>

                    <div class="col-sm-9 col-md-9 statDetail">
                        <div class="row">
                            <div class="col-sm-12 col-md-12">
                                <?php
                                $boxNumber = array(
                                    'grey' => 'one',
                                    'red' => 'two',
                                    'yellow' => 'three',
                                    'orange' => 'four',
                                    'green' => 'five',
                                    'blue' => 'six',
                                    'purple' => 'seven',
                                    'maroon' => 'eight');
                                ?>
                                <?php
                                $keys = array_keys($tahapPengaduan);
                                foreach (array_values($tahapPengaduan) as $i => $val) {
                                  if ($keys[$i] != 'Registrasi' && $keys[$i] != 'Dialihkan') {
                                    // $link = \yii\helpers\Url::toRoute(['site/list-pengaduan', 'tahap' => $keys[$i]]);
                                    if ($keys[$i] == 'Tanda Tangan MoU') {
                                      // do nothing. This approach might not be a good idea.
                                    } else if (($keys[$i] == 'Selesai')) {
                                      echo '
                                    <div class="col-sm-2 col-md-2 boxed">
                                        <div class="boxnumber bg-' . $val['warna'] . '">
                                            <div class="icon">' . $val['count'] . '</div>
                                            <div id="triangle-down"></div>
                                            <div id="circle"></div>
                                        </div>
                                        <div class="boxtext">
                                            <h5><a>Tahap ' . ($i - 2) . '</a> <br/>' . 'Tanda Tangan MoU' . '<br/></h5>
                                        </div>
                                    </div>';
                                    } else {
                                      echo '
                                    <div class="col-sm-2 col-md-2 boxed">
                                        <div class="boxnumber bg-' . $val['warna'] . '">
                                            <div class="icon">' . $val['count'] . '</div>
                                            <div id="triangle-down"></div>
                                            <div id="circle"></div>
                                        </div>
                                        <div class="boxtext">
                                            <h5><a>Tahap ' . ($i - 1) . '</a> <br/>' . $keys[$i] . '<br/></h5>
                                        </div>
                                    </div>';
                                    }
                                  }
                                }
                                ?>
                                <!-- 
                                <div class="col-sm-1 col-md-1 boxed">
                                    <div class="boxnumber one">
                                        <div class="icon"><?php echo $countDialihkan ?></div>
                                        <div id="triangle-down"></div>
                                        <div id="circle"></div>
                                    </div>
                                    <div class="boxtext">
                                        <h5><a>Dialihkan</a> <br/><br/></h5>
                                    </div>
                                </div>
                                -->
                                <div class="clear"></div>
                                <div id="statgaris-pengaduan-konflik"></div>

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4 col-md-4 boxed-dialihkan">
                                <div class="boxnumber bg-black">
                                    <div class="icon">
                                        <div style="width: 80%; text-align: left; float: left;">Dialihkan</div>
                                        <div style="width: 20%; text-align: right; float: left;"><?= $countDialihkan ?></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>                    
                </div>
                <?php if(1==0) { ?>
                <div class="row statwrap2">
                    <div id="totpeng" class="col-sm-12 col-md-12" >
                        <h3 >TOTAL PENGADUAN KONFLIK TERDAFTAR : <b><?php echo $tahapPengaduan['Registrasi']['count'] ?> </b>, DIALIHKAN : <b><?php echo $countDialihkan ?></b> </h3>
                    </div>
                </div>
                <?php } ?>
                <br/>
            </div>
            <div class="clear">
            </div>
        </div>
        <div id="statpengaduan" style="margin-top: 0;">
            <div class="statpeng">
                <div class="row statwrap_1 cl_4">
                    <div class="col-sm-2 col-md-2 boxed200">
                        <div class="summary bg-blue">
                            <div class="icon">
                                Total Usulan Hutan Adat Terdaftar<br />
                                <div class="numTotal">38</div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-1 col-md-1">
                        <div class="arrowed">
                            <div class="arrow-right"></div>
                        </div>
                    </div>

                    <div class="col-sm-9 col-md-9 statDetail">
                        <?php
                        $keys = array_keys($tahapPengusulan);
                        foreach (array_values($tahapPengusulan) as $i => $val) {
                          if ($keys[$i] != 'Registrasi' && $keys[$i] != 'Dikembalikan' && $keys[$i] != 'Selesai') {
                            // $link = \yii\helpers\Url::toRoute(['site/list-pengusulan', 'tahap' => $keys[$i]]);
                            echo '
                            <div class="col-sm-2 col-md-2 boxed200">
                                <div class="boxnumber ' . $boxNumber[$val['warna']] . '">
                                    <div class="icon">' . $val['count'] . '</div>
                                    <div id="triangle-down200"></div>
                                    <div id="circle200"></div>
                                </div>
                                <div class="boxtext">
                                    <h5><a>Tahap ' . ($i - 1) . '</a><br/>' . $keys[$i] . '<br/></h5>
                                </div>
                            </div>';
                          }
                        }
                        ?>

                        <div class="clear"></div>
                        <div id="statgaris-pengusulan-konflik"></div>
                    </div>
                    <div class="clear"></div>
                </div>

                <?php if(1==0) { ?>
                <div class="row statwrap2_1">
                    <div id="totpeng" class="col-sm-12 col-md-12" >
                        <h3 >TOTAL USULAN HUTAN ADAT TERDAFTAR: <b><?php echo $tahapPengusulan['Registrasi']['count'] ?></b> </h3>
                    </div>
                </div>
                <?php } ?>
                <br/>
            </div>
            <div class="clear"></div>
        </div>

        <!-- PETA KONFLIK -->
        <div class="title_wrapper" >
            <div class="ikon ikon_2"></div>
            <h6><b>PETA KONFLIK</b></h6>
        </div>
        <div class="map_wrapper last_cnt">
            <div id="map" style="width:100%;height:500px;background:white"></div>
        </div>
        <div class="clear"></div>
    </div>
</div>
<!--end content left-->
</div>
