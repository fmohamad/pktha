<div class="site-page">
    <div class="row">
        <div class="title_wrapper2">
            <div class="ikon2 ikon_5" style="height:145px; margin-bottom:0;">
            </div>
            <h2 style="top:50px;">
                <?php if ($model && $model->judul) : ?>
                    <?= \yii\helpers\Html::encode($model->judul) ?>
                <?php else : ?>
                    Kontak Direktorat PKTHA
                <?php endif; ?>
            </h2>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?php if ($model && $model->image) : ?>
                <img src="<?= Yii::getAlias('@web') . '/../../uploads/' . $model->image; ?>" alt="image" width="100%" class="img-responsive" /> 
            <?php endif ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?php if ($model && $model->konten) : ?>
                <p><?= $model->konten ?></p>
            <?php else : ?>
                <div class="under_construction cl_4">
                    <div class="ikon_uc"></div>
                    THIS PAGE IS UNDER CONSTRUCTION
                </div>
            <?php endif ?>
        </div>
    </div>
</div>