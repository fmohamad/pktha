<div class="site-page">
    <div class="row">
      <div class="title_wrapper200">
        <div class="ikon2 ikon_4" style="height:80px;">
        </div>

        <h2>Link Pengaduan</h2>
      </div>
        <div class="box_wrapper cl_4" style="border:none;">
            <div class="row">
                <div class="col-sm-12 col-md-6">
                    <button class="btn_big linkkonflik" onclick="location.href='<?php echo \yii\helpers\Url::to(['site/form-pengaduan-konflik']) ?>'" type="button">
                        <div class="ikonbtn ikon_konflik"></div>
                        <h6>KONFLIK TENURIAL</h6>
                    </button>
                </div>
                <div class="col-sm-12 col-md-6">
                    <button class="btn_big linkusulan" onclick="location.href='<?php echo \yii\helpers\Url::to(['site/form-pengusulan-registrasi']) ?>'" type="button">
                        <div class="ikonbtn"></div>
                        <h6>USULAN HUTAN ADAT</h6>
                    </button>
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</div>
