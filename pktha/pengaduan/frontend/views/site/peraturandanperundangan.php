<?php
/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\widgets\ListView;

$this->title = 'Peraturan dan Perundangan';
$this->params['breadcrumbs'][] = $this->title;
?>
<head>
    <script src="js/map/jquery.js"></script>
</head>
<div class="site-ceritasukses site-page">
    <div class="title_wrapper2" style="margin-bottom: 60px;">
      <div class="ikon2 ikon_10" style="height:90px">
      </div>
      <h2>Peraturan dan Perundangan</h2>
    </div>
    <div class="rules cl_4">
      <div class="title_r cl_1 per1">
        <span>1. </span>HUTAN HAK
      </div>
      <div class="ov_r per_1">
        <p>PERMEN LHK NO P 32 TAHUN 2015 TENTANG HUTAN HAK</p>
        <a href="uploads/3_PERMEN_LHK_NO_P_32_TAHUN_2015_TENTANG_HUTAN_HAK.pdf" download>Download Pdf</a>
        <div class="clear"></div>
      </div>
    </div>
    <div class="rules cl_4">
      <div class="title_r cl_1 per2">
        <span>2. </span>PENANGANAN KONFLIK TENURIAL
      </div>
      <div class="ov_r per_2">
        <p>PERMEN LHK NO P 84 TAHUN 2015 TENTANG PENANGANAN KONFLIK TENURIAL</p>
        <a href="uploads/4_PERMEN_LHK_NO_P_84_TAHUN_2015_TENTANG_PENANGANAN_KONFLIK_TENURIAL.pdf" download>Download Pdf</a>
        <div class="clear"></div>
      </div>
    </div>
    <div class="rules cl_4">
      <div class="title_r cl_1 per3">
        <span>3. </span>VERIFIKASI HUTAN HAK
      </div>
      <div class="ov_r per_3">
        <p> PERDIRJEN PSKL NO P 1 TENTANG VERIFIKASI HUTAN HAK</p>
        <a href="uploads/5_PERDIRJEN_PSKL_NO_P_1_TENTANG_VERIFIKASI_HUTAN_HAK.pdf" download>Download Pdf</a>
        <div class="clear"></div>
      </div>
    </div>
    <div class="rules cl_4">
      <div class="title_r cl_1 per4">
        <span>4. </span>PEDOMAN MEDIASI KONFLIK TENURIAL KAWASAN HUTAN
      </div>
      <div class="ov_r per_4">
        <p>PERDIRJEN PSKL NO P 4 TENTANG PEDOMAN MEDIASI KONFLIK TENURIAL KAWASAN HUTAN</p>
        <a href="uploads/6_PERDIRJEN_PSKL_NO_P_4_TENTANG_PEDOMAN_MEDIASI_KONFLIK_TENURIAL_KAWASAN_HUTAN.pdf" download>Download Pdf</a>
        <div class="clear"></div>
      </div>
    </div>
    <div class="rules cl_4">
      <div class="title_r cl_1 per5">
        <span>5. </span>PEDOMAN ASESMEN KONFLIK TENURIAL KAWASAN HUTAN
      </div>
      <div class="ov_r per_5">
        <p>PERDIRJEN PSKL NO P 6 TENTANG PEDOMAN ASESMEN KONFLIK TENURIAL KAWASAN HUTAN</p>
        <a href="uploads/7_PERDIRJEN_PSKL_NO_P_6_TENTANG_PEDOMAN_ASESMEN_KONFLIK_TENURIAL_KAWASAN_HUTAN.pdf" download>Download Pdf</a>
        <div class="clear"></div>
      </div>
    </div>
    <div class="rules cl_4">
      <div class="title_r cl_1 per6">
        <span>6. </span>PERHUTANAN SOSIAL
      </div>
      <div class="ov_r per_6">
        <p>PERMENLHK No 83 TENTANG PERHUTANAN SOSIAL</p>
        <a href="uploads/8_PERMENLHK_No_83_TENTANG_PERHUTANAN_SOSIAL.pdf" download>Download Pdf</a>
        <div class="clear"></div>
      </div>
    </div>
    <script type="text/javascript">
    $(".per_1,.per_2,.per_3,.per_4,.per_5,.per_6").hide();
    $(document).ready(function(){
        function toggling(btn,cls) {
            $(btn).click(function(){
                $(cls).slideToggle(150);
            })
        }
        toggling(".per1",".per_1");
        toggling(".per2",".per_2");
        toggling(".per3",".per_3");
        toggling(".per4",".per_4");
        toggling(".per5",".per_5");
        toggling(".per6",".per_6");
    });
    </script>

</div>
