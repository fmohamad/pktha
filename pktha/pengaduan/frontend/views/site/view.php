<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/**
 * @var yii\web\View $this
 * @var app\models\Post $model
 */
$this->title = $model->judul;
$this->params['breadcrumbs'][] = ['label' => "$st", 'url' => ["$st"]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="post-view">
    <h1 class="post-title"><?= Html::a(Html::encode($model->judul), ['view', 'id' => $model->id]); ?></h1>
    <div class="post-info">
        <span class="posted"><?= date("d F Y h:m:s", $model->created_at); ?></span> | <span class="author"><?= Html::encode($model->user->username); ?></span>
    </div>
    <div class="post-body">
    <?= \yii\helpers\HtmlPurifier::process(\yii\helpers\Markdown::process($model->konten)) ?>
    </div>
    <?php
    //echo yii\helpers\BaseHtml::a("Selanjutnya",(['site/view','id'=>$model->id]));
    ?>
</div>