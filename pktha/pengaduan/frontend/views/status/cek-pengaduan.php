<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\viewmodels\PengaduanViewModel */
/* @var $form ActiveForm */
?>
<div class="cek-pengaduan">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'kode_pengaduan') ?>
        <?= $form->field($model, 'nama_pengadu') ?>
        <?= $form->field($model, 'status_pengaduan') ?>
        <?= $form->field($model, 'tanggal_pengaduan') ?>
        <?= $form->field($model, 'tanggal_status_pengaduan') ?>
    
        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- cek-pengaduan -->
