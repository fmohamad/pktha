<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\viewmodels\PengusulanViewModel */
/* @var $form ActiveForm */
?>
<div class="cek-pengusulan">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'kode_pengusulan') ?>
        <?= $form->field($model, 'nama_pengusul') ?>
        <?= $form->field($model, 'status_pengusulan') ?>
        <?= $form->field($model, 'tanggal_pengusulan') ?>
        <?= $form->field($model, 'tanggal_status_pengusulan') ?>
    
        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- cek-pengusulan -->
