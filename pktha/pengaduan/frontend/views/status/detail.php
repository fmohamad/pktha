<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use common\viewmodels\PengaduanViewModel;
use common\viewmodels\PengusulanViewModel;
?>

<div style="padding: 3rem 1.5rem; text-align: center;">
    <h1>
        Status
        <?php if (isset($model)) : ?>
            <?php if ($model instanceof PengaduanViewModel) : ?>
                Pengaduan <?= $model->kode_pengaduan ?>
            <?php elseif ($model instanceof PengusulanViewModel) : ?>
                Pengusulan <?= $model->kode_pengusulan ?>
            <?php endif ?>
        <?php endif ?>
    </h1>
</div>
<div class="col-sm-12" style="padding-left: 33%;">
    <form>
        <div class="form-group row">
            <label class="col-sm-3">
                <?php if (isset($model)) : ?>
                    <?php if ($model instanceof PengaduanViewModel) : ?>
                        Kode Pengaduan
                    <?php elseif ($model instanceof PengusulanViewModel) : ?>
                        Kode Pengusulan
                    <?php endif ?>
                <?php else : ?>
                    <h3>Kode</h3>
                <?php endif ?>
            </label>
            <div class="col-sm-9">
                <?php if (isset($model)) : ?>
                    <?php if ($model instanceof PengaduanViewModel) : ?>
                        <?= $model->kode_pengaduan ?>
                    <?php elseif ($model instanceof PengusulanViewModel) : ?>
                        <?= $model->kode_pengusulan ?>
                    <?php endif ?>
                <?php else : ?>
                    -
                <?php endif ?>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-3">
                <?php if (isset($model)) : ?>
                    <?php if ($model instanceof PengaduanViewModel) : ?>
                        Nama Pengadu
                    <?php elseif ($model instanceof PengusulanViewModel) : ?>
                        Nama Pengusul
                    <?php endif ?>
                <?php else : ?>
                    Nama Pelapor
                <?php endif ?>
            </label>
            <div class="col-sm-9">
                <?php if (isset($model)) : ?>
                    <?php if ($model instanceof PengaduanViewModel) : ?>
                        <?= $model->nama_pengadu ?>
                    <?php elseif ($model instanceof PengusulanViewModel) : ?>
                        <?= $model->nama_pengusul ?>
                    <?php endif ?>
                <?php else : ?>
                    -
                <?php endif ?> 
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-3">
                <?php if (isset($model)) : ?>
                    <?php if ($model instanceof PengaduanViewModel) : ?>
                        Tanggal Pengaduan
                    <?php elseif ($model instanceof PengusulanViewModel) : ?>
                        Tanggal Pengusulan
                    <?php endif ?>
                <?php else : ?>
                    Tanggal Pembuatan
                <?php endif ?>
            </label>
            <div class="col-sm-9">
                <?php if (isset($model)) : ?>
                    <?php if ($model instanceof PengaduanViewModel) : ?>
                        <?= $model->tanggal_pengaduan ?>
                    <?php elseif ($model instanceof PengusulanViewModel) : ?>
                        <?= $model->tanggal_pengusulan ?>
                    <?php endif ?>
                <?php else : ?>
                    -
                <?php endif ?>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-3">
                <?php if (isset($model)) : ?>
                    <?php if ($model instanceof PengaduanViewModel) : ?>
                        Status Pengusulan
                    <?php elseif ($model instanceof PengusulanViewModel) : ?>
                        Status Pengusulan
                    <?php endif ?>
                <?php else : ?>
                    Status
                <?php endif ?>
            </label>
            <div class="col-sm-9">
                <?php if (isset($model)) : ?>
                    <?php if ($model instanceof PengaduanViewModel) : ?>
                        <?= $model->status_pengaduan ?>
                    <?php elseif ($model instanceof PengusulanViewModel) : ?>
                        <?= $model->status_pengusulan ?>
                    <?php endif ?>
                <?php else : ?>
                    -
                <?php endif ?>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-3">
                <?php if (isset($model)) : ?>
                    <?php if ($model instanceof PengaduanViewModel) : ?>
                        Tanggal Update Pengaduan
                    <?php elseif ($model instanceof PengusulanViewModel) : ?>
                        Tanggal Update Pengusulan
                    <?php endif ?>
                <?php else : ?>
                    Tanggal Update
                <?php endif ?>
            </label>
            <div class="col-sm-9">
                <?php if (isset($model)) : ?>
                    <?php if ($model instanceof PengaduanViewModel) : ?>
                        <?= $model->tanggal_status_pengaduan ?>
                    <?php elseif ($model instanceof PengusulanViewModel) : ?>
                        <?= $model->tanggal_status_pengusulan ?>
                    <?php endif ?>
                <?php else : ?>
                    -
                <?php endif ?>
            </div>
        </div>
    </form>    
</div>
