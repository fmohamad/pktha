<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model \common\viewmodels\StatusCheckForm */
/* @var $form ActiveForm */
?>
<div class="status-status-check">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <h2>Cek Status</h2>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'kode_kasus')->textInput() ?>
            <?= $form->field($model, 'kunci_kasus')->textInput(['type' => 'password']) ?>

            <div class="form-group">
                <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-primary']) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
        <div class="col-md-3"></div>
    </div>
</div><!-- status-status-check -->
