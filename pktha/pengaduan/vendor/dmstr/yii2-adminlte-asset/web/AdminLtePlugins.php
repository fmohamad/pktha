<?php
namespace dmstr\web;

use yii\base\Exception;
use yii\web\AssetBundle;

/**
 * AdminLte AssetBundle
 * @since 0.1
 */
class AdminLtePlugins extends AssetBundle
{
    public $sourcePath = '@vendor/almasaeed2010/adminlte/plugins';
    public $css = [
        'datepicker/datepicker3.css',
    ];
    public $js = [
        'datepicker/bootstrap-datepicker.js'
    ];
    public $depends = [
        // 'rmrevin\yii\fontawesome\AssetBundle',
        // 'yii\web\YiiAsset',
        // 'yii\bootstrap\BootstrapAsset',
        // 'yii\bootstrap\BootstrapPluginAsset',
        'dmstr\web\AdminLteAsset'
    ];
    
}
